package SCMSGS;
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)SCMSGS.pm	1.7	08/05/20 SMI"
#
# common routines used by perl scripts that read/write scmsgs.

use Exporter();

@ISA = qw(Exporter);

@EXPORT = qw(STRLOG_MAKE_MSGID tagstring print_entry);

#
# STRLOG_MAKE_MSGID() --
#
# the computation is defined in "sys/sc_syslog_msg.h"
#
# #define	STRLOG_MAKE_MSGID(fmt, msgid)				\
# {									\
# 	uchar_t *__cp = (uchar_t *)fmt;					\
# 	uchar_t __c;							\
# 	uint32_t __id = 0;						\
# 	while ((__c = *__cp++) != '\0')					\
# 		if (__c >= ' ')						\
# 			__id = (__id >> 5) + (__id << 27) + __c;	\
# 	msgid = (__id % 899981) + 100000;				\
# }
#
sub STRLOG_MAKE_MSGID
{
	use integer;
	$MASK32 = 0xffffffff;
	$BLANK = ord(' ');
	my $fmt = shift;

	my $id = 0;
	foreach $c (unpack("C*", $fmt)) {
		if ($c >= $BLANK) {
			$id = (($id >> 5) + ($id << 27)) + $c;
			$id &= $MASK32;
		}
	}

 	my $msgid = ($id % 899981) + 100000;
	$msgid &= $MASK32;
	return $msgid;
}

#
# ->new() --
# create a new object containing one message object
# for each entry in the input file.
#
sub new
{
	local ($class, $fn, $optfh) = @_;
	local $fh;
	my $this = {
		fn => $fn,
		buf => '',
		explist => [],
		LINENO => 0,
		};

	if ($optfh eq '') {
		open(FH, $fn) || warn "can't open EXPL_FILE \"$fn\"\n";
		$fh = \*FH;
	}
	else {
		$fh = $optfh;
	}
	bless $this, $class;
	$this->_slurp($fh);
	if ($optfh eq '') {
		close($fh);
	}

	return $this;
}

%PLACEHOLDER_STRING = (
	explanation => "Need explanation of this message!",
	user_action => "Need a user action for this message.",
);

#
# this pattern is used to determine if an explanation is a "placeholder"
# devoid of real information.
#
$PLACEHOLDER_PAT = "^\\s*\$|^"
	. join("|^", map(substr($_,0,18), values %PLACEHOLDER_STRING));

#
# ->_slurp() --
# read all entries from the file.  internal.
#
sub _slurp
{
	my ($this) = shift;
	my $fh = shift;
	my $x;
	while ($x = $this->_readnext($fh)) {
		if ($x->{msgtext} eq '') {
# print STDERR "jackpot ", $this->{fn}, ":", $this->{LINENO}, "\n";
# print_entry(\*STDERR, $x);
		}
		push @{$this->{explist}}, $x;
	}
}

#
# ->_readnext() --
# read the next entry.  internal.
#
sub _readnext
{
	my $this = shift;
	my $fh = shift;
	my ($msgid, $msgtext, $tag);

	if (!$this->{buf}) {
		$this->{buf} = <$fh>;
		return undef if !$this->{buf};
		$this->{LINENO}++;
		chomp $this->{buf};
	}

	# read and discard until we head the first line of the next entry
	# note that if whitespace is present after the colon separating
	# the msgid and msgtext, that whitespace must be tabs, not spaces.
	# this restriction is so that there is at least a possibility of
	# preserving leading whitespace in msgtext.
	while (!(($msgid, $msgtext) =
			($this->{buf} =~ m/^(\d+)\s*:?\t*(.*)/))) {
		$this->{buf} = <$fh>;
		return undef if !$this->{buf};
		$this->{LINENO}++;
		chomp $this->{buf};
	}
	# we'd like to remove trailing whitespace, but we can't because
	# it would alter the msgid.  even though the whitespace may be
	# a typo in the first place, if it's there in the code, it will
	# go into the kernel's computed msgid when syslog is called.
	# so we have to preserve it here.
	## $msgtext =~ s/\s*$//;

	my $r = {
		LINENO => $this->{LINENO},
		msgid => $msgid,
		msgtext => $msgtext,
	};

	$tag = 'msgtext';
	$this->{buf} = <$fh>;
	return $r if !$this->{buf};

	$this->{LINENO}++;
	chomp $this->{buf};

	while ($this->{buf} !~ m/^\d/) {
		if ($this->{buf} =~
m/^(User [Aa]ction[^ :]*:?|Explanation[^ :]*:?|[A-Z][^ :]*:)(.*)/) {
			# a tag line: set the current tag.
			# the above pattern is a bit tricky, because it will
			# match "User Action" or "Explanation" at the start
			# of a line with or without a colon following, but
			# any other tag must be followed by a colon.
			# we allow the possibility for "User Action" and
			# "Explanation" to match without the colon in order
			# to handle the old manually-maintained entries that
			# had typos of this kind.
			# in any case, after matching, the colon (if any)
			# is discarded.
			$tag = lc $1;
			$tag =~ s/:+//g;
			$tag =~ s/\s+/_/g;
			($r->{$tag} = $2) =~ s/^\s*//;
		}
		elsif ($this->{buf} =~ m/^\s*\#/) {
		}
		else {
			# continuation line: append to value of current tag.
			## $r->{$tag} =~ s/\s*$//;
			if (!$r->{MISSX}
			 && $tag eq 'msgtext'
			 && $r->{msgtext} ne '' && $this->{buf} =~ m/^\t/) {
				# (*) special case: the header line is
				# not immediately followed by a tag line,
				# and the following line starts with a tab.
				# the following line would normally be taken
				# as a continuation of the msgtext of the
				# header line.  but because the old expl
				# file was manually maintained, there were
				# a lot of entries where the someone just
				# "forgot" to put in the "Explanation:" tag
				# line.  we try to detect those cases and
				# do the right thing.
				$r->{MISSX} = length $r->{msgtext};
			}
			$r->{$tag} .= "\n"    if ($r->{$tag} ne '');
			$this->{buf} =~ s/^\t//;
			$r->{$tag} .= $this->{buf};
			## $r->{$tag} =~ s/\s*$//;
		}
		$this->{buf} = <$fh>;
		last if !$this->{buf};
		$this->{LINENO}++;
		chomp $this->{buf};
	}

	if ($r->{MISSX}) {
		# see (*) comment above.
		# if while reading this entry we never found a proper
		# Explanation tag, we'll assume the continuation lines
		# following the header line were meant to be the
		# explanation text.
		if ($r->{explanation} eq '') {
			$r->{explanation} = substr($r->{msgtext}, $r->{MISSX});
			$r->{explanation} =~ s/^\s*//;
			$r->{msgtext} = substr($r->{msgtext}, 0, $r->{MISSX});
		}
		else {
			$r->{MISSX} = '';
		}
	}

	return $r;
}

#
# ->explist() --
# return a reference to the list of entries that were read in.
#
sub explist
{
	my $this = shift;
	return $this->{explist};
}

#
# indentof() --
# return the number of columns by which a line of text is indented.
# space=1, tab=next multiple of 8.
#
sub indentof
{
	my $s = shift;
	my $c;
	my $r = 0;

	foreach (split //, $s) {
		if ($_ eq "\t") { $r += 8 - $r%8; }
		else { $r++; }
	}

	return $r;
}

#
# blockfold() --
# inputs: length $ll, string $s.
# return: array of folded lines.
# separate the lines into blocks, and format each block.
#
sub blockfold
{
	my ($ll, $s) = @_;
	my @block = ();
	my $lastwhite = "x";
	my $lastind = -1;
	my ($white, $rest);
	my $ind;
	my @r = ();

	$s =~ s/\n*$//s;
	foreach (split /\n/, $s) {
		($white, $rest) = (m{^(\s*)(.*)});

		if ($rest ne ""
		 && ($white eq $lastwhite
		 || ($ind = &indentof($white)) == $lastind)) {
			# indentation of current line is same as last line
			push @block, split /\s+/, $rest;
		}
		else {
			# indentation changed: format the accumulated block,
			# and continue
			if (@block > 0) {
				$lastwhite = " " x $lastind;
				push @r, map($lastwhite.$_,
				    &SCMSGS::_wordfill($ll-$lastind, \@block));
				@block = ();
			}
			if ($rest eq "") {
				$lastwhite = "";
				$lastind = 0;
				push @r, "";
				next;
			}
			$lastind = $ind;
			$lastwhite = $white;
			push @block, split /\s+/, $rest;
		}
	}
	if (@block > 0) {
		$lastwhite = " " x $lastind;
		push @r, map($lastwhite.$_,
		    &SCMSGS::_wordfill($ll-$lastind, \@block));
		@block = ();
	}

	return @r;
}

#
# _wordfill() --
# internal line-filling algorithm.
# arguments:
#	$ll - line length
#	$p - reference to array of words
# return value:
#	@r - array of filled lines
#
sub _wordfill
{
	my $ll = shift;
	my $p = shift;
	my @r = ();
	while (@{$p} > 0) {
		my $line = shift @{$p};
		while (@{$p} > 0
		 && length($line) + 1 + length($p->[0]) <= $ll) {
			$line .= ' ' . (shift @{$p});
		}
		push @r, $line;
	}
	return @r;
}

#
# fold() --
# fold the given text at the given line length,
# returning a multi-line string with lines indented by one tab.
#
sub fold
{
	my ($llen, $s) = @_;
	return '' if ($s eq '');
	return "$_print_prefix\t"
	 . join("\n$_print_prefix\t",blockfold($llen,$s))
	 . "\n";
}

#
# printable_tag() --
# return the printable name of a given tag.
#
sub printable_tag
{
	my $s = shift;
	$s =~ s/_/ /g;
	$s =~ s/\S+/\u$&/g;
	return $s;
}

#
# tagstring() --
# return a string consisting of the tag name, converted to
# printable form; followed by the value associated with the tag,
# indented by one tab.
#
sub tagstring
{
	my ($x, $tag) = @_;
	# turn tag into externally printable form, in which
	# underscores turn into spaces, and the first letter
	# of each word is capitalized.  thus,
	# user_action becomes "User Action".  this is the inverse
	# of what was done when reading in the tag from the EXPL_FILE.
	return $_print_prefix.&printable_tag($tag).":\n"
	 . &fold(70, $x->{$tag});
}


@_special_keys = qw(msgid msgtext LINENO MISSX commentloc location code);
@SPECIAL_KEYS{@_special_keys} = (1) x @_special_keys;
%_lockeys = (location => 1, commentloc => 1);
%_stdkeys = (explanation => 1, user_action => 1);

#
# merge_msgs() --
# merge two objects that are presumably duplicates.
# in case of a conflict where both objects define a field,
# the second object's fields are added under different field
# names (created by tacking on a sequence number).
# for now, merge fields of second object into
# the existing first object, instead of creating a new object.
# return a reference to the (modified) first object.
#
sub merge_msgs
{
	my ($x, $y) = @_;
	my $newx = $x;
	my @xkeys = sort keys %$newx;

	foreach $yk (sort keys %$y) {

		# special cases:
		# in the case of the tag 'location' or 'commentloc',
		# just append values.
		if ($_lockeys{$yk}) {
			$newx->{$yk} .= ' '.$y->{$yk};
			next;
		}

		# don't dup if the secondary object's tag has a null value.
		next if $y->{$yk} eq '';

		if ($_stdkeys{$yk}) {
			$newx->{$yk} =~ s/\s*$//;
			$y->{$yk} =~ s/\s*$//;
			# this is an explanation or user action.

			# if the primary's value is a placeholder,
			# adopt the secondary's value.
			$newx->{$yk} = $y->{$yk}
				if ($newx->{$yk} =~ m/$PLACEHOLDER_PAT/o);

			# if the secondary's value is a placeholder,
			# ignore it.
			next if ($y->{$yk} =~ m/$PLACEHOLDER_PAT/o);
		}

		# don't dup if the tag has the same value in both objects.
		# BUG: we should check all numbered instances of the key too.
		next if $newx->{$yk} eq $y->{$yk};

		# if the primary's object's tag is null,
		# just take the value directly from the secondary.
		if ($newx->{$yk} eq '') {
			$newx->{$yk} = $y->{$yk};
			next;
		}

		# don't dup this tag if it's one that normally is not printed.
		next if ($SPECIAL_KEYS{$yk});

		# find all keys that have the same prefix.
		# if any, advance the sequence number and
		# create a new key from it.
		my $ntk = 1 + (grep m/^\Q$yk\E\b/, keys %$newx);
		my $tag = $ntk == 1 ? $yk : "$yk-$ntk";
		$newx->{$tag} = $y->{$yk};
	}

	return $newx;
}

#
# merge_expl_msg() --
# blend information from two objects (expl and msg).
# in case of a conflict where both objects define a field,
# the second object takes precedence.
# for now, merge fields of second object into
# the existing first object, instead of creating a new object.
# return a reference to the (modified) first object.
#
sub merge_expl_msg
{
	my ($x, $y) = @_;
	my $newx = $x;

	foreach $yk (sort keys %$y) {
		$newx->{$yk} = $y->{$yk}   if $y->{$yk} ne '';
	}

	return $newx;
}

$_print_prefix = "";

#
# set_print_prefix() --
# set the internal prefix used by print_entry.
#
sub set_print_prefix
{
	$_print_prefix = $_[0];
}

#
# print_entry(\*FH, $x) --
# print an entry to the given file.
#
sub print_entry
{
	local ($fh, $x) = @_;

	my $msgid = $x->{msgid};
	my $msgtext = $x->{msgtext};

	print $fh "$_print_prefix$msgid\t:$msgtext\n";
	foreach $tag (sort keys %$x) {
		print $fh &tagstring($x, $tag)
			unless $SPECIAL_KEYS{$tag};
	}
}

#
# is_placeholder_entry($x) --
# return true if an entry carries no
# worthwhile information (ie, lacks both
# a valid explanation and a valid user action.
#
sub is_placeholder_entry
{
	my $x = shift;
	return $x->{explanation} =~ m/$PLACEHOLDER_PAT/o
		&& $x->{user_action} =~ m/$PLACEHOLDER_PAT/o;
}

sub is_placeholder_string
{
	return $_[0] =~ m/$PLACEHOLDER_PAT/o;
}


#
# is_incomplete($x) --
# return true if an entry is incomplete
# (ie, lacking either a valid explanation or user action).
#
sub is_incomplete
{
	my $x = shift;
	return $x->{explanation} =~ m/$PLACEHOLDER_PAT/o
		|| $x->{user_action} =~ m/$PLACEHOLDER_PAT/o;
}

%ESCTAB = (
	'&', '&amp;',
	'*', '&star;',
	'@', '&at;',
);

%UNESCTAB = (
	amp => '&',
	star => '*',
	at => '@',
);

#
# escapetext() --
# add html-style escapes (entities) for a few special characters.
#
sub escapetext
{
	my $s = shift;
	$s =~ s/([&*\@])/$ESCTAB{$1}/g;
	return $s;
}

#
# unescapetext() --
# remove html-style escapes (entities) used by escapetext.
#
sub unescapetext
{
	my $s = shift;
	$s =~ s/\&(\w*);/$UNESCTAB{$1}/g;
	return $s;
}

#
# suppress_tags(t1, t2, ...) --
# add t1, t2, etc, to the list of tags that will
# not be printed by print_entry() and will not
# be merged by merge_msgs().
#
sub suppress_tags
{
	foreach $tag (@_) {
		$SPECIAL_KEYS{$tag} = 1;
	}
}

1;
