!   D   �
  R
  @  ��������               .   ,   ��������i   g         �   �   ���������   �           �   ��������?          p  7  ���������  h     
   �  y  ���������  �  	      �  �  ���������  �          �  ����   �  ^  ���������  �          �  ��������G          o  2  ���������  d        �  �  ��������  �        2    ��������f  ?        �  r  ���������  �        �  �  ���������  �          �  ��������          0    ����    ~  q  ���������  �     2   �  �  ���������  �  "   $       ��������7  4  #   '   O  P  ��������v  z  &   (   �  �  ���������  �  %   -   �  �  ��������  (  *   ,     /  ��������2  P  +   /   Q  q  ��������u  �  .   0   �  �  ����1   �  �  ���������  �  )   ;        ��������?  :  3   5   Y  R  ��������t  j  4   8   �  �  ���������  �  7   9   �  �  ����:   �  �  ���������  �  6   ?   1	  	  ��������C	  #	  <   >   �	  L	  ���������	  b	  =   A   �	  	  ���������	  �	  @   B   #
  �	  ����C   ?
  �	  ��������p
  '
  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 必须指定要清除的法定服务器。
 必须指定 "%s" 或法定服务器列表两者之一。
 必须同时指定群集名称和群集 ID。
 不能同时指定 "%s" 和法定服务器。
 只能指定 "-c" 选项一次。
 只能指定 "-I" 选项一次。
 一次只能清除一个法定服务器。
 用法：%s <子命令> [<法定服务器> ...]
 用法错误。
 用法 不可识别的子命令 - "%s"。

 不可识别的选项 - "%s"。
 意外的选项 - "%s"。
 要清除的法定服务器必须已从群集中删除。清除有效的法定服务器可能会影响群集法定设备。
 行 "%s" 中的默认法定目录已被使用。
 未为端口 "%s" 上的法定服务器配置群集名称或 ID。
 已停止端口 "%s" 上的法定服务器。
 已启动端口 "%s" 上的法定服务器。
 端口 "%s" 上的法定服务器未在运行。
 未在任何群集中配置端口 "%s" 上的法定服务器。
 已清除端口 "%s" 上的法定服务器。
 法定服务器尚未在端口 "%s" 上启动。

 未在文件 "%2$s" 中配置法定服务器 "%1$s"。
 行 "%2$s" 中的法定目录 "%1$s" 不唯一。
 选项 "%s" 需要有参数。
 内存不足。
 端口 "%s" 无效。
 文件 "%2$s" 中的端口 "%1$s" 无效。
 群集 ID "%s" 无效。
 内部错误。
 无法启动端口 "%s" 上的法定服务器。有关详细信息，请参阅系统日志。
 在端口 "%s" 上禁用法定服务器失败。
 是否要继续 群集 ID "%s" 不是十六进制形式的。
 无法在端口 "%s" 上发送消息。
 无法在端口 "%s" 上接收消息。
 无法打开文件 "%s"。
 无法获得主机 "%s" 的 IP 节点。
 无法获得已接收数据的长度。
 无法执行文件 "%2$s" 中的 "%1$s" 行。
 无法在端口 "%s" 上创建套接字。
 === 端口 %s 上的法定服务器 ===
 %s %s
     保留 关键字：		0x%llx
     注册 关键字：		0x%llx
        %s <子命令> -? | --help
        %s -V | --version
 
stop	停止法定服务器 
start	启动法定服务器 
show	显示法定服务器的配置 
clear	清除法定服务器的群集配置

 
停止法定服务器
 
启动法定服务器
 
显示法定服务器的配置
 
选项：
 
管理法定服务器

子命令：
 
已禁用			True

 
已禁用			False

 
清除法定服务器的群集配置
 
  节点 ID：			%d
 
  -y	对确认问题自动回答 "yes"
 
  -v	详细输出


 
  -d	禁用法定服务器
 
  -c <群集名称>
	指定群集名称
 
  -I <群集 ID>
	指定十六进制形式的群集 ID
 
  -?	显示帮助消息
 
  ---  群集 %s (id 0x%8.8X) 保留 ---
 
  ---  群集 %s (id 0x%8.8X) 注册 ---
 