'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_pmf_signal 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_pmf_signal \- PMF の制御下にあるプロセスツリーにシグナルを送信する
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_pmf_signal\fR(\fBscds_handle_t \fR \fIhandle\fR, \fBscds_pmf_type_t\fR \fIprogram_type\fR, \fBint\fR \fIinstance\fR, \fBint\fR \fIsignal\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_pmf_signal()\fR 関数は、PMF 制御下で実行されているプロセスツリーに指定のシグナルを送信します。この関数は、\fB-k\fR オプションを指定した pmfadm(1M) コマンドと同じです。
.sp
.LP
\fBscds_pmf_signal()\fR 関数は、シグナルの送信後、指定のタイムアウト時間が経過するまでプロセスツリーの終了を待機してから終了します。\fBtimeout\fR に \fB0\fR が指定されている場合は、プロセスの終了を待たずにただちに終了します。\fB-1\fR が指定されている場合は、プロセスが終了するまで無期限に待機します。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
\fBscds_initialize()\fR から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIprogram_type\fR\fR
.ad
.RS 20n
.rt  
実行するプログラムの型です。有効な型は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fBSCDS_PMF_TYPE_SVC\fR\fR
.ad
.RS 28n
.rt  
データサービスアプリケーション
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCDS_PMF_TYPE_MON\fR\fR
.ad
.RS 28n
.rt  
障害モニター
.RE

.sp
.ne 2
.mk
.na
\fB\fB\fR\fBSCDS_PMF_TYPE_OTHER\fR\fR
.ad
.RS 28n
.rt  
その他
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fIinstance\fR\fR
.ad
.RS 20n
.rt  
複数のインスタンスを持つリソースの場合、この整数 (0 以上) はインスタンスを一意に識別します。単一のインスタンスの場合、0 を使用します。 
.RE

.sp
.ne 2
.mk
.na
\fB\fIsignal\fR\fR
.ad
.RS 20n
.rt  
送信される Solaris シグナルです。\fBsignal\fR(3HEAD)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
タイムアウト値 (秒) です。
.RE

.SH 戻り値
.sp
.LP
\fBscds_pmf_signal()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
シグナルの送信後、タイムアウト時間が経過するまでの間にプロセスツリーが終了しない。 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
関数の実行に成功。 
.RE

.sp
.ne 2
.mk
.na
\fBその他の値\fR
.ad
.RS 28n
.rt  
関数の実行に失敗。障害コードの意味については、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
pmfadm(1M)、scds_initialize(3HA)、scha_calls(3HA)、\fBsignal\fR(3HEAD)、\fBattributes\fR(5)
