'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scha_resource_get 1HA "2008 年 8 月 19 日" "Sun Cluster 3.2" "Sun Cluster コマンド"
.SH 名前
scha_resource_get \- リソース情報にアクセス
.SH 形式
.LP
.nf
\fBscha_resource_get\fR [\fB-Q\fR] \fB-O \fR \fIoptag\fR \fB-R\fR \fIresource \fR [\fB-G\fR \fIgroup\fR] [\fIargs\fR]
.fi

.SH 機能説明
.sp
.LP
\fBscha_resource_get\fR コマンドは、クラスタの Resource Group Manager (\fBRGM\fR) の制御下にあるリソースの情報にアクセスします。r_properties(5) に記載されているリソースのプロパティーと同様に、rt_properties(5) に記載されているこのコマンドを使用して、リソースタイプのプロパティーのクエリーを行うことができます。
.sp
.LP
\fBscha_resource_get\fR コマンドは、クラスタの RGM で制御されるサービスを表すリソースタイプのコールバックメソッドのシェルスクリプト実装で使用します。このコマンドを実行すると、scha_resource_get(3HA) C 関数を実行したときと同じ情報が得られます。
.sp
.LP
この情報は、別々の行で書式付き文字列として標準出力 \fBstdout\fR に生成されます。scha_cmds(1HA)の説明を参照してください。あとでスクリプトで使用できるように、この出力内容をシェル変数に格納し、シェルまたは \fBawk\fR(1) で解析することができます。
.sp
.LP
このコマンドを使用するには \fBsolaris.cluster.resource.read\fR の役割に基づくアクセス制御 (RBAC) が必要です。\fBrbac\fR(5)を参照してください。
.sp
.LP
さらにこのコマンドを使用するにあたっては、Sun Cluster コマンド権プロファイルを割り当てておく必要があります。認証されたユーザーは、\fBpfsh\fR(1)、\fBpfcsh\fR(1)、または \fBpfksh\fR(1) プロファイルシェルから、Sun Cluster の特権コマンドをコマンド行で実行できます。プロファイルシェルは、Sun Cluster コマンド権プロファイルに割り当てられた Sun Cluster の特権コマンドへのアクセスを可能にする特別なシェルです。プロファイルシェルは、\fBsu\fR(1M) を実行すると起動されます。\fBpfexec\fR(1) を使用しても、Sun Cluster の特権コマンドを実行できます。
.SH オプション
.sp
.LP
次のオプションがサポートされています。 
.sp
.ne 2
.mk
.na
\fB\fB-G\fR \fIgroup\fR\fR
.ad
.sp .6
.RS 4n
内部でリソースが構成されているリソースグループの名前です。この引数はオプションですが、これを含めるとコマンドの実行効率が上がります。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-O\fR \fIoptag\fR\fR
.ad
.sp .6
.RS 4n
アクセスする情報を指定します。指定する \fIoptag\fR 値によっては、情報を取得するクラスタノードまたはゾーンを示すための追加の値を含める必要があります。
.LP
注 - 
.sp
.RS 2
\fBAFFINITY_TIMEOUT\fR や \fBBOOT_TIMEOUT\fR などの \fIoptag\fR 値には、大文字と小文字の区別はありません。\fB\fR\fIoptag\fR 値を指定するときには、大文字と小文字の任意の組み合わせを使用できます。
.RE
次の \fIoptag\fR 値は、対応するリソースのプロパティーを取得します。リソースの名前付きプロパティーの値が生成されます。\fBNUM_RG_RESTARTS\fR、\fBNUM_RESOURCE_RESTARTS\fR、\fBMONITORED_SWITCH\fR、\fBON_OFF_SWITCH\fR、\fBRESOURCE_STATE\fR、および \fBSTATUS\fR プロパティーは、コマンドが実行されたノードまたはゾーン上の値を参照します。次の \fIoptag\fR 値に対応するリソースプロパティーについては、r_properties(5) のマニュアルページを参照してください。次のリスト内のいくつかの \fIoptag\fR 値は、r_properties(5) のマニュアルページ内ではなく、リストのあとに説明されています。
.sp
\fBPER_NODE\fR プロパティー属性が設定されている場合、\fBEXTENSION\fR プロパティーは、コマンドが実行されたノードまたはゾーン上の値も参照します。property_attributes(5) のマニュアルページを参照してください。
.sp
.in +2
.nf
AFFINITY_TIMEOUT
ALL_EXTENSIONS
BOOT_TIMEOUT
CHEAP_PROBE_INTERVAL
EXTENSION
EXTENSION_NODE
FAILOVER_MODE
FINI_TIMEOUT
GLOBAL_ZONE_OVERRIDE
GROUP
INIT_TIMEOUT
LOAD_BALANCING_POLICY
LOAD_BALANCING_WEIGHTS
MONITORED_SWITCH
MONITORED_SWITCH_NODE
MONITOR_CHECK_TIMEOUT
MONITOR_START_TIMEOUT
MONITOR_STOP_TIMEOUT
NETWORK_RESOURCES_USED
NUM_RESOURCE_RESTARTS
NUM_RESOURCE_RESTARTS_ZONE
NUM_RG_RESTARTS
NUM_RG_RESTARTS_ZONE
ON_OFF_SWITCH
ON_OFF_SWITCH_NODE
PORT_LIST
POSTNET_STOP_TIMEOUT
PRENET_START_TIMEOUT
RESOURCE_DEPENDENCIES
RESOURCE_DEPENDENCIES_OFFLINE_RESTART
RESOURCE_DEPENDENCIES_RESTART
RESOURCE_DEPENDENCIES_WEAK
RESOURCE_PROJECT_NAME
RESOURCE_STATE
RESOURCE_STATE_NODE
RETRY_COUNT
RETRY_INTERVAL
R_DESCRIPTION
SCALABLE
START_TIMEOUT
STATUS
STATUS_NODE
STOP_TIMEOUT
THOROUGH_PROBE_INTERVAL
TYPE
TYPE_VERSION
UDP_AFFINITY
UPDATE_TIMEOUT
VALIDATE_TIMEOUT
WEAK_AFFINITY
.fi
.in -2

次の \fIoptag\fR 値は、r_properties(5) のマニュアルページ内に記載されていません。
.sp
.ne 2
.mk
.na
\fB\fBALL_EXTENSIONS\fR\fR
.ad
.sp .6
.RS 4n
リソースの拡張プロパティー名を複数の行に出力します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBEXTENSION\fR\fR
.ad
.sp .6
.RS 4n
プロパティーの型とその値を連続した複数の行に生成します。プロパティーがノードごとの拡張プロパティーである場合、返される値は \fBscha_resource_get\fR が実行されるノード上のプロパティー値です。フラグが設定されていない引数を使って、リソースの拡張プロパティーを指定する必要があります。例 に示すように、シェルスクリプトは、値を取得するためにタイプを破棄する必要がある場合があります。
.sp
明示的な値が割り当てられていないノードまたはゾーン上でユーザーがこのプロパティーの値を要求すると、Resource Type Registration (RTR) ファイルで宣言されているデフォルト値が返されます。rt_reg(4) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBEXTENSION_NODE\fR\fR
.ad
.sp .6
.RS 4n
名前付きノードまたはゾーン用に、プロパティーのタイプに続いて、その値を後続の行に生成します。この値には、特別なノードまたはゾーン上のリソースの拡張子を指定する、2 つのフラグなしの引数が次の順序で必要です。
.RS +4
.TP
.ie t \(bu
.el o
拡張子プロパティー名
.RE
.RS +4
.TP
.ie t \(bu
.el o
ノードまたはゾーン名
.RE
シェルスクリプトは、値を取得するためにタイプを破棄する必要がある場合があります。
.sp
明示的な値が割り当てられていないノードまたはゾーン上でこのプロパティーの値をユーザーが要求すると、RTR ファイルで宣言されているデフォルト値が返されます。rt_reg(4) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBGROUP\fR\fR
.ad
.sp .6
.RS 4n
リソースを構成するリソースグループの名前を指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBRESOURCE_STATE_NODE\fR\fR
.ad
.sp .6
.RS 4n
名前付きノードまたはゾーン用に、リソースの \fBRESOURCE_STATE\fR プロパティー値を生成します。フラグが設定されていない引数を使って、ノードまたはゾーンを指定する必要があります。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSTATUS_NODE\fR\fR
.ad
.sp .6
.RS 4n
名前付きノードまたはゾーン用に、リソースの \fBSTATUS\fR プロパティー値を生成します。フラグが設定されていない引数を使って、ノードまたはゾーンを指定する必要があります。
.RE

次の \fIoptag\fR 値は、対応するリソースタイプのプロパティーを取得します。リソースのタイプの名前付きプロパティーの値が生成されます。
.LP
注 - 
.sp
.RS 2
\fBAPI_VERSION\fR や \fBBOOT\fR などの \fIoptag\fR 値には、大文字と小文字の区別はありません。\fB\fR\fIoptag\fR 値を指定するときには、大文字と小文字の任意の組み合わせを使用できます。
.RE
リソースタイプのプロパティーについての詳細は、rt_properties(5)を参照してください。
.sp
.in +2
.nf
API_VERSION
BOOT
FAILOVER
FINI
GLOBAL_ZONE
INIT
INIT_NODES
INSTALLED_NODES
IS_LOGICAL_HOSTNAME
IS_SHARED_ADDRESS
MONITOR_CHECK
MONITOR_START
MONITOR_STOP
PKGLIST
POSTNET_STOP
PRENET_START
PROXY
RT_BASEDIR
RT_DESCRIPTION
RT_SYSTEM
RT_VERSION
SINGLE_INSTANCE
START
STOP
UPDATE
VALIDATE
.fi
.in -2

このリソースタイプが \fBGLOBAL_ZONE_OVERRIDE\fR リソースプロパティーを宣言した場合、\fBGLOBAL_ZONE\fR \fI optag\fR によって取得された値は、\fBGLOBAL_ZONE\fR プロパティーの値ではなく、\fBGLOBAL_ZONE_OVERRIDE\fR プロパティーの現在の値となります。詳細は、rt_properties(5)のマニュアルページ内の \fBGlobal_zone\fR プロパティー、およびr_properties(5)のマニュアルページ内の \fBGlobal_zone_override\fR プロパティーを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-Q\fR\fR
.ad
.sp .6
.RS 4n
指定された修飾子は、いずれもリソース依存性リストに含めます。修飾子 \fB{LOCAL_NODE}\fR、\fB{ANY_NODE}\fR、および \fB{FROM_RG_AFFINITIES}\fR については、r_properties(5) のマニュアルページを参照してください。\fB-Q\fR オプションを省略すると、リソース依存性リストの返される値には、宣言された修飾子のないリソース名のみが含まれます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-R\fR \fIresource\fR\fR
.ad
.sp .6
.RS 4n
RGM クラスタ機能によって管理されているリソースの名前
.RE

.SH 使用例
.LP
\fB例 1 \fR\fBscha_resource_get\fR コマンドを使用したサンプルスクリプト
.sp
.LP
次のスクリプトには、必要なリソース名とリソースグループ名を指定する \fB-R\fR 引数と \fB-G\fR 引数が渡されます。\fBscha_resource_get\fR コマンドは、リソースの \fBRetry_count\fR プロパティーと \fBenum\fR 型の \fBLogLevel\fR 拡張プロパティーにアクセスします。

.sp
.in +2
.nf
#!/bin/sh

while getopts R:G: opt
do
    case $opt in
          R)      resource="$OPTARG";;
          G)      group="$OPTARG";;
    esac
done

retry_count=`scha_resource_get -O Retry_count -R $resource \\ 
-G $group`
printf "retry count for resource %s is %d\en" $resource \\
$retry_count

LogLevel_info=`scha_resource_get -O Extension -R $resource \\
-G $group LogLevel`

# Get the enum value that follows the type information
# of the extension property.  Note that the preceding
# assignment has already changed the newlines separating
# the type and the value to spaces for parsing by awk.

loglevel=`echo $LogLevel_info | awk '{print $2}'`
.fi
.in -2

.LP
\fB例 2 \fRリソース依存性のクエリーを行うために \fB-Q\fR オプション付きまたはオプションなしで \fBscha_resource_get\fR コマンドを使用
.sp
.LP
この例では、\fBclresource\fR コマンドを使用して、\fBmyres\fR という名前のリソースを作成する方法を示します。このリソースは、\fB{LOCAL_NODE}\fR スコープ修飾子または \fB{ANY_NODE}\fR スコープ修飾子があるか、あるいはスコープ修飾子がない複数のリソース依存性を持つという前提です。この例では、次に、\fBscha_resource_get\fR コマンドをどのように使用して、\fBResource_dependencies\fR プロパティーのクエリーを行うかを示します。\fB-Q\fR オプションがない場合は、リソース名のみが返されます。\fB-Q\fR オプションがある場合は、宣言されたスコープ修飾子も返されます。

.sp
.in +2
.nf
# \fBclresource create -g mygrp -t myrestype \
-p Resource_dependencies=myres2{LOCAL_NODE},myres3{ANY_NODE},myres4 \
myres\fR
# \fBscha_resource_get -O Resource_dependencies -R myres -G mygrp\fR
myres2
myres3
myres4
# \fBscha_resource_get -Q -O Resource_dependencies -R myres -G mygrp\fR
myres2{LOCAL_NODE}
myres3{ANY_NODE}
myres4
# 
.fi
.in -2
.sp

.SH 終了ステータス
.sp
.LP
次の終了ステータスコードが返されます。 
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 13n
.rt  
コマンドは正常に完了しました。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 13n
.rt  
エラーが発生しました。
.sp
障害エラーコードについては、scha_calls(3HA)を参照してください。 
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
\fBawk\fR(1)、scha_cmds(1HA)、scha_calls(3HA)、scha_resource_get(3HA)、rt_reg(4)、\fBattributes\fR(5)、property_attributes(5)、r_properties(5)、rt_properties(5)
