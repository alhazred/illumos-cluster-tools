'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_get_netaddr_list 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_get_netaddr_list \- リソースによって使用されるネットワークアドレスの取得
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h> \fBscha_err_t\fR \fBscds_get_netaddr_list\fR(\fBscds_handle_t \fR \fIhandle\fR, \fBscds_netaddr_list_t **\fR\fInetaddr_list\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_get_netaddr_list()\fR 関数は、リソースが使用しているホスト名、ポート、プロトコルのすべての組み合わせを返します。これらの組み合わせは、リソースの \fBPort_list\fR プロパティー設定と、リソースが使用しているすべてのホスト名 (\fBscds_get_rs_hostnames()\fR 関数が返す値) を組み合わせることによって生成されます。
.sp
.LP
リソースを監視したり、リソースが使用しているホスト名、ポート、およびプロトコルのリストを得たりするには、\fBscds_get_netaddr_list()\fR を障害モニターで使用します。
.sp
.LP
プロトコルタイプの値は、ヘッダーファイル \fBrgm/libdsdev.h\fR で定義されます。
.sp
.LP
この関数が割り当て、返すメモリーを解放するには、\fBscds_free_netaddr_list()\fR を使用します。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
\fBscds_initialize()\fR から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fInetaddr_list\fR\fR
.ad
.RS 20n
.rt  
リソースグループが使用するホスト名、ポート、およびプロトコルのリストです。
.RE

.SH 戻り値
.sp
.LP
\fBscds_get_netaddr_list()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 20n
.rt  
関数が正常に終了。
.RE

.sp
.ne 2
.mk
.na
\fBその他の値\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。障害コードの意味については、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_free_netaddr_list(3HA)、scds_get_rs_hostnames(3HA)、scha_calls(3HA)、r_properties(5)、\fBattributes\fR(5)
