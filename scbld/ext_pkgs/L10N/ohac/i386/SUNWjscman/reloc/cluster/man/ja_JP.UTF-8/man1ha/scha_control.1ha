'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scha_control 1HA "2008 年 7 月 18 日" "Sun Cluster 3.2" "Sun Cluster コマンド"
.SH 名前
scha_control \- リソースおよびリソースグループ制御を要求する関数
.SH 形式
.LP
.nf
\fBscha_control\fR \fB-O\fR \fI optag\fR \fB-G\fR \fIgroup\fR \fB -R\fR \fIresource\fR [\fB -Z\fR \fIzonename\fR]
.fi

.SH 機能説明
.sp
.LP
\fBscha_control\fR コマンドは、クラスタの Resource GroupManager (\fBRGM\fR) の制御下にあるリソースまたはリソースグループの再起動または再配置を要求します。このコマンドは、リソースモニターのシェルスクリプト実装で使用します。このコマンドの機能は、scha_control(3HA) の C 関数の機能と同じです。
.sp
.LP
このコマンドの終了コードは、要求された処理が拒絶されたかどうかを示します。要求が受理された場合、このコマンドは、リソースグループまたはリソースがオフラインになったあと完全にオンラインに戻った時点で終了します。scha_control(1HA) を呼び出した障害モニターは、リソースまたはリソースグループがオフラインになった結果として停止される場合があります。結果として、障害モニターは、成功した要求の戻りステータスを一度も受け取らない場合があります。
.sp
.LP
このコマンドを使用するには \fBsolaris.cluster.resource.admin\fR の役割に基づくアクセス制御 (RBAC) が必要です。\fBrbac\fR(5)を参照してください。
.sp
.LP
さらにこのコマンドを使用するにあたっては、Sun Cluster コマンド権プロファイルを割り当てておく必要があります。認証されたユーザーは、\fBpfsh\fR(1)、\fBpfcsh\fR(1)、または \fBpfksh\fR(1) プロファイルシェルから、Sun Cluster の特権コマンドをコマンド行で実行できます。プロファイルシェルは、Sun Cluster コマンド権プロファイルに割り当てられた Sun Cluster の特権コマンドへのアクセスを可能にする特別なシェルです。プロファイルシェルは、\fBsu\fR(1M) を実行すると起動されます。\fBpfexec\fR(1) を使用しても、Sun Cluster の特権コマンドを実行できます。
.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-G\fR \fIgroup\fR\fR
.ad
.sp .6
.RS 4n
再起動または再配備するリソースグループ、あるいは再起動または再配置するリソースを含むリソースグループの名前。要求元ノードまたはゾーンでリソースグループがオンラインになっていない場合、要求は拒否されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-O\fR \fIoptag\fR\fR
.ad
.sp .6
.RS 4n
\fIoptag\fR オプションを要求します。
.LP
注 - 
.sp
.RS 2
\fBCHECK_GIVEOVER\fR や \fBCHECK_RESTART\fR などの \fIoptag\fR オプションには、大文字と小文字の区別はありません。\fB\fR\fIoptag\fR オプションを指定するときには、大文字と小文字の任意の組み合わせを使用できます。
.RE
次の \fIoptag\fR 値がサポートされています。
.sp
.ne 2
.mk
.na
\fB\fBCHANGE_STATE_OFFLINE\fR\fR
.ad
.sp .6
.RS 4n
\fB-R\fR オプションによって指定されるプロキシリソースがローカルノードまたはゾーンでオフラインにされるように要求します。\fB\fR「プロキシリソース」は、リソースの状態を (現在は Oracle clusterware CRS と呼ばれる) Oracle ClusterReady Services (CRS) などの別のクラスタからインポートする Sun Cluster リソースです。Sun Cluster ソフトウェアのコンテキスト内での、状態のこの変化には、外部リソースの状態の変化が反映されます。
.sp
プロキシリソースの状態をこの \fIoptag\fR 値で変更すると、プロキシリソースのメソッドは実行されません。
.sp
ノードまたはゾーン上にある「依存先の」リソースで障害が発生して、リソースを回復できない場合、モニターは該当ノードまたはゾーン上の該当リソースをオフラインにします。モニターは \fBCHANGE_STATE_OFFLINE\fR \fIoptag\fR 値を指定して \fBscha_control\fR コマンドを呼び出すことでリソースをオフラインにします。また、モニターは、依存先リソースのオフライン、再起動に従属する対象をすべて、それらに対して再起動をトリガーすることにより、オフラインにします。クラスタ管理者が障害を解決し、依存先のリソースを再度有効にすると、モニターは、リソースのオフライン再起動依存リソースも再度オンラインにします。
.RE

.sp
.ne 2
.mk
.na
\fB\fBCHANGE_STATE_ONLINE\fR\fR
.ad
.sp .6
.RS 4n
\fB-R\fR オプションによって指定されるプロキシリソースがローカルノードまたはゾーンでオンラインにされるように要求します。「プロキシリソース」は、リソースの状態を Oracle ClusterReady Services (CRS) などの別のクラスタからインポートする Sun Cluster リソースです。\fB\fRSun Cluster ソフトウェアのコンテキスト内での、状態のこの変化には、外部リソースの状態の変化が反映されます。
.sp
プロキシリソースの状態をこの \fIoptag\fR 値で変更すると、プロキシリソースのメソッドは実行されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBCHECK_GIVEOVER\fR\fR
.ad
.sp .6
.RS 4n
\fB-G\fR オプションで指定されたリソースグループに対して \fBGIVEOVER\fR を実行するために必要な有効性チェックをすべて実行します。ただし、実際にリソースグループを再配置することはありません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBCHECK_RESTART\fR\fR
.ad
.sp .6
.RS 4n
\fB-G\fR オプションで指定されたリソースグループに対して \fBRESTART\fR を実行するために必要な有効性チェックをすべて実行します。ただし、実際にリソースグループを再起動することはありません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBGIVEOVER\fR\fR
.ad
.sp .6
.RS 4n
\fB-G\fR オプションで指定されたリソースグループをローカルノードまたはゾーン上でオフラインにしたあと、\fBRGM\fR によって選択された別のノードまたはゾーン上でオンラインに戻すことを要求します。リソースグループが 2 つ以上のノードまたはゾーン上でオンライン状態にあり、これ以上オンライン化できるノードまたはゾーンが存在しない場合、このリソースグループは、ローカルノードまたはゾーン上でオフラインにされたままオンラインにならない可能性があります。またこうした処理要求は、各種のチェックの結果を受けた結果として、拒絶される場合もあります。たとえば、あるノードまたはゾーン上で \fBGIVEOVER\fR 要求により、該当するグループが \fBPINGPONG_INTERVAL\fR プロパティーの指定間隔内にオフラインされた場合、このノードまたはゾーンはホストとして拒絶されることがあります。
.sp
クラスタ管理者が 1 つまたは複数のリソースグループの \fBRG_Affinities\fR プロパティーを構成している場合、あるリソースグループで \fBscha_control\ GIVEOVER\fR 要求を発行すると、複数のリソースグループが再配置されることがあります。\fBRG_Affinities\fR プロパティーについては、rg_properties(5)を参照してください。
.sp
\fBMONITOR_CHECK\fR メソッドは、リソースを含むリソースグループが障害モニターからの \fBscha_control\fR コマンドまたは \fBscha_control()\fR 関数の呼び出しの結果として新しいノードまたはゾーンに再配置される前に呼び出されます。
.sp
\fBMONITOR_CHECK\fR メソッドは、リソースグループの潜在的な新しいマスターであるいずれかのノードまたはゾーン上で呼び出せます。\fBMONITOR_CHECK\fR メソッドは、リソースを実行するのにノードまたはゾーンが十分に健全であるかどうかを確認します。\fBMONITOR_CHECK\fR メソッドは、並行して実行されるほかのメソッドと衝突しない方法で実装する必要があります。
.sp
\fBMONITOR_CHECK\fR が失敗した場合、コールバックが呼び出されたノードまたはゾーンへのリソースグループの再配置は拒否されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fBIGNORE_FAILED_START\fR\fR
.ad
.sp .6
.RS 4n
現在実行中の \fBPrenet_start\fR または \fBStart\fR メソッドが失敗した場合、\fBFailover_mode\fR プロパティーの設定とは関係なく、リソースグループのフェイルオーバーを行わないように要求します。
.sp
言い換えると、この \fIoptag\fR 値は、\fBFailover_Mode\fR プロパティーが \fBSOFT\fR または \fBHARD\fR に設定されているリソースに対して、通常、そのリソースが起動に失敗したときに行われる回復アクションを無効にします。通常、リソースグループは異なるノードまたはゾーンにフェイルオーバーします。ところが、(この値が設定されている場合)、リソースは \fBFailover_Mode\fR が \fBNONE\fR に設定されているかのように動作します。このリソースは \fBSTART_FAILED\fR 状態になり、その他のエラーが発生していない場合、リソースグループは \fBONLINE_FAULTED\fR 状態で終了します。
.sp
この \fBoptag\fR 値は、0 以外の終了コードまたはタイムアウトで終了する \fBStart\fR または \fBPrenet_start\fR メソッドから呼び出された場合にのみ有効です。また、この \fBoptag\fR 値は、現在の \fBStart\fR または \fBPrenet_start\fR メソッドの呼び出しに対してのみ有効です。\fBStart\fR メソッドにより、リソースを別のノードまたはゾーン上で正常に起動できないと判断された場合は、この \fBoptag\fR 値を指定して \fBscha_control\fR コマンドを呼び出すことをお勧めします。この \fBoptag\fR 値がその他のメソッドによって呼び出された場合、\fBSCHA_ERR_INVAL\fR エラーが返されます。この \fBoptag\fR 値により、発生するはずのリソースグループの「ping pong」フェイルオーバーが行われません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBRESOURCE_DISABLE\fR\fR
.ad
.sp .6
.RS 4n
\fB-R\fR オプションで指定されたリソースを \fBscha_control\fR コマンドが呼び出されるノードまたはゾーンで無効にします。
.sp
ノードまたはゾーン上にある「依存先の」リソースで障害が発生して、リソースを回復できない場合、モニターは該当ノードまたはゾーン上の該当リソースをオフラインにします。モニターは、\fBscha_control\fR コマンドを \fBRESOURCE_DISABLE\fR \fIoptag\fR 値とともに呼び出して、リソースをオフラインにします。また、モニターは、依存先リソースのオフライン、再起動に従属する対象をすべて、それらに対して再起動をトリガーすることにより、オフラインにします。クラスタ管理者が障害を解決し、依存先のリソースを再度有効にすると、モニターは、リソースのオフライン再起動依存リソースも再度オンラインにします。
.RE

.sp
.ne 2
.mk
.na
\fB\fBRESOURCE_IS_RESTARTED\fR\fR
.ad
.sp .6
.RS 4n
\fB-R\fR オプションで指定されたリソースのリソース再起動カウンタの値を、実際にリソースを再起動することなく、ローカルノードまたはゾーン上で増分することを要求します。
.sp
\fBscha_control\fR の \fBRESOURCE_RESTART\fR オプションを呼び出さず pmfadm(1M) などで直接リソースを再起動するリソースモニターの場合、このオプションを使ってリソースが再起動したことを RGMに通知できます。この増分は、scha_resource_get(1HA) の後続の \fBNUM_RESOURCE_RESTARTS\fR クエリーに反映されます。
.sp
リソースタイプが \fBRETRY_INTERVAL\fR 標準プロパティーの宣言に失敗した場合、\fBscha_control\fR コマンドの \fBRESOURCE_IS_RESTARTED\fR オプションは使用できなくなります。その結果、\fBscha_control\fR コマンドは失敗し、終了ステータスコード 13 (\fBSCHA_ERR_RT\fR) を生成します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBRESOURCE_RESTART\fR\fR
.ad
.sp .6
.RS 4n
\fB-R\fR オプションで指定されたリソースをオフラインにしたあと、同じリソースグループ内のその他のリソースを停止することなくローカルノードまたはゾーン上でオンラインに戻すことを要求します。リソースの停止と起動は、ローカルノードまたはゾーン上で次のメソッドを実行することで行われます。
.sp
.in +2
.nf
MONITOR_STOP
STOP
POSTNET_STOP
PRENET_START
START
MONITOR_START
.fi
.in -2

リソースタイプが \fBMONITOR_STOP\fR および \fBMONITOR_START\fR メソッドを宣言していない場合、\fBSTOP\fR メソッドと \fB START\fR メソッドだけが呼び出され、再起動が実行されます。リソースタイプが \fBSTART\fR と \fBSTOP\fR のどちらのメソッドも宣言していない場合、\fBscha_control\fR コマンドは失敗し、終了ステータスコード 13 (\fBSCHA_ERR_RT\fR) を生成します。
.sp
リソースの再起動時にメソッドの実行に失敗した場合は、RGM はリソースの \fBFAILOVER_MODE\fR プロパティー設定に応じて、エラーステータスの設定、リソースグループの再配置、またはノードまたはゾーンの再起動を行います。詳細は、r_properties(5) の \fBFAILOVER_MODE\fR プロパティーを参照してください。
.sp
リソースモニターで、このオプションによりリソースの再起動を行う場合、scha_resource_get(1HA) の \fBNUM_RESOURCE_RESTARTS\fR クエリーを使用して、再起動を試みた最近の回数を追跡することができます。
.sp
\fBRESOURCE_RESTART\fR 関数は、\fBPRENET_START\fR、\fBPOSTNET_STOP\fR、またはこの両方のメソッドを持つリソースタイプによって使用されなければなりません。こうしたリソースに対して使用できるのは \fBMONITOR_STOP\fR、\fBSTOP\fR、\fBSTART\fR、\fBMONITOR_START\fR のメソッドのみです。このリソースが依存するネットワークアドレスリソースは再起動されず、オンライン状態のままになります。
.sp
ノードまたはゾーン上にある「依存先の」リソースで障害が発生して、リソースを回復できない場合、モニターは該当ノードまたはゾーン上の該当リソースをオフラインにします。モニターは \fBRESOURCE_RESTART\fR \fIoptag\fR 値を指定して \fBscha_control\fR コマンドを呼び出すことでリソースをオフラインにします。また、モニターは、依存先リソースのオフライン、再起動に従属する対象をすべて、それらに対して再起動をトリガーすることにより、オフラインにします。クラスタ管理者が障害を解決し、依存先のリソースを再度有効にすると、モニターは、リソースのオフライン再起動依存リソースも再度オンラインにします。
.RE

.sp
.ne 2
.mk
.na
\fB\fBRESTART\fR\fR
.ad
.sp .6
.RS 4n
\fB-G\fR オプションで指定されたリソースグループをオフラインにしたあと、別のノードまたはゾーンに再配置することなくオンラインに戻すことを要求します。ただし、この要求を実行する際にグループ内のリソースの再起動が失敗した場合は、最終的にリソースグループが再配置されることもあります。リソースモニターで、このオプションによりリソースグループの再起動を行う場合、scha_resource_get(1HA) の \fBNUM_RG_RESTARTS\fR クエリーを使用して、再起動を試みた最近の回数を追跡することができます。
.sp
\fBCHECK_GIVEOVER\fR と \fBCHECK_RESTART\fR の \fI optag\fR 値は、再配置や再起動を行う際、\fBscha_control\fR コマンドを呼び出すのではなく、リソースに対して直接的なアクション (プロセスの強制終了と再起動、ノードまたはゾーンの再起動など) を取るようなリソースモニターによって使用されます。チェックに失敗した場合、モニターは、再起動やフェイルオーバーのアクションを呼び出す代わりに、しばらく休止したあと検証を再開します。詳細は、scha_control(3HA)を参照してください。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-R\fR \fIresource\fR\fR
.ad
.sp .6
.RS 4n
リソースグループ内のリソースの名前です。このリソースはおそらく、そのモニターが scha_control(1HA) 要求を行なっていたものです。指定されたリソースがリソースグループ内に存在しない場合、要求は拒否されます。
.sp
指示されたリソースの \fBFailover_mode\fR プロパティーの設定は、要求された \fBscha_control \fR アクションを抑制する場合があります。\fBFailover_mode\fR が \fBRESTART_ONLY\fR である場合、\fBscha_control\fR \fB GIVEOVER\fR と \fBscha_control\fR \fBCHECK_GIVEOVER\fR を除くすべての要求が許可されます。\fBGIVEOVER\fR 要求および \fBCHECK_GIVEOVER\fR 要求は \fBSCHA_ERR_CHECKS\fR 終了コードを返し、要求されたギブオーバー動作は実行されず、\fBsyslog\fR メッセージのみを作成します。
.sp
\fBRetry_count\fR プロパティーと \fBRetry_interval\fR プロパティーがリソースで設定されている場合、リソース再起動の回数は \fBRetry_interval\fR 内での \fBRetry_count\fR の試行回数に制限されます。\fBFailover_mode\fR が \fBLOG_ONLY\fR である場合、任意の \fBscha_control\fR ギブオーバー、再起動、または無効の要求は \fBSCHA_ERR_CHECKS\fR 終了コードを返し、また要求されたギブオーバーまたは再起動アクションは実行されず、\fBsyslog\fR メッセージのみを作成します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-Z\fR \fIzonename\fR\fR
.ad
.sp .6
.RS 4n
リソースグループが実行を設定されているゾーンの名前です。
.sp
\fBGlobal_zone\fR プロパティーに \fBTRUE\fR が設定されている場合、リソースを含むリソースグループが非大域ゾーンで動作しているときでも、メソッドは大域ゾーンで実行されます。このオプションは、リソースグループが実行するよう構成されている非大域ゾーンの名前を指定します。
.sp
\fBGlobal_zone\fR プロパティーが \fBTRUE\fR に設定されているリソースタイプにのみ、\fB-Z\fR オプションを使用します。\fBGlobal_zone\fR プロパティーが \fBFALSE\fR に設定されている場合、このオプションは必要ありません。\fBGlobal_zone\fR プロパティーの詳細については、rt_properties(5) のマニュアルページを参照してください。
.RE

.SH 終了ステータス
.sp
.LP
次の終了ステータスコードが返されます。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 13n
.rt  
コマンドは正常に完了しました。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 13n
.rt  
エラーが発生しました。
.sp
障害エラーコードについては、scha_calls(3HA)を参照してください。
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性安定
.TE

.SH 関連項目
.sp
.LP
pmfadm(1M)、rt_callbacks(1HA)、scha_cmds(1HA)、scha_resource_get(1HA)、scha_calls(3HA)、scha_control(3HA)、scha_control_zone(3HA)、\fBattributes\fR(5)、r_properties(5)、\fBrbac\fR(5)、rg_properties(5)、rt_properties(5)
