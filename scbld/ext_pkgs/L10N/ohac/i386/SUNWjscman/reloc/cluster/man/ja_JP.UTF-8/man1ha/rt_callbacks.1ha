'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH rt_callbacks 1HA "2008 年 8 月 18 日" "Sun Cluster 3.2" "Sun Cluster 保守コマンド"
.SH 名前
rt_callbacks \- Sun Cluster リソースとしてサービスを管理するためのコールバックインタフェース
.SH 形式
.LP
.nf
\fImethod-path\fR \fB-R\fR \fI resource\fR \fB-T\fR \fItype\fR \fB -G\fR \fIgroup\fR [\fB-Z \fR \fIzonename\fR]
.fi

.LP
.nf
 \fIvalidate-path\fR [\fB-c\fR | \fB -u\fR] \fB-R\fR \fIresource\fR \fB -T\fR \fItype\fR \fB-G\fR \fI group\fR [\fB-r\fR \fIprop\fR=\fIval\fR] [\fB -x\fR \fIprop\fR=\fIval\fR] [\fB -g\fR \fIprop\fR=\fIval\fR] [\fB -Z\fR \fIzonename\fR]
.fi

.SH 機能説明
.sp
.LP
Sun Cluster リソースタイプのコールバックインタフェースは、Resource Group Manager (\fBRGM\fR) がサービスをクラスタリソースとして制御するためのインタフェースを定義します。リソースタイプの実装者は、コールバックメソッドとして機能するプログラムまたはスクリプトを提供します。
.sp
.ne 2
.mk
.na
\fB\fImethod-path\fR\fR
.ad
.RS 20n
.rt  
リソースタイプ登録ファイルで宣言されているプログラムへのパスです。このプログラムは、次に示すリソースタイプのコールバックメソッドの 1 つとして \fBclresourcetype\fR コマンドで登録されます。\fBSTART\fR、\fBSTOP\fR、\fB INIT\fR、\fBFINI\fR、\fBBOOT\fR、\fB PRENET_START\fR、\fBPOSTNET_STOP\fR、\fBMONITOR_START \fR、\fBMONITOR_STOP\fR、\fBMONITOR_CHECK\fR、または \fBUPDATE\fR があります。clresourcetype(1CL) および rt_reg(4) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fIvalidate-path\fR\fR
.ad
.RS 20n
.rt  
リソースタイプ登録ファイル内でリソースタイプの \fBVALIDATE\fR メソッドとして宣言されているプログラムへのパスです。このプログラムは \fBclresourcetype\fR コマンドで登録されます。
.RE

.sp
.LP
コールバックメソッドは指定されたオプションを渡され、クラスタ上のサービスの操作を制御する特定のアクションを実行します。
.sp
.LP
リソースタイプの開発者は、リソースタイプ登録ファイルでコールバックメソッドプログラムへのパスを宣言します。クラスタ管理者は、\fBclresourcetype\fR コマンドを使用してクラスタ構成にリソースタイプを登録します。クラスタ管理者は、登録後、登録されたリソースタイプを使用してリソースを作成できます。これらのリソースは、RGM が管理するリソースグループ内に構成されます。
.sp
.LP
RGM は、管理するリソースグループ内のリソースのコールバックメソッドを自動的に呼び出して、イベントに応答します。コールバックメソッドは、リソースによって表されるサービスに関して特定のアクションを実行すると想定されています。これらのアクションの例には、クラスタノードまたはゾーン上でのサービスの停止と開始などがあります。
.sp
.LP
コールバックメソッドから返される終了ステータスコードは、コールバックメソッドの成功または失敗を RGM に通知します。メソッドが失敗またはリソース状態の障害を報告すると、RGM がアクションを実行します。この結果、クラスタ管理者は障害に気付き、適切なアクションを実行できます。
.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-c\fR\fR
.ad
.sp .6
.RS 4n
クラスタ管理者が\fBすべての\fRリソースとリソース グループのプロパティーの初期設定を検証するためのリソースを作成する際に、このメソッドを呼び出すことを指定します。
.sp
RGM は \fB-c\fR オプションまたは \fB-u\fR オプションを個別に指定しますが、同時に両方を指定しないでください。
.sp
クラスタ管理者がリソースを作成して、\fBVALIDATE\fR メソッドが呼び出されると、\fBすべての\fRシステム定義、拡張機能、およびリソースグループのプロパティーが \fBVALIDATE \fR メソッドに渡されます。クラスタ管理者がリソースを更新して、\fBVALIDATE\fR メソッドが呼び出されると、更新されているプロパティーだけが \fBVALIDATE\fR メソッドに渡されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-g\fR \fIprop\fR\fB =\fR\fIval\fR\fR
.ad
.sp .6
.RS 4n
\fBVALIDATE\fR メソッドに渡されるリソースグループプロパティーの値を指定します。 
.sp
.ne 2
.mk
.na
\fB\fIprop\fR\fR
.ad
.RS 13n
.rt  
リソースグループプロパティーの名前です。 
.RE

.sp
.ne 2
.mk
.na
\fB\fIval\fR\fR
.ad
.RS 13n
.rt  
クラスタ管理者がリソースを作成または更新した際にメソッドに渡される値です。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-G\fR \fIgroup\fR\fR
.ad
.sp .6
.RS 4n
リソースを構成するリソースグループの名前を指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-r\fR \fIprop\fR\fB =\fR\fIval\fR\fR
.ad
.sp .6
.RS 4n
\fBVALIDATE\fR メソッドに渡されるシステムが定義したリソースプロパティーの値を指定します。
.sp
.ne 2
.mk
.na
\fB\fIprop\fR\fR
.ad
.RS 13n
.rt  
システムが定義したリソースプロパティーの名前です。 
.RE

.sp
.ne 2
.mk
.na
\fB\fIval\fR\fR
.ad
.RS 13n
.rt  
クラスタ管理者がリソースを作成または更新した際にメソッドに渡される値です。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-R\fR \fIresource\fR\fR
.ad
.sp .6
.RS 4n
メソッドが呼び出されるリソース名を指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-T\fR \fItype\fR\fR
.ad
.sp .6
.RS 4n
リソースが属するリソースタイプの名前を指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-u\fR\fR
.ad
.sp .6
.RS 4n
クラスタ管理者が既存のリソースまたは既存のリソースグループを更新するとメソッドが呼び出されることを指定します。
.sp
RGM は \fB-c\fR オプションまたは \fB-u\fR オプションを個別に指定しますが、同時に両方を指定することはありません。
.sp
クラスタ管理者がリソースを作成して、\fBVALIDATE\fR メソッドが呼び出されると、\fBすべての\fRシステム定義、拡張機能、およびリソースグループのプロパティーが \fBVALIDATE \fR メソッドに渡されます。クラスタ管理者がリソースを更新して、\fBVALIDATE\fR メソッドが呼び出されると、更新されているプロパティーだけが \fBVALIDATE\fR メソッドに渡されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-x\fR \fIprop\fR\fB =\fR\fIval\fR\fR
.ad
.sp .6
.RS 4n
\fBVALIDATE\fR メソッドに渡されるリソース拡張プロパティーの値を指定します。
.sp
.ne 2
.mk
.na
\fB\fIprop\fR\fR
.ad
.RS 13n
.rt  
リソース拡張プロパティー名です。拡張プロパティーは、リソースタイプの実装によって定義されます。この拡張プロパティーは、リソースタイプ登録ファイルのパラメータ表で宣言されます。 
.RE

.sp
.ne 2
.mk
.na
\fB\fIval\fR\fR
.ad
.RS 13n
.rt  
クラスタ管理者がリソースを作成または更新した際にメソッドに渡される値です。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-Z\fR \fIzonename\fR\fR
.ad
.sp .6
.RS 4n
リソースグループが実行を設定されている非大域ゾーンの名前を指定します。
.sp
\fBGlobal_zone\fR リソースタイププロパティーに \fBTRUE\fR が設定されている場合、リソースを含むリソースグループが非大域ゾーンで動作しているときでも、メソッドは大域ゾーンで実行されます。このオプションは、リソースグループが実行するよう構成されている非大域ゾーンの名前を指定します。
.sp
次のいずれかの条件が満たされると、\fB-Z\fR オプションは渡されません。
.RS +4
.TP
.ie t \(bu
.el o
\fBGlobal_zone\fR プロパティーに \fBFALSE\fR が設定されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースグループが大域ゾーンで実行するよう構成されている。
.RE
.RE

.SH 使用法
.sp
.LP
コールバックメソッドは、それらを呼び出す RGM によって定義されます。これらのメソッドは、クラスタリソース上で処理を実行します。また、これらのメソッドは、メソッドが成功したか失敗したかを報告する終了状態を返します。各コールバックメソッドについては、次のセクションで説明します。
.sp
.ne 2
.mk
.na
\fB\fBBOOT\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、クラスタが起動または再起動されたときに、ノードまたはゾーンがクラスタに結合または再結合すると呼び出されます。このメソッドは、\fBInit_nodes\fR リソースタイププロパティーにより指定されるノードまたはゾーン上で呼び出されます。\fBINIT\fR と同様に、このメソッドは、リソースを含むリソースグループがオンラインになったあと、クラスタに結合するノードまたはゾーン上でリソースを初期化します。このメソッドは、管理されているソースグループ内のリソース上で呼び出されますが、管理されていないリソースグループ内のリソース上では呼び出されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBFINI\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースを含むリソースグループが RGM の管理から外れたときに呼び出されます。このメソッドは、\fBInit_nodes\fR リソースタイププロパティーにより指定されるノードまたはゾーン上で呼び出されます。このメソッドは、リソースの構成を解除し、\fBINIT\fR メソッドまたは \fBBOOT\fR メソッドにより行われたすべての永続的な設定をクリーンアップします。
.RE

.sp
.ne 2
.mk
.na
\fB\fBINIT\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースを含むリソースグループが RGM の管理下に入ったときに呼び出されます。このメソッドは、\fBInit_nodes\fR リソースタイププロパティーにより指定されるノードまたはゾーン上で呼び出されます。このメソッドは、リソースを初期化します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBMONITOR_CHECK\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースを含むリソースグループが新しいノードまたはゾーンに再配置される前に呼び出されます。このメソッドは、障害モニターが \fBscha_control\fR コマンドまたは \fBscha_control\fR 関数の \fBGIVEOVER()\fR オプションを実行するときに呼び出されます。scha_control(1HA) および scha_control(3HA) のマニュアルページを参照してください。
.sp
このメソッドは、リソースグループの潜在的な新しいマスターである任意のノードまたはゾーンで呼び出されます。\fBMONITOR_CHECK\fR メソッドは、ノードまたはゾーンがリソースを実行するのに十分なほど健全であるかどうかを評価します。\fBMONITOR_CHECK\fR メソッドは、並行して実行されるほかのメソッドと衝突しない方法で実装する必要があります。
.sp
\fBMONITOR_CHECK\fR メソッドが失敗した場合、コールバックが呼び出されたノードまたはゾーンへのリソースグループの再配置は拒否されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fBMONITOR_START\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースが起動したあと、リソースが起動したノードまたはゾーン上で呼び出されます。このメソッドは、リソースの監視を開始します。
.sp
\fBMONITOR_START\fR が失敗した場合、RGM によりリソースの状態が \fBMONITOR_FAILED\fR に設定されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fBMONITOR_STOP\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースが停止する前、リソースを実行しているノードまたはゾーン上で呼び出されます。このメソッドは、リソースの監視を停止します。このメソッドは、監視がクラスタ管理者によって無効化されている場合も呼び出されます。
.sp
\fBMONITOR_STOP\fR メソッドが失敗した場合に RGM が実行するアクションは、リソースの \fBFailover_mode\fR プロパティーの設定に依存します。\fBFailover_mode\fR に \fBHARD\fR が設定されている場合、RGM はノードまたはゾーンをリブートして、リソースを強制的に停止します。その他の場合、RGM はリソースの状態に \fBSTOP_FAILED\fR を設定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBPOSTNET_STOP\fR\fR
.ad
.sp .6
.RS 4n
\fBSTOP\fR メソッドを補助するメソッドで、関連ネットワークアドレスを停止したあとに必要な停止アクションを実行します。このメソッドは、\fBSTOP\fR メソッドが呼び出されたノードまたはゾーンで呼び出されます。このメソッドは、リソースグループ内のネットワークアドレスを停止したあとに、リソースの \fBSTOP\fR メソッドが呼び出されたあと呼び出されます。ただし、このメソッドは、ネットワークアドレスが unplumb される前に呼び出されます。\fBPOSTNET_STOP\fR メソッドは、リソースの \fBSTOP\fR メソッドと、このリソースに依存するすべてのリソースの \fBPOSTNET_STOP\fR メソッドのあとに呼び出されます。
.sp
\fBPOSTNET_STOP\fR メソッドが失敗したときに RGM が実行するアクションは、リソースの \fBFailover_mode\fR プロパティーの設定によって異なります。\fBFailover_mode\fR に \fBHARD\fR が設定されている場合、RGM はノードまたはゾーンを終了させて、リソースを強制的に停止します。その他の場合、RGM はリソースの状態に \fBSTOP_FAILED\fR を設定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBPRENET_START\fR\fR
.ad
.sp .6
.RS 4n
\fBSTART\fR メソッドを補助するメソッドで、関連ネットワークアドレスを構成する前に必要な起動アクションを実行します。このメソッドは、\fBSTART\fR メソッドが呼び出されるノードまたはゾーンで呼び出されます。このメソッドは、同じリソースグループ内のネットワークアドレスが plumb されたあと、呼び出されます。ただし、このメソッドは、アドレスが設定され、リソースの \fBSTART\fR メソッドが呼び出される前に呼び出されます。リソースの \fBSTART\fR メソッドと、このリソースに依存するすべてのリソースの \fBPRENET_START\fR メソッドより前に、\fBPRENET_START\fR メソッドが呼び出されます。
.sp
\fBPRENET_START\fR メソッドが失敗したときに RGM が実行するアクションは、リソースの \fBFailover_mode\fR プロパティーの設定によって異なります。\fBFailover_mode\fR に \fBSOFT\fR または \fBHARD\fR が設定されている場合、RGM は、そのリソースを含むリソースのグループを、別のノードまたはゾーンに再配置しようとします。その他の場合、RGM はリソースの状態に \fBSTART_FAILED\fR を設定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSTART\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースを含むリソースグループがクラスタノードまたはゾーン上でオンラインになったとき、そのクラスタノードまたはゾーン上で呼び出されます。クラスタ管理者は、\fBclresourcegroup\fR コマンドを使って、オンとオフの状態を切り替えることができます。\fBSTART\fR メソッドは、ノードまたはゾーン上のリソースを有効にします。
.sp
\fBSTART\fR メソッドが失敗したときに RGM が実行するアクションは、リソースの \fBFailover_mode\fR プロパティーの設定によって異なります。\fBFailover_mode\fR に \fBSOFT\fR または \fBHARD\fR が設定されている場合、RGM はリソースのグループを別のノードまたはゾーンに再配置しようとします。その他の場合、RGM はリソースの状態に \fBSTART_FAILED\fR を設定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSTOP\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースを含むリソースグループがクラスタノードまたはゾーン上でオフラインになったとき、そのクラスタノードまたはゾーン上で呼び出されます。クラスタ管理者は、\fBclresourcegroup\fR コマンドを使って、オンとオフの状態を切り替えることができます。このメソッドは、リソースを (アクティブであれば) 停止します。
.sp
\fBSTOP\fR メソッドが失敗したときに RGM が実行するアクションは、リソースの \fBFailover_mode\fR プロパティーの設定によって異なります。\fBFailover_mode\fR に \fBHARD\fR が設定されている場合、RGM はノードまたはゾーンをリブートして、リソースを強制的に停止します。その他の場合、RGM はリソースの状態に \fBSTOP_FAILED\fR を設定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBUPDATE\fR\fR
.ad
.sp .6
.RS 4n
このメソッドを呼び出すと、プロパティーが変更されたことを実行中のリソースに通知することができます。\fBUPDATE\fR メソッドは、RGM がソースまたはそのリソースグループのプロパティーの設定に成功したあとに呼び出されます。このメソッドは、リソースがオンラインであるノードまたはゾーン上で呼び出されます。このメソッドは、\fBscha_resource_get\fR と \fBscha_resourcegroup_get\fR コマンドを呼び出して、有効なリソースに影響を与える場合があるプロパティー値を読み取り、実行中のリソースを適宜調整できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fBVALIDATE\fR\fR
.ad
.sp .6
.RS 4n
このメソッドは、リソースが作成された場合やリソースまたはリソースを含むリソースグループが更新された場合に呼び出されます。\fBVALIDATE\fR は、リソースタイプの \fBInit_nodes\fR プロパティーにより指定されるクラスタノードまたはゾーンのセットに対して呼び出されます。
.sp
\fBVALIDATE\fR メソッドは、リソースの作成または更新が適用される前に呼び出されます。メソッドがノードまたはゾーン上で失敗し、障害終了ステータスコードが生成されると、作成または更新は中止されます。
.sp
クラスタ管理者がリソースを作成して、\fBVALIDATE\fR メソッドが呼び出されると、\fBすべての\fRシステム定義、拡張機能、およびリソースグループのプロパティーが \fBVALIDATE \fR メソッドに渡されます。クラスタ管理者がリソースを更新して、\fBVALIDATE\fR メソッドが呼び出されると、更新されているプロパティーだけが \fBVALIDATE\fR メソッドに渡されます。\fBscha_resource_get\fR および \fBscha_resourcegroup_get\fR コマンドを使用すると、更新されないリソースのプロパティーを取得できます。
.sp
\fBVALIDATE\fR メソッドを実装すると、ユーザーが \fBstdout\fR または \fBstderr\fR に書き込むすべてのメッセージはユーザーコマンドに戻されます。このアクションは、検証障害の理由を説明したり、リソースに関係してユーザーに指示を与えるのに便利です。
.RE

.SH 環境
.sp
.LP
Sun Cluster リソース管理コールバックメソッドは、RGM によりスーパーユーザー権限で実行されます。これらのメソッドを実装するプログラムは、適切な実行権でインストールされます。セキュリティ上の理由から、これらのプログラムに対する書き込み権は設定しません。
.sp
.LP
コールバックメソッドの実行のために設定される環境変数は次のとおりです。
.sp
.in +2
.nf
HOME=/
PATH=/usr/bin:/usr/cluster/bin
LD_LIBRARY_PATH=/usr/cluster/lib
.fi
.in -2

.SH シグナル
.sp
.LP
コールバックメソッド呼び出しがタイムアウト期間内に成功しないと、そのプロセスにまず \fBSIGABRT\fR シグナルが送信され、コアダンプファイルが生成されます。このコアダンプファイルは、そのメソッドを実行した各ノードの \fB/var/cluster/core\fR ディレクトリまたは \fB/var/cluster/core\fR ディレクトリのサブディレクトリに格納されます。このコアダンプファイルは、メソッドがタイムアウトした原因を判断できるようにするために生成されます。\fBSIGABRT\fR シグナルがメソッドの実行を停止できなかった場合は、プロセスに \fBSIGKILL\fR シグナルが送信されます。
.SH 終了ステータス
.sp
.LP
次の終了ステータスコードが返されます。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 13n
.rt  
コマンドは正常に完了しました。
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 13n
.rt  
エラーが発生しました。
.RE

.sp
.LP
失敗の終了ステータス値は、失敗時の RGM のアクションに影響を及ぼしません。ただし、終了ステータスはメソッドの失敗時にクラスタログに記録されます。リソースタイプを実装して、0 以外の複数の終了ステータスを定義することにより、クラスタログを介して管理者とエラー情報を交換できます。
.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
clresourcegroup(1CL)、clresourcetype(1CL)、scha_cmds(1HA)、scha_control(1HA)、scha_resource_get(1HA)、scha_resourcegroup_get(1HA)、\fBsignal\fR(3C)、\fBstdio\fR(3C)、scha_calls(3HA)、scha_control(3HA)、rt_reg(4)、\fBattributes\fR(5)
