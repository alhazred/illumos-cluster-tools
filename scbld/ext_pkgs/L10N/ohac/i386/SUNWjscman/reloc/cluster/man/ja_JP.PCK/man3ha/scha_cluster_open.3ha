'\" te
.\"  Copyright 2007 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  scha_cluster_open   3HA  "2007 年 9 月 7 日" " Sun Cluster 3.2 " " Sun Cluster HA およびデータサービス "
.SH 名前
scha_cluster_open, scha_cluster_close, scha_cluster_get \-  クラスタについての情報へのアクセスとクラスタについての情報の入手
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR scha #include <scha.h> \fBscha_err_t\fR \fBscha_cluster_open\fR(\fBscha_cluster_t *\fR\fIhandle\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_cluster_get\fR(\fBscha_cluster_t\fR \fIhandle\fR, \fBconst char **\fR\fItag\fR, ...);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_cluster_close\fR(\fBscha_cluster_t\fR \fIhandle\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscha_cluster_open()\fR、\fBscha_cluster_get ()\fR、\fBscha_cluster_close()\fR の 3 つの関数を併用して、クラスタについての情報を入手できます。
.sp
.LP
\fBscha_cluster_open()\fR はクラスタアクセスを初期化し、\fBscha_cluster_get()\fR が使用するアクセスハンドルを返します。\fIhandle\fR 引数は、関数が返す値を格納する変数のアドレスです。
.sp
.LP
\fBscha_cluster_get()\fR 関数は、\fItag\fR 引数に指定されるクラスタの情報にアクセスします。この関数を非大域ゾーンで呼び出すと、ゾーン名とノード名が返されます。\fIhandle\fR 引数は、事前に \fBscha_cluster_open()\fR を使って得た値です。\fItag\fR 引数は、\fBscha_tags.h\fR ヘッダーファイルのマクロで定義される文字列値です。\fItag\fR に続く引数は、指定したタグの値によって変わります。
.sp
.LP
\fItag\fR 引数のあとに引数を追加して、情報取得の対象となるクラスタノードまたはゾーンを指定しなければならない場合もあります。引数リストの最後の引数は、\fItag\fR 引数で指定される情報の格納に適した変数型にする必要があります。これは出力引数で、クラスタの情報を格納します。関数の実行に失敗した場合、出力引数に値は返されません。\fBscha_cluster_get()\fR 関数が返す情報を格納するために割り当てられたメモリーは、\fBscha_cluster_get ()\fR 関数が使用したハンドルで \fBscha_cluster_close ()\fR を呼び出すまで、そのまま残ります。
.sp
.LP
\fBscha_cluster_close()\fR は、以前の \fBscha_cluster_get()\fR 関数の呼び出しから返された \fBhandle\fR 引数を取ります。この関数は、このハンドルを無効にして、このハンドルで行われた \fBscha_cluster_get()\fR 呼び出しが返した値に割り当てられたメモリーを解放します。値を返す必要が生じるごとに、個々の \fBget\fR 呼び出しでメモリーが割り当てられる点に注意してください。ある呼び出しで値を返すために割り当てられたメモリーが、以降の呼び出しによって上書きされたり、再利用されたりすることはありません。
.SS "\fItag\fR 引数に使用できるマクロ"
.sp
.LP
次に、\fBscha_tags.h\fR に定義されており、\fItag\fR 引数に使用できるマクロを示します。ここでは出力引数および追加引数の型を説明します。構造と \fBenum\fR 型は、scha_calls(3HA) で説明しています。
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_NODEIDS\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_uint_array_t**\fR です。
.sp
返される値は、クラスタ内のすべてのノードの数値識別子です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_NODENAMES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
返される値は、クラスタ内のすべてのノードの名前です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_NONGLOBAL_ZONES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
このマクロは、クラスタ内の全ノード上にあるすべての非大域ゾーンのゾーン名を返します。返されるゾーン名の形式は、\fBnodename:zonename\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_NONGLOBAL_ZONES_NODEID\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
このマクロは、ノードの数値識別子が引数として与えられたクラスタノード上のすべての非大域ゾーンのゾーン名を返します。返されるゾーン名の形式は、\fBnodename:zonename\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_PRIVATELINK_HOSTNAMES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
返される値は、クラスタインターコネクトで、全クラスタノードまたはゾーンを対象にノードアドレスまたはゾーンアドレスを指定するホスト名です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_RESOURCEGROUPS\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
返される値は、クラスタ上で管理されているすべてのリソースグループの名前です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_RESOURCETYPES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
返される値は、クラスタに登録されているすべてのリソースタイプの名前です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_ZONES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
返される値は、クラスタ内の全ノード上にあるすべてのゾーン (大域ゾーンも含まれる) のゾーン名です。返されるゾーン名の形式は、\fBnodename:zonename\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ALL_ZONES_NODEID\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
返される値は、ノードの数値識別子が引数として与えられたクラスタノード (大域ゾーンも含まれる) 上の全ゾーンのゾーン名です。返されるゾーン名の形式は、\fBnodename:zonename\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_CLUSTERNAME\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.sp
返される値は、クラスタの名前です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODEID_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBuint_t*\fR です。
.sp
返される値は、コマンドを実行するノードの数値識別子です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODEID_NODENAME\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBuint_t*\fR です。追加引数の型は \fBchar *\fR です。このマクロに必要な追加引数には、クラスタノードの名前を指定します。
.sp
返される値は、指定した名前に該当するノードの数値識別子です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODENAME_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.sp
返される値は、関数が実行されたクラスタノードの名前です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODENAME_NODEID\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。追加引数の型は \fBuint_t\fR です。追加引数には、数値式のクラスタノード識別子を指定します。
.sp
返される値は、数値識別子で指定されるクラスタノードの名前です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODESTATE_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_node_state_t*\fR です。
.sp
返される値は、コマンドが実行されたノードの状態に応じて \fBSCHA_NODE_UP\fR または \fBSCHA_NODE_DOWN\fR となります。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODESTATE_NODE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_node_state_t*\fR です。追加引数の型は \fBchar*\fR です。このマクロに必要なフラグ無し追加引数には、クラスタノードの名を指定します。
.sp
返される値は、名前付きノードの状態に応じて \fBSCHA_NODE_UP\fR または \fBSCHA_NODE_DOWN\fR となります。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PRIVATELINK_HOSTNAME_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.sp
返される値は、コマンドを実行するノードまたはゾーンのクラスタインターコネクトでアドレス指定に使用されるホスト名です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PRIVATELINK_HOSTNAME_NODE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。追加引数の型は \fBchar *\fR です。このマクロに必要なフラグ無し追加引数には、クラスタノードの名前が入ります。
.sp
返される値は、クラスタインターコネクトで特定のノードまたはゾーンのアドレスを指定するホスト名です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_SYSLOG_FACILITY\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBint*\fR です。
.sp
RGM がログメッセージで使用する \fBsyslog\fR(3C) 関数の番号を返します。返される値は \fB24\fR です。これは \fBLOG_DAEMON\fR 機能の値に対応しています。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ZONE_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.sp
返される値は、呼び出しが実行されたゾーンの名前です。返されるゾーン名の形式は、\fBnodename:zonename\fR です。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 36n
.rt  
関数の実行に成功。
.RE

.sp
.LP
その他のエラーコードについては、scha_calls(3HA)を参照してください。
.SH 使用例
.LP
\fB例 1 \fR\fBscha_cluster_get()\fR 関数の使用
.sp
.LP
次の例では、\fBscha_cluster_get()\fR 関数を使用して、全クラスタノードの名前を取得します。この関数を使用すると、ノードが稼働しているかどうかもわかります。

.sp
.in +2
.nf
#include <scha.h>
#include <stdio.h>
#include <stdlib.h>

main()
{
       scha_err_t              err;
       scha_node_state_t       node_state;
       scha_str_array_t        *all_nodenames;
       scha_cluster_t          handle;
       int                     ix;
       const char              *str;

       err = scha_cluster_open(&handle);
       if (err != SCHA_ERR_NOERR) {
               fprintf(stderr, "FAILED: scha_cluster_open()0);
               exit(err);
       }

       err = scha_cluster_get(handle, SCHA_ALL_NODENAMES, &all_nodenames);
       if (err != SCHA_ERR_NOERR) {
               fprintf(stderr, "FAILED: scha_cluster_get()0);
               exit(err);
       }

       for (ix = 0; ix < all_nodenames->array_cnt; ix++) {
               err = scha_cluster_get(handle, SCHA_NODESTATE_NODE,
                   all_nodenames->str_array[ix], &node_state);
               if (err != SCHA_ERR_NOERR) {
                       fprintf(stderr, "FAILED: scha_cluster_get()"
                           "SCHA_NODESTATE_NODE0);
                       exit(err);
               }

               switch (node_state) {
               case SCHA_NODE_UP:
                       str = "UP";
                       break;
               case SCHA_NODE_DOWN:
                       str = "DOWN";
                       break;
               }

               printf("State of node: %s value: %s\
",
                   all_nodenames->str_array[ix], str);
       }
}
.fi
.in -2

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.RS 36n
.rt  
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.RS 36n
.rt  
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scha_cluster_get(1HA)、scha_calls(3HA)、scha_cluster_getlogfacility(3HA)、scha_cluster_getnodename(3HA)、scha_strerror(3HA)、\fBsyslog\fR(3C)、\fBattributes\fR(5)
