'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_property_functions 3HA "2008 年 9 月 11 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_property_functions \- 一般に使用されるリソースプロパティー、リソースグループプロパティー、リソースタイププロパティー、および拡張プロパティーの値を取得するために使用できる関数群
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h> \fB\fIreturn-value-type scds-get-property-name\fR\fR(\fB scds_handle_t\fR \fB\fIhandle\fR\fR); 
.fi

.SH 機能説明
.sp
.LP
Data Service Development Library (DSDL) は、一般に使用されるリソースプロパティー、リソースグループプロパティー、リソースタイププロパティー、および拡張プロパティーの値を取得するために使用できる関数群を提供します。scds_get_ext_property(3HA) を使用してユーザー定義の拡張プロパティーを取得します。
.sp
.LP
すべての関数は、次の規則に従うものとします。
.RS +4
.TP
.ie t \(bu
.el o
\fIhandle\fR 引数だけを取ります。プロパティー取得関数に渡される \fIhandle\fR 引数は、scds_initialize(3HA) 呼び出しによって返されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
各関数が特定のプロパティーに対応します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
関数の戻り値のタイプは取得するプロパティー値のタイプに一致します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
これらの関数はエラーを返しません。これは、あらかじめ scds_initialize(3HA) によって戻り値が計算されるからです。ポインタを返す関数の場合、エラー条件が発生すると \fBNULL\fR 値が返されます。たとえば、以前に \fBscds_initialize()\fR が呼び出されていない場合などが該当します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
プロパティーの新しい値が、呼び出し元のプログラム(\fIargv[]\fR) に渡されるコマンド行引数で指定されていると、この新しい値が返されます(Validate メソッドの実装の場合)。この方法で、新しいプロパティーの予想値を実際に設定する前に検証できます。それ以外の場合は、RGM から取得した値が返されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
これらの関数の中には、DSDL に属するメモリーへのポインタを返すものもあります。このメモリーは変更できません。このポインタは、scds_close(3HA) 呼び出しによって無効化されます。
.RE
.sp
.LP
標準プロパティーについての詳細は、r_properties(5)、rg_properties(5)、および rt_properties(5) のマニュアルページを参照してください。拡張プロパティーについては、個々のデータサービスのマニュアルページを参照してください。
.sp
.LP
これらの関数によって使用されるデータ型については、scha_calls(3HA) のマニュアルページとヘッダーファイル \fBscha_types.h\fR を参照してください。\fBscha_prop_type_t\fR、\fBscha_extprop_value_t \fR、\fBscha_initnodes_flag_t\fR、\fBscha_str_array_t \fR、\fBscha_failover_mode_t\fR、\fBscha_switch_t \fR、\fBscha_rsstatus_t\fR.
.sp
.LP
これらの関数は、次の命名規則に従うものとします。
.sp
.ne 2
.mk
.na
\fBリソースプロパティー\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_\fR\fIproperty-name \fR
.RE

.sp
.ne 2
.mk
.na
\fBリソースグループプロパティー\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rg_\fR\fIproperty-name \fR
.RE

.sp
.ne 2
.mk
.na
\fBリソースタイププロパティー\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rt_\fR\fIproperty-name \fR
.RE

.sp
.ne 2
.mk
.na
\fB一般に使用される拡張プロパティー\fR
.ad
.sp .6
.RS 4n
\fBscds_get_ext_\fR\fIproperty-name \fR
.RE

.LP
注 - 
.sp
.RS 2
プロパティー名には、大文字と小文字の区別はありません。\fB\fRプロパティー名を指定する際には、大文字と小文字を任意に組み合わせることができます。
.RE
.SS "リソース固有の関数"
.sp
.LP
この関数は、特定のリソースプロパティーの値を返します。一部のプロパティー値は、RTR ファイル内に明示的に設定されるか、clresource(1CL) コマンドによって設定されます。その他のプロパティー値は、RGM によって動的に決定されます。関数は、要求されたプロパティーにふさわしいデータ型を返します。
.sp
.LP
次のリソースの依存関係のクエリー関数には、それぞれ対応する "Q" または "qualified" バージョンがあります。
.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies\fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies_offline_restart \fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q_offline_restart \fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies_restart\fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q_restart\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies_weak\fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q_weak\fR
.RE

.sp
.LP
資格を備えたバージョンは、各リソースの依存関係に宣言された有効範囲または修飾子 (ある場合) を返します。\fB{LOCAL_NODE} \fR、\fB{ANY_NODE}\fR、および \fB{FROM_RG_AFFINITIES} \fR 修飾子については、r_properties(5) のマニュアルページを参照してください。
.sp
.ne 2
.mk
.na
\fB\fBCheap_probe_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_cheap_probe_interval(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBFailover_mode\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_failover_mode_t scds_get_rs_failover_mode(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_stop_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_monitor_stop_timeout(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitored_switch\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_switch_t scds_get_rs_monitored_switch(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBNetwork_resources_used\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_str_array_t * scds_get_rs_network_resources_used(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBOn_off_switch\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_switch_t scds_get_rs_on_off_switch(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q\fR(qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_offline_restart\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_offline_restart(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q_offline_restart\fR (qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q_offline_restart(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_restart\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_restart(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q_restart\fR (qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q_restart(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_weak\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_weak(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q_weak\fR (qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q_weak(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_project_name\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rs_resource_project_name(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRetry_count\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_retry_count(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRetry_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_retry_interval(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBScalable\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean scds_get_rs_scalable(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStart_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_start_timeout(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStop_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_stop_timeout(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBThorough_probe_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_thorough_probe_interval(scds_handle_t handle)\fR
.RE

.SS "リソースグループ固有の関数"
.sp
.LP
この関数は、特定のリソースグループプロパティーの値を返します。一部のプロパティー値は、clresourcegroup(1CL) コマンドによって明示的に設定されます。その他のプロパティー値は、RGM によって動的に決定されます。関数は、要求されたプロパティーにふさわしいデータ型を返します。
.sp
.ne 2
.mk
.na
\fB\fBDesired_primaries\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_desired_primaries(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBGlobal_resources_used\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rg_global_resources_used(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBImplicit_network_dependencies\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean_t scds_get_rg_implicit_network_dependencies(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMaximum_primaries\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_maximum_primaries(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBNodelist\fR\fR
.ad
.sp .6
.RS 4n
\fB\fR\fBconst scha_str_array_t * scds_get_rg_nodelist (scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBPathprefix\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_pathprefix(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBPingpong_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_pingpong_interval(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_list\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rg_resource_list(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_affinities\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rg_rg_affinities(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_mode\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_rgmode_t scds_get_rg_rg_mode(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_project_name\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_rg_project_name(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_cpu_shares\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_rg_slm_cpu_shares(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_pset_min\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_rg_slm_pset_min(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_pset_type\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_rg_slm_pset_type(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_type\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_rg_slm_type(scds_handle_t handle)\fR
.RE

.SS "リソースタイプ固有の関数"
.sp
.LP
この関数は、特定のリソースタイププロパティーの値を返します。一部のプロパティー値は、RTR ファイル内に明示的に設定されるか、clresourcetype(1CL) コマンドによって設定されます。その他のプロパティー値は、RGM によって動的に決定されます。関数は、要求されたプロパティーにふさわしいデータ型を返します。
.sp
.ne 2
.mk
.na
\fB\fBAPI_version\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rt_api_version(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBFailover\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean_t scds_get_rt_failover(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBInit_nodes\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_initnodes_flag_t scds_get_rt_init_nodes(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBInstalled_nodes\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rt_installed_nodes(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_basedir\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_rt_basedir(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_version\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_rt_version(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBSingle_instance\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean_t scds_get_rt_single_instance(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStart_method\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_start_method(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStop_method\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_stop_method(scds_handle_t handle)\fR
.RE

.SS "拡張プロパティー固有の関数"
.sp
.LP
この関数は、特定のリソース拡張プロパティーの値を返します。このプロパティーの値は、RTR ファイル内に明示的に設定されるか、clresource(1CL) コマンドによって設定されます。関数は、要求されたプロパティーにふさわしいデータ型を返します。 
.sp
.LP
リソースタイプでは、ここに記載されている 4 つの拡張プロパティー以外の拡張プロパティーも定義できますが、この 4 つのプロパティーには有用な関数が定義されています。このようなプロパティーを取得するには、これらの有用な関数を使用するか、scds_get_ext_property(3HA) 関数を使用します。その他の拡張プロパティーを取得する場合は、\fBscds_get_ext_property()\fR を使用する必要があります。
.sp
.ne 2
.mk
.na
\fB\fBConfdir_list\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_str_array_t * scds_get_ext_confdir_list(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_retry_count\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_ext_monitor_retry_count(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_retry_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_ext_monitor_retry_interval(scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBProbe_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_ext_probe_timeout(scds_handle_t handle)\fR
.RE

.SH パラメータ
.sp
.LP
すべての有用な関数について、次のパラメータがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.SH 戻り値
.sp
.LP
関数の戻り値のタイプは取得するプロパティー値のタイプに一致します。
.sp
.LP
これらの関数はエラーを返しません。これは、あらかじめ scds_initialize(3HA) によって戻り値が計算されるからです。ポインタを返す関数の場合、エラー条件が発生すると \fBNULL\fR 値が返されます。たとえば、以前に \fBscds_initialize()\fR が呼び出されていない場合などが該当します。
.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
clresource(1CL)、clresourcegroup(1CL)、clresourcetype(1CL)、scds_close(3HA)、scds_get_ext_property(3HA)、scds_get_port_list(3HA)、scds_get_resource_group_name(3HA)、scds_get_resource_name(3HA)、scds_get_resource_type_name(3HA)、scds_initialize(3HA)、scha_calls(3HA)、\fBattributes\fR(5)、r_properties(5)、rg_properties(5)、rt_properties(5) 
