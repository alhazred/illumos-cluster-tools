'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_print_net_list 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_print_net_list \- ネットワーク資源リストの内容を印刷する
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBvoid\fR \fBscds_print_net_list\fR(\fBscds_handle_t \fR \fIhandle\fR, \fBint\fR \fIdebug_level\fR, \fBconstscds_net_resource_list_t *\fR\fInetresource_list\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_print_net_list()\fR 関数は、\fInetresource_list\fR で指定されたネットワーク資源リストの内容を、\fIdebug_level\fR で指定されたデバッグレベルでシステムログに書き込みます。指定されたデバッグレベルが現在使用されているデバッグレベルより大きい場合、情報は書き込まれません。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 28n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIdebug_level\fR\fR
.ad
.RS 28n
.rt  
データが書き込まれるデバッグレベルです。 
.RE

.sp
.ne 2
.mk
.na
\fB\fInetresource_list\fR\fR
.ad
.RS 28n
.rt  
初期化されたネットワーク資源リストへのポインタです。scds_get_rg_hostnames(3HA) または scds_get_rs_hostnames(3HA) によって取得されます。 
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_get_rg_hostnames(3HA)、scds_get_rs_hostnames(3HA)、scds_initialize(3HA)、scds_syslog_debug(3HA)、\fBattributes\fR(5)
