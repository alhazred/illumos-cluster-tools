'\" te
.\"  Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  clnasdevice   1CL  " 2008 年 9 月 11 日" " Sun Cluster 3.2 " " Sun Cluster 保守コマンド "
.SH 名前
clnasdevice, clnas \- Sun Cluster 向け NAS デバイスへのアクセスの管理
.SH 形式
.LP
.nf
\fB/usr/cluster/bin/clnasdevice\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice [\fIsubcommand\fR]\fR \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice \fIsubcommand\fR\fR [\fIoptions\fR] \fB-v\fR [\fInasdevice\fR[ \&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice add\fR \fB-t\fR \fItype\fR {\fB-p\fR\fIname\fR=\fIvalue\fR[,\&.\|.\|.] | \fB-u \fR\fIuserid\fR} [\fB-f\fR \fIpasswdfile\fR] \fInasdevice\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice add\fR \fB-i\fR {- | \fIclconfigfile\fR} [\fB-t\fR \fItype\fR] [\fB-p\fR\fIname\fR=\fIvalue\fR | \fB-u \fR\fIuserid\fR] {\fB-f\fR \fIpasswdfile\fR} {+ | \fInasdevice\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice add-dir\fR \fB-d\fR \fIdirectory\fR[,\&.\|.\|.] \fInasdevice\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice add-dir\fR \fB-i\fR {- | \fIclconfigfile\fR} [\fB-d\fR all | \fIdirectory\fR[,\&.\|.\|.]] [\fB-f\fR \fIpasswordfile\fR] {+ | \fInasdevice\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice export\fR [\fB-o\fR {- | \fIclconfigfile\fR}] [\fB-t\fR \fItype\fR[,\&.\|.\|.]] [\fB-d\fR all | \fIdirectory\fR[,\&.\|.\|.]] [+ | \fInasdevice\fR[ \&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice list\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] [+ | \fInasdevice\fR[ \&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice remove\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] [\fB-F \fR] {+ | \fInasdevice\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice remove-dir\fR \fB-d\fR all | \fIdirectory\fR[,\&.\|.\|.] \fInasdevice\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice set\fR {\fB-p\fR\fIname\fR=\fIvalue\fR[,\&.\|.\|.] | \fB-u \fR\fIuserid\fR} [\fB-f\fR \fIpasswdfile\fR] \fInasdevice\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clnasdevice show\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] [+ | \fInasdevice\fR[ \&.\|.\|.]]
.fi

.SH 機能説明
.sp
.LP
\fBclnasdevice\fR コマンドは、NAS デバイスとそのディレクトリについての Sun Cluster 構成情報を管理します。
.sp
.LP
\fBclnas\fR コマンドは、\fBclnasdevice\fR コマンドの短縮形式です。\fBclnas\fR コマンドと \fBclnasdevice\fR コマンドは同じです。どちらの形式のコマンドも使用できます。
.sp
.LP
このコマンドの書式は次のとおりです。
.sp
.LP
\fBclnasdevice\fR [\fIsubcommand\fR] [\fIoptions\fR] [\fIoperands\fR]
.sp
.LP
\fIoptions\fR に \fB-?\fR または \fB-V\fR オプションを指定する場合だけは、\fIsubcommand\fR を省略できます。
.sp
.LP
このコマンドの各オプションには、長い形式と短い形式があります。各オプションの両方の形式は、このマニュアルページの「オプション」セクションのオプションの説明で紹介されています。
.sp
.LP
\fBclnasdevice\fR コマンドを使用して NAS デバイスをクラスタに構成する前に、NAS デバイスが次の状態であることを確認してください。
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスが設定されており、機能している。
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスが起動され、動作している。
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスのディレクトリが作成され、クラスタノードに利用可能である。
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスが定足数デバイスになる予定である場合、その定足数デバイスの LUN が作成されている必要があります。NAS 定足数デバイスの構成については、\fBclquorum\fR.1cl のマニュアルページを参照してください。
.RE
.sp
.LP
NAS デバイスのベンダーによっては、デバイスをクラスタに構成する前に、追加の作業を行う必要がある場合もあります。これらの作業についての詳細は、「オプション」セクションの \fB-t\fR オプションを参照してください。NAS デバイスの設定手順やそのディレクトリのエクスポート手順については、NAS デバイスのマニュアルを参照してください。
.sp
.LP
NAS デバイスが完全に機能し、クラスタにストレージを提供する準備ができたあとは、\fBclnasdevice\fR コマンドを使用して、クラスタ内の NAS デバイス構成情報を管理します。これを行わないと、クラスタは NAS デバイスとそのエクスポートされるディレクトリを検出できません。結果として、クラスタはこれらのディレクトリにある情報の整合性を保護できません。
.sp
.LP
\fBclnasdevice\fR コマンドを使用して、次のような管理作業を行います。
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイス構成の作成
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS タイプ固有プロパティーの更新
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスディレクトリのクラスタ構成からの削除
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスのクラスタ構成からの削除
.RE
.sp
.LP
\fBclnasdevice\fR コマンドは、アクティブなクラスタノードだけで実行できます。コマンドを実行して得られる結果は、実行するノードに関係なく、常に同じです。このコマンドは、大域ゾーンだけで使用できます。
.SH サブコマンド
.sp
.LP
サポートされるサブコマンドには次のものがあります。
.sp
.ne 2
.mk
.na
\fB\fBadd\fR\fR
.ad
.sp .6
.RS 4n
Sun Cluster 構成に NAS デバイスを追加します。
.sp
\fB-t\fR オプションは、NAS デバイスのベンダーを指定するのに使用します。詳細は、「オプション」セクションの \fB-t\fR オプションの説明を参照してください。
.sp
NAS デバイスのタイプによって、追加のプロパティーを設定する必要がある場合があります。このような追加のプロパティーについても、「オプション」セクションの \fB-t\fR オプションの説明を参照してください。
.sp
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.modify\fR 役割に基づくアクセス制御 (RBAC) の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBremove\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBadd-dir\fR\fR
.ad
.sp .6
.RS 4n
クラスタ構成に、すでに構成されている NAS デバイスの指定ディレクトリを追加します。このサブコマンドを使用する前に、デバイス上でこれらのディレクトリを作成し、クラスタで使用できるようにしておく必要があります。ディレクトリの作成については、NAS デバイスのマニュアルを参照してください。
.sp
NAS デバイスのディレクトリを追加する方法は、次の 2 つのうちのいずれかです。
.RS +4
.TP
.ie t \(bu
.el o
\fBclnasdevice add\fR コマンドを使用して、NAS デバイスをクラスタに構成します。\fBclnasdevice add-dir\fR コマンドを使用して、そのデバイスのディレクトリをクラスタに構成します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBclnasdevice add-dir\fR \fB-i\fR \fIconfigurationfile\fR 形式のコマンドを使用して、単一の手順で、デバイスを追加し、そのディレクトリを構成します。この方法でディレクトリを追加するには、\fB-f\fR オプションを使用してパスワードファイルを指定します。このオプションについての詳細は、「オプション」セクションを参照してください。clconfiguration(5CL)ファイルの形式については、このファイルのマニュアルページを参照してください。
.RE
新しいディレクトリを NAS デバイスで作成して、そのディレクトリをクラスタノードに利用可能にするには、この \fBadd-dir\fR サブコマンドを使用して、これらのディレクトリをクラスタ構成に追加してください。
.sp
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBremove-dir\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBexport\fR\fR
.ad
.sp .6
.RS 4n
クラスタ NAS デバイス構成情報をエクスポートします。\fB-o\fR オプションでファイルを指定すると、そのファイルに構成情報が書き込まれます。\fB-o\fR オプションを使用しない場合、出力は標準出力 (\fBstdout\fR) に書き込まれます。
.sp
\fBexport\fR サブコマンドはクラスタ構成情報を変更しません。
.sp
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
クラスタに構成されている NAS デバイス構成を表示します。
.sp
クラスタに構成されているデバイスのディレクトリとデバイスタイプを表示するには、詳細オプション \fB-v\fR を使用します。
.sp
特定のタイプの NAS デバイスを表示するには、\fB-t\fR オプションを使用します。
.sp
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBremove\fR\fR
.ad
.sp .6
.RS 4n
指定されている単数または複数の NAS デバイスを、Sun Cluster 構成から削除します。
.sp
強制オプション \fB-F\fR を指定しない場合、\fBremove-dir\fR サブコマンドを使用して、NAS デバイスディレクトリを構成から削除しておいてください。
.sp
強制オプション \fB-F\fR を指定する場合、このコマンドは、NAS デバイスとそのディレクトリをクラスタ構成から削除します。「\fIオプション\fR」セクションの \fB-F\fR の説明を参照してください。
.sp
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBadd\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBremove-dir\fR\fR
.ad
.sp .6
.RS 4n
指定されている単数または複数の NAS ディレクトリを Sun Cluster 構成から削除します。
.sp
\fBremove-dir\fR サブコマンドは、\fB-d\fR オプションで指定されたエクスポートされるディレクトリを削除します。\fB-d\fR \fBall\fR を使用するとき、このサブコマンドは、指定された NAS デバイスのすべてのディレクトリを削除します。
.sp
ディレクトリを NAS デバイスから削除したあとは、この \fBremove-dir\fR サブコマンドを使用して、そのディレクトリをクラスタ構成から削除してください。クラスタ構成内の NAS ディレクトリは、NAS デバイスからエクスポートされる既存のディレクトリと一致する必要があります。
.sp
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBadd-dir\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBset\fR\fR
.ad
.sp .6
.RS 4n
特定の NAS デバイスの指定されたプロパティーを設定します。
.LP
注 - 
.sp
.RS 2
Sun Microsystems, Inc. の NAS デバイスにプロパティーを指定しないでください。このデバイスは、どのようなプロパティーも持たないため、\fBset\fR サブコマンド、および \fB-f\fR、\fB-p\fR、\fB-u\fR オプションは適用されません。\fB\fR\fB\fR
.RE
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
オプションが何も指定されていない場合は、次の情報を表示します。 
.RS +4
.TP
.ie t \(bu
.el o
Sun Cluster に構成されている現在のすべての NAS デバイスのリスト
.RE
.RS +4
.TP
.ie t \(bu
.el o
各 NAS デバイスの利用可能なディレクトリ
.RE
.RS +4
.TP
.ie t \(bu
.el o
各 NAS デバイスに関連付けられたすべてのプロパティー
.RE
特定のタイプの NAS デバイスを表示するには、\fB-t\fR オプションを指定します。特定のデバイスに関する情報を表示するには、NAS デバイスのホスト名をオペランドとしてコマンドに渡します。
.sp
スーパーユーザー以外のユーザーがこのコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB-\fB-help\fR\fR
.ad
.sp .6
.RS 4n
ヘルプ情報を表示します。このオプションを使用するとき、ほかのオプションの処理は実行されません。
.sp
このオプションはサブコマンド付きでもサブコマンドなしでも指定できます。
.sp
サブコマンドなしでこのオプションを指定すると、このコマンドのサブコマンドのリストが表示されます。
.sp
サブコマンド付きでこのオプションを指定すると、サブコマンドの使用方法が表示されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-d\fR \fIdirectory\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB-\fB-directory=\fR\fIdirectory\fR\fB-[,...]\fR\fR
.ad
.br
.na
\fB-\fB-directory \fR\fIdirectory\fR\fB-[,...]\fR\fR
.ad
.sp .6
.RS 4n
NAS デバイスディレクトリ (1 つまたは複数) を指定します。このオプションを一緒に指定できるのは、\fBadd-dir\fR、\fBremove-dir\fR、および \fBexport\fR サブコマンドだけです。
.sp
このオプションは、特別なキーワード \fBall\fR を受け入れます。\fB-d\fR \fBall\fR オプションを使用すると、指定した NAS デバイス上のすべてのディレクトリが指定されます。
.RS +4
.TP
.ie t \(bu
.el o
\fBremove-dir\fR サブコマンドと一緒に使用する場合、指定したデバイス上のすべてのディレクトリが削除されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBexport\fR サブコマンドと一緒に使用する場合、指定したデバイス上のすべてのディレクトリの構成情報が、指定した出力に表示されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBadd-dir\fR サブコマンドと \fB-i\fR \fIconfigfile\fR オプションを使用すると、構成ファイルに一覧表示されている指定された NAS デバイス上のすべてのディレクトリが、追加されます。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB-F\fR\fR
.ad
.br
.na
\fB-\fB-force\fR\fR
.ad
.sp .6
.RS 4n
指定した NAS デバイスを強制的に削除します。
.sp
強制オプションを一緒に指定できるのは、\fBremove\fR サブコマンドだけです。この強制オプションを \fBremove\fR サブコマンドと一緒に使用すると、NAS デバイスとその構成されているディレクトリが Sun Cluster 構成から削除されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-f\fR \fIpasswd-file\fR\fR
.ad
.br
.na
\fB-\fB-passwdfile=\fR\fIpasswd-file\fR\fR
.ad
.br
.na
\fB-\fB-passwdfile \fR\fIpasswd-file\fR\fR
.ad
.sp .6
.RS 4n
NAS デバイスにログインするときに使用するパスワードが格納されているパスワードファイルを指定します。
.LP
注 - 
.sp
.RS 2
Sun Microsystems, Inc. の NAS デバイスにプロパティーを指定しないでください。このデバイスは、どのようなプロパティーも持たないため、\fBset\fR サブコマンド、および \fB-f\fR、\fB-p\fR、\fB-u\fR オプションは適用されません。\fB\fR\fB\fR
.RE
セキュリティー上の理由により、パスワードはコマンド行オプションには指定できません。パスワードのセキュリティーを強化するには、パスワードをテキストファイルに格納し、\fB-f\fR オプションでこのファイルを指定してください。パスワード用の入力ファイルを指定しないと、パスワードの入力が求められます。
.sp
入力ファイルの読み取り可能アクセス権を root だけに与え、このファイルにグループからも一般ユーザーからもアクセスできないようにします。
.sp
\fBclnasdevice add\fR を \fB-i\fR オプションと一緒に使用するとき、\fIclconfigfile\fR にパスワードが格納されていない場合、\fB-f\fR \fIpasswdfile\fR オプションを必ず指定してください。
.sp
入力ファイルでは、次の制限を守ってください。
.RS +4
.TP
.ie t \(bu
.el o
パスワードは単一行に指定します。パスワードを複数行にまたがって入力してはいけません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
行頭にあるブランクやタブは無視されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
コメントは引用符で囲まれていない # 記号で始まります。コメントは次の新しい行に続きます。
.sp
すべてのコメントはパーサーによって無視されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
デバイスユーザーのパスワードに入力ファイルを使用する場合は、# 記号をパスワードの一部に使用できません。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB-i\fR \fIclconfigfile\fR\fR
.ad
.br
.na
\fB-\fB-input={- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.br
.na
\fB-\fB-input {- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
NAS デバイスを作成または変更するのに使用される構成情報を指定します。この情報は clconfiguration(5CL) のマニュアルページに定義されている書式に準拠させてください。この情報はファイルに含まれている場合と、標準入力 \fBstdin\fR を通して指定される場合があります。標準入力を指定するには、ファイル名の代わりに \fB-\fR を指定します。
.sp
コマンド行と \fIclconfigfile\fR ファイルで同じプロパティーを指定する場合、コマンド行で設定したプロパティーが優先されます。
.sp
\fBclnasdevice add\fR を \fB-i\fR オプションと一緒に使用するとき、\fB-f\fR \fIpasswdfile\fR オプションを必ず指定してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o\fR {- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB-\fB-output={- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.br
.na
\fB-\fB-output {- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
clconfiguration(5CL) のマニュアルページで定義されている形式で、NAS デバイス構成情報を記述します。この情報は、ファイルまたは標準出力 (\fBstdout\fR) のどちらにでも書き込むことができます。標準出力を指定するには、ファイル名の代わりに \fB-\fR を指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR \fIname\fR=\fIvalue\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB-\fB-property=\fR\fIname\fR\fB-=\fR\fIvalue\fR\fB-[,...]\fR\fR
.ad
.br
.na
\fB-\fB-property \fR\fIname\fR\fB- \fR\fIvalue\fR\fB-[,...]\fR\fR
.ad
.sp .6
.RS 4n
ある NAS デバイスタイプに固有のプロパティーを指定します。
.LP
注 - 
.sp
.RS 2
Sun Microsystems, Inc. の NAS デバイスにプロパティーを指定しないでください。このデバイスは、どのようなプロパティーも持たないため、\fBset\fR サブコマンド、および \fB-f\fR、\fB-p\fR、\fB-u\fR オプションは適用されません。\fB\fR\fB\fR
.RE
\fBadd\fR サブコマンドを使用して新しい NAS デバイスをクラスタ構成に追加する場合は、このオプションを指定する必要があります。\fBset\fR サブコマンドで NAS デバイスのプロパティーを変更する場合も、このオプションを指定する必要があります。詳細は、\fB-t\fR オプションの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-t\fR \fInas-device-type\fR\fR
.ad
.br
.na
\fB-\fB-type=\fR\fInas-device-type\fR\fR
.ad
.br
.na
\fB-\fB-type \fR\fInas-device-type\fR\fR
.ad
.sp .6
.RS 4n
NAS デバイスタイプを指定します。このオプションは、NAS デバイスを Sun Cluster 構成に追加するときに指定してください。NAS デバイスタイプは、ベンダー名で指定します。たとえば、Sun Microsystems, Inc. の NAS デバイスタイプは、\fBsun\fR です。
.sp
異なるタイプの NAS デバイスには異なるプロパティーがあるか、またはプロパティーがまったくない場合もあります。
.sp
.ne 2
.mk
.na
\fB\fBsun\fR\fR
.ad
.RS 28n
.rt  
Sun の NAS デバイスを指定します。
.LP
注 - 
.sp
.RS 2
Sun Microsystems, Inc. の NAS デバイスにプロパティーを指定しないでください。このデバイスは、どのようなプロパティーも持たないため、\fBset\fR サブコマンド、および \fB-f\fR、\fB-p\fR、\fB-u\fR オプションは適用されません。\fB\fR\fB\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBnetapp\fR\fR
.ad
.RS 28n
.rt  
Network Appliance 社の NAS デバイスを指定します。Network Appliance 社の NAS デバイスには、次のプロパティーがあります。このプロパティーは、\fBadd\fR サブコマンドを使用して NAS デバイスを追加する場合に必要です。
.sp
\fB-p\fR userid=\fIuserid\fR [\fB-f\fR \fIpasswdfile\fR]
.sp
または
.sp
\fB-u\fR \fIuserid\fR [\fB-f\fR \fIpasswdfile\fR]
.sp
\fBuserid\fR は、クラスタが NAS デバイスで管理作業を実行するときに使用するユーザー ID です。userid をデバイス構成に追加するときには、対応するパスワードを入力するように求められます。パスワードは、テキストファイルに格納しておき、\fB-f\fR オプションで指定してもかまいません。
.RE

NAS デバイスとそのエクスポートされたディレクトリをクラスタ構成に追加する前に、次に示す手順を実行しておく必要があります。
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスを設定します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
ディレクトリを設定し、クラスタノードに利用可能にします。
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS デバイスで管理作業を実行するときに使用するユーザー ID とパスワードを決定します。
.RE
また、NAS デバイスは起動され、動作している必要があります。\fBnetapp\fR NAS デバイスのサポートをクラスタに提供するには、さらに、Network Appliance 社が提供する必要なソフトウェアモジュールをインストールしてください。また、Network Appliance 社の NAS デバイス用に iSCSI ライセンスが有効である必要もあります。サポートモジュールを入手する方法については、『\fISun Cluster With Network-Attached Storage Devices Manual for Solaris OS\fR』を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-u\fR \fIuserid\fR\fR
.ad
.br
.na
\fB-\fB-userid=\fR\fIuserid\fR\fR
.ad
.br
.na
\fB-\fB-userid \fR\fIuserid\fR\fR
.ad
.sp .6
.RS 4n
NAS デバイスにログインするのに使用するユーザー ID を指定します。
.LP
注 - 
.sp
.RS 2
Sun Microsystems, Inc. の NAS デバイスにプロパティーを指定しないでください。このデバイスは、どのようなプロパティーも持たないため、\fBset\fR サブコマンド、および \fB-f\fR、\fB-p\fR、\fB-u\fR オプションは適用されません。\fB\fR\fB\fR
.RE
クラスタは、ログインし、デバイス上で管理処理を実行するために、ユーザー ID を把握する必要があります。
.sp
あるいは、\fB-p\fR オプションでもユーザー ID を指定できます。詳細は、\fB-p\fR オプションを参照してください。
.sp
このオプションを一緒に指定できるのは、\fBadd\fR および \fBset\fR サブコマンドだけです。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB-\fB-version\fR\fR
.ad
.sp .6
.RS 4n
コマンドのバージョンを表示します。
.sp
このオプションには、サブコマンドやオペランドなどのオプションは指定しないでください。サブコマンドやオペランドなどのオプションは無視されます。コマンドのバージョンが表示されます。ほかの処理は実行されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB-verbose\fR\fR
.ad
.sp .6
.RS 4n
詳細な情報を標準出力 \fBstdout\fR に表示します。
.RE

.SH オペランド
.sp
.LP
次のオペランドがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fInasdevice\fR\fR
.ad
.sp .6
.RS 4n
 NAS デバイスの名前。NAS デバイス名は、ネットワークで通信を行うために NAS デバイスが使用するホスト名です。NAS デバイスと通信するために、クラスタは NAS デバイスの NAS ホスト名を知っておく必要があります。サブコマンドが複数の NAS デバイスを受け入れる場合は、プラス記号 + を使用して、すべての NAS デバイスを指定できます。\fBadd\fR および \fBadd-dir\fR サブコマンドの場合、プラス記号オペランドは、指定された構成ファイルにあるすべての NAS デバイスを指定します。
.RE

.SH 終了ステータス
.sp
.LP
指定したすべてのオペランドでコマンドが成功すると、コマンドはゼロ (\fBCL_NOERR\fR) を返します。あるオペランドでエラーが発生すると、コマンドはオペランドリストの次のオペランドを処理します。戻り値は常に、最初に発生したエラーを反映します。
.sp
.LP
次の終了値が返される可能性があります。
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
エラーなし
.sp
実行したコマンドは正常に終了しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
十分なスワップ空間がありません。
.sp
クラスタノードがスワップメモリーまたはその他のオペレーティングシステムリソースを使い果たしました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
無効な引数
.sp
コマンドを間違って入力したか、\fB-i\fR オプションで指定したクラスタ構成情報の構文が間違っていました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
アクセス権がありません
.sp
指定したオブジェクトにアクセスできません。このコマンドを実行するには、スーパーユーザーまたは RBAC アクセスが必要である可能性があります。詳細は、\fBsu\fR(1M) および \fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB15\fR \fBCL_EPROP\fR\fR
.ad
.sp .6
.RS 4n
無効なプロパティーです。
.sp
\fB-p\fR、\fB-y\fR、または \fB-x\fR オプションで指定したプロパティーまたは値が存在しないか、許可されていません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR \fBCL_EINTERNAL\fR\fR
.ad
.sp .6
.RS 4n
内部エラーが発生しました。
.sp
内部エラーは、ソフトウェアの欠陥またはその他の欠陥を示しています。
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
そのようなオブジェクトはありません。
.sp
次のいずれかの理由のために、指定したオブジェクトを見つけることができません。
.RS +4
.TP
.ie t \(bu
.el o
オブジェクトが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-o\fR オプションで作成しようとした構成ファイルへのパスのディレクトリが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-i\fR オプションでアクセスしようとした構成ファイルにエラーが含まれています。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB39\fR \fBCL_EEXIST\fR\fR
.ad
.sp .6
.RS 4n
オブジェクトは存在します。
.sp
指定したデバイス、デバイスグループ、クラスタインターコネクトコンポーネント、ノード、クラスタ、リソース、リソースタイプ、またはリソースグループはすでに存在します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB41\fR \fBCL_ETYPE\fR\fR
.ad
.sp .6
.RS 4n
無効なタイプです。
.sp
\fB-t\fR または \fB-p\fR オプションで指定したタイプは存在しません。
.RE

.SH 使用例
.LP
\fB例 1 \fRSun Microsystems, Inc. の NAS デバイスのクラスタへの追加
.sp
.LP
次の \fBclnasdevice\fR コマンドは、Sun Microsystems, Inc. のストレージシステム \fBsunnas1\fR を Sun Cluster 構成に追加します。

.sp
.in +2
.nf
# \fBclnasdevice add -t sun sunnas1\fR
.fi
.in -2
.sp

.LP
\fB例 2 \fRNetwork Appliance 社の NAS デバイスのクラスタへの追加
.sp
.LP
次の \fBclnasdevice\fR コマンドは、Network Appliance 社のストレージシステム \fBnetapp1\fR を Sun Cluster 構成に追加します。

.sp
.in +2
.nf
# \fBclnasdevice add -t netapp -p userid=root netapp1\fR
Please enter password
.fi
.in -2
.sp

.LP
\fB例 3 \fRいくつかの NAS デバイスディレクトリのクラスタへの追加
.sp
.LP
次の \fBclnasdevice\fR コマンドは、すでに構成されている NAS デバイス \fBsunnas1\fR のエクスポートされた 2 つのディレクトリをクラスタ構成に追加します。

.sp
.in +2
.nf
# \fBclnasdevice add-dir -d /export/dir1,/export/dir2 sunnas1\fR
.fi
.in -2
.sp

.LP
\fB例 4 \fRすべての NAS デバイスディレクトリのクラスタ構成からの削除
.sp
.LP
次の \fBclnasdevice\fR コマンドは、NAS デバイス \fBnetapp1\fR に属しているすべてのディレクトリをクラスタ構成から削除します。

.sp
.in +2
.nf
# \fBclnasdevice remove-dir -d all netapp1\fR
.fi
.in -2
.sp

.LP
\fB例 5 \fRNAS デバイスのクラスタからの削除
.sp
.LP
次の \fBclnasdevice\fR コマンドは、NAS デバイス \fBsunnas1\fR と、残りのディレクトリがある場合はそのディレクトリをすべて、Sun Cluster 構成から削除します。

.sp
.in +2
.nf
# \fBclnasdevice remove -F sunnas1\fR
.fi
.in -2
.sp

.LP
\fB例 6 \fRクラスタに構成されている NAS デバイスの表示
.sp
.LP
次の \fBclnasdevice\fR コマンドは、クラスタに構成されているすべての NAS デバイスの名前を表示します。デバイスとそのディレクトリのリストを表示するには、詳細オプションか \fBshow\fR サブコマンドを使用します。

.sp
.in +2
.nf
# \fBclnasdevice list\fR
sunnas1
.fi
.in -2
.sp

.LP
\fB例 7 \fRNAS デバイスとそのディレクトリの表示
.sp
.LP
次の \fBclnasdevice\fR コマンドは、クラスタに構成されているすべての NAS デバイスの名前に加えて、そのディレクトリのうち、クラスタに構成されているディレクトリを表示します。

.sp
.in +2
.nf
# \fBclnasdevice show -v\fR
Nas Device:    sunnas1.sfbay.sun.com
Type:          sun
Userid:        root
Directory:     /export/dir1
              /export/dir2

Nas Device:    netapp2
Type:          netapp
Userid:        root
Directory:     /export/dir1
              /export/dir2
.fi
.in -2
.sp

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWsczu
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
Intro(1CL)、cluster(1CL)
.SH 注意事項
.sp
.LP
スーパーユーザーはこのコマンドのすべての形式を実行できます。
.sp
.LP
任意のユーザーは次のサブコマンドとオプションを指定してこのコマンドを実行できます。
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR オプション
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR オプション
.RE
.sp
.LP
スーパーユーザー以外のユーザーがほかのサブコマンドを指定してこのコマンドを実行するには、RBAC の承認が必要です。次の表を参照してください。
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
サブコマンドRBAC の承認
_
\fBadd\fR\fBsolaris.cluster.modify\fR
_
\fBadd-dir\fR\fBsolaris.cluster.modify\fR
_
\fBexport\fR\fBsolaris.cluster.read\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBset\fR\fBsolaris.cluster.modify\fR
_
\fBremove\fR\fBsolaris.cluster.modify\fR
_
\fBremove-dir\fR\fBsolaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
.TE

