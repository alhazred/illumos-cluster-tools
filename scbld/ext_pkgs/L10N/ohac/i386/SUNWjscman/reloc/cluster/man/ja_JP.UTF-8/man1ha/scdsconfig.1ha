'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scdsconfig 1HA "2008 年 9 月 11 日" "Sun Cluster 3.2" "Sun Cluster コマンド"
.SH 名前
scdsconfig \- リソースタイプテンプレートを構成する
.SH 形式
.LP
.nf
\fBscdsconfig\fR \fB-s\fR \fI start-command\fR [\fB-u\fR \fIstart-method-timeout \fR] [\fB-e\fR \fIvalidate-command\fR] [\fB -y\fR \fIvalidate-method-timeout\fR] [\fB-t\fR \fIstop-command\fR] [\fB -v\fR \fIstop-method-timeout\fR] [\fB-m \fR \fIprobe-command\fR] [\fB-n\fR \fIprobe-timeout\fR] [\fB -d\fR \fIworking-directory\fR]
.fi

.SH 機能説明
.sp
.LP
\fBscdsconfig\fR コマンドは、scdscreate(1HA) コマンドで作成したリソースタイプテンプレートを構成します。\fBscdsconfig\fR コマンドを使用すると、ネットワーク対応(クライアントサーバーモデル) アプリケーションと非ネットワーク対応(クライアントを持たない) アプリケーションの両方に対して、C、GDS(Generic Data Service)、または Korn シェルベースのテンプレートを作成できます。
.sp
.LP
\fBscdsconfig\fR コマンドは、アプリケーションを起動、停止、検証、およびプローブするアプリケーション固有のコマンドを構成します。\fBscdsconfig\fR コマンドを使用して、\fBstart\fR、\fBstop\fR、 \fBvalidate\fR、および \fBprobe\fR コマンドのタイムアウト値を設定することもできます。\fBscdsconfig\fR コマンドは、ネットワーク対応(クライアントサーバーモデル) のアプリケーションと非ネットワーク対応(クライアントレス) のアプリケーションを両方ともサポートします。ユーザーは \fBscdscreate\fR コマンドが実行されたディレクトリから \fBscdsconfig\fR コマンドを実行できます。\fB-d\fR オプションを使用すると、\fBscdscreate\fR を実行したディレクトリを指定できます。\fBscdsconfig\fR コマンドは、生成されたコード内の正しい場所にユーザー指定のパラメータを挿入することにより、リソースタイプテンプレートを構成します。生成されたソースコードの種類が C の場合、このコマンドはさらに、そのソースコードをコンパイルします。\fBscdsconfig\fR コマンドは、出力をインストール可能な Solaris パッケージにします。このコマンドは、\fBscdscreate\fR コマンドによって作成された $\fIvendor-id \fR$\fIresource-type-name\fRディレクトリ下の \fBpkg\fR サブディレクトリにパッケージを作成します。
.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-d\fR \fIworking-directory\fR\fR
.ad
.RS 28n
.rt  
\fBscdscreate\fR コマンドが実行されたディレクトリを指定します。
.sp
\fBscdscreate\fR コマンドが実行されたディレクトリと異なるディレクトリから \fBscdsconfig\fR を実行する場合に、このオプションを指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-e\fR \fIvalidate-command\fR\fR
.ad
.RS 28n
.rt  
アプリケーションを検証するために呼び出すコマンドの絶対パスを指定します。絶対パスを指定しない場合、アプリケーションは検証されません。アプリケーションの実行が成功した場合、\fIvalidate-command\fR は \fB0\fR の終了ステータスを返します。\fB0\fR 以外の終了ステータスは、アプリケーションの実行が失敗したことを示します。この場合、過去のアプリケーションの障害履歴に応じて、次の 2 つの結果のいずれかが生じます。
.RS +4
.TP
.ie t \(bu
.el o
このリソースタイプのリソースは、同じモードまたはゾーンのいずれかから再起動される。
.RE
.RS +4
.TP
.ie t \(bu
.el o
このリソースを含むリソースグループは、別の健全なノードまたはゾーンにフェイルオーバーされる。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB-m\fR \fIprobe-command\fR\fR
.ad
.RS 28n
.rt  
ネットワーク対応アプリケーションまたは非ネットワーク対応アプリケーションの健全性を定期的にチェックするためのコマンドを指定します。直接シェルに渡すことができる完全なコマンドを指定してください。アプリケーションの実行が成功した場合、\fIprobe-command\fR は \fB0\fR の終了ステータスを返します。\fB0\fR 以外の終了ステータスは、アプリケーションの実行が失敗したことを示します。この場合、過去のアプリケーションの障害履歴に応じて、次の 2 つの結果のいずれかが生じます。
.RS +4
.TP
.ie t \(bu
.el o
このリソースタイプのリソースは、同じモードまたはゾーンのいずれかから再起動される。
.RE
.RS +4
.TP
.ie t \(bu
.el o
このリソースを含むリソースグループは、別の健全なノードまたはゾーンにフェイルオーバーされる。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fIprobe-timeout\fR\fR
.ad
.RS 28n
.rt  
プローブコマンドのタイムアウトを秒単位で指定します。障害の検出ミスを防ぐため、タイムアウト値は、システムの過負荷を考慮に入れて指定する必要があります。デフォルト値は 30 秒です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-s\fR \fIstart-command\fR\fR
.ad
.RS 28n
.rt  
アプリケーションを起動するコマンドを指定します。start コマンドには、直接シェルに渡すことができる完全なコマンドを指定してください。コマンド行引数を使って、ホスト名、ポート番号など、アプリケーションの起動に必要な各種構成データを指定できます。複数の独立したプロセスツリーを持つリソースタイプを作成する場合は、1 行に 1 個ずつコマンドを記述したテキストファイルを指定し、複数の異なったプロセスツリーを起動します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-t\fR \fIstop-command\fR\fR
.ad
.RS 28n
.rt  
アプリケーションの停止コマンドを指定します。stop コマンドには、直接シェルに渡すことができる完全なコマンドを指定してください。このオプションを省略した場合、アプリケーションは、生成されたコードからのシグナルを発行することによって停止します。停止コマンドには、タイムアウト値の 80% が割り当てられます。この時間内に停止コマンドがアプリケーションを停止できない場合、\fBSIGKILL\fR には、タイムアウト値の 15% が割り当てられます。それでもアプリケーションを停止できない場合、停止メソッドはエラーを返して終了します。\fB\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fB-u\fR \fIstart-method-timeout\fR\fR
.ad
.RS 28n
.rt  
起動コマンドのタイムアウトを秒単位で指定します。障害の検出ミスを防ぐため、タイムアウト値は、システムの過負荷を考慮に入れて指定する必要があります。デフォルト値は 300 秒です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR \fIstop-method-timeout\fR\fR
.ad
.RS 28n
.rt  
停止コマンドのタイムアウトを秒単位で指定します。障害の検出ミスを防ぐため、タイムアウト値は、システムの過負荷を考慮に入れて指定する必要があります。デフォルト値は 300 秒です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-y\fR \fIvalidate-method-timeout \fR\fR
.ad
.RS 28n
.rt  
起動コマンドのタイムアウトを秒単位で指定します。障害の検出ミスを防ぐため、タイムアウト値は、システムの過負荷を考慮に入れて指定する必要があります。デフォルト値は 300 秒です。
.RE

.SH 終了ステータス
.sp
.LP
次の終了ステータスコードが返されます。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
コマンドは正常に完了しました。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
エラーが発生しました。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fIworking-directory\fR\fB /rtconfig\fR\fR
.ad
.RS 36n
.rt  
前のセッションの情報が格納されているため、ツールの終了と再起動をスムーズに行うことが可能です。
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
\fBksh\fR(1)、scdsbuilder(1HA)、scdscreate(1HA)、\fBattributes\fR(5)
