'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH clsetup 1CL "2008 年 4 月 4 日" "Sun Cluster 3.2" "Sun Cluster 保守コマンド"
.SH 名前
clsetup \- Sun Cluster の対話式構成
.SH 形式
.LP
.nf
\fB/usr/cluster/bin/clsetup\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clsetup\fR \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clsetup\fR [\fB-f\fR \fI logfile\fR]
.fi

.SH 機能説明
.sp
.LP
\fBclsetup\fR コマンドは、実行するときのクラスタの状態によって、次に示す構成機能を提供します。このコマンドを実行するには、\fBsuperuser\fR であることが必要です。
.sp
.LP
このコマンドに短形式はありません。
.RS +4
.TP
.ie t \(bu
.el o
\fBclsetup\fR コマンドをインストールのあと実行すると、コマンドは定足数デバイスの設定や \fBinstallmode\fR プロパティーのリセットなどの初期セットアップ作業を実行します。\fBscinstall\fR または \fBcluster create\fR コマンドのいずれかを使用してクラスタを作成した場合に自動定足数構成の選択を解除するときは、クラスタがインストールされた直後に \fBclsetup\fR コマンドを実行します。すべてのノードがクラスタに参加していることを確認してから、\fBclsetup\fR コマンドを実行し、\fBinstallmode\fR プロパティーをリセットします。
.sp
クラスタを作成したときに自動定足数構成を使用した場合は、クラスタのインストール後に \fBclsetup\fR コマンドを実行する必要はありません。自動定足数構成機能は、クラスタの \fBinstallmode\fR プロパティーもリセットします。
.sp
この形式の \fBclsetup\fR コマンドは、クラスタ内のどのノードからでも実行できます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
通常のクラスタ動作中に \fBclsetup\fR コマンドを実行すると、\fBscsetup\fR コマンドは、クラスタ構成作業を実行するための対話形式でメニュー選択方式のユーティリティーを提供します。このユーティリティーが管理するクラスタコンポーネントの一部を次に示します。
.RS +4
.TP
.ie t \(bu
.el o
定足数 (quorum)
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースグループ
.RE
.RS +4
.TP
.ie t \(bu
.el o
データサービス
.RE
.RS +4
.TP
.ie t \(bu
.el o
クラスタインターコネクト
.RE
.RS +4
.TP
.ie t \(bu
.el o
デバイスグループとボリューム
.RE
.RS +4
.TP
.ie t \(bu
.el o
プライベートホスト名
.RE
.RS +4
.TP
.ie t \(bu
.el o
新規ノード
.RE
.RS +4
.TP
.ie t \(bu
.el o
クラスタのその他のプロパティー
.RE
この形式の \fBclsetup\fR コマンドは、クラスタ内のどのノードからでも実行できます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBclsetup\fR コマンドを非クラスタモードのノードから実行すると、\fBclsetup\fR コマンドは、プライベート IP アドレス範囲を変更および表示するメニュー形式のユーティリティーを提供します。
.sp
この形式の \fBclsetup\fR ユーティリティーを開始する前に、すべてのノードを非クラスタモードに再起動してください。
.RE
.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB\fB--help\fR\fR
.ad
.sp .6
.RS 4n
コマンドのヘルプ情報を出力します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-f\fR \fIlogfile\fR\fR
.ad
.br
.na
\fB\fB--file \fR\fIlogfile\fR\fR
.ad
.sp .6
.RS 4n
コマンドログを記録するログファイル名を指定します。このオプションを指定すると、\fBclsetup\fR によって生成されたほとんどのコマンドセットを実行および記録できます。ユーザーからの応答内容によっては、記録だけも可能です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB\fB--version\fR\fR
.ad
.sp .6
.RS 4n
コマンドセットのバージョンを出力します。コマンド行処理はまったく実行されず、コマンドは対話形式のメニューに入りません。
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWsczu
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
Intro(1CL)、cldevicegroup(1CL)、clnode(1CL)、clquorum(1CL)、clreslogicalhostname(1CL)、clresourcegroup(1CL)、clresourcetype(1CL)、clressharedaddress(1CL)、cluster(1CL)、cltelemetryattribute(1CL)
