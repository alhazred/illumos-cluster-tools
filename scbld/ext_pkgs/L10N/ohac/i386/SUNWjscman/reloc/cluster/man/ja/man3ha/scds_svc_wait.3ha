'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_svc_wait 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_svc_wait \- 指定のタイムアウト時間が経過するまで監視対象のプロセスの終了を待機する
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_svc_wait\fR(\fBscds_handle_t \fR \fIhandle\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_svc_wait()\fR 関数は、指定のタイムアウト時間が経過するまで監視対象のプロセスグループの終了を待機します。この関数は、呼び出し元の \fBSTART\fR メソッドで指定されたリソースの scds_pmf_start(3HA) によって起動されたすべてのプロセスグループを待機します。\fBscds_svc_wait()\fR 関数は、リソースの \fBRetry_interval\fR プロパティーと \fBRetry_count\fR プロパティーを使用して、待機する終了プロセス数を制限します。\fBRetry_interval\fR 内の終了プロセス数が \fBRetry_count\fR 個に達した場合、\fBscds_svc_wait()\fR は \fBSCHA_ERR_FAIL\fR とともに終了します。
.sp
.LP
失敗プロセス数が \fBRetry_count\fR を下回った場合、プロセスは再起動されます。\fBscds_svc_wait()\fR は、タイムアウト時間がすべて経過するまで、さらにプロセスの終了を待機します。失敗プロセス数は、そのあとの \fBscds_svc_wait()\fR 呼び出しでも計測されます。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
タイムアウト時間 (秒) です。
.RE

.SH 戻り値
.sp
.LP
\fBscds_svc_wait()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
関数はタイムアウト。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
プロセスの終了が行われなかったが、プロセスが正常に再起動された。 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_FAIL\fR\fR
.ad
.RS 28n
.rt  
失敗回数が \fBRetry_count\fR プロパティーの値に到達。 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_STATE\fR\fR
.ad
.RS 28n
.rt  
システムエラー、または予期せぬエラーが発生。 
.RE

.sp
.LP
その他のエラーコードについては、scha_calls(3HA)を参照してください。
.SH 使用例
.LP
\fB例 1 \fR\fBSTART\fR メソッドの \fBscds_svc_wait()\fR の使用法
.sp
.LP
次の例では、\fBSTART\fR メソッド内で \fBscds_svc_wait\fR を使用して、サービスの起動に失敗した場合ただちに終了する方法を示します。\fBscds_pmf_start()\fR を使ってアプリケーションプロセスを起動したあと、\fBSTART\fR メソッドは、処理が正常終了して戻る前にアプリケーションが完全に初期化され、使用可能な状態になるまで待機します。アプリケーションの起動に失敗した場合、\fBSTART\fR メソッドは、エラーを戻して終了する前に、\fBStart_timeout\fR 時間が経過するまで待機する必要があります。次の例のように、\fBscds_svc_wait()\fR を使用することで、\fBSTART\fR メソッドによるアプリケーションの再起動回数(\fBRetry_count\fR) を指定できます。サービスを起動できない場合、\fBSTART\fR メソッドはエラーを出してただちに終了します。

.sp
.in +2
.nf
/*
* scds_svc_wait is a subroutine in a START method to
* check that the service is fully available before returning.
* Calls svc_probe() to check service availability.
*/
int
svc_wait(scds_handle_t handle)
{
	while (1) {
		/* Wait for 5 seconds */
		if (scds_svc_wait(handle, 5) != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Service failed to start.");
			return (1);		/* Start Failure */
		}
		/* Check if service is fully up every 5 seconds */
		if (svc_probe(handle) == 0) {
			scds_syslog(LOG_INFO, "Service started successfully.");
			return (0);
		}
	}
	return (0);
}
.fi
.in -2

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_initialize(3HA)、scds_pmf_start(3HA)、scha_calls(3HA)、\fBattributes\fR(5)、r_properties(5)
.SH 注意事項
.RS +4
.TP
.ie t \(bu
.el o
\fBSTART\fR メソッドがリソース上の \fBStart_timeout\fR 設定を超過した場合、Resource Group Manager (RGM) は \fBSTART\fR メソッドを強制終了します。この動作は、\fBSTART\fR メソッドが \fBscds_svc_wait()\fR の終了を待機しているときも変わりません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソース上の \fBRetry_interval\fR の値が \fBStart_timeout\fR の値より大きい場合、\fBSTART\fR メソッドは RGM によってタイムアウトになります。この動作は、失敗回数が \fBRetry_count\fR に達していない場合も変わりません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBSTART\fR メソッドの複数の \fBscds_pmf_start()\fR を呼び出して複数のプロセスを起動した場合、\fBscds_svc_wait()\fR は終了したプロセスグループを起動します。プロセスグループ間の依存関係は強制されません。プロセスグループ間に依存関係があり、失敗時にその他のプロセスグループの再起動を要求するようなプロセスグループが存在する場合は、\fBscds_svc_wait()\fR を使用してはいけません。プロセスグループの健全性検査の間待機する場合は、\fBsleep()\fR を使用してください。
.RE
