'\" te
.\"  Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  clressharedaddress   1CL  " 2008 年 9 月 16 日" " Sun Cluster 3.2 " " Sun Cluster 保守コマンド "
.SH 名前
clressharedaddress, clrssa \- 共有アドレスの Sun Cluster リソースの管理
.SH 形式
.LP
.nf
\fB/usr/cluster/bin/clressharedaddress\fR [\fIsubcommand\fR] \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress\fR [\fIsubcommand\fR [\fIoptions\fR]] \fB-v\fR [\fIsaresource\fR]...
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress create\fR \fB-g\fR \fIresourcegroup\fR [\fB-h\fR \fIlhost\fR[,\&.\|.\|.] ] [\fB-N\fR \fInetif\fR@\fInode\fR[,\&.\|.\|.]] [\fB-X\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]] [\fB-p\fR \fIname\fR=\fIvalue\fR] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}] [\fB-d\fR] \fIsaresource\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress create\fR \fB-i\fR {- | \fIclconfiguration\fR} [\fB-a\fR] [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-X\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]] [\fB-p\fR \fIname\fR=\fIvalue\fR] [\fB-d\fR] {+ | \fIsaresource\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress delete\fR [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}] [\fB-F\fR] {+ | \fIsaresource\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress disable\fR [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-R\fR] [ [\fB-z\fR \fIzone\fR] \fB-n\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}] {+ | \fIsaresource\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress enable\fR [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-R\fR] [ [\fB-z\fR \fIzone\fR] \fB-n\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}]{+ | \fIsaresource\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress export\fR [\fB-o\fR {- | \fIconfigfile\fR}] [+ | \fIsaresource\fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress list\fR [\fB-s\fR \fIstate\fR[,\&.\|.\|.]] [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR [,...] | \fBglobal\fR | \fBall\fR}] [+ | \fIsaresource\fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress list-props\fR [\fB-l\fR \fIlisttype\fR] [\fB-p\fR \fIname\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR [,...] | \fBglobal\fR | \fBall\fR}] [+ | \fIlhresource\fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress monitor\fR [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}] {+ | \fIsaresource\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress reset\fR [\fB-f\fR \fIerrorflag\fR] [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}] {+ | \fIsaresource\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress set\fR [\fB-i\fR {- | \fIclconfiguration\fR}] [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-X\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]] [\fB-p\fR \fIname\fR[+|-]=\fIvalue\fR] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}] {+ | \fIsaresource\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress show\fR [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-p\fR \fIname\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR [,...] | \fBglobal\fR | \fBall\fR}] [+ | \fIsaresource\fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress status\fR [\fB-s\fR \fIstate\fR[,\&.\|.\|.]] [ [\fB-z\fR \fIzone\fR] \fB-n\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]] [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR [,...] | \fBglobal\fR | \fBall\fR}] [+ | \fIsaresource\fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/clressharedaddress unmonitor\fR [\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]] [\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR}] {+ | \fIsaresource\fR...}
.fi

.SH 機能説明
.sp
.LP
\fBclressharedaddress\fR コマンドは、Sun Cluster 共有アドレスのリソースを管理します。\fBclrssa\fR コマンドは \fBclressharedaddress\fR コマンドの短形式です。\fBclressharedaddress\fR コマンドと \fBclrssa\fR コマンドは同じものです。どちらの形式のコマンドも使用できます。
.sp
.LP
clresource(1CL) コマンドを使用して、共有アドレスのリソースを管理することもできます。
.sp
.LP
\fBclressharedaddress\fR コマンドの一部のサブコマンドは、リソース構成を変更します。これらのサブコマンドは、大域ゾーンまたは非大域ゾーンから使用できます。リソース構成変更するサブコマンドを非大域ゾーンから使用すると、非大域ゾーンがマスターできるリソースだけが変更されます。リソース構成を変更するサブコマンドは、次のとおりです。
.RS +4
.TP
.ie t \(bu
.el o
\fBdisable\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBenable\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBmonitor\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBreset\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBset\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBunmonitor\fR
.RE
.sp
.LP
\fBclressharedaddress\fR コマンドの一部のサブコマンドは、リソースに関する情報だけを取得します。ただし、これらのサブコマンドを非大域ゾーンで使用する場合でさえ、コマンドの適用範囲はそのゾーンに限定\fBされません\fR。非大域ゾーンがリソースをマスターできるかどうかにかかわらず、そのコマンドに対するオペランドとして提供されているすべてのリソースに関する情報が取得されます。リソースに関する情報だけを取得するコマンドは、次のとおりです。
.RS +4
.TP
.ie t \(bu
.el o
\fBexport\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBlist\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBlist-props\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBshow\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBstatus\fR
.RE
.sp
.LP
このコマンドからの予想不能な結果を避けるには、コマンドのすべての書式を広域ノードから実行してください。
.sp
.LP
コマンドの一般的な形式は次のとおりです。
.sp
.LP
\fBclressharedaddress\fR [\fIsubcommand\fR] [\fIoptions\fR] [\fIoperands\fR]
.sp
.LP
\fIsubcommand\fR を省略できるのは、\fIoptions\fR がオプション \fB-?\fR または \fB-V\fR を指定している場合だけです。
.sp
.LP
このコマンドの各オプションには、長い形式と短い形式があります。各オプションの両方の形式については、このマニュアルページの「オプション」セクションを参照してください。
.SS "ゾーンクラスタでの操作"
.sp
.LP
ゾーンクラスタでは、\fBexport\fR 以外のすべてのサブコマンドのある \fBclressharedaddress\fR を使用できます。
.sp
.LP
\fBexport\fR 以外のすべてのサブコマンドで \fB-Z\fR オプションを使用して、操作を制限する特定のゾーンクラスタの名前を指定することもできます。また、共有アドレスリソース (\fIzoneclustername\fR:\fIresourcegroup\fR) にゾーン・クラスタ名を添付して、操作を特定ゾーンクラスタに制限することもできます。
.sp
.LP
広域クラスタノードからすべてのゾーンクラスタ情報にアクセスできますが、特定のゾーンクラスタは他のゾーンクラスタを認識しません。特定のゾーンクラスタに操作を制限できない場合、使用するサブコマンドは現在のクラスタでのみ機能します。
.LP
注 - 
.sp
.RS 2
\fB-z\fR オプションと \fB-Z\fR オプションは、すべてのサブコマンドに対して手動で制限されます。
.RE
.SH サブコマンド
.sp
.LP
サポートされるサブコマンドには次のものがあります。
.sp
.ne 2
.mk
.na
\fB\fBcreate\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして共有アドレス指定されたリソースを作成します。
.sp
\fBcreate\fR を \fB-i\fR オプションと使用して構成ファイルを指定した場合、サブコマンドは正符号 (+) をオペランドとして受け付けます。+ オペランドを使用すると、構成ファイル内の存在しないすべてのｿースが作成されます。
.sp
デフォルトでは、リソースは監視対象となり、有効な状態で作成されます。ただし、リソースがオンライン状態になり、監視されるのは、リソースのリソースグループがオンラインになったあとだけです。無効な状態でリソースを作成するには、\fB-d\fR オプションを指定します。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
広域クラスタからゾーンクラスタに共有アドレスリソースを作成するために、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定できます。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR 役割に基づくアクセス制御 (RBAC) の承認が必要です。
.sp
\fBdelete\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBdelete\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定された共有アドレスリソースを削除します。このサブコマンドに対しオペランドとしてプラス記号 (+) を指定すると、すべてのリそースが削除されます。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
広域クラスタからゾーンクラスタに共有アドレスリソースを作成するために、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定できます。
.sp
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、削除するリソースを限定することができます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけを削除します。
.RS +4
.TP
.ie t \(bu
.el o
デフォルトでは、リソースは次の条件が満たされる場合に \fB のみ \fR 削除されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースが無効な状態である。
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースに対するすべての依存性が削除されている。
.RE
.RS +4
.TP
.ie t \(bu
.el o
指定したすべてのリソースを確実に削除するには、\fB-F\fR オプションを指定します。\fB-F\fR オプションの効果は、次のとおりです。
.RE
.RS +4
.TP
.ie t \(bu
.el o
指定したすべてのリソース (無効になっていないリソースも含む) が削除されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
指定したすべてのリソースが、他のリソースのリソース依存性設定から削除されます。
.RE
リソースは、コマンド行でリソースを指定した順序とは無関係に、リソース間の依存性を満たすのに必要な順序に従って削除されます。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
\fBcreate\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBdisable\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定された共有アドレスリソースを無効にします。このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、すべてのリソースが無効になります。
.sp
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、無効にするリソースを限定することができます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけを無効にします。
.sp
必要なリソース依存性をすべて確実に満たすには、\fB-R\fR オプションを指定します。\fB-R\fR オプションは、コマンドに対するオペランドとして指定したリソースに依存しているリソース (コマンドに対するオペランドとして指定しなかったリソースも含まれる) をすべて無効にします。\fB-g\fR オプションと \fB-t\fR オプションは、リソース依存性を満たすためだけに無効化されるリソースには適用されません。
.sp
リソースは、コマンド行でリソースを指定した順序とは無関係に、リソース間の依存性を満たすのに必要な順序に従って無効化されます。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、その非大域ゾーンがマスターゾーンになることができるリソースだけが変更されます。広域クラスタからゾーンクラスタに登録された共有アドレスリソースを無効にするには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.sp
\fBenable\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBenable\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定された共有アドレスリソースを有効にします。このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、すべてのリソースが有効になります。
.sp
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、有効にするリソースを限定できます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけを有効にします。
.sp
必要なリソース依存性をすべて確実に満たすには、\fB-R\fR オプションを指定します。\fB-R\fR オプションは、コマンドに対するオペランドとして指定したリソースに依存しているリソース (コマンドに対するオペランドとして指定しなかったリソースも含まれる) をすべて有効にします。\fB-g\fR オプションは、リソース依存性を満たすためだけに有効化されるリソースには適用されません。
.sp
リソースは、コマンド行でリソースを指定した順序とは無関係に、リソース間の依存性を満たすのに必要な順序に従って有効化されます。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、その非大域ゾーンがマスターゾーンになることができるリソースだけが変更されます。広域クラスタからゾーンクラスタに登録された共有アドレスリソースを有効にするには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.sp
\fBdisable\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBexport\fR\fR
.ad
.sp .6
.RS 4n
clconfiguration(5CL) のマニュアルページで規定されている形式で、クラスタリソース構成をエクスポートします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、コマンドの適用範囲はそのゾーンに限定\fBされません\fR。非大域ゾーンがリソースをマスターできるかどうかにかかわらず、そのコマンドに対するオペランドとして提供されているすべてのリソースに関する情報が取得されます。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
マンドに対するオペランドとして指定した共有アドレスリソースのリストを表示します。デフォルトでは、すべてのリソースが表示されます。
.sp
\fB-\fR オプションを指定すると、オペランドのリストをフィルタリングし、表示するリソースを限定できます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけを表示します。
.sp
このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、指定したリソースグループ内のすべてのリソースまたは指定したリソースタイプのインスタンスであるすべてのリソースを指定できます。オペランドが指定されていない場合、指定されているリソースグループ内のすべてのリソースまたは指定されているリソースタイプのインスタンスであるすべてのリソースが表示されます。
.sp
\fB-v\fR オプションを指定すると、該当するリソースグループおよびリスト内の各リソースのリソースタイプも表示されます。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、コマンドの適用範囲はそのゾーンに限定\fBされません\fR。非大域ゾーンがリソースをマスターできるかどうかにかかわらず、そのコマンドに対するオペランドとして提供されているすべてのリソースに関する情報が取得されます。広域クラスタからゾーンクラスタに登録された共有アドレスリソースを表示するには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist-props\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定した共有アドレスリソースのプロパティーのリストを表示します。デフォルトでは、すべてのリソースの拡張プロパティーが表示されます。
.sp
次のオプションを指定すると、オペランドのリストをフィルタリングし、プロパティーが表示されるリソースを限定できます。
.sp
.ne 2
.mk
.na
\fB\fB-g\fR \fIresourcegrouplist\fR\fR
.ad
.RS 28n
.rt  
\fIresourcegrouplist\fR 内のリソースグループのメンバーであるオペランドのリストの共有アドレスリソースのプロパティーだけを表示します。
.RE

\fB-l\fR オプションでは、表示するリソースプロパティーのタイプを指定します。
.sp
.ne 2
.mk
.na
\fB\fB-l all\fR\fR
.ad
.RS 28n
.rt  
標準プロパティーと拡張プロパティーを表示するように指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-l extension\fR\fR
.ad
.RS 28n
.rt  
拡張プロパティーだけを表示するように指定します。デフォルトでは、拡張プロパティーだけが表示されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-l standard\fR\fR
.ad
.RS 28n
.rt  
標準プロパティーだけを表示するように指定します。
.RE

\fB-l\fR オプションを指定しない場合、\fB-p\fR オプションまたは \fB-y\fR オプションを使用して標準プロパティーを明示的に指定しないかぎり、拡張プロパティーだけが表示されます。
.sp
\fB-p\fR オプションは、表示するリソースプロパティーのセットを制限します。\fB-p\fR オプションを指定すると、\fInamelist\fR で指定したプロパティーだけが表示されます。\fInamelist\fR では、標準プロパティーと拡張プロパティーを指定できます。
.sp
\fB-v\fR オプションを指定すると、各プロパティーの説明も表示されます。
.sp
このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、指定したリソースグループ内のすべてのリソースまたは指定したリソースタイプのインスタンスであるすべてのリソースを指定できます。オペランドを指定しないと、指定したリソースグループ内のすべてのリソースのプロパティーまたは指定したリソースタイプのインスタンスであるすべてのリソースのプロパティーが表示されます。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、コマンドの適用範囲はそのゾーンに限定\fBされません\fR。非大域ゾーンがリソースをマスターできるかどうかにかかわらず、そのコマンドに対するオペランドとして提供されているすべてのリソースに関する情報が取得されます。広域クラスタからゾーンクラスタの共有アドレスリソースのプロパティーのリストを表示するには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBmonitor\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定した共有アドレスリソースの監視を有効にします。このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、すべてのリソースに対して監視が有効になります。
.sp
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、監視対象のリソースを限定できます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけを監視します。
.sp
リソースは、監視が有効になっている場合、次の条件が満たされているときだけ監視されます。
.RS +4
.TP
.ie t \(bu
.el o
リソースが有効な状態にある。
.RE
.RS +4
.TP
.ie t \(bu
.el o
該当リソースが含まれるリソースグループが、1 つ以上のクラスタノード上でオンライン状態にある。
.RE
.LP
注 - 
.sp
.RS 2
リソースに対する監視を有効にする場合、該当リソースを有効にする必要は\fBありません\fR。
.RE
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、その非大域ゾーンがマスターゾーンになることができるリソースだけが変更されます。広域クラスタからゾーンクラスタのリソースを監視するには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.sp
\fBunmonitor\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBreset\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定した共有アドレスリソースに関連付けられているエラーフラグをクリアします。このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、すべてのリソースのエラーフラグがクリアされます。
.sp
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、リセットするリソースを限定できます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけをリセットします。
.sp
デフォルト時、\fBreset\fR サブコマンドはエラーフラグ \fBSTOP_FAILED\fR をクリアします。クリアするエラーフラグを明示的に指定するには、\fB-f\fR オプションを使用します。\fB-f\fR オプションが受け付けるエラーフラグは、\fBSTOP_FAILED\fR だけです。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、その非大域ゾーンがマスターゾーンになることができるリソースだけが変更されます。広域クラスタからゾーンクラスタの共有アドレスリソースをリセットするには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBset\fR\fR
.ad
.sp .6
.RS 4n
コマンドのオペランドとして指定されている共有アドレスリソースの指定プロパティーを変更します。このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、すべてのリソースの指定したプロパティーが変更されます。
.sp
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、変更するリソースを限定できます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけを変更します。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、その非大域ゾーンがマスターゾーンになることができるリソースだけが変更されます。広域クラスタからゾーンクラスタに共有アドレスリソースのプロパティーを設定するには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定した共有アドレスリソースの構成を表示します。デフォルトでは、すべてのリソースの構成が表示されます。
.sp
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、構成が表示されるリソースを限定できます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけの構成を表示します。
.sp
\fB-p\fR オプションは、表示するリソースプロパティーのセットを制限します。\fB-p\fR オプションを指定すると、\fInamelist\fR で指定したプロパティーだけが表示されます。\fInamelist\fR では、標準プロパティーと拡張プロパティーを指定できます。
.sp
このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、指定したリソースグループ内のすべてのリソースまたは指定したリソースタイプのインスタンスであるすべてのリソースを指定できます。オペランドを指定しないと、指定したリソースグループ内のすべてのリソースの構成または指定したリソースタイプのインスタンスであるすべてのリソースの構成が表示されます。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、コマンドの適用範囲はそのゾーンに限定\fBされません\fR。非大域ゾーンがリソースをマスターできるかどうかにかかわらず、そのコマンドに対するオペランドとして提供されているすべてのリソースに関する情報が取得されます。広域クラスタからゾーンクラスタに共有アドレスリソースの構成を表示するには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstatus\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定したリソースのステータスを表示します。デフォルトでは、すべてのリソースのステータスが表示されます。
.sp
次のオプションを指定すると、オペランドのリストをフィルタリングし、状態が表示されるリソースを限定できます。
.sp
.ne 2
.mk
.na
\fB\fB-g\fR \fIresourcegrouplist\fR\fR
.ad
.RS 28n
.rt  
\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースだけの状態を表示します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fInodelist\fR\fR
.ad
.RS 28n
.rt  
\fInodelist\fR 内のノード上またはゾーン内にホストされている、オペランドのリスト内のリソースだけの状態を表示します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-s\fR \fIstatelist\fR\fR
.ad
.RS 28n
.rt  
\fIstatelist\fR 内の状態にある、オペランドのリスト内のリソースだけの状態を表示します。
.RE

このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、指定したリソースグループ内のすべてのリソースまたは指定したリソースタイプのインスタンスであるすべてのリソースを指定できます。オペランドを指定しないと、指定したリソースグループ内のすべてのリソースの状態または指定したリソースタイプのインスタンスであるすべてのリソースの状態が表示されます。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、コマンドの適用範囲はそのゾーンに限定\fBされません\fR。非大域ゾーンがリソースをマスターできるかどうかにかかわらず、そのコマンドに対するオペランドとして提供されているすべてのリソースに関する情報が取得されます。広域クラスタからゾーンクラスタに共有アドレスリソースの状態を表示するには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBunmonitor\fR\fR
.ad
.sp .6
.RS 4n
コマンドに対するオペランドとして指定した共有アドレスリソースの監視を無効にします。このサブコマンドでプラス記号 (\fB+\fR) をオペランドとして指定すると、すべてのリソースに対する監視が無効になります。
.sp
無効になっているリソースの監視を無効にしても、リソースは影響を受けません。リソースとそのモニターは、すでにオフライン状態です。
.LP
注 - 
.sp
.RS 2
リソースの監視を無効にしても、リソースは無効に\fBなりません\fR。ただし、リソースを無効にする場合、監視を無効にする必要はありません。無効なリソースとそのモニターは、オフライン状態が維持されます。
.RE
\fB-g\fR オプションを指定すると、オペランドのリストをフィルタリングし、監視を無効にするリソースを限定できます。\fB-g\fR オプションは、\fIresourcegrouplist\fR 内のリソースグループのメンバーである、オペランドのリスト内のリソースの監視を無効にします。
.sp
このサブコマンドは、ベースクラスタまたはゾーンクラスタで使用できます。
.sp
このサブコマンドを非大域ゾーンで使用する場合、コマンドの適用範囲はそのゾーンに限定\fBされません\fR。非大域ゾーンがリソースをマスターできるかどうかにかかわらず、そのコマンドに対するオペランドとして提供されているすべてのリソースに関する情報が取得されます。広域クラスタからゾーンクラスタへの共有アドレスリソースの監視をオフにするには、\fB-Z\fR オプションを使用してゾーンクラスタの名前を指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.sp
\fBdisable\fR サブコマンドおよび \fBmonitor\fR サブコマンドの説明も参照してください。
.RE

.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB\fB--help\fR\fR
.ad
.sp .6
.RS 4n
ヘルプ情報を表示します。このオプションを使用する場合、ほかの処理は実行されません。
.sp
このオプションを指定するとき、サブコマンドは指定してもしなくてもかまいません。
.sp
サブコマンドなしでこのオプションを指定すると、このコマンドのサブコマンドのリストが表示されます。
.sp
サブコマンド付きでこのオプションを指定すると、サブコマンドの使用方法が表示されます。
.sp
特定のサブコマンドを指定した場合のこのオプションの効果は、次のようになります。
.sp
.ne 2
.mk
.na
\fB\fBcreate\fR\fR
.ad
.RS 28n
.rt  
\fB-g\fR オプションとともに指定された場合、指定されたリソースグループのすべてのリソースプロパティーのヘルプ情報を表示します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBset\fR\fR
.ad
.RS 28n
.rt  
コンドに対するオペランドとして指定したリソースのプロパティーに関する情報を表示します。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-a\fR\fR
.ad
.br
.na
\fB\fB--automatic\fR\fR
.ad
.sp .6
.RS 4n
クラスタ構成情報からリソースが作成される場合、次の処理も自動的に実行します。
.RS +4
.TP
.ie t \(bu
.el o
リソースタイプの登録
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースグループの作成
.RE
.RS +4
.TP
.ie t \(bu
.el o
オペランドのリスト内で指定されているリソースの依存先リソースの作成
.RE
クラスタ構成情報には、次の処理をすべて実行するのに必要な十分な情報が含まれている必要があります。
.RS +4
.TP
.ie t \(bu
.el o
リソースタイプの登録を有効にする
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースグループの作成を有効にする
.RE
.RS +4
.TP
.ie t \(bu
.el o
作成されるリソースの有効化
.RE
このオプションを指定できるのは、\fBcreate\fR サブコマンドの場合だけです。このオプションを指定する場合は、\fB-i\fR オプションも指定し、構成ファイルを指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-d\fR\fR
.ad
.br
.na
\fB\fB--disable\fR\fR
.ad
.sp .6
.RS 4n
リソースの作成時にリソースを無効にします。このオプションを指定できるのは、\fBcreate\fR サブコマンドの場合だけです。デフォルトでは、リソースは作成されたあと、有効な状態になります。
.sp
リソースは、有効化しても、オンライン状態になるとは限りません。リソースは、リソースのリソースグループが 1 つ以上のノードでオンライン状態になったあとでのみオンライン状態になります。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-f\fR \fIerrorflag\fR\fR
.ad
.br
.na
\fB\fB--flag \fR\fIerrorflag\fR\fR
.ad
.sp .6
.RS 4n
\fBreset\fR サブコマンドによってクリアするエラーフラグを明示的に指定します。このオプションを指定できるのは、\fBreset\fR サブコマンドの場合だけです。デフォルト時、\fBreset\fR サブコマンドはエラーフラグ \fBSTOP_FAILED\fR をクリアします。
.sp
\fB-f\fR オプションが受け付けるエラーフラグは、\fBSTOP_FAILED\fR だけです。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-F\fR\fR
.ad
.br
.na
\fB\fB--force\fR\fR
.ad
.sp .6
.RS 4n
無効状態でないリソースの削除が、強制的に実行されます。このオプションは、\fBdelete\fR サブコマンドの場合にだけ指定できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-g\fR \fIresourcegroup\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--resourcegroup \fR\fIresourcegroup\fR\fB-[,\&.\|.\|.]\fR\fR
.ad
.sp .6
.RS 4n
1 つのリソースグループまたはリソースグループのリストを指定します。
.sp
\fBcreate\fR 以外のサブコマンドの場合、コマンドは \fB-g\fR オプションで指定したリソースグループのメンバーである、オペランドのリスト内のリソースにだけ作用します。
.sp
特定のサブコマンドを指定した場合のこのオプションの効果は、次のようになります。
.sp
.ne 2
.mk
.na
\fB\fBcreate\fR\fR
.ad
.RS 28n
.rt  
指定したリソースグループ内でリソースを作成するように指定します。\fB-create\fR サブコマンドを指定して \fBg\fR を使用する場合、リソースグループは 1 つしか指定することができません。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-h\fR \fIlhost\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--logicalhost \fR\fIlhost\fR\fB-[,\&.\|.\|.]\fR\fR
.ad
.sp .6
.RS 4n
ホスト名リストを指定します。複数の論理ホストを新しい \fB-SharedAddress\fR リソースに関連付ける必要がある場合や論理ホストがリソース自体と同じ名前を持っていない場合は、\fBh\fR オプションを使用します。\fBSharedAddress\fR リソースの \fBHostnameList\fR 内のすべての論理ホストは同じサブネット上に置いてください。\fBHostnameList\fR プロパティーを指定しない場合、\fBHostnameList\fR は \fBSharedAddress\fR リソースと同じになります。
.sp
\fBSharedAddress\fR リソースの論理ホスト名は同じサブネット上に置いてください。
.sp
\fB-HostnameList\fR プロパティーを \fBp\fR とともに設定する代わりに、\fB-h\fR を使用することは可能です。ただし、\fB-h\fR を使用して、同じコマドで明示的に \fBHostnameList\fR を設定できません。
.sp
\fB-h\fR は、\fBcreate\fR サブコマンドの場合にだけ使用できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-i\fR {- | \fIclconfiguration\fR}\fR
.ad
.br
.na
\fB\fB--input {- | \fR\fIclconfiguration\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
共有アドレスリソースの作成または変更に使用する構成情報を指定します。この情報は clconfiguration(5CL) のマニュアルページに定義されている書式に準拠させてください。この情報は、ファイルに含めることも、標準入力を介して指定することもできます。標準入力を指定するには、ファイル名の代わりに \fB-\fR を指定します。
.sp
コマンドに対するオペランドとして指定したリソースだけが、作成または変更されます。コマンドで指定したオプションは、構成情報で設定されているオプションより優先されます。構成成パラメータは、構成情報内で設定されていない場合、コマンド行で指定します。
.sp
特定のサブコマンドを指定した場合のこのオプションの効果は、次のようになります。
.sp
.ne 2
.mk
.na
\fB\fBcreate\fR\fR
.ad
.RS 28n
.rt  
\fB-a\fR オプションと指定された場合は、必要なすべてのリソースタイプを登録し、必要なすべてのリソースグループを作成します。登録および構成に必要なすべての情報を指定します。その他の構成データはすべて無視します。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-l\fR \fIlisttype\fR\fR
.ad
.br
.na
\fB\fB--listtype \fR\fIlisttype\fR\fR
.ad
.sp .6
.RS 4n
\fBlist-props\fR サブコマンドによって表示するリソースプロパティーのタイプを指定します。このオプションは、\fBlist-props\fR サブコマンドの場合にだけ指定できます。
.sp
\fIlisttype\fR に対して、次のリストから値を 1 つ指定します。
.sp
.ne 2
.mk
.na
\fB\fBall\fR\fR
.ad
.RS 28n
.rt  
標準プロパティーと拡張プロパティーを表示するように指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBextension\fR\fR
.ad
.RS 28n
.rt  
拡張プロパティーだけを表示するように指定します。デフォルトでは、拡張プロパティーだけが表示されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstandard\fR\fR
.ad
.RS 28n
.rt  
標準プロパティーだけを表示するように指定します。
.RE

\fB-l\fR オプションを指定しないと、\fB-p\fR オプションを使って標準プロパティーを明示的に指定しないかぎり、拡張プロパティーしか表示されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--node \fR\fInode\fR\fB-[:\fR\fIzone\fR\fB-][,\&.\|.\|.]\fR\fR
.ad
.sp .6
.RS 4n
ターゲットのグローバルクラスタまたはゾーンクラスタに、ノードまたはノードのリストを指定します。各ノードは、ノード名またはノード ID として指定できます。ノードごとに、ノード上の非大域ゾーンを指定することもできます。
.sp
\fB-Z\fR オプションが指定されている場合、広域クラスタホスト名ではなく、\fB-n\fR オプションの付いたゾーン・クラスタのみを指定できます。\fB-Z\fR オプションが指定されていない場合、\fB-n\fR の付いた広域クラスタホスト名のみを指定できます。
.sp
このオプションとともに指定できるサブコマンドは、次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fBdisable\fR\fR
.ad
.RS 28n
.rt  
指定したノード上または指定したゾーン内でホストされている、オペランドのリスト内のリソースだけを無効にします。
.RE

.sp
.ne 2
.mk
.na
\fB\fBenable\fR\fR
.ad
.RS 28n
.rt  
指定したノード上または指定したゾーン内でホストされている、オペランドのリスト内のリソースだけを有効にします。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstatus\fR\fR
.ad
.RS 28n
.rt  
指定したノード上または指定したゾーン内でホストされている、オペランドのリスト内のリソースだけの状態を報告します。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-N\fR \fInetif\fR@\fInode\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--netiflist \fR\fInetif\fR\fB-@\fR\fInode\fR\fB-[,\&.\|.\|.]\fR\fR
.ad
.sp .6
.RS 4n
リソースプロパティーを指定します。\fB-N\fR オプションを使用すると、プロパティーの \fBp\fR オプションを使用せずに \fB-NetIfList\fR プロパティーを設定できます。\fB-N\fR を指定しないと、\fBclressharedaddress\fR コマンドは利用可能な IPMP グループまたはパブリックアダプタと、\fBHostnameList\fR プロパティーと関連付けられているサブネットに基づいて、\fBNetIfList\fR プロパティーを自動的に設定するよう試みます。
.sp
\fBNetIfList\fR プロパティーは、次の形式で指定できます。\fIipmpgroup\fR@\fInode\fR[,\&.\|.\|.]。ただし、\fB-N\fR は両方の \fIipmpgroup\fR@\fInode\fR[,\&.\|.\|.] および \fIpublicNIC\fR@\fInode\fR[,\&.\|.\|.] を受け入れます。\fB-N\fR を使用しなかったり、\fIpublicNIC\fR@\fInode\fR とともに使用したりすると、\fBclressharedaddress\fR コマンドは、必要な IPMP グループを作成しようとします。システムは、標準 Solaris インタフェースを使用して複数のアダプタを取り込むためにあとで変更されるデフォルトのセットで 1 つ以上の単一アダプタ IPMP グループから成るセットを作成します。
.sp
\fB-p\fR を指定して \fBNetIfList\fR プロパティーを直接設定する代わりに \fB-N\fR を指定することができます。ただし、\fB-N\fR を使用し、同じコマンド内で \fBNetIfList\fR を明示的に設定できません。
.sp
\fB-N\fR は、\fBcreate\fR サブコマンドの場合にだけ使用できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o\fR {- | \fIclconfiguration\fR}\fR
.ad
.br
.na
\fB\fB--output {- | \fR\fIclconfiguration\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
リソース構成情報の書き込み先を指定します。この書き込み先は、ファイルでも、標準出力でもかまいません。標準出力を指定するには、ファイル名の代わりに \fB-\fR を指定します。標準出力を指定すると、該当コマンドのほかのすべての標準出力が抑制されます。このオプションは、\fBexport\fR サブコマンドの場合にだけ指定できます。
.sp
構成情報は、コマンドに対するオペランドとして指定したリソースの場合だけ書き込まれます。この情報は、clconfiguration(5CL) のマニュアルページに定義されている形式で書き込まれます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR \fIname\fR=\fIvalue\fR\fR
.ad
.br
.na
\fB\fB-p \fR\fIname\fR\fB-+=\fR\fIarray-values\fR\fR
.ad
.br
.na
\fB\fB-p\fR \fIname\fR-=\fIarray-values\fR\fR
.ad
.br
.na
\fB\fB--property \fR\fIname\fR\fB-=\fR\fIvalue\fR\fR
.ad
.br
.na
\fB\fB--property\fR \fIname\fR+=\fIarray-values\fR\fR
.ad
.br
.na
\fB\fB--property \fR\fIname\fR\fB--=\fR\fIarray-values\fR\fR
.ad
.sp .6
.RS 4n
リソースの標準プロパティーと拡張プロパティーを設定します。このオプションは、\fBcreate\fR サブコマンドおよび \fBset\fR サブコマンドの場合にだけ指定できます。
.sp
すべての標準リソースプロパティーの詳細は、r_properties(5)のマニュアルページを参照してください。
.sp
リソースタイプの拡張プロパティーについては、リソースタイプに関するマニュアルを参照してください。
.sp
このオプションとともに使用する演算子は、次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB=\fR\fR
.ad
.RS 13n
.rt  
プロパティーに、指定した値を設定します。この演算子は、\fBcreate\fR サブコマンドおよび \fBset\fR サブコマンドで使用できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB+=\fR\fR
.ad
.RS 13n
.rt  
1 つまたは複数の値を文字列配列値に追加します。この演算子は、\fBset\fR サブコマンドでのみ使用できます。この演算子は、文字列配列値に対してだけ指定できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-=\fR\fR
.ad
.RS 13n
.rt  
1 つまたは複数の値が、文字列配列値から削除されます。この演算子は、\fBset\fR サブコマンドでのみ使用できます。この演算子は、文字列配列値に対してだけ指定できます。
.RE

ノード単位のプロパティーがクラスタノードのサブセット上でのみ設定される場合は、中括弧内のノードのリストを次のようにプロパティー名に付け加えることで、プロパティーが設定されるノードを指定します。
.sp
.in +2
.nf
\fIname\fR{\fInodelist\fR}
.fi
.in -2
.sp

\fInodelist\fR は、ノード名またはノード ID をコンマで区切ったリストです。per-node プロパティーの詳細については、rt_properties(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR \fIname\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--property \fR\fIname\fR\fB-[,\&.\|.\|.]\fR\fR
.ad
.sp .6
.RS 4n
\fBlist-props\fR サブコマンドおよび \fBshow\fR サブコマンドのプロパティーのリストを指定します。
.sp
このオプションは、リソースの標準プロパティーおよび拡張プロパティーに対して使用できます。
.sp
すべての標準リソースプロパティーの詳細は、r_properties(5)のマニュアルページを参照してください。
.sp
リソースタイプの拡張プロパティーについては、リソースタイプに関するマニュアルを参照してください。
.sp
このオプションを指定しなかった場合、\fBlist-props\fR サブコマンドおよび \fBshow\fR サブコマンドは、\fB-v\fR オプションも指定されているかどうかに基づいて、すべてまたはほとんどのリソースプロパティーを一覧表示します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-R\fR\fR
.ad
.br
.na
\fB\fB--recursive\fR\fR
.ad
.sp .6
.RS 4n
必要な依存性がすべて満たされるように、リソースの有効化または無効化を再帰的に実行します。このオプションは、\fBdisable\fR サブコマンドおよび \fBenable\fR サブコマンドの場合にだけ指定できます。
.sp
このオプションをこれらのサブコマンドとともに指定した場合の効果は、次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fBdisable\fR\fR
.ad
.RS 28n
.rt  
コマンドに対するオペランドとして指定したリソースに依存しているリソース (コマンドに対するオペランドとして指定しなかったリソースも含まれる) をすべて無効にします。
.RE

.sp
.ne 2
.mk
.na
\fB\fBenable\fR\fR
.ad
.RS 28n
.rt  
コマンドに対するオペランドとして指定したリソースの依存先リソース (コマンドに対するオペランドとして指定しなかったリソースも含まれる) がすべて有効になります。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-s\fR \fIstate\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--state \fR\fIstate\fR\fB-[,\&.\|.\|.]\fR\fR
.ad
.sp .6
.RS 4n
\fBlist\fR サブコマンドおよび \fBstatus\fR サブコマンドの状態のリストを指定します。
.sp
このオプションは出力を制限し、ノードリスト内の 1 つまたは複数のノード上で指定されている状態の 1 つにあるリソースだけが含まれるようにします。
.sp
可能な状態は、次のとおりです。
.RS +4
.TP
.ie t \(bu
.el o
\fBdegraded\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBdetached\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBfaulted\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBmonitor_failed\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBnot_online\fR - \fBonline\fR または \fBonline_not_monitored\fR 以外の全てのステータスを指定します
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBoffline\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBonline\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBonline_not_monitored\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBstart_failed\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBstop_failed\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBunknown\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBunmonitored\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBwait\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB\fB--version\fR\fR
.ad
.sp .6
.RS 4n
コマンドのバージョンを表示します。
.sp
このオプションには、サブコマンドやオペランドなどのオプションは指定しないでください。サブコマンドやオペランドなどのオプションは無視されます。\fB-V\fRオプションは、コマンドのバージョンを表示するだけです。その他の処理は行いません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB\fB--verbose\fR\fR
.ad
.sp .6
.RS 4n
詳細なメッセージを標準出力に表示します。
.sp
このオプションは、このコマンドの任意の形式とともに指定できます。
.sp
\fB-v\fR オプションと \fB-o\fR\fB -\fR オプションを同時に指定してはいけません。\fB-v\fR オプ ションは無視されます。\fB-o\fR\fB -\fR オプションは、ほかのすべての標準出力を抑制します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-X\fR \fInode\fR[:\fIzone\fR][,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--auxnode \fR\fInode\fR\fB-[:\fR\fIzone\fR\fB-][,\&.\|.\|.]\fR\fR
.ad
.sp .6
.RS 4n
\fBAuxNodeList\fR\fB SharedAddress\fR リソースプロパティーを設定します。
.sp
\fBAuxNodeList\fR リスト内のノードは、共有アドレスリソースに関連付けられている論理ホストのセットをホストできます。ただし、フェイルオーバー時に主ノードの役割を果たすことはできません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-z\fR \fIzone\fR\fR
.ad
.br
.na
\fB\fB--zone \fR\fIzone\fR\fR
.ad
.sp .6
.RS 4n
ゾーンが明示的に指定されていないノードリスト内のすべてのノードに、同じゾーン名を適用します。このオプションと同時に指定できるのは、\fB-n\fR オプションだけです。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-Z\fR {\fIzoneclustername\fR | \fBglobal\fR | \fBall\fR}\fR
.ad
.br
.na
\fB\fB--zoneclustername=\fR{\fIzoneclustername\fR | \fBglobal\fR | \fBall\fR}\fR
.ad
.br
.na
\fB\fB--zoneclustername\fR {\fIzoneclustername\fR | \fBglobal\fR | \fBall\fR}\fR
.ad
.sp .6
.RS 4n
リソースが存在する場合、操作する必要がある 1 つまたは複数のクラスタを指定します。
.sp
このオプションは、\fBexport\fR サブコマンド以外のすべてのサブコマンドでサポートされています。
.sp
このオプションを指定すると、次のリストから 1 つの引数を指定する必要もあります。
.sp
.ne 2
.mk
.na
\fB\fIzoneclustername\fR\fR
.ad
.RS 20n
.rt  
このオプションを使用するコマンドが、\fIzoneclustername\fR という名前のゾーンクラスタでのみ指定されたすべてのリソースで機能するように指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBglobal\fR\fR
.ad
.RS 20n
.rt  
このオプションを使用するコマンドが、広域クラスタでのみ指定されたすべてのリソースで機能するように指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBall\fR\fR
.ad
.RS 20n
.rt  
広域クラスタでこの引数を使用する場合、それを使用するコマンドが広域クラスタとすべてのゾーンクラスタを含め、すべてのクラスタで指定されたすべてのリソースで機能するように指定します。
.sp
ゾーンクラスタでこの引数を使用する場合、この引数を使用するコマンドがそのゾーンクラスタでのみ指定されたすべてのリソースで機能するように指定します。
.RE

.RE

.SH オペランド
.sp
.LP
次のオペランドがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fIresource\fR\fR
.ad
.RS 20n
.rt  
Sun Cluster のリソース名をオペランドとして受け付けるように指定します。サブコマンドで複数のリソースを指定できる場合は、プラス記号 (\fB+\fR) を使用すると、すべてのリソースを指定できます。
.RE

.SH 終了ステータス
.sp
.LP
指定したすべてのオペランドでコマンドが成功すると、コマンドはゼロ (\fBCL_NOERR\fR) を返します。あるオペランドでエラーが発生すると、コマンドはオペランドリストの次のオペランドを処理します。戻り値は常に、最初に発生したエラーを反映します。
.sp
.LP
このコマンドは、次の終了ステータスコードを返します。
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
エラーなし
.sp
実行したコマンドは正常に終了しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
十分なスワップ空間がありません。
.sp
クラスタノードがスワップメモリーまたはその他のオペレーティングシステムリソースを使い果たしました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
無効な引数
.sp
コマンドを間違って入力したか、\fB-i\fR オプションで指定したクラスタ構成情報の構文が間違っていました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
アクセス権がありません
.sp
指定したオブジェクトにアクセスできません。このコマンドを実行するには、スーパーユーザーまたは RBAC アクセスが必要である可能性があります。詳細は、\fBsu\fR(1M)、および \fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB9\fR \fBCL_ESTATE\fR\fR
.ad
.sp .6
.RS 4n
オブジェクトの状態が不正です
.sp
その時点で変更できない、または常時変更できないプロパティー、リソースグループ、またはその他のオブジェクトを変更しようとしました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB10\fR \fBCL_EMETHOD\fR\fR
.ad
.sp .6
.RS 4n
リソースメソッドが失敗しました
.sp
リソースのメソッドが失敗しました。次のいずれかの理由のために、メソッドは失敗しました。
.RS +4
.TP
.ie t \(bu
.el o
\fBvalidate\fR メソッドは、リソースを作成しようとしたときに、あるいは、リソースのプロパティーを変更しようとしたときに失敗しました。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBvalidate\fR 以外のメソッドは、リソースを有効、無効、または削除しようとしたときに失敗しました。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB15\fR \fBCL_EPROP\fR\fR
.ad
.sp .6
.RS 4n
無効なプロパティーです
.sp
\fB-p\fR、\fB-y\fR、または \fB-x\fR オプションで指定したプロパティーまたは値が存在しないか、許可されていません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O エラー
.sp
物理的な入出力エラーが発生しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
そのようなオブジェクトはありません。
.sp
次のいずれかの理由のために、指定したオブジェクトを見つけることができません。
.RS +4
.TP
.ie t \(bu
.el o
オブジェクトが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-o\fR オプションで作成しようとした構成ファイルへのパスのディレクトリが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-i\fR オプションでアクセスしようとした構成ファイルにエラーが含まれています。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB39\fR \fBCL_EEXIST\fR\fR
.ad
.sp .6
.RS 4n
オブジェクトは存在します。
.sp
指定したデバイス、デバイスグループ、クラスタインターコネクトコンポーネント、ノード、クラスタ、リソース、リソースタイプ、またはリソースグループはすでに存在します。
.RE

.sp
.LP
これらの終了値は、scha_calls(3HA) のマニュアルページで規定されているリターンコードと互換性があります。
.SH 使用例
.LP
\fB例 1 \fR共有アドレスリソースの作成
.sp
.LP
このコマンドは、\fBsharedhost1\fR という名前のリソースを \fBrg-failover\fR という名前のリソースグループ内に作成します。リソースは有効な状態で作成され、監視も有効になっています。

.sp
.in +2
.nf
# \fBclressharedaddress create -g rg-failover sharedhost1\fR
.fi
.in -2
.sp

.sp
.LP
次の 2 つのコマンドのうち、どちらかがゾーンクラスタ ZC に \fBsharedhost1\fR という名前のリソースを作成します。これらのコマンドは、広域クラスタノードで、またはゾーンクラスタ ZC の内部で実行できます。

.sp
.in +2
.nf
# \fBclressharedaddress create -g rg-failover -Z ZC sharedhost1\fR
.fi
.in -2
.sp

.sp
.in +2
.nf
# \fBclressharedaddress create -g rg-failover ZC:sharedhost1\fR
.fi
.in -2
.sp

.LP
\fB例 2 \fR異なる論理ホスト名を持つ共有アドレスリソースの作成
.sp
.LP
このコマンドは、\fBrs-sharedhost1\fR という名前のリソースを \fBrg-failover\fR という名前のリソースグループ内に作成します。

.sp
.LP
論理ホスト名はリソース名と同じではありませんが、論理ホストの名前と IP アドレスは同じままです。

.sp
.in +2
.nf
# \fBclressharedaddress create -g rg-failover \\
-h sharedhost1 rs-sharedhost1\fR
.fi
.in -2
.sp

.LP
\fB例 3 \fR共有アドレスリソースの IPMP グループの指定
.sp
.LP
このマンドは、\fBsharedhost1\fR リソースの IPMP グループを設定します。

.sp
.in +2
.nf
# \fBclressharedaddress create -g rg-failover \\
-N ipmp0@black,ipmp0@white sharedhost1\fR
.fi
.in -2
.sp

.LP
\fB例 4 \fR共有アドレスリソースの削除
.sp
.LP
このコマンドは、\fBsharedhost1\fR という名前のリソースを削除します。

.sp
.in +2
.nf
# \fBclressharedaddress delete sharedhost1\fR
.fi
.in -2
.sp

.LP
\fB例 5 \fR共有アドレスリソースの一覧表示
.sp
.LP
このコマンドは、すべての共有アドレスリソースを一覧表示します。

.sp
.in +2
.nf
# \fBclressharedaddress list\fR
sharedhost1
sharedhost2
.fi
.in -2
.sp

.LP
\fB例 6 \fR共有アドレスリソース、リソースグループ、およびリソースタイプの一覧表示
.sp
.LP
このコマンドは、すべての共有アドレスリソースをそれらのリソースグループおよびリソースタイプと合わせて一覧表示します。

.sp
.in +2
.nf
# \fBclressharedaddress list -v\fR
Resources   Resource Groups Resource Types
---------   --------------- --------------
sharedhost1 rg-failover-1   SUNW.SharedAddress
sharedhost2 rg-failover-2   SUNW.SharedAddress
.fi
.in -2
.sp

.LP
\fB例 7 \fR共有アドレスリソースの拡張プロパティーの一覧表示
.sp
.LP
このコマンドは、すべての共有アドレスリソースの拡張プロパティーを一覧表示します。

.sp
.in +2
.nf
# \fBclressharedaddress list-props -v\fR
Properties       Descriptions
----------       ------------
NetIfList        List of IPMP groups on each node
AuxNodeList      List of nodes on which this resource is available
HostnameList     List of hostnames this resource manages
CheckNameService Name service check flag
.fi
.in -2
.sp

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWsczu
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
Intro(1CL)、cluster(1CL), clresource(1CL)、clreslogicalhostname(1CL)、clresourcegroup(1CL)、clresourcetype(1CL)、scha_calls(3HA)、clconfiguration(5CL)、\fBrbac\fR(5)、r_properties(5)
.SH 注意事項
.sp
.LP
スーパーユーザーはこのコマドのすべての形式を実行できます。
.sp
.LP
任意のユーザーは次のオプションを指定してこのコマンドを実行できます。
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR オプション
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR オプション
.RE
.sp
.LP
サブコマンドを指定してこのコマンドを実行する場合、スーパーユーザー以外のユーザーは RBAC の承認が必要です。次の表を参照してください。
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
サブコマンドRBAC の承認
_
\fBcreate \fR\fB solaris.cluster.modify\fR
_
\fBdelete\fR\fB solaris.cluster.modify\fR
_
\fBdisable\fR\fBsolaris.cluster.admin\fR
_
\fBenable\fR\fBsolaris.cluster.admin\fR
_
\fBexport\fR\fBsolaris.cluster.read\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBlist-props\fR\fBsolaris.cluster.read\fR
_
\fBmonitor\fR\fBsolaris.cluster.admin\fR
_
\fBreset\fR\fBsolaris.cluster.admin\fR
_
\fBset\fR\fB solaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstatus\fR\fBsolaris.cluster.read\fR
_
\fBunmonitor\fR\fBsolaris.cluster.admin\fR
.TE

