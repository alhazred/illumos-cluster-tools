'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_get_resource_group_name  3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_get_resource_group_name  \- リソースグループ名の取得
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fB const char *\fR\fBscds_get_resource_group_name\fR(\fBscds_handle_t\fR \fIhandle\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_get_resource_group_name()\fR 関数は、呼び出し元プログラムに渡されるリソースが含まれる、リソースグループ名へのポインタを返します。このポインタは、DSDL に属するメモリーを指定しています。このメモリーは変更できません。このポインタは、\fBscds_close()\fR 呼び出しによって無効化されます。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
\fBscds_initialize()\fR から返されるハンドルです。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBNULL\fR\fR
.ad
.RS 20n
.rt  
以前に scds_initialize(3HA) を呼び出していないなどのエラー条件を示します。
.RE

.sp
.LP
その他のエラーコードについては、scha_calls(3HA)を参照してください。
.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_close(3HA)、scds_initialize(3HA)、scha_calls(3HA)、\fBattributes\fR(5)
