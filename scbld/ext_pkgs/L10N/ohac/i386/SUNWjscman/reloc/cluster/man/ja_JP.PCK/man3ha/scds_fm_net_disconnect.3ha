'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_fm_net_disconnect  3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_fm_net_disconnect \- アプリケーションとの TCP 接続の終了
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fB scds_fm_net_disconnect\fR (\fBscds_handle_t\fR \fIhandle\fR, \fBscds_socket_t *\fR\fIsocklist\fR, \fBint\fR \fIcount\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_fm_net_disconnect()\fR 関数は、監視されているプロセスとの TCP 接続を終了します。
.sp
.LP
指定された \fBtimeout\fR 間隔で、\fBsocklist\fR 配列内のすべての有効なソケット接続の終了が試行されます。終了時、\fBsocklist\fR の各メンバーに \fBSCDS_FMSOCK_NA\fR という値が追加されます。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIsocklist\fR\fR
.ad
.RS 20n
.rt  
scds_fm_net_connect(3HA) から返されるソケットリストです。これは入出力引数です。
.RE

.sp
.ne 2
.mk
.na
\fB\fIcount\fR\fR
.ad
.RS 20n
.rt  
\fIsocklist\fR 配列のメンバー数です。このパラメータは \fBSCDS_MAX_IPADDR_TYPES\fR に設定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
タイムアウト値 (秒) です。各ソケットはこれと同じ時間をタイムアウトとして使用して、接続を切断しようとします。これらの時間は並行に進行するため、この値は事実上、関数の実行にかかる最大時間となります。
.RE

.SH 戻り値
.sp
.LP
\fBscds_fm_net_disconnect()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_INVAL\fR\fR
.ad
.RS 20n
.rt  
関数を呼び出したときのパラメータが無効です。
.RE

.sp
.ne 2
.mk
.na
\fBその他の 0 以外の値\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。障害コードの意味については、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_fm_net_connect(3HA)、scds_fm_tcp_disconnect(3HA)、scds_initialize(3HA)、scha_calls(3HA)、\fBattributes\fR(5)
