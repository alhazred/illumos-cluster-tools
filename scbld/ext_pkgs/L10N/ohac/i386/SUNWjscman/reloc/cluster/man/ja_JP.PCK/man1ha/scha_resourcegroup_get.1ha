'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scha_resourcegroup_get 1HA "2008 年 9 月 11 日" "Sun Cluster 3.2" "Sun Cluster コマンド"
.SH 名前
scha_resourcegroup_get \- リソースグループ情報へのアクセス
.SH 形式
.LP
.nf
\fBscha_resourcegroup_get\fR \fB-O\fR \fI optag\fR \fB-G\fR \fIgroup\fR [\fI args\fR]
.fi

.SH 機能説明
.sp
.LP
\fBscha_resourcegroup_get\fR コマンドは、クラスタの Resource Group Manager (\fBRGM\fR) の制御下にあるリソースグループの情報にアクセスします。
.sp
.LP
このコマンドは、リソースタイプに対するコールバックメソッドのシェルスクリプト実装で使用するためのものです。このようなリソースタイプは、クラスタの \fBRGM\fR によって制御されるサービスを表しています。このコマンドを実行すると、scha_resourcegroup_get(3HA) C 関数を実行したときと同じ情報が得られます。
.sp
.LP
この情報は、scha_cmds(1HA) で説明されている書式付き文字列として標準出力 (\fBstdout\fR) に出力されます。各行に単一または複数の文字列が出力されます。あとでスクリプトで使用できるように、この出力内容をシェル変数に格納し、シェルまたは \fBawk\fR(1) で解析することができます。
.sp
.LP
このコマンドを使用するには \fBsolaris.cluster.resource.read\fR の役割に基づくアクセス制御 (RBAC) が必要です。\fBrbac\fR(5)を参照してください。
.sp
.LP
さらにこのコマンドを使用するにあたっては、Sun Cluster コマンド権プロファイルを割り当てておく必要があります。認証されたユーザーは、\fBpfsh\fR(1)、\fBpfcsh\fR(1)、または \fBpfksh\fR(1) プロファイルシェルから、Sun Cluster の特権コマンドをコマンド行で実行できます。プロファイルシェルは、Sun Cluster コマンド権プロファイルに割り当てられた Sun Cluster の特権コマンドへのアクセスを可能にする特別なシェルです。プロファイルシェルは、\fBsu\fR(1M) を実行すると起動されます。\fBpfexec\fR(1) を使用しても、Sun Cluster の特権コマンドを実行できます。
.SH オプション
.sp
.LP
次のオプションがサポートされています。 
.sp
.ne 2
.mk
.na
\fB\fB-G\fR \fIgroup\fR\fR
.ad
.RS 13n
.rt  
リソースグループの名前。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-O\fR \fIoptag\fR\fR
.ad
.RS 13n
.rt  
アクセスする情報を指定します。指定した \fIoptag\fR によっては、情報を取得するクラスタノードまたはゾーンを示すオペランドを追加する必要があります。
.LP
注 - 
.sp
.RS 2
\fBDESIRED_PRIMARIES\fR や \fBFAILBACK\fR などの \fIoptag\fR 値には、大文字と小文字の区別はありません。\fB\fR\fIoptag\fR オプションを指定するときには、大文字と小文字の任意の組み合わせを使用できます。
.RE
次の \fIoptag\fR 値は、対応するリソースグループのプロパティーを取得します。リソースグループの名前付きプロパティー値が生成されます。\fBRG_STATE\fR プロパティーは、コマンドの実行元である特定のノードまたはゾーン上の値を示します。 
.sp
.in +2
.nf
AUTO_START_ON_NEW_CLUSTER
DESIRED_PRIMARIES
FAILBACK
GLOBAL_RESOURCES_USED
IMPLICIT_NETWORK_DEPENDENCIES
MAXIMUM_PRIMARIES
NODELIST
PATHPREFIX
PINGPONG_INTERVAL
RESOURCE_LIST
RG_AFFINITIES
RG_DEPENDENCIES
RG_DESCRIPTION
RG_IS_FROZEN
RG_MODE
RG_PROJECT_NAME
RG_SLM_TYPE
RG_SLM_PSET_TYPE
RG_SLM_CPU
RG_SLM_CPU_MIN
RG_STATE
RG_STATE_NODE
RG_SYSTEM
SUSPEND_AUTOMATIC_RECOVERY
.fi
.in -2

.RE

.LP
注 - 
.sp
.RS 2
\fBRG_STATE_NODE\fR には、ノードまたはゾーンを指定するフラグなし引数が必要です。この \fIoptag\fR 値は、指定したノードまたはゾーンに対するリソースグループの \fBRG_STATE\fR プロパティーの値を生成します。フラグなしの引数が非大域ゾーンを指定する場合、その形式は \fInodename\fR:\fI zonename\fR となります。
.RE
.SH 使用例
.LP
\fB例 1 \fR\fBscha_resourcegroup_get\fR を使用するサンプルスクリプト
.sp
.LP
次のスクリプトには、必要なリソースグループ名を示す引数 \fB-G\fR が渡されます。次に、\fBscha_resourcegroup_get\fR コマンドにより、リソースグループ内のリソースのリストが取得されます。

.sp
.in +2
.nf
#!/bin/sh

while getopts G: opt
do
    case $opt in
         G)      group="$OPTARG";;
    esac
done

resource_list=`scha_resourcegroup_get -O Resource_list -G $group`

for resource in $resource_list
do
    printf "Group: %s contains resource: %s\n" "$group" "$resource"
done
.fi
.in -2
.sp

.SH 終了ステータス
.sp
.LP
次の終了ステータスコードが返されます。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 13n
.rt  
コマンドは正常に完了しました。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 13n
.rt  
エラーが発生しました。
.sp
障害エラーコードについては、scha_calls(3HA)を参照してください。 
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性安定
.TE

.SH 関連項目
.sp
.LP
\fBawk\fR(1)、scha_cmds(1HA)、scha_calls(3HA)、scha_resourcegroup_get(3HA)、\fBattributes\fR(5)、rg_properties(5)、\fBrbac\fR(5)
