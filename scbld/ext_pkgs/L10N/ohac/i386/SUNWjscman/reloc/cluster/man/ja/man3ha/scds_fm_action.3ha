'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_fm_action 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_fm_action \- プローブ完了関数のあとでアクションを実行する
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_fm_action\fR (\fBscds_handle_t\fR \fIhandle\fR, \fBint\fR \fIprobe_status\fR, \fBlong\fR \fIelapsed_milliseconds\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_fm_action()\fR 関数は、データサービスの \fBprobe_status\fR と過去の障害の履歴を元に、次のいずれかのアクションを実行します。
.RS +4
.TP
.ie t \(bu
.el o
アプリケーションを再起動します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースグループのフェイルオーバーを行います。
.RE
.RS +4
.TP
.ie t \(bu
.el o
何もしません。
.RE
.sp
.LP
引数 \fBprobe_status\fR の値で、障害の重要度を表します。たとえば、アプリケーションに接続する処理の失敗を完全な失敗とみなし、アプリケーションとの接続を切断する処理を部分的な失敗とみなすことができます。後者の場合、\fBprobe_status\fR に 0 から \fBSCDS_PROBE_COMPLETE_FAILURE\fR までの値を指定します。
.sp
.LP
DSDL の定義では、\fBSCDS_PROBE_COMPLETE_FAILURE\fR は \fB100\fR です。成功または失敗の部分的なプローブには、\fB0\fR から \fBSCDS_PROBE_COMPLETE_FAILURE\fR までの値を使用します。
.sp
.LP
その後、\fBscds_fm_action()\fR を呼び出すことにより、\fBprobe_status\fR 入力パラメータの値とリソースの \fBRetry_interval\fR プロパティーによって定義された時間間隔が足し合わされ、障害履歴が計算されます。\fBRetry_interval\fR よりも古い障害履歴はメモリーからパージされ、再起動やフェイルオーバーの決定には使用されません。
.sp
.LP
\fBscds_fm_action()\fR 関数は、次のアルゴリズムを使って、実行するアクションを選択します。
.sp
.ne 2
.mk
.na
\fB\fB再起動\fR\fR
.ad
.RS 20n
.rt  
累積された障害履歴が \fBSCDS_PROBE_COMPLETE_FAILURE\fR に達すると、\fBscds_fm_action()\fR はリソースの \fBSTOP\fR メソッドと \fB START\fR メソッドを順番に呼び出し、リソースを再起動します。リソースタイプに定義された \fBPRENET_START\fR メソッドや \fBPOSTNET_STOP\fR メソッドは無視します。
.sp
リソースのステータスは、そのリソースがすでに設定されている場合を除き、\fBscha_resource_setstatus()\fR 呼び出しによって \fBSCHA_RSSTATUS_DEGRADED\fR に設定されます。
.sp
リソースの \fBSTART\fR メソッドや \fBSTOP\fR メソッドの失敗により再起動の試行に失敗した場合、\fBscha_control()\fR が \fBGIVEOVER\fR オプション付きで呼び出され、リソースグループが別のノードまたはゾーンにフェイルオーバーされます。\fBscha_control()\fR 呼び出しに成功した場合、リソースグループは別のクラスタノードまたはゾーンにフェイルオーバーされます。このとき、\fBscds_fm_action()\fR 呼び出しは戻りません。
.sp
再起動に成功すると、障害履歴はパージされます。次の再起動は、障害履歴が再び \fBSCDS_PROBE_COMPLETE_FAILURE\fR に達するまで試行されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBFailover\fR\fR
.ad
.RS 20n
.rt  
\fBscds_fm_action()\fR 呼び出しが繰り返されることによって、試行される再起動回数がリソースに定義された \fBRetry_count\fR 値に達した場合、\fBscha_control()\fR と \fBGIVEOVER\fR オプションが呼び出され、フェイルオーバーが試行されます。
.sp
リソースのステータスは、そのリソースがすでに設定されている場合を除き、\fBscha_resource_setstatus()\fR 呼び出しによって \fBSCHA_RSSTATUS_FAULTED\fR に設定されます。
.sp
\fBscha_control()\fR 呼び出しに失敗した場合、\fBscds_fm_action()\fR によって管理されている障害履歴全体がクリアされます。
.sp
\fBscha_control()\fR 呼び出しに成功した場合、リソースグループは別のクラスタノードまたはゾーンにフェイルオーバーされます。このとき、\fBscds_fm_action()\fR 呼び出しは戻りません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBアクションなし\fR\fR
.ad
.RS 20n
.rt  
障害の蓄積された履歴が \fBSCDS_PROBE_COMPLETE_FAILURE\fR の下に残っている場合、アクションは行われません。さらに、サービスのチェックが正常に行われ、\fBprobe_status\fR 値が \fB0\fR になっている場合も、障害履歴は考慮されず、アクションは実行されません。
.sp
リソースのステータスは、そのリソースがすでに設定されている場合を除き、\fBscha_resource_setstatus()\fR 呼び出しによって \fBSCHA_RSSTATUS_OK\fR に設定されます。
.RE

.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 28n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIprobe_status\fR\fR
.ad
.RS 28n
.rt  
0 から \fBSCDS_PROBE_COMPLETE_FAILURE\fR までの指定の値です。データサービスの状態を示します。\fB0\fR は、最近のデータサービスの検査が正常に実行されたことを示します。\fBSCDS_PROBE_COMPLETE_FAILURE\fR は、完全な障害を示します。サービスは完全に失敗しました。サービスの部分的な障害を示す、0 から \fBSCDS_PROBE_COMPLETE_FAILURE\fR までの値を指定することもできます。
.RE

.sp
.ne 2
.mk
.na
\fB\fIelapsed_milliseconds\fR\fR
.ad
.RS 28n
.rt  
データサービスの検査を完了するまでの時間 (ミリ秒) です。この値は、将来の拡張のために予約されています。
.RE

.SH 戻り値
.sp
.LP
\fBscds_fm_action()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 20n
.rt  
アクションは実行されませんでした。または、再起動が正常に試行されました。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_FAIL\fR\fR
.ad
.RS 20n
.rt  
フェイルオーバーが試行されましたが、正常に実行されませんでした。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOMEM\fR\fR
.ad
.RS 20n
.rt  
システムメモリーが不足しています。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_fm_sleep(3HA)、scds_initialize(3HA)、scha_calls(3HA)、scha_control(3HA)、scds_fm_print_probes(3HA)、scha_resource_setstatus(3HA)、\fBattributes\fR(5)
