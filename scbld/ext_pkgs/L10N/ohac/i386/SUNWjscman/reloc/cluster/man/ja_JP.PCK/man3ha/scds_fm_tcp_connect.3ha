'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_fm_tcp_connect 3HA  "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_fm_tcp_connect \- アプリケーションとの TCP 接続の確立
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_fm_tcp_connect\fR (\fBscds_handle_t\fR \fIhandle\fR, \fBint *\fR\fIsock\fR, \fBconst char*\fR\fIhostname\fR, \fBint\fR \fIport\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_fm_tcp_connect()\fR 関数は、監視対象のプロセスとの TCP 接続を確立します。
.sp
.LP
scds_get_rs_hostnames(3HA) または scds_get_rg_hostnames(3HA) のいずれかを使用してホスト名を取得します。
.sp
.LP
この関数の代わりに scds_fm_net_connect(3HA) の使用を検討します。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIsock\fR\fR
.ad
.RS 20n
.rt  
この関数によって確立されるソケットへのハンドルです。このパラメータは、この関数が設定する出力引数です。
.RE

.sp
.ne 2
.mk
.na
\fB\fIhostname\fR\fR
.ad
.RS 20n
.rt  
プロセスが待機しているホスト名です。\fIhostname\fR が IPv4 アドレスだけにマッピングするか、IPv4 と IPv6 アドレスの両方にマッピングする場合、この関数は接続するアドレスとして IPv4 マッピングを使用します。\fIhostname\fR が IPv6 アドレスだけにマッピングする場合、この関数は接続するアドレスとして IPv6 マッピングを使用します。
.RE

.sp
.ne 2
.mk
.na
\fB\fIport\fR\fR
.ad
.RS 20n
.rt  
TCP ポート番号です。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
タイムアウト値 (秒) です。
.RE

.SH 戻り値
.sp
.LP
\fBscds_fm_tcp_connect()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
関数が正常に終了。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_STATE\fR\fR
.ad
.RS 28n
.rt  
ソケットでの接続を初期化する試みがタイムアウト以外の理由で失敗。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
関数がタイムアウト。
.RE

.sp
.ne 2
.mk
.na
\fBその他の値\fR
.ad
.RS 28n
.rt  
関数の実行に失敗。障害コードの意味については、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性非推奨
.TE

.SH 関連項目
.sp
.LP
scds_fm_net_connect(3HA)、scds_fm_tcp_disconnect(3HA)、scds_get_rg_hostnames(3HA)、scds_get_rs_hostnames(3HA)、scds_initialize(3HA)、scha_calls(3HA)、\fBattributes\fR(5)
