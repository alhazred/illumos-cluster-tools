'\" te
.\" Copyright 2007 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_syslog 3HA "2007 年 9 月 7 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_syslog \- システムログにメッセージを書き込む
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h> \fBvoid\fR \fBscds_syslog\fR(\fBint\fR \fI priority\fR, \fBconst char*\fR\fIformat\fR... 
.fi

.SH 機能説明
.sp
.LP
\fBscds_syslog()\fR 関数は、システムログにメッセージを書き込みます。scha_cluster_getlogfacility(3HA) 関数から返される機能を使用します。これらのメッセージは、適切なログファイルとユーザーに転送できます。詳細は、\fBsyslog.conf\fR(4)を参照してください。
.sp
.LP
すべての syslog メッセージには、次の接頭辞がつきます。\fBSC[<\fIresourceTypeName \fR>,<\fIresourceGroupName\fR>,<\fI resourceName\fR>,<\fImethodName\fR>\fR
.LP
注意 - 
.sp
.RS 2
システムログに書き込まれるメッセージは多言語化されません。この関数と \fBgettext()\fR などのメッセージ変換関数を併用しないでください。
.RE
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIpriority\fR\fR
.ad
.RS 20n
.rt  
\fBsyslog\fR(3C) によって指定されるメッセージ優先順位です。
.RE

.sp
.ne 2
.mk
.na
\fB\fIformat\fR\fR
.ad
.RS 20n
.rt  
\fBprintf\fR(3C) によって指定されるメッセージ形式の文字列です。
.RE

.sp
.ne 2
.mk
.na
\fB\fI\&...\fR\fR
.ad
.RS 20n
.rt  
\fBprintf()\fR によって指定される変数です。\fIformat\fR パラメータで表されます。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
\fBprintf\fR(3C)、scds_syslog_debug(3HA)、scha_cluster_getlogfacility(3HA)、\fBsyslog\fR(3C)、\fBsyslog.conf\fR(4)、\fBattributes\fR(5) 
