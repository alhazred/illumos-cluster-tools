'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_fm_print_probes 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_fm_print_probes \- プローブデバッグ情報の印刷
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBvoid\fR \fBscds_fm_print_probes\fR (\fBscds_handle_t \fR \fIhandle\fR, \fBint\fR \fIdebug_level\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_fm_print_probes()\fR 関数は、scds_fm_action(3HA) によって報告されたプローブステータス情報をシステムログに書き込みます。この情報には、DSDL によって管理されるすべてのプローブステータス履歴とタイムスタンプのリストが含まれます。
.sp
.LP
DSDL の定義では、最大デバッグレベル \fBSCDS_MAX_DEBUG_LEVEL\fR は \fB9\fR です。
.sp
.LP
現在使用中のデバッグレベルよりも \fBdebug_level\fR を上げた場合、情報は書き込まれません。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIdebug_level\fR\fR
.ad
.RS 20n
.rt  
データが書き込まれるデバッグレベルです。\fB1\fR から \fBSCDS_MAX_DEBUG_LEVEL\fR (DSDL では \fB9\fR と定義) までの整数値です。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_fm_action(3HA)、scds_initialize(3HA)、scds_syslog_debug(3HA)、\fBattributes\fR(5)
