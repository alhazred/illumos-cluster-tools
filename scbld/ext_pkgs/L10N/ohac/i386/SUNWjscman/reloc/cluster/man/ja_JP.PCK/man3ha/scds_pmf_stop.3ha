'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_pmf_stop 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_pmf_stop \- PMF の制御下で実行中のプロセスを終了する
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fB scds_pmf_stop\fR(\fBscds_handle_t \fR \fIhandle\fR, \fBscds_pmf_type_t\fR \fI program_type\fR, \fBint\fR \fIinstance\fR, \fBint\fR \fIsignal\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_pmf_stop()\fR 関数は、PMF の制御下で実行中のプログラムを停止します。この関数は、\fB-s\fR オプションを指定した pmfadm(1M) コマンドと同じです。
.sp
.LP
要求されたインスタンスが実行中でない場合、\fBscds_pmf_stop()\fR は \fBSCHA_ERR_NOERR\fR を返します。
.sp
.LP
要求されたインスタンスが実行中の場合、このインスタンスに指定のシグナルが送信されます。タイムアウト値の 80% が経過する間に終了しなかったインスタンスには、\fBSIGKILL\fR が送信されます。このインスタンスが、さらにタイムアウト値の 15% が経過する間に終了しなかった場合、この関数は失敗したとみなされ、\fBSCHA_ERR_TIMEOUT\fR が返されます。タイムアウト値の残りの 5% は、この関数のオーバーヘッドによって消費されたものと見なされます。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIprogram_type\fR\fR
.ad
.RS 20n
.rt  
実行するプログラムの型です。有効な型は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fBSCDS_PMF_TYPE_SVC\fR\fR
.ad
.RS 28n
.rt  
データサービスアプリケーション
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCDS_PMF_TYPE_MON\fR\fR
.ad
.RS 28n
.rt  
障害モニター
.RE

.sp
.ne 2
.mk
.na
\fB\fB\fR\fBSCDS_PMF_TYPE_OTHER\fR\fR
.ad
.RS 28n
.rt  
その他
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fIinstance\fR\fR
.ad
.RS 20n
.rt  
複数のインスタンスを持つリソースの場合、この整数 (0 以上) はインスタンスを一意に識別します。単一のインスタンスの場合、0 を使用します。 
.RE

.sp
.ne 2
.mk
.na
\fB\fIsignal\fR\fR
.ad
.RS 20n
.rt  
インスタンスを強制終了するために送信される Solaris シグナルです。\fBsignal\fR(3HEAD)を参照してください。指定のシグナルでインスタンスを強制終了できない場合は、\fBSIGKILL\fR を使用します。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
タイムアウト値 (秒) です。
.RE

.SH 戻り値
.sp
.LP
\fBscds_pmf_stop()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB\fB0 以外\fR\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
関数はタイムアウト。 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
関数の実行に成功。 
.RE

.sp
.ne 2
.mk
.na
\fBその他の値\fR
.ad
.RS 28n
.rt  
関数の実行に失敗。その他のエラーコードについては、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
pmfadm(1M)、scds_initialize(3HA)、scds_pmf_start(3HA)、scha_calls(3HA)、\fBsignal\fR(3HEAD)、\fBattributes\fR(5)
