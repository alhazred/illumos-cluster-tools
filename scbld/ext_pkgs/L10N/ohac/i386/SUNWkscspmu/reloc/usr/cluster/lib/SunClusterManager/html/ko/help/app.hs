<?xml version='1.0' encoding='UTF-8'?>
<!-- ident   "%Z%%M% %I%   %E% SMI" -->
<!DOCTYPE helpset PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 2.0//EN" "helpset_1_0.dtd">

<helpset version="2.0" xml:lang="ko">
<title>Sun Cluster Manager 온라인 도움말</title>
<maps>
	<homeID></homeID>
	<mapref location="map.jhm"/>
</maps>
<view>
	<name>TOC</name>
	<label>목차</label>
	<type>javax.help.TOCView</type>
	<data>toc.xml</data>
</view>
<view>
	<name>Index</name>
	<label>색인</label>
	<type>javax.help.IndexView</type>
	<data>index.xml</data>
</view>
<view>
	<name>Search</name>
	<label>검색</label>
	<type>javax.help.SearchView</type>
	<data engine="com.sun.java.help.search.DefaultSearchEngine">JavaHelpSearch</data>
</view>
</helpset>
