!   D   �
  �
  @  ��������               .   0   ��������i   d         �   �   ���������   �             ��������?  )        p  Y  ���������  �     
   �  �  ���������  �  	      �  �  ���������  �            ����   �  �  ���������  �          
  ��������G  ?        o  t  ���������  �        �  �  ��������  %        2  ^  ��������f  �        �  �  ���������  �        �  
  ���������  &          ^  ��������  ~        0  �  ����    ~  �  ���������  ,     2   �  ?  ���������  m  "   $     �  ��������7  �  #   '   O  �  ��������v    &   (   �  :  ���������  p  %   -   �  �  ��������  �  *   ,     �  ��������2  �  +   /   Q    ��������u  .  .   0   �  H  ����1   �  f  ���������  �  )   ;      �  ��������?  �  3   5   Y  �  ��������t  	  4   8   �  2	  ���������  <	  7   9   �  b	  ����:   �  v	  ���������  �	  6   ?   1	  �	  ��������C	  �	  <   >   �	  �	  ���������	  
  =   A   �	  *
  ���������	  T
  @   B   #
  �
  ����C   ?
  �
  ��������p
  �
  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 您必須指定要清除的法定 伺服器。
 您必須指定「%s」或法定伺服器清單。
 您必須同時指定 叢集名稱和叢集 ID 兩者。
 您無法同時指定「%s」和法定 伺服器兩者。
 您只能指定「-c」選項一次。
 您只能指定「-I」選項一次。
 您一次只能清除一個法定 伺服器。
 用法: %s <子指令> [<quorum 伺服器> ...]
 用法錯誤。
 用途 無法辨識子指令 -「%s」。

 無法識別的選項 -「%s」。
 未預期的選項 -「%s」。
 要清除的法定伺服器必須已經 從叢集中移除。清除有效的 法定伺服器會影響叢集法定。
 行「%s」中 的預設 法定目錄已 在使用中。
 未針對連接埠「%s」上 的法定伺服器配置叢集名稱 或 ID。
 已停止連接埠「%s」上的 法定伺服器。
 已啟動 連接埠「%s」上的法定伺服器。
 連接埠「%s」上的法定伺服器 未執行。
 未在任何叢集中配置連接埠「%s」上的 法定伺服器。
 已清除連接埠「%s」上的 法定伺服器。
 尚未啟動連接埠「%s」上的 法定伺服器。

 法定伺服器「%s」未 在檔案「%s」中配置。
 行「%$2s」中的法定目錄 「%$1s」不是 唯一的。
 選項「%s」需有引數。
 記憶體不足。
 無效連接埠「%s」。
 檔案「%$2s」中存在無效連接埠 「%$1s」。
 無效的叢集 ID 「%s」。
 內部錯誤。
 無法啟動 連接埠「%s」上的 法定伺服器。如需詳細資訊，請參閱系統記錄檔。
 無法在連接埠「%s」上停用法定裝置。
 您想要繼續嗎 叢集 ID「%s」不是十六進位格式。
 無法在連接埠「%s」上傳送訊息。
 無法在連接埠「%s」上接收訊息。
 無法開啟檔案「%s」。
 無法取得主機「%s」的 IP 節點。
 無法取得收到資料的長度。
 無法於檔案 「%$2s」中執行 「%$1s」行。
 無法在連接埠「%s」上建立通訊端。
 === 連接埠 %s 上的 quorum 伺服器 ===
 %s %s
     保留 金鑰:		0x%llx
     註冊 碼:		0x%llx
        %s <子指令> -? | --help
        %s -V | --version
 
stop	停止 quorum 伺服器 
start	啟動 quorum 伺服器 
show	顯示 quorum 伺服器的配置 
clear	清除法定伺服器的 叢集配置

 
停止 quorum 伺服器
 
啟動 quorum 伺服器
 
顯示 quorum 伺服器的配置
 
選項:
 
管理 quorum 伺服器

子指令:
 
已停用			True

 
已停用			False

 
清除法定伺服器的 叢集配置
 
  節點 ID:			%d
 
  -y	自動在詢問確認時回應 「yes」
 
  -v	詳細輸出


 
  -d	停用法定伺服器
 
  -c <叢集名稱>
	指定叢集名稱
 
  -I <叢集 ID>
	指定十六進位格式的 叢集 ID
 
  -?	顯示說明訊息
 
  ---  叢集 %s (id 0x%8.8X)  保留 ---
 
  ---  叢集 %s (id 0x%8.8X) 註冊 ---
 