!   D   �
  &  @  ��������               .   !   ��������i   E         �   n   ���������   �           �   ��������?  �         p  �   ���������       
   �  %  ���������  *  	      �  E  ���������  _          w  ����   �  �  ���������  �          '  ��������G  L        o  q  ���������  �        �  �  ��������  �        2    ��������f  =        �  k  ���������  �        �  �  ���������  �          �  ��������  �        0  �  ����    ~  8  ���������  \     2   �  i  ���������  �  "   $     �  ��������7  �  #   '   O  �  ��������v     &   (   �    ���������  B  %   -   �  d  ��������  �  *   ,     �  ��������2  �  +   /   Q  �  ��������u  �  .   0   �  �  ����1   �    ���������  ,  )   ;      K  ��������?  n  3   5   Y  �  ��������t  �  4   8   �  �  ���������  �  7   9   �  �  ����:   �  �  ���������  �  6   ?   1	    ��������C	  *  <   >   �	  N  ���������	  `  =   A   �	  v  ���������	  �  @   B   #
  �  ����C   ?
  �  ��������p
  �  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 �z�������w�n�M�����k�w ���A���C
 �z�������w�u%s�v�Ϊk�w���A���M��C
 �z�����P�ɫ��w �O���W�٩M�O�� ID ��̡C
 �z�L�k�P�ɫ��w�u%s�v�M�k�w ���A����̡C
 �z�u����w�u-c�v�ﶵ�@���C
 �z�u����w�u-I�v�ﶵ�@���C
 �z�@���u��M���@�Ӫk�w ���A���C
 �Ϊk: %s <�l���O> [<quorum ���A��> ...]
 �Ϊk���~�C
 �γ~ �L�k���Ѥl���O -�u%s�v�C

 �L�k�ѧO���ﶵ -�u%s�v�C
 ���w�����ﶵ -�u%s�v�C
 �n�M�����k�w���A�������w�g �q�O���������C�M�����Ī� �k�w���A���|�v�T�O���k�w�C
 ��u%s�v�� ���w�] �k�w�ؿ��w �b�ϥΤ��C
 ���w��s����u%s�v�W ���k�w���A���t�m�O���W�� �� ID�C
 �w����s����u%s�v�W�� �k�w���A���C
 �w�Ұ� �s����u%s�v�W���k�w���A���C
 �s����u%s�v�W���k�w���A�� ������C
 ���b�����O�����t�m�s����u%s�v�W�� �k�w���A���C
 �w�M���s����u%s�v�W�� �k�w���A���C
 �|���Ұʳs����u%s�v�W�� �k�w���A���C

 �k�w���A���u%s�v�� �b�ɮסu%s�v���t�m�C
 ��u%$2s�v�����k�w�ؿ� �u%$1s�v���O �ߤ@���C
 �ﶵ�u%s�v�ݦ��޼ơC
 �O���餣���C
 �L�ĳs����u%s�v�C
 �ɮסu%$2s�v���s�b�L�ĳs���� �u%$1s�v�C
 �L�Ī��O�� ID �u%s�v�C
 �������~�C
 �L�k�Ұ� �s����u%s�v�W�� �k�w���A���C�p�ݸԲӸ�T�A�аѾ\�t�ΰO���ɡC
 �L�k�b�s����u%s�v�W���Ϊk�w�˸m�C
 �z�Q�n�~��� �O�� ID�u%s�v���O�Q���i��榡�C
 �L�k�b�s����u%s�v�W�ǰe�T���C
 �L�k�b�s����u%s�v�W�����T���C
 �L�k�}���ɮסu%s�v�C
 �L�k���o�D���u%s�v�� IP �`�I�C
 �L�k���o�����ƪ����סC
 �L�k���ɮ� �u%$2s�v������ �u%$1s�v��C
 �L�k�b�s����u%s�v�W�إ߳q�T�ݡC
 === �s���� %s �W�� quorum ���A�� ===
 %s %s
     �O�d ���_:		0x%llx
     ���U �X:		0x%llx
        %s <�l���O> -? | --help
        %s -V | --version
 
stop	���� quorum ���A�� 
start	�Ұ� quorum ���A�� 
show	��� quorum ���A�����t�m 
clear	�M���k�w���A���� �O���t�m

 
���� quorum ���A��
 
�Ұ� quorum ���A��
 
��� quorum ���A�����t�m
 
�ﶵ:
 
�޲z quorum ���A��

�l���O:
 
�w����			True

 
�w����			False

 
�M���k�w���A���� �O���t�m
 
  �`�I ID:			%d
 
  -y	�۰ʦb�߰ݽT�{�ɦ^�� �uyes�v
 
  -v	�Բӿ�X


 
  -d	���Ϊk�w���A��
 
  -c <�O���W��>
	���w�O���W��
 
  -I <�O�� ID>
	���w�Q���i��榡�� �O�� ID
 
  -?	��ܻ����T��
 
  ---  �O�� %s (id 0x%8.8X)  �O�d ---
 
  ---  �O�� %s (id 0x%8.8X) ���U ---
 