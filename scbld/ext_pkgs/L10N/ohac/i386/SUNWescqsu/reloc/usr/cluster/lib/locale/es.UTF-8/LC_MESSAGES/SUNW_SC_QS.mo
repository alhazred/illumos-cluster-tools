!   D   �
  �  @  ��������               .   ;   ��������i   }         �   �   ���������             5  ��������?  g        p  �  ���������  �     
   �  �  ���������  �  	      �    ���������  /          K  ����   �    ���������  Z          �  ��������G  �        o  2  ���������  u        �  �  ��������          2  H  ��������f  �        �  �  ���������  �        �    ���������  /          [  ��������  }        0  �  ����    ~    ���������  N     2   �  a  ���������  �  "   $     �  ��������7  �  #   '   O  "  ��������v  U  &   (   �  �  ���������  �  %   -   �  �  ��������  %	  *   ,     ,	  ��������2  O	  +   /   Q  s	  ��������u  �	  .   0   �  �	  ����1   �  �	  ���������  
  )   ;      U
  ��������?  �
  3   5   Y  �
  ��������t    4   8   �  ?  ���������  K  7   9   �  ~  ����:   �  �  ���������  �  6   ?   1	  �  ��������C	    <   >   �	  _  ���������	  y  =   A   �	  �  ���������	  �  @   B   #
  5  ����C   ?
  Y  ��������p
  �  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 Debe especificar el servidor del quórum que va a borrar.
 Debe especificar "%s" o una lista de los servidores del quórum.
 Debe especificar tanto el nombre del clúster como su Id.
 No se pueden especificar  "%s" y el servidor del quórum al mismo tiempo.
 Sólo puede especificar la opción "-c" una vez.
 Sólo puede especificar la opción "-I" una vez.
 Sólo puede borrar servidores del quórum de uno en uno.
 Sintaxis: %s <subcommand> [<quorumserver> ...]
 Error de sintaxis.
 Sintaxis Subcomando no reconocido - "%s".

 Opción no reconocida: "%s".
 Opción inesperada - "%s".
 El servidor del quórum que se va a borrar debe haberse eliminado previamente del clúster. Si se borra un servidor del quórum válido, esta acción podría poner en peligro el quórum del clúster .
 Ya se está utilizando el directorio del quórum de la línea "%s".
 El nombre o Id. de clúster no se ha configurado para el servidor del quórum en el puerto "%s".
 Se ha detenido el servidor del quórum en el puerto "%s".
 Se ha iniciado el servidor del quórum en el puerto "%s".
 El servidor del quórum en el puerto "%s" no se está ejecutando.
 El servidor del quórum en el puerto "%s" no se ha configurado en ningún clúster.
 Se ha borrado el servidor del quórum en el puerto "%s".
 El servidor del quórum aún no se ha iniciado en el puerto "%s".

 El servidor del quórum "%s" no se ha configurado en el archivo "%s".
 El directorio del quórum "%s" de la línea "%s" no es exclusivo.
 La opción "%s" necesita un argumento.
 No hay suficiente memoria.
 Puerto no válido "%s".
 Puerto no válido "%s" en el archivo "%s".
 Id. de clúster no válido "%s".
 Error interno.
 No se ha podido iniciar el servidor del quórum en el puerto "%s". Consulte el registro del sistema para obtener información.
 Error al deshabilitar el servidor de quórum en el puerto "%s".
 ¿Desea continuar? El Id. de clúster "%s" no presenta formato hexadecimal.
 No se pueden enviar mensajes en el puerto "%s".
 No se pueden recibir mensajes en el puerto "%s".
 No se puede abrir el archivo "%s".
 No se puede obtener el nodo IP para el host "%s".
 No se puede obtener la longitud de los datos recibidos.
 No se puede ejecutar la línea "%s" en el archivo "%s".
 No se puede crear el socket en el puerto "%s".
 === Servidor del quórum en el puerto %s ===
 %s %s
         Clave de reserva:		0x%llx
         Clave de registro:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Permite detener los servidores del quórum. 
start	Permite iniciar los servidores del quórum. 
show	Muestra la configuración de los servidores del quórum. 
clear	Permite borrar la configuración del clúster de un servidor del quórum.

 
Permite detener los servidores del quórum.
 
Permite iniciar los servidores del quórum.
 
Muestra la configuración de los servidores del quórum.
 
OPCIONES:
 
Administrar servidores del quórum

SUBCOMANDOS:
 
Desactivado			Verdadero

 
Desactivado			Falso

 
Permite borrar la configuración del clúster de un servidor del quórum.
 
  Id. de nodo:			%d
 
  -y	Responde automáticamente "yes" (sí) a la pregunta de confirmación.
 
  -v	Salida detallada


 
  -d	Desactivar servidor de quórum
 
  -c <clustername>
	Permite especificar el nombre del clúster.
 
  -I <clusterID>
	Permite especificar el Id. de clúster con formato hexadecimal.
 
  -?	Muestra un mensaje de ayuda.
 
  ---  Reserva del clúster %s (id 0x%8.8X) ---
 
  ---  Registro del clúster %s (id 0x%8.8X) ---
 