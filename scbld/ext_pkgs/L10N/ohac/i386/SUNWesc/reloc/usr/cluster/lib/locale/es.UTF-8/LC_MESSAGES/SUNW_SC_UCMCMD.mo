   "   =  j     ��������                     ��������(   ,         �   �   ���������   �         �   �   ���������   �         �   �   ���������        
   �     ��������  8  	      4  X  ��������F  l        Q  w  ����   Y    ��������h  �        y  �  ���������  �        �  �  ���������  �        �  �  ���������  �        �    ����     5  ��������%  N        D  m  ��������]  �        u  �  ���������  �        �  �  ���������  �         �    ����!   �  '  ��������  ?  waiting for user: %d
 usage: %s -{h|t}
 usage: %s -s severity -N step-name -n substep-name -t start-time -d duration -r result-code
 usage:
 minimum required quorum: %d
 maximum step number: %d
 local nodeid: %d
 generation stamp: %lu
 current state: %s
 current cluster quorum: %d
 current cluster members: configured nodes: cm_stopall cm_stop cm_reconfigure cm_getcluststate cm_getclustmbyname cm_abortall cm_abort CM_getclustmbyname 	%s stopall clustname
 	%s stop clustname { nodeid | this }
 	%s reconfigure clustname
 	%s iswaiting clustname
 	%s ismember clustname nodeid
 	%s hasquorum clustname
 	%s getstate clustname
 	%s getseqnum clustname
 	%s getlocalnodeid clustname
 	%s getcurrmembers clustname
 	%s getallnodes clustname
 	%s dumpstate clustname
 	%s abortall clustname
 	%s abort clustname { nodeid | this }
 esperando usuario: %d
 sintaxis: %s -{h|t}
 sintaxis: %s -s severity -N step-name -n substep-name -t start-time -d duration -r result-code
 sintaxis:
 quórum mínimo requerido: %d
 número máximo de pasos: %d
 id de nodo local: %d
 marca de generación: %lu
 estado actual: %s
 quórum actual de clúster: %d
 miembros actuales del clúster: nodos configurados: cm_stopall cm_stop cm_reconfigure cm_getcluststate cm_getclustmbyname cm_abortall cm_abort CM_getclustmbyname 	%s stopall clustname
 	%s detener clustname ( nodeid | this )
 	%s reconfigure clustname
 	%s iswaiting clustname
 	%s ismember clustname nodeid
 	%s hasquorum clustname
 	%s getstate clustname
 	%s getseqnum clustname
 	%s getlocalnodeid clustname
 	%s getcurrmembers clustname
 	%s getallnodes clustname
 	%s dumpstate clustname
 	%s abortall clustname
 	%s abandonar clustname ( nodeid | this )
 