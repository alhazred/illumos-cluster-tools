   "   =  m     ��������                     ��������(   ,         �   �   ���������   �         �   �   ���������   �         �   �   ���������        
   �   %  ��������  C  	      4  `  ��������F  v        Q  �  ����   Y  �  ��������h  �        y  �  ���������  �        �  �  ���������  �        �  �  ���������  �        �  !  ����     <  ��������%  U        D  t  ��������]  �        u  �  ���������  �        �  �  ���������  �         �    ����!   �  .  ��������  F  waiting for user: %d
 usage: %s -{h|t}
 usage: %s -s severity -N step-name -n substep-name -t start-time -d duration -r result-code
 usage:
 minimum required quorum: %d
 maximum step number: %d
 local nodeid: %d
 generation stamp: %lu
 current state: %s
 current cluster quorum: %d
 current cluster members: configured nodes: cm_stopall cm_stop cm_reconfigure cm_getcluststate cm_getclustmbyname cm_abortall cm_abort CM_getclustmbyname 	%s stopall clustname
 	%s stop clustname { nodeid | this }
 	%s reconfigure clustname
 	%s iswaiting clustname
 	%s ismember clustname nodeid
 	%s hasquorum clustname
 	%s getstate clustname
 	%s getseqnum clustname
 	%s getlocalnodeid clustname
 	%s getcurrmembers clustname
 	%s getallnodes clustname
 	%s dumpstate clustname
 	%s abortall clustname
 	%s abort clustname { nodeid | this }
 Warten auf Benutzer: %d
 Syntax: %s -{h|t}
 Syntax: %s -s Schweregrad -N Schrittname -n Unterschrittname -t Startzeit -d Dauer -r Ergebniscode
 Syntax:
 Erforderliches Mindest-Quorum: %d
 Maximale Schrittanzahl: %d
 ID des lokalen Knotens: %d
 Generierungsstempel: %lu
 aktueller Zustand: %s
 aktuelles Cluster-Quorum: %d
 aktuelle Cluster-Mitglieder: konfigurierte Knoten: cm_stopall cm_stop cm_reconfigure cm_getcluststate cm_getclustmbyname cm_abortall cm_abort CM_getclustmbyname 	%s stopall clustname
 	%s stop clustname { nodeid | this }
 	%s reconfigure clustname
 	%s iswaiting clustname
 	%s ismember clustname nodeid
 	%s hasquorum clustname
 	%s getstate clustname
 	%s getseqnum clustname
 	%s getlocalnodeid clustname
 	%s getcurrmembers clustname
 	%s getallnodes clustname
 	%s dumpstate clustname
 	%s abortall clustname
 	%s abort clustname { nodeid | this }
 