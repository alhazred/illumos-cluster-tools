<?xml version='1.0' encoding='UTF-8'?>
<!-- ident   "%Z%%M% %I%   %E% SMI" -->
<!DOCTYPE helpset PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 2.0//EN" "helpset_1_0.dtd">

<helpset version="2.0" xml:lang="es">
<title>Ayuda en l&#237;nea de Administrador de Sun Cluster</title>
<maps>
	<homeID></homeID>
	<mapref location="map.jhm"/>
</maps>
<view>
	<name>TOC</name>
	<label>Contenido</label>
	<type>javax.help.TOCView</type>
	<data>toc.xml</data>
</view>
<view>
	<name>Index</name>
	<label>&Iacute;ndice</label>
	<type>javax.help.IndexView</type>
	<data>index.xml</data>
</view>
<view>
	<name>Search</name>
	<label>B&uacute;squeda</label>
	<type>javax.help.SearchView</type>
	<data engine="com.sun.java.help.search.DefaultSearchEngine">JavaHelpSearch</data>
</view>
</helpset>
