   "   =  i     ��������                     ��������(   %         �   �   ���������   �         �   �   ���������   �         �   �   ���������   �      
   �     ��������    	      4  1  ��������F  E        Q  P  ����   Y  X  ��������h  g        y  x  ���������  �        �  �  ���������  �        �  �  ���������  �        �  �  ����       ��������%  1        D  U  ��������]  q        u  �  ���������  �        �  �  ���������  �         �    ����!   �  $  ��������  ?  waiting for user: %d
 usage: %s -{h|t}
 usage: %s -s severity -N step-name -n substep-name -t start-time -d duration -r result-code
 usage:
 minimum required quorum: %d
 maximum step number: %d
 local nodeid: %d
 generation stamp: %lu
 current state: %s
 current cluster quorum: %d
 current cluster members: configured nodes: cm_stopall cm_stop cm_reconfigure cm_getcluststate cm_getclustmbyname cm_abortall cm_abort CM_getclustmbyname 	%s stopall clustname
 	%s stop clustname { nodeid | this }
 	%s reconfigure clustname
 	%s iswaiting clustname
 	%s ismember clustname nodeid
 	%s hasquorum clustname
 	%s getstate clustname
 	%s getseqnum clustname
 	%s getlocalnodeid clustname
 	%s getcurrmembers clustname
 	%s getallnodes clustname
 	%s dumpstate clustname
 	%s abortall clustname
 	%s abort clustname { nodeid | this }
 等待用户: %d
 用法: %s -{h|t}
 用法：%s -s 严重程度 -N 步骤名称 -n 子步骤名称 -t 开始时间 -d 持续时间 -r 结果代码
 用法:
 所需最小法定: %d
 最大步骤数: %d
 本地节点 ID: %d
 生成标记: %lu
 当前状况: %s
 当前群集法定: %d
 当前群集成员: 已配置的节点: cm_stopall cm_stop cm_reconfigure cm_getcluststate cm_getclustmbyname cm_abortall cm_abort CM_getclustmbyname 	%s stopall 群集名称
 	%s 停止群集名称 { nodeid | this }
 	%s 重新配置群集名称
 	%s iswaiting 群集名称
 	%s ismember 群集名称节点 ID
 	%s hasquorum 群集名称
 	%s getstate 群集名称
 	%s getseqnum 群集名称
 	%s getlocalnodeid 群集名称
 	%s getcurrmembers 群集名称
 	%s getallnodes 群集名称
 	%s dumpstate 群集名称
 	%s abortall 群集名称
 	%s 终止群集名称 { nodeid | this }
 