!   D   £
  a  @  ÿÿÿÿÿÿ               .   0   ÿÿÿÿÿÿi   h         ¥      ÿÿÿÿÿÿç   Ö              ÿÿÿÿÿÿ?  *        p  _  ÿÿÿÿÿÿ       
   «    ÿÿÿÿÿÿ±  ¢  	      Ó  Æ  ÿÿÿÿÿÿð  ë            ÿÿÿ       ÿÿÿÿÿÿÕ  Þ          8  ÿÿÿÿÿÿG  h        o    ÿÿÿÿÿÿ  Ê        Ù  
  ÿÿÿÿÿÿ  >        2  s  ÿÿÿÿÿÿf  ·          ÷  ÿÿÿÿÿÿ¼          Ð  9  ÿÿÿÿÿÿä  S            ÿÿÿÿÿÿ  °        0  Â  ÿÿÿ    ~    ÿÿÿÿÿÿ­  T     2   Å  a  ÿÿÿÿÿÿì    "   $     ¾  ÿÿÿÿÿÿ7  ð  #   '   O    ÿÿÿÿÿÿv  D  &   (     t  ÿÿÿÿÿÿÇ  ¦  %   -   ë  Ö  ÿÿÿÿÿÿ  ú  *   ,       ÿÿÿÿÿÿ2    +   /   Q  /  ÿÿÿÿÿÿu  S  .   0     m  ÿÿÿ1   ­    ÿÿÿÿÿÿÍ  ´  )   ;      Û  ÿÿÿÿÿÿ?  	  3   5   Y  8	  ÿÿÿÿÿÿt  W	  4   8   £  z	  ÿÿÿÿÿÿ®  	  7   9   Ô  ®	  ÿÿÿ:   ç  ½	  ÿÿÿÿÿÿû  Í	  6   ?   1	  ú	  ÿÿÿÿÿÿC	  
  <   >   	  ?
  ÿÿÿÿÿÿ	  S
  =   A   ¸	  y
  ÿÿÿÿÿÿç	  ¨
  @   B   #
  á
  ÿÿÿC   ?
    ÿÿÿÿÿÿp
  4  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 NA[·éè«T[o[ðwèµÄ­¾³¢B
 u%svÜ½Íè«T[o[ÌXgðwèµÄ­¾³¢B
 NX^¼ÆNX^ ID Ì¼ûðwèµÄ­¾³¢B
 u%svÆè«T[o[ð¯Éwè·é±ÆÍÅ«Ü¹ñB
 u-cvIvVÍêx¾¯wèÅ«Ü·B
 u-lvIvVÍêx¾¯wèÅ«Ü·B
 êxÉNA[Å«éè«T[o[Í 1 ÂÌÝÅ·B
 gp@: %s <subcommand> [<quorumserver> ...]
 gp@ÌëèB
 gp F¯³êÈ¢TuR}h - u%sv

 IvV u%svÍF¯Å«Ü¹ñB
 \úµÄ¢È¢IvV - u%sv
 NA[·éè«T[o[ÍANX^©çí³êÄ¢éKvª èÜ·BLøÈè«T[o[ðNA[·éÆANX^è«ª¸­µÜ·B
 u%svÌsÌftHgè«fBNgÍ·ÅÉgp³êÄ¢Ü·B
 |[gu%svÌè«T[o[ÉÍA±ÌNX^¼Ü½ÍNX^ ID Í\¬³êÄ¢Ü¹ñB
 |[gu%svÌè«T[o[ªâ~³êÜµ½B
 |[gu%svÅè«T[o[ªN®³êÜµ½B
 |[gu%svÌè«T[o[ÍÒ­µÄ¢Ü¹ñB
 |[gu%svÌè«T[o[ÍANX^É\¬³êÄ¢Ü¹ñB
 |[gu%svÌè«T[o[ªNA[³êÜµ½B
 |[gu%svÅè«T[o[ÍN®³êÄ¢Ü¹ñB

 t@Cu%2$svÉÍAè«T[o[u%1$svÍ\¬³êÄ¢Ü¹ñB
 u%2$svÌsÌè«fBNgu%1$svªêÓÅÍ èÜ¹ñB
 IvVu%svÉÍøªKvÅ·B
 [ªs«µÄ¢Ü·B
 |[gu%svÍ³øÅ·B
 t@Cu%2$svàÉ³øÈ|[gu%1$svªwè³êÄ¢Ü·B
 NX^ IDu%svÍ³øÅ·B
 àG[Å·B
 |[gu%svÅè«T[o[ÌN®É¸sµÜµ½BÚ×ÍAVXeOðQÆµÄ­¾³¢B
 |[gu%svÅè«T[o[Ì³ø»É¸sµÜµ½B
 ±sµÜ·© NX^ IDu%svÍ 16 iÅÍ èÜ¹ñB
 |[gu%svÅbZ[WðMÅ«Ü¹ñÅµ½B
 |[gu%svÅbZ[WðóMÅ«Ü¹ñÅµ½B
 t@Cu%svðJ­±ÆªÅ«Ü¹ñB
 zXgu%svÌ IP m[hðæ¾Å«Ü¹ñB
 óM³ê½·³Ìf[^ðæ¾Å«Ü¹ñÅµ½B
 t@Cu%2$svÌu%1$svÌsÍÀsÅ«Ü¹ñB
 |[gu%svÉ\Pbgðì¬Å«Ü¹ñÅµ½B
 === |[g %s Ìè«T[o[ ===
 %s %s
     \ñL[:		0x%llx
     o^L[:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	è«T[o[ðâ~µÜ·B 
start	è«T[o[ðN®µÜ·B 
show	è«T[o[Ì\¬ð¦µÜ·B 
clear	è«T[o[ÌNX^\¬ðN[AbvµÜ·B

 
è«T[o[ðâ~µÜ·B
 
è«T[o[ðN®µÜ·B
 
è«T[o[Ì\¬ð¦µÜ·B
 
IvV:
 
è«T[o[ÌÇ

TuR}h:
 
³ø			True

 
³ø			False

 
è«T[o[ÌNX^\¬ðÁµÜ·B
 
  m[h ID:			%d
 
  -y	mFÌ¿âÉ©®IÉuyesvÅµÜ·B
 
  -v	ç·ÈoÍ


 
  -d	è«T[o[ð³øÉµÜ·B
 
  -c <clustername>
	NX^¼ðwèµÜ·B
 
  -I <clusterID>
	NX^ ID ð 16 iÅwèµÜ·B
 
  -?	wvbZ[Wð\¦µÜ·B
 
  ---  NX^ %s (ID 0x%8.8X) Ì\ñ ---
 
  ---  NX^ %s (ID 0x%8.8X) Ìo^ ---
 