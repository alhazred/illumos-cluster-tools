'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms.
.TH scds_hasp_check 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_hasp_check \- get status information about \fBSUNW.HAStoragePlus\fR resources that are used by a resource 
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_hasp_check\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBscds_hasp_status_t *\fR\fIhasp_status\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_hasp_check()\fR function retrieves status information
about \fBSUNW.HAStoragePlus\fR(5) resources that
are used by a resource. This information is obtained from the state, online
or otherwise, of all \fBSUNW.HAStoragePlus\fR resources on which
the resource depends. This state is obtained by using the \fBResource_dependencies\fR or \fBResource_dependencies_weak\fR system properties
that are defined for the resource.
.sp
.LP
Resource type implementations can use \fBscds_hasp_check()\fR
in \fBVALIDATE\fR and \fBMONITOR_CHECK\fR method
callback implementations to determine whether checks that are specific to
any file systems that are managed by \fBSUNW.HAStoragePlus\fR
resources should be carried out.
.sp
.LP
When the function succeeds, a status code is stored in \fIhasp_status\fR. This code can be one of the following values:
.sp
.ne 2
.mk
.na
\fB\fBSCDS_HASP_NO_RESOURCE\fR\fR
.ad
.sp .6
.RS 4n
Indicates that the resource does not depend on a \fBSUNW.HAStoragePlus\fR resource.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCDS_HASP_ERR_CONFIG\fR\fR
.ad
.sp .6
.RS 4n
Indicates that at least one of the \fBSUNW.HAStoragePlus\fR
resources on which the resource depends is located in a different resource
group.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCDS_HASP_NOT_ONLINE\fR\fR
.ad
.sp .6
.RS 4n
Indicates that a \fBSUNW.HAStoragePlus\fR resource on which
the resource depends is not online on any potential primary node or zone.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCDS_HASP_ONLINE_NOT_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
Indicates that at least one \fBSUNW.HAStoragePlus\fR resource
on which the resource depends is online, but on another node or zone from
which this function is called.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCDS_HASP_ONLINE_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
Indicates that all \fBSUNW.HAStoragePlus\fR resources on
which the resource depends are online on the node or zone from which this
function is called.
.RE

.LP
Note - 
.sp
.RS 2
The preceding status codes have precedence over each other in the order
in which they appear. For example, if a \fBSUNW.HAStoragePlus\fR
resource is not online and another \fBSUNW.HAStoragePlus\fR resource
is online on a different node or zone, the status code is set to \fBSCDS_HASP_NOT_ONLINE\fR rather than \fBSCDS_HASP_ONLINE_NOT_LOCAL\fR.
.RE
.sp
.LP
The \fBscds_hasp_check()\fR function ignores all \fBSUNW.HAStoragePlus\fR resources for which the \fBFilesystemMountPoints\fR or \fBZpools\fR property is set to an empty list,
the default.
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
Handle
that is returned from \fBscds_initialize\fR(3HA).
.RE

.sp
.ne 2
.mk
.na
\fB\fIhasp_status\fR\fR
.ad
.RS 20n
.rt  
Status of \fBSUNW.HAStoragePlus\fR resources that are used by
the resource.
.RE

.SH RETURN VALUES
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 36n
.rt  
The function succeeded.
.sp
This value also indicates that the status code that is stored in \fBhasp_status\fR is valid.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_INTERNAL\fR\fR
.ad
.RS 36n
.rt  
The function failed.
.sp
The value that is stored in \fBhasp_status\fR is undefined.
Ignore this undefined value.
.RE

.sp
.LP
See the \fBscha_calls\fR(3HA)
man page for a description of other error codes.
.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5)
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscds_initialize\fR(3HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5), \fBSUNW.HAStoragePlus\fR(5)
