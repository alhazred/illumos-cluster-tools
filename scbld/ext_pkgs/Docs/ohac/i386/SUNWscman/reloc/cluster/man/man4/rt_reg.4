'\" te
.\" Copyright 2008 Sun Microsystems, Inc.
.\" All rights reserved. Use is subject to license terms.
.TH rt_reg 4 "11 Sep 2008" "Sun Cluster 3.2" "Sun Cluster File Formats"
.SH NAME
rt_reg \- resource type registration (RTR)
file
.SH DESCRIPTION
.sp
.LP
The resource type registration (RTR) file describes a resource
type. Resource types represent highly-available or scalable services
that run under the control of the Resource Group Manager (RGM) cluster
facility. The file is part of a resource type implementation and is
used as an input file for the \fBscrgadm\fR(1M) command to register the resource type in a
cluster configuration. Registering the resource type is a prerequisite
to creating resources of that type to run in the cluster. By
convention, the RTR file resides in the \fB/opt/cluster/lib/rgm/rtreg\fR directory.
.sp
.LP
A RTR file declares the resource type properties and resource
properties of a resource type. The file is divided into two parts,
the declaration of resource type properties, and of resource properties.
Note that recognition of property names is not case sensitive.
.sp
.LP
The resource type property declarations provide the information
on the resource type implementation, such as paths to the callback
methods that are to be invoked by the RGM to control resources of
the type. Most resource type properties have fixed values set in the \fBrt_reg\fR file. These properties are inherited by all resources
of the type.
.sp
.LP
A resource type implementor can also customize and extend the
administrative view of resource properties. There are two kinds of
resource properties that can have entries in the second part of an \fBrt_reg\fR file: system defined properties and extension properties.
.sp
.LP
System-defined resource properties have predetermined types
and semantics. The \fBrt_reg\fR file can be used to
set attributes such as default, minimum and maximum values for system
defined resource properties. The \fBrt_reg\fR file
can also be used to declare extension properties that are defined
entirely by the resource type implementation. Extension properties
provide a way for a resource type to add information to the configuration
data for a resource that is maintained and managed by the cluster
system.
.sp
.LP
The \fBrt_reg\fR file can set default values
for resource properties, but the actual values are set in individual
resources. The properties in the \fBrt_reg\fR file
can be variables that can be set to different values and adjusted
by the cluster administrator.
.SS "Resource Type Property Declarations"
.sp
.LP
The resource type property declarations consist of a number
of property value assignments.
.sp
.in +2
.nf
PROPERTY_NAME = "Value"; 
.fi
.in -2

.sp
.LP
See the \fBrt_properties\fR(5) man page for a list of the resource type properties
you can declare in the \fBrt_reg\fR file. Since most
properties have default values or are optional, the only declarations
that are essential in a RTR file are the type name, the paths to the \fBSTART\fR and \fBSTOP\fR callback methods, and
RT_version.
.sp
.LP
Note that the first property in the file must be the \fBResource_type\fR property.
.sp
.LP
Starting in Sun Cluster 3.1, a resource type name is of the
form \fIvendor-id.RT-name:version\fR.
.sp
.LP
The three components of the resource type name are properties
specified in the RTR file as \fIvendor-id\fR, \fIresource-type\fR, and \fIRT-version\fR.
The \fBscrgadm\fR command inserts the period and colon
delimiters. Although optional, the \fIvendor-id\fR prefix
is recommended to distinguish between two registration files of the
same name provided by different vendors. To ensure that the \fIvendor-id\fR is unique, use the stock symbol for the company
that is creating the resource type.
.sp
.LP
Resource type names created prior to Sun Cluster 3.1 continue
to be of the form: \fIvendor-id.RT-name\fR.
.SS "Resource Property Declarations"
.sp
.LP
Resource property declarations consist of a number of entries,
each entry being a bracketed list of attribute value assignments.
The first attribute in the entry must be the resource property name.
.sp
.LP
System-defined properties have predetermined type and description
attributes and so these attributes cannot be redeclared in the \fBrt_reg\fR file. Range restrictions, a default value, and constraints
on when the value can be set by the administrator can be declared
for system defined properties.
.sp
.LP
Attributes that can be set for system-defined properties are
listed in the \fBproperty_attributes\fR(5) man page. Attributes not available for system-defined
properties are noted as such in the table.
.sp
.LP
System-defined properties that can have entries in the \fBrt_reg\fR file are listed in the \fBr_properties\fR(5) man page. The following is a sample entry
for the system defined \fBRETRY_COUNT\fR resource property.
.sp
.in +2
.nf
{ 
 PROPERTY = RETRY_COUNT; 
 MIN=0;
 MAX=10;
 DEFAULT=2;
 TUNABLE = ANYTIME;
}
.fi
.in -2

.sp
.LP
Entries for extension properties must indicate a type for the
property. Attributes that can be set for extension properties are
listed in the \fBproperty_attributes\fR(5) man page.
.sp
.LP
The following is a sample entry for an extension property named
"\fBConfigDir\fR" that is of string type. The \fBTUNABLE\fR attribute indicates that the cluster administrator can
set the value of the property when a resource is created.
.sp
.in +2
.nf
{ 
 PROPERTY = ConfigDir; 
 EXTENSION;
 STRING;
 DEFAULT="/";
 TUNABLE = AT_CREATION;
}
.fi
.in -2

.SS "Usage"
.sp
.LP
An \fBrt_reg\fR file is an \fBASCII\fR text
file. It can include comments describing the contents of the file.
The contents are the two parts described above, with the resource
type property list preceding the resource property declarations.
.sp
.LP
White space can be blanks, tabs, newlines, or comments. White
space can exist before or after tokens. Blanks and the pound sign
(#) are not considered to be white space when found in quoted value
tokens. White space separates tokens but is otherwise ignored.
.sp
.LP
Comments begin with # and end with the first newline encountered,
inclusively.
.sp
.LP
Directives begin with #$ and end with the first newline encountered,
inclusively. Directives must appear in the RTR file between the resource
type property declaration section and the resource property declaration
section. Directives inserted in any other location in the RTR file
will produce parser errors. The only valid directives are \fB#$upgrade\fR and \fB#$upgrade_from\fR. Any other directive
will produce parser errors.
.sp
.LP
Tokens are property names, property values, and the following:
.sp
.ne 2
.mk
.na
\fB\fB{  }\fR\fR
.ad
.RS 13n
.rt  
Encloses parameter table properties
.RE

.sp
.ne 2
.mk
.na
\fB\fB;\fR\fR
.ad
.RS 13n
.rt  
Terminates properties and attributes
.RE

.sp
.ne 2
.mk
.na
\fB\fB=\fR\fR
.ad
.RS 13n
.rt  
Separates property names and property values or attribute
names and attribute values
.RE

.sp
.ne 2
.mk
.na
\fB\fB,\fR\fR
.ad
.RS 13n
.rt  
Separates values in a value list
.RE

.sp
.LP
The recognition of property-name keywords in the file is case
insensitive.
.sp
.LP
Properties and attributes have one of three formats.
.br
.in +2
\fB\fIproperty-name\fR = \fIproperty-value\fR;\fR
.in -2
.br
.in +2
\fB\fIproperty-name\fR;\fR
.in -2
.br
.in +2
\fB\fIproperty-name\fR = \fIproperty-value\fR [, \fIproperty-value\fR];\fR
.in -2
.sp
.LP
In the format above, the square brackets, \fB[ ]\fR,
enclose optional items. That is, the property value can be a single \fB\fIproperty-value\fR\fR or a list of two or more \fB\fIproperty-value\fR\fRs separated by commas.
.sp
.LP
The first property in the property list must be the simple resource
type name.
.sp
.LP
Boolean properties and attributes have the following syntax:
.br
.in +2
\fB\fIboolean-property-name\fR;\fR
.in -2
.br
.in +2
\fB\fIboolean-property-name\fR =
TRUE;\fR
.in -2
.br
.in +2
\fB\fIboolean-property-name\fR =
FALSE;\fR
.in -2
.sp
.LP
The first and second forms both set the \fB\fIboolean-property-name\fR\fR to \fBTRUE\fR.
.sp
.LP
The only property name taking a list for its value is \fBPKGLIST\fR. An example is:
.sp
.in +2
.nf
PKGLIST = SUNWsczu, SUNWrsm;
.fi
.in -2

.sp
.LP
Resource type property names are listed in the \fBrt_properties\fR(5) man page. System-defined
properties are listed in the \fBr_properties\fR(5) man page.
.sp
.LP
Resource declarations consist of any number of entries, each
being a bracketed list of resource property attributes.
.sp
.in +2
.nf
{\fIattribute-value-list\fR}
.fi
.in -2

.sp
.LP
Each attribute-value-list consists of attribute values for a
resource property, in the same syntax used for property values, with
the addition of the two type-attribute formats.
.br
.in +2
\fB\fItype-attribute-value\fR;\fR
.in -2
.br
.in +2
\fB\fIenum-type-attribute\fR { \fIenum-value\fR [ , \fIenum-value\fR ]
};\fR
.in -2
.sp
.LP
The \fB\fItype-attribute-value\fR\fR syntax
declares the data type of the extension property to have the value \fB\fItype-attribute-value\fR\fR. It differs from the
first format of the \fB\fIboolean-property-name\fR\fR,
which defines the property named by \fB\fIboolean-property-name\fR\fR to have the value \fBTRUE\fR.
.sp
.LP
For example, the \fBTUNABLE\fR attribute can have
one of the following values: \fBFALSE\fR or \fBNONE\fR, \fBAT_CREATION\fR, \fBTRUE\fR or \fBANYTIME\fR,
and \fBWHEN_DISABLED\fR. When the \fBTUNABLE\fR attribute
uses the syntax:
.sp
.in +2
.nf
TUNABLE;
.fi
.in -2

.sp
.LP
it gets the value of \fBANYTIME\fR.
.SS "Grammar"
.sp
.LP
The following is a description of the syntax of the \fBrt_reg\fR file with a BNF-like grammar. Non-terminals are in lower
case, and terminal keywords are in upper case, although the actual
recognition of keywords in the \fBrt_reg\fR file is
case insensitive. The colon (\fB:\fR) following a non-terminal
at the beginning of a lines indicates a grammar production. Alternative
right-hand-sides of a grammar production are indicated on lines starting
with a vertical bar (\fB|\fR). Variable terminal tokens
are indicated in angled brackets and comments are parenthesized. Other
punctuation in the right-hand side of a grammar production, such as
semi-colon (\fB;\fR), equals sign (\fB=\fR),
and angled brackets (\fB{}\fR) are literals.
.sp
.LP
A comment has the form:
.sp
.in +2
.nf
COMMENT	: # \fIanything but NEWLINE\fR NEWLINE
.fi
.in -2

.sp
.LP
Comments may appear after any token. Comments are treated as
white-space.
.sp
.in +2
.nf
    rt_reg_file	:  Resource_type = value ; proplist upgradesect paramtable

   proplist	:  (NONE: empty)
	| proplist rtproperty

   rtproperty	: rtboolean_prop ;
	| rtvalue_prop ;

   rtboolean_prop	: SINGLE_INSTANCE
	| FAILOVER | RT_SYSTEM

   rtvalue_prop	: rtprop = value
	| PKGLIST = valuelist

   rtprop	: RT_BASEDIR
	| RT_VERSION
	| API_VERSION
	| INIT_NODES
	| START
	| STOP
	| VALIDATE
	| UPDATE
	| INIT
	| FINI
	| BOOT
	| MONITOR_START
	| MONITOR_STOP
	| MONITOR_CHECK
	| PRENET_START
	| POSTNET_STOP
	| RT_DESCRIPTION
	| VENDOR_ID 
	| rtboolean_prop (booleans may have explicit assignments.)

   value	: \fIcontiguous-non-ws-non-;-characters\fR
	| "\fIanything but quote\fR"
	| TRUE
	| FALSE
	| ANYTIME
	| WHEN_DISABLED
	| AT_CREATION
	| RG_PRIMARIES
	| RT_INSTALLED_NODES
	|  (NONE: Empty value)

   valuelist	: value
	| valuelist , value

   upgradesect : (empty)
	| #$UPGRADE upgradelist 

   upgradelist :  (empty)
	| upgradelist #$UPGRADE_FROM rt_version upgtunability

   upgtunability : ANYTIME
	| AT_CREATION
	| WHEN_DISABLED
	| WHEN_OFFLINE
	| WHEN_UNMANAGED
	| WHEN_UNMONITORED

   paramtable	:  (empty)
	| paramtable parameter

   parameter	:  { pproplist }

   pproplist	: PROPERTY = value ;  (property name must come first)
	| pproplist pproperty

   pproperty	: pboolean_prop ;
	| pvalue_prop ;
	| typespec ;

   pvalue_prop	: tunable_prop
	| pprop = value
	| pprop =  (NONE: no value setting)
	| DEFAULT = valuelist

   pprop	: DESCRIPTION
	| MIN
	| MAX
	| MINLENGTH
	| MAXLENGTH
	| ARRAY_MINSIZE
	| ARRAY_MAXSIZE
	| pboolean_prop

   tunable_prop	: TUNABLE
	| TUNABLE = AT_CREATION
	| TUNABLE = ANYTIME
	| TUNABLE = WHEN_DISABLED
	| TUNABLE = TRUE
	| TUNABLE = FALSE
	| TUNABLE = NONE

   typespec	: INT
	| BOOLEAN
	| STRING
	| STRINGARRAY
	| ENUM { valuelist }
.fi
.in -2

.SH EXAMPLES
.LP
\fBExample 1 \fRA Sample Registration File
.sp
.LP
The following is the registration file for a simple example
resource type.

.sp
.in +2
.nf
#
# Registration information for example resource type
#

Resource_type = example_RT;
Vendor_id = SUNW;
RT_Version = 2.0
RT_Basedir= /opt/SUNWxxx;
START = bin/example_service_start;
STOP  = bin/example_service_stop;
Pkglist = SUNWxxx;

#$upgrade
#$upgrade_from "1.0" when_unmonitored

#
# Set range and defaults for method timeouts and Retry_count.
#
{ Property = START_TIMEOUT;  Tunable; MIN=60; DEFAULT=300; }
{ Property = STOP_TIMEOUT;  Tunable; MIN=60; DEFAULT=300; }
{ Property = Retry_count;  Tunable; MIN=1; MAX=20; DEFAULT=10; }

#
# An extension property that can be set at resource creation
#
{ Property = LogLevel;
 Extension;
 enum { OFF, TERSE, VERBOSE };
 Default = TERSE;
 Tunable = AT_CREATION;
 Description = "Controls the detail of example_service logging";
}
.fi
.in -2

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscrgadm\fR(1M), \fBattributes\fR(5), \fBrt_properties\fR(5), \fBr_properties\fR(5), \fBproperty_attributes\fR(5)
.sp
.LP
\fISun Cluster 3.1 Data
Services Developer's Guide\fR
