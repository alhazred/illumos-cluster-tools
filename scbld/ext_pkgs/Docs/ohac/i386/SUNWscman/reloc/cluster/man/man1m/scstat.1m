'\" te
.\" Copyright 2006 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH scstat 1M "10 Jul 2006" "Sun Cluster 3.2" "System Administration Commands"
.SH NAME
scstat \- monitor the status of a Sun Cluster configuration
.SH SYNOPSIS
.LP
.nf
\fBscstat\fR [\fB-DWginpqv\fR [v]] [\fB-h\fR \fInode\fR]
.fi

.SH DESCRIPTION
.LP
Note - 
.sp
.RS 2
Beginning with the Sun Cluster 3.2 release, Sun Cluster software includes an object-oriented command set. Although Sun Cluster software still supports the original command set, Sun Cluster procedural documentation uses only the object-oriented command set. For more information about the object-oriented command set, see the \fBIntro\fR(1CL) man page.
.RE
.sp
.LP
The \fBscstat\fR command displays the current state of Sun Cluster components. Only one instance of the \fBscstat\fR command needs to run on any machine in the Sun Cluster configuration.
.sp
.LP
When run without any options, \fBscstat\fR displays the status for all components of the cluster. This display includes the following information:
.RS +4
.TP
.ie t \(bu
.el o
A list of cluster members
.RE
.RS +4
.TP
.ie t \(bu
.el o
The status of each cluster member
.RE
.RS +4
.TP
.ie t \(bu
.el o
The status of resource groups and resources
.RE
.RS +4
.TP
.ie t \(bu
.el o
The status of every path on the cluster interconnect
.RE
.RS +4
.TP
.ie t \(bu
.el o
The status of every disk device group
.RE
.RS +4
.TP
.ie t \(bu
.el o
The status of every quorum device
.RE
.RS +4
.TP
.ie t \(bu
.el o
The status of every IP network multipathing (IPMP) group and public network adapter
.RE
.sp
.LP
From a non-global zone, referred to simply as a zone, you can run all forms of this command except the \fB-i\fR option. When you run the \fBscstat\fR command from a non-global zone, the output is the same as when run from the global zone except that no status information
is displayed for IP network multipathing groups or public network adapters.
.sp
.LP
You need \fBsolaris.cluster.device.read\fR, \fBsolaris.cluster.transport.read\fR, \fBsolaris.cluster.resource.read\fR, \fBsolaris.cluster.node.read\fR, \fBsolaris.cluster.quorum.read\fR, and \fBsolaris.cluster.system.read\fR RBAC
authorization to use this command without options. See \fBrbac\fR(5).
.SS "Resources and Resource Groups"
.sp
.LP
The resource state, resource group state, and resource status are all maintained on a per-node basis. For example, a given resource has a distinct state on each cluster node and a distinct status on each cluster node.
.sp
.LP
The resource state is set by the Resource Group Manager (\fBRGM\fR) on each node, based only on which methods have been invoked on the resource. For example, after the \fBSTOP\fR method has run successfully on a resource on a given node, the resource's state will
be \fBOFFLINE\fR on that node. If the \fBSTOP\fR method exits nonzero or times out, then the state of the resource is \fBStop_failed\fR.
.sp
.LP
Possible resource states include: \fBOnline\fR, \fBOffline\fR, \fBStart_failed\fR, \fBStop_failed\fR, \fBMonitor_failed\fR, \fBOnline_not_monitored\fR, \fBStarting\fR, and \fBStopping\fR.
.sp
.LP
Possible resource group states are: \fBUnmanaged\fR, \fBOnline\fR, \fBOffline\fR, \fBPending_online\fR, \fBPending_offline\fR, \fBError_stop_failed\fR, \fBOnline_faulted\fR, and \fBPending_online_blocked\fR.
.sp
.LP
In addition to resource state, the \fBRGM\fR also maintains a resource status that can be set by the resource itself by using the API. The field \fBStatus Message\fR actually consists of two components: status keyword and status message. Status message is optionally
set by the resource and is an arbitrary text string that is printed after the status keyword.
.sp
.LP
Descriptions of possible values for a resource's status are as follows:
.sp
.ne 2
.mk
.na
\fB\fBDEGRADED\fR\fR
.ad
.RS 20n
.rt  
The resource is online, but its performance or availability might be compromised in some way.
.RE

.sp
.ne 2
.mk
.na
\fB\fBFAULTED\fR\fR
.ad
.RS 20n
.rt  
The resource has encountered an error that prevents it from functioning.
.RE

.sp
.ne 2
.mk
.na
\fB\fBOFFLINE\fR\fR
.ad
.RS 20n
.rt  
The resource is offline.
.RE

.sp
.ne 2
.mk
.na
\fB\fBONLINE\fR\fR
.ad
.RS 20n
.rt  
The resource is online and providing service.
.RE

.sp
.ne 2
.mk
.na
\fB\fBUNKNOWN\fR\fR
.ad
.RS 20n
.rt  
The current status is unknown or is in transition.
.RE

.SS "Device Groups"
.sp
.LP
Device group status reflects the availability of the devices in that group.
.sp
.LP
The following are possible values for device group status and their descriptions:
.sp
.ne 2
.mk
.na
\fB\fBDEGRADED\fR\fR
.ad
.RS 20n
.rt  
The device group is online, but not all of its potential primaries (secondaries) are up. For two-node connectivity, this status basically indicates that a stand-by primary does not exist, which means a failure of the primary node will result in a loss of access to the devices in
the group.
.RE

.sp
.ne 2
.mk
.na
\fB\fBOFFLINE\fR\fR
.ad
.RS 20n
.rt  
The device group is offline. There is no primary node. The device group must be brought online before any of its devices can be used.
.RE

.sp
.ne 2
.mk
.na
\fB\fBONLINE\fR\fR
.ad
.RS 20n
.rt  
The device group is online. There is a primary node, and devices within the group are ready for I/O.
.RE

.sp
.ne 2
.mk
.na
\fB\fBWAIT\fR\fR
.ad
.RS 20n
.rt  
The device group is between one status and another. This status might occur, for example, when a device group is going from offline to online.
.RE

.SS "IP Network Multipathing Groups"
.sp
.LP
IP network multipathing (IPMP) group status reflects the availability of the backup group and the adapters in the group.
.sp
.LP
The following are possible values for IPMP group status and their descriptions:
.sp
.ne 2
.mk
.na
\fB\fBOFFLINE\fR\fR
.ad
.RS 20n
.rt  
The backup group failed. All adapters in the group are offline.
.RE

.sp
.ne 2
.mk
.na
\fB\fBONLINE\fR\fR
.ad
.RS 20n
.rt  
The backup group is functional. At least one adapter in the group is online.
.RE

.sp
.ne 2
.mk
.na
\fB\fBUNKNOWN\fR\fR
.ad
.RS 20n
.rt  
Any other state than those listed before. This could result when an adapter is detached or marked as down by Solaris commands such as \fBif_mpadm\fR or \fBifconfig\fR.
.RE

.sp
.LP
The following are possible values for IPMP adapter status and their descriptions:
.sp
.ne 2
.mk
.na
\fB\fBOFFLINE\fR\fR
.ad
.RS 20n
.rt  
The adapter failed or the backup group is offline.
.RE

.sp
.ne 2
.mk
.na
\fB\fBONLINE\fR\fR
.ad
.RS 20n
.rt  
The adapter is functional.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSTANDBY\fR\fR
.ad
.RS 20n
.rt  
The adapter is on standby.
.RE

.sp
.ne 2
.mk
.na
\fB\fBUNKNOWN\fR\fR
.ad
.RS 20n
.rt  
Any other state than those listed before. This could result when an adapter is detached or marked as down by Solaris commands such as \fBif_mpadm\fR or \fBifconfig\fR.
.RE

.SH OPTIONS
.sp
.LP
You can specify command options to request the status for specific components.
.sp
.LP
If more than one option is specified, the \fBscstat\fR command prints the status in the specified order.
.sp
.LP
The following options are supported:
.sp
.ne 2
.mk
.na
\fB\fB-D\fR \fR
.ad
.RS 13n
.rt  
Shows status for all disk device groups.
.sp
You can use this option in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.

Output is the same when run from a zone as when run from the global zone.
.sp
You need \fBsolaris.cluster.device.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-g\fR \fR
.ad
.RS 13n
.rt  
Shows status for all resource groups.
.sp
You can use this option in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.

Output is the same when run from a zone as when run from the global zone.
.sp
You need \fBsolaris.cluster.resource.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-h\fR \fInode\fR\fR
.ad
.RS 13n
.rt  
Shows status for the specified node (\fInode\fR) and status of the disk device groups of which this node is the primary node. Also shows the status of the quorum devices to which this node holds reservations of the resource groups to which the node is a potential
master, and holds reservations of the transport paths to which the \fInode\fR is attached.
.sp
You need \fBsolaris.cluster.device.read\fR, \fBsolaris.cluster.transport.read\fR, \fBsolaris.cluster.resource.read\fR, \fBsolaris.cluster.node.read\fR, \fBsolaris.cluster.quorum.read\fR, and \fBsolaris.cluster.system.read\fR RBAC
authorization to use this command option. See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-i\fR\fR
.ad
.RS 13n
.rt  
Shows status for all IPMP groups and public network adapters.
.sp
You can use this option only in the global zone.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fR
.ad
.RS 13n
.rt  
Shows status for all nodes.
.sp
You can use this option in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.

Output is the same when run from a zone as when run from the global zone.
.sp
You need \fBsolaris.cluster.node.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR \fR
.ad
.RS 13n
.rt  
Shows status for all components in the cluster. Use with \fB-v\fR to display more verbose output.
.sp
You can use this option in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.

Output is the same when run from a zone as when run from the global zone, except that no status for IPMP groups or public network adapters is displayed.
.sp
You need \fBsolaris.cluster.device.read\fR, \fBsolaris.cluster.transport.read\fR, \fBsolaris.cluster.resource.read\fR, \fBsolaris.cluster.node.read\fR, \fBsolaris.cluster.quorum.read\fR, and \fBsolaris.cluster.system.read\fR RBAC
authorization to use \fB-p\fR with \fB-v\fR. See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-q\fR\fR
.ad
.RS 13n
.rt  
Shows status for all device quorums and node quorums.
.sp
You can use this option in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.

Output is the same when run from a zone as when run from the global zone.
.sp
You need \fBsolaris.cluster.quorum.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR[\fBv\fR]\fR
.ad
.RS 13n
.rt  
Shows verbose output.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-W\fR \fR
.ad
.RS 13n
.rt  
Shows status for cluster transport path.
.sp
You can use this option in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.

Output is the same when run from a zone as when run from the global zone.
.sp
You need \fBsolaris.cluster.transport.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRUsing the \fBscstat\fR Command
.sp
.LP
The following command displays the status of all resource groups followed by the status of all components related to the specified host:

.sp
.in +2
.nf
% \fBscstat -g -h \fIhost\fR\fR
.fi
.in -2
.sp

.sp
.LP
The output that is displayed appears in the order in which the options are specified.

.sp
.LP
These results are the same results you would see by typing the two commands:

.sp
.in +2
.nf
% \fBscstat -g\fR
.fi
.in -2
.sp

.sp
.LP
and

.sp
.in +2
.nf
% \fBscstat -h \fIhost\fR\fR
.fi
.in -2
.sp

.SH EXIT STATUS
.sp
.LP
The following exit values are returned:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The command completed successfully.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 20n
.rt  
An error has occurred.
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
Availability\fBSUNWsczu\fR
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL), \fBcluster\fR(1CL), \fBif_mpadm\fR(1M), \fBifconfig\fR(1M), \fBscha_resource_setstatus\fR(1HA), \fBscha_resource_setstatus\fR(3HA), \fBattributes\fR(5)
.SH NOTES
.sp
.LP
An online quorum device means that the device was available for contributing to the formation of quorum when quorum was last established. From the context of the quorum algorithm, the device is online because it actively contributed to the formation of quorum. However, an online quorum device
might not necessarily continue to be in a healthy enough state to contribute to the formation of quorum when quorum is re-established. The current version of Sun Cluster does not include a disk monitoring facility or regular probes to the quorum devices.
