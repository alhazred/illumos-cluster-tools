'\" te
.\" Copyright 2007 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH SUNW.HAStoragePlus 5 "25 Sep 2007" "Sun Cluster 3.2" "Sun Cluster Miscellaneous"
.SH NAME
SUNW.HAStoragePlus \- resource type that enforces dependencies between Sun Cluster device services, file systems, and data services
.SH DESCRIPTION
.sp
.LP
\fBSUNW.HAStoragePlus\fR describes a resource type that enables you to specify dependencies between data service resources and device groups, cluster file systems, and local file systems.
.LP
Note - 
.sp
.RS 2
Local file systems include the UNIX File System (UFS), Quick File System (QFS), Veritas File System (VxFS), and Solaris ZFS (Zettabyte File System).
.RE
.sp
.LP
This resource type enables you to bring data services online only after their dependent device groups and file systems are guaranteed to be available. The \fBSUNW.HAStoragePlus\fR resource type also provides support for mounting, unmounting, and checking file systems.
.sp
.LP
Resource groups by themselves do not provide for direct synchronization with disk device groups, cluster file systems, or local file systems. As a result, during a cluster reboot or failover, an attempt to start a data service can occur while its dependent global devices and file systems
are still unavailable. Consequently, the data service's \fBSTART\fR method might time out, and your data service might fail.
.sp
.LP
The \fBSUNW.HAStoragePlus\fR resource type represents the device groups, cluster, and local file systems that are to be used by one or more data service resources. You add a resource of type \fBSUNW.HAStoragePlus\fR to a resource group and set up dependencies between
other resources and the \fBSUNW.HAStoragePlus\fR resource. These dependencies ensure that the data service resources are brought online after the following situations occur:
.RS +4
.TP
1.
All specified device services are available (and collocated, if necessary).
.RE
.RS +4
.TP
2.
All specified file systems are checked and mounted.
.RE
.sp
.LP
You can also use the \fBSUNW.HAStoragePlus\fR resource type to access a local file system from a non-global zone.
.SH EXTENSION PROPERTIES
.sp
.LP
The following extension properties are associated with the \fBSUNW.HAStoragePlus\fR resource type:
.sp
.ne 2
.mk
.na
\fB\fBAffinityOn\fR\fR
.ad
.sp .6
.RS 4n
Specifies whether a \fBSUNW.HAStoragePlus\fR resource needs to perform an affinity switchover for all global devices that are defined in the \fBGlobalDevicePaths\fR and \fBFilesystemMountPoints\fR extension properties. You can specify \fBTRUE\fR or \fBFALSE\fR. Affinity switchover is set by default, that is, \fBAffinityOn\fR is set to \fBTRUE\fR.
.sp
The \fBZpools\fR extension property ignores the \fBAffinityOn\fR extension property.  The \fBAffinityOn\fR extension property is intended for use with the \fBGlobalDevicePaths\fR and \fBFilesystemMountPoints\fR extension properties
only.
.sp
When you set the \fBAffinityOn\fR extension property to \fBFALSE\fR, the \fBSUNW.HAStoragePlus\fR resource passively waits for the specified global services to become available. In this case, the primary node or zone of each online global device service
might not be the same node or zone that is the primary node for the resource group.
.sp
The purpose of an affinity switchover is to enhance performance by ensuring the colocation of the device groups and the resource groups on a specific node or zone. Data reads and writes always occur over the device primary paths. Affinity switchovers require the potential primary node list
for the resource group and the node list for the device group to be equivalent. The \fBSUNW.HAStoragePlus\fR resource performs an affinity switchover for each device service only once, that is, when the \fBSUNW.HAStoragePlus\fR resource is brought online.
.sp
The setting of the \fBAffinityOn\fR flag is ignored for scalable services. Affinity switchovers are not possible with scalable resource groups.
.RE

.sp
.ne 2
.mk
.na
\fB\fBFilesystemCheckCommand\fR\fR
.ad
.sp .6
.RS 4n
Overrides the check that \fBSUNW.HAStoragePlus\fR conducts on each unmounted file system before attempting to mount it. You can specify an alternate command string or executable, which is invoked on all unmounted file systems.
.sp
When a \fBSUNW.HAStoragePlus\fR resource is configured in a scalable resource group, the file-system check on each unmounted cluster file system is omitted.
.sp
The default value for the \fBFilesystemCheckCommand\fR extension property is \fBNULL\fR. When you set this extension property to \fBNULL\fR, Sun Cluster checks UFS or VxFS by issuing the \fB/usr/sbin/fsck -o p\fR command. Sun Cluster checks
other file systems by issuing the \fB/usr/sbin/fsck\fR command. When you set the \fBFilesystemCheckCommand\fR extension property to another command string, \fBSUNW.HAStoragePlus\fR invokes this command string with the file system mount point as an argument.
You can specify any arbitrary executable in this manner. A nonzero return value is treated as an error that occurred during the file system check operation. This error causes the \fBSTART\fR method to fail. When you do not require a file system check operation, set the \fBFilesystemCheckCommand\fR extension property to \fB/bin/true\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBFilesystemMountPoints\fR\fR
.ad
.sp .6
.RS 4n
Specifies a list of valid file system mount points. You can specify global or local file systems. Global file systems are accessible from all nodes or zones in a cluster. Local file systems are accessible from a single cluster node or zone. Local file systems that are managed by
a \fBSUNW.HAStoragePlus\fR resource are mounted on a single cluster node or zone. These local file systems require the underlying devices to be Sun Cluster global devices.
.sp
These file system mount points are defined in the format \fBpaths[,...]\fR. You can specify both the path in a non-global zone and the path in a global zone, in this format:
.sp
.in +2
.nf
\fINon-GlobalZonePath\fR:\fIGlobalZonePath\fR
.fi
.in -2

The global zone path is optional. If you do not specify a global zone path, Sun Cluster assumes that the path in the non-global zone and in the global zone are the same. If you specify the path as \fINon-GlobalZonePath\fR:\fIGlobalZonePath\fR, you
must specify \fIGlobalZonePath\fR in the global zone's \fB/etc/vfstab\fR.
.sp
The default setting for this property is an empty list.
.sp
You can use the \fBSUNW.HAStoragePlus\fR resource type to make a file system available to a non-global zone. To enable the \fBSUNW.HAStoragePlus\fR resource type to do this, you must create a mount point in the global zone and in the non-global zone. The \fBSUNW.HAStoragePlus\fR resource type makes the file system available to the non-global zone by mounting the file system in the global zone. The resource type then performs a loopback mount in the non-global zone.
.sp
Each file system mount point should have an equivalent entry in \fB/etc/vfstab\fR on all cluster nodes and in all global zones. The \fBSUNW.HAStoragePlus\fR resource type does not check \fB/etc/vfstab\fR in non-global zones.
.sp
\fBSUNW.HAStoragePlus\fR resources that specify local file systems can only belong in a failover resource group with affinity switchovers enabled. These local file systems can therefore be termed failover file systems. You can specify both local and global file system mount
points at the same time.
.sp
Any file system whose mount point is present in the \fBFilesystemMountPoints\fR extension property is assumed to be local if its \fB/etc/vfstab\fR entry satisfies both of the following conditions:
.RS +4
.TP
1.
The non-global mount option is specified.
.RE
.RS +4
.TP
2.
The "mount at boot" field for the entry is set to "no."
.RE
A Solaris ZFS is always a local file system. Do not list a ZFS in \fB/etc/vfstab\fR. Also, do not include ZFS mount points in the \fBFilesystemMountPoints\fR property.
.RE

.sp
.ne 2
.mk
.na
\fB\fBGlobalDevicePaths\fR\fR
.ad
.sp .6
.RS 4n
Specifies a list of valid global device group names or global device paths. The paths are defined in the format \fBpaths[,...]\fR. The default setting for this property is an empty list.
.RE

.sp
.ne 2
.mk
.na
\fB\fBZpools\fR\fR
.ad
.sp .6
.RS 4n
Specifies a list of valid ZFS storage pools, each of which contains at least one ZFS. These ZFS storage pools are defined in the format \fBpaths[,...]\fR. The default setting for this property is an empty list. All file systems in a ZFS storage pool are mounted and
unmounted together.
.sp
The \fBZpools\fR extension property enables you to specify ZFS storage pools. The devices that make up a ZFS storage pool must be accessible from all the nodes or zones that are configured in the node list of the resource group to which a \fBSUNW.HAStoragePlus\fR resource
belongs. A \fBSUNW.HAStoragePlus\fR resource that manages a ZFS storage pool can only belong to a failover resource group. When a \fBSUNW.HAStoragePlus\fR resource that manages a ZFS storage pool is brought online, the ZFS storage pool is imported, and every file system
that the ZFS storage pool contains is mounted. When the resource is taken offline on a node, for each managed ZFS storage pool, all file systems are unmounted and the ZFS storage pool is exported.
.LP
Note - 
.sp
.RS 2
 \fBSUNW.HAStoragePlus\fR does not support file systems created on ZFS volumes.
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBZpoolsSearchDir\fR\fR
.ad
.sp .6
.RS 4n
Specifies the location to search for the devices of \fBZpools\fR. The default value for the \fBZpoolsSearchDir\fR extension property is \fB/dev/dsk\fR. The \fBZpoolsSearchDir\fR extension property is similar to the \fB-d\fR option
of the \fBzpool\fR command.
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
Availability\fBSUNWscu\fR
.TE

.SH SEE ALSO
.sp
.LP
\fBrt_reg\fR(4), \fBattributes\fR(5)
.SH WARNINGS
.sp
.LP
Make data service resources within a given resource group dependent on a \fBSUNW.HAStoragePlus\fR resource. Otherwise, no synchronization is possible between the data services and the global devices or file systems. Strong resource dependencies ensure that the \fBSUNW.HAStoragePlus\fR resource is brought online before other resources. Local file systems that are managed by a \fBSUNW.HAStoragePlus\fR resource are mounted only when the resource is brought online.
.sp
.LP
Enable logging on UFS systems.
.sp
.LP
Avoid configuring multiple \fBSUNW.HAStoragePlus\fR resources in different resource groups that refer to the same device group and with \fBAffinityOn\fR flags set to \fBTRUE\fR. Redundant device switchovers can occur. As a result, resource and device
groups might be dislocated.
.sp
.LP
Avoid configuring a ZFS storage pool under multiple \fBSUNW.HAStoragePlus\fR resources in different resource groups.
.SH NOTES
.sp
.LP
The \fBSUNW.HAStoragePlus\fR resource is capable of mounting any cluster file system that is found in an unmounted state.
.sp
.LP
All file systems are mounted in the overlay mode.
.sp
.LP
Local file systems are forcibly unmounted.
.sp
.LP
The waiting time for all device services and file systems to become available is specified by the \fBPrenet_Start_Timeout\fR property in \fBSUNW.HAStoragePlus\fR. This is a tunable property.
