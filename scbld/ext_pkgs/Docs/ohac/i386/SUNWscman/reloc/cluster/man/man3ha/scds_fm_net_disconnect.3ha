'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms.
.TH scds_fm_net_disconnect 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_fm_net_disconnect \- terminate
a TCP connection to an application
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR \fB scds_fm_net_disconnect\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBscds_socket_t *\fR\fIsocklist\fR,
    \fBint\fR \fIcount\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_fm_net_disconnect()\fR function terminates
one or more TCP connections to a process that is being monitored.
.sp
.LP
An attempt is made to close all valid socket connections in the \fBsocklist\fR array within the specified \fBtimeout\fR interval.
On return, each member of \fBsocklist\fR contains the value \fBSCDS_FMSOCK_NA\fR.
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The
handle that is returned by \fBscds_initialize\fR(3HA).
.RE

.sp
.ne 2
.mk
.na
\fB\fIsocklist\fR\fR
.ad
.RS 20n
.rt  
The socket list that is returned by \fBscds_fm_net_connect\fR(3HA). This argument is an input/output argument.
.RE

.sp
.ne 2
.mk
.na
\fB\fIcount\fR\fR
.ad
.RS 20n
.rt  
The
number of members in the \fIsocklist\fR array. Set this parameter
to \fBSCDS_MAX_IPADDR_TYPES\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
The timeout value in seconds. Each socket gets the same time period to disconnect
before it is timed out. As these time intervals proceed in parallel, this
value is effectively the maximum time that the function takes to execute.
.RE

.SH RETURN VALUES
.sp
.LP
The \fBscds_fm_net_disconnect()\fR function returns the
following values:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function
succeeded.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_INVAL\fR\fR
.ad
.RS 20n
.rt  
The function was called with invalid paramaters.
.RE

.sp
.ne 2
.mk
.na
\fBOther nonzero values\fR
.ad
.RS 20n
.rt  
The function
failed. See \fBscha_calls\fR(3HA) for the meaning
of failure codes.
.RE

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5)
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
Availability\fBSUNWscdev\fR
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscds_fm_net_connect\fR(3HA), \fBscds_fm_tcp_disconnect\fR(3HA), \fBscds_initialize\fR(3HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5)
