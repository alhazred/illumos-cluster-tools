'\" te
.\" Copyright 2006 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH clvxvm 1CL "19 Jul 2006" "Sun Cluster 3.2" "Sun Cluster Maintenance Commands"
.SH NAME
clvxvm \- configure Veritas Volume Manager for Sun Cluster
.SH SYNOPSIS
.LP
.nf
\fB/usr/cluster/bin/clvxvm\fR  \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clvxvm\fR  [\fIsubcommand\fR] \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clvxvm\fR  \fIsubcommand\fR \fB-v\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clvxvm encapsulate\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/clvxvm initialize\fR 
.fi

.SH DESCRIPTION
.sp
.LP
The \fBclvxvm\fR utility initializes Veritas Volume Manager (VxVM) on a Sun Cluster node and optionally performs root-disk encapsulation. There is no short form of this command name.
.sp
.LP
You can only use this utility with VxVM versions 4.1 or later. For older versions of VxVM, instead use the \fBscvxinstall\fR(1M) utility.
.sp
.LP
The general form of this command is as follows:
.sp
.LP
\fBclvxvm\fR [\fIsubcommand\fR] [\fIoptions\fR]
.sp
.LP
You can omit \fIsubcommand\fR only if \fIoptions\fR specifies the \fB-?\fR option  or the \fB-V\fR option.
.sp
.LP
Each option of this command has a long form and a short form. Both forms of each option are given with the description of the option in the OPTIONS section of this man page.
.sp
.LP
You can only use this command from a node that is booted in cluster mode. All nodes in the cluster configuration must be current cluster members. All nodes must be added to the node authentication list.
.sp
.LP
You can use this command only in the global zone.
.SH SUBCOMMANDS
.sp
.LP
The following subcommands are supported:
.sp
.ne 2
.mk
.na
\fB\fBencapsulate\fR\fR
.ad
.sp .6
.RS 4n
Encapsulates the root disk and performs other Sun Cluster-specific tasks.
.sp
The \fBencapsulate\fR subcommand performs the following tasks:
.RS +4
.TP
.ie t \(bu
.el o
Verifies that the current node is booted in cluster mode and that all other cluster nodes are running in cluster mode.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Verifies that the user is superuser.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Enforces a cluster-wide value for the \fBvxio\fR major number by modifying the node's \fB/etc/name_to_major\fR file, if necessary. This task ensures that the \fBvxio\fR number is the same on all cluster nodes.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Runs several VxVM commands to prepare for root-disk encapsulation.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Modifies the global-devices entry in the \fB/etc/vfstab\fR file that is specified for the \fB/global/.devices/node@\fIn\fR\fR file system, where \fIn\fR is the node ID number. The \fBclvxvm\fR utility
replaces the existing device path \fB/dev/did/{r}dsk\fR with \fB/dev/{r}dsk\fR. This change ensures that VxVM recognizes that the global-devices file system resides on the root disk.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Twice reboots each node that is running \fBclvxvm\fR. The first reboot allows VxVM to complete the encapsulation process. The second reboot resumes normal operation. The \fBclvxvm\fR utility includes a synchronization mechanism to ensure that the utility reboots
only one node at a time, to prevent loss of quorum.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Unmounts the global-devices file system. The file system is automatically remounted after the encapsulation process is complete.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Recreates the special files for the root-disk volumes with a unique minor number on each node.
.RE
This subcommand expects that VxVM packages and licenses are already installed and that the VxVM configuration daemon is successfully enabled on this node.
.sp
Each root disk that you encapsulate must have at least two free (unassigned) partitions.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR RBAC authorization to use this subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fBinitialize\fR\fR
.ad
.sp .6
.RS 4n
Initializes VxVM and performs other Sun Cluster-specific tasks.
.sp
The \fBinitialize\fR subcommand performs the following tasks:
.RS +4
.TP
.ie t \(bu
.el o
Verifies that the current node is booted in cluster mode and that all other cluster nodes are running in cluster mode.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Verifies that the user is superuser.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Enforces a cluster-wide value for the \fBvxio\fR major number by modifying the node's \fB/etc/name_to_major\fR file, if necessary. This task ensures that the \fBvxio\fR number is the same on all cluster nodes.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Instructs the user to reboot the node to resume operation with the new \fBvxio\fR major number in effect, in case the number had to be changed.
.RE
This subcommand expects that VxVM packages and licenses are already installed and that the VxVM configuration daemon is successfully enabled on this node.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR RBAC authorization to use this subcommand.
.RE

.SH OPTIONS
.sp
.LP
The following options are supported:
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB-\fB--help\fR\fR
.ad
.sp .6
.RS 4n
Displays help information.
.sp
You can use this option either alone or with a subcommand.
.RS +4
.TP
.ie t \(bu
.el o
If you use this option alone, the list of available subcommands is printed.
.RE
.RS +4
.TP
.ie t \(bu
.el o
If you use this option with a subcommand, the usage options for that subcommand are printed.
.RE
When you use this option, no other processing is performed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB-\fB--version\fR\fR
.ad
.sp .6
.RS 4n
Displays the version of the command.
.sp
Do not specify this option with subcommands, operands, or other options. The subcommands, operands, or other options are ignored. The \fB-V\fR option only displays the version of the command. No other operations are performed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB--verbose\fR\fR
.ad
.sp .6
.RS 4n
Displays verbose information to standard output.
.sp
You can use this option with any form of the command.
.RE

.SH OPERANDS
.sp
.LP
No form of this command accepts an operand.
.SH EXIT STATUS
.sp
.LP
The complete set of exit status codes for all commands in this command set are listed on the \fBIntro\fR(1CL) man page.
.sp
.LP
This command returns the following exit status codes:
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
No error
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
Not enough swap space
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
Invalid argument
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
Permission denied
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O error
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRInitializing VxVM on a Cluster Node
.sp
.LP
The following example shows how to initialize VxVM the cluster node from which the command is issued.

.sp
.in +2
.nf
# \fBclvxvm initialize\fR
.fi
.in -2
.sp

.LP
\fBExample 2 \fRInitializing VxVM on a Cluster Node and Encapsulating the Root Disk
.sp
.LP
The following example shows how to initialize VxVM and encapsulate the root disk on the cluster node from which the command is issued.

.sp
.in +2
.nf
# \fBclvxvm encapsulate\fR
.fi
.in -2
.sp

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWsczu
_
Interface StabilityEvolving
.TE

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/var/cluster/cmvxvm/*\fR\fR
.ad
.sp .6
.RS 4n
Location of temporary files that are used by the \fBclvxvm\fR utility.
.RE

.sp
.ne 2
.mk
.na
\fB\fB/var/cluster/logs/install/clvxvm.log.\fIpid\fR\fR\fR
.ad
.sp .6
.RS 4n
Log file that is created by the \fBscvxinstall\fR utility.
.RE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL), \fBcldevice\fR(1CL), \fBcldevicegroup\fR(1CL), \fBclsetup\fR(1CL), \fBcluster\fR(1CL), \fBscinstall\fR(1M), \fBscvxinstall\fR(1M), \fBrbac\fR(5) 
.sp
.LP
\fISun Cluster Software Installation Guide for Solaris OS\fR
.SH NOTES
.sp
.LP
The superuser can run any forms of this command.
.sp
.LP
Any user can also run this command with the following options:
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR (help) option
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR (version) option
.RE
.sp
.LP
To run this command with other subcommands, users other than superuser require RBAC authorizations. See the following table.
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
SubcommandRBAC Authorization
_
\fBencapsulate\fR\fBsolaris.cluster.modify\fR
_
\fBinitialize\fR\fBsolaris.cluster.modify\fR
.TE

