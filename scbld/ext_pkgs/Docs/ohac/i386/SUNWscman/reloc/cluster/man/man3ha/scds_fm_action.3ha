'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms.
.TH scds_fm_action 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_fm_action \- take action after probe completion function
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_fm_action\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBint\fR \fIprobe_status\fR, \fBlong\fR \fIelapsed_milliseconds\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_fm_action()\fR function uses the \fBprobe_status\fR of the data service in conjunction with the past history
of failures to take one of the following actions:
.RS +4
.TP
.ie t \(bu
.el o
Restart the application.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Fail over the resource group.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Do nothing.
.RE
.sp
.LP
Use the value of the input \fBprobe_status\fR argument
to indicate the severity of the failure. For example, you might consider a
failure to connect to an application as a complete failure, but a failure
to disconnect as a partial failure. In the latter case you would have to specify
a value for \fBprobe_status\fR between 0 and \fBSCDS_PROBE_COMPLETE_FAILURE\fR.
.sp
.LP
The DSDL defines \fBSCDS_PROBE_COMPLETE_FAILURE\fR as \fB100\fR. For partial probe success or failure, use a value between \fB0\fR and \fBSCDS_PROBE_COMPLETE_FAILURE\fR.
.sp
.LP
Successive calls to \fBscds_fm_action()\fR compute a failure
history by summing the value of the \fBprobe_status\fR input
parameter over the time interval defined by the \fBRetry_interval\fR
property of the resource. Any failure history older than \fBRetry_interval\fR is purged from memory and is not used towards making the restart
or failover decision.
.sp
.LP
The \fBscds_fm_action()\fR function uses the following
algorithm to choose which action to take:
.sp
.ne 2
.mk
.na
\fB\fBRestart\fR\fR
.ad
.RS 20n
.rt  
If the accumulated history of failures reaches \fBSCDS_PROBE_COMPLETE_FAILURE\fR, \fBscds_fm_action()\fR restarts the resource by calling
the \fBSTOP\fR method of the resource followed by the \fBSTART\fR method. It ignores any \fBPRENET_START\fR or \fBPOSTNET_STOP\fR methods defined for the resource type.
.sp
The status of the resource is set to \fBSCHA_RSSTATUS_DEGRADED\fR by making a \fBscha_resource_setstatus()\fR call,
unless the resource is already set.
.sp
If the restart attempt fails because the \fBSTART\fR or \fBSTOP\fR methods of the resource fail, a \fBscha_control()\fR
is called with the \fBGIVEOVER\fR option to fail the resource
group over to another node or zone. If the \fBscha_control()\fR
call succeeds, the resource group is failed over to another cluster node or
zone, and the call to \fBscds_fm_action()\fR never returns.
.sp
Upon a successful restart, failure history is purged. Another restart
is attempted only if the failure history again accumulates to \fBSCDS_PROBE_COMPLETE_FAILURE\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBFailover\fR\fR
.ad
.RS 20n
.rt  
If the number of restarts attempted by successive calls to \fBscds_fm_action()\fR reaches the \fBRetry_count\fR value
defined for the resource, a failover is attempted by making a call to \fBscha_control()\fR with the \fBGIVEOVER\fR option.
.sp
The status of the resource is set to \fBSCHA_RSSTATUS_FAULTED\fR by making a \fBscha_resource_setstatus()\fR call,
unless the resource is already set.
.sp
If the \fBscha_control()\fR call fails, the entire failure
history maintained by \fBscds_fm_action()\fR is purged.
.sp
If the \fBscha_control()\fR call succeeds, the resource
group is failed over to another cluster node or zone, and the call to \fBscds_fm_action()\fR never returns.
.RE

.sp
.ne 2
.mk
.na
\fB\fBNo Action\fR\fR
.ad
.RS 20n
.rt  
If the accumulated history of failures remains below \fBSCDS_PROBE_COMPLETE_FAILURE\fR, no action is taken. In addition, if the \fBprobe_status\fR value is \fB0\fR, which indicates a successful check
of the service, no action is taken, irrespective of the failure history.
.sp
The status of the resource is set to \fBSCHA_RSSTATUS_OK\fR
by making a \fBscha_resource_setstatus()\fR call, unless the
resource is already set.
.RE

.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 28n
.rt  
The
handle that is returned from \fBscds_initialize\fR(3HA).
.RE

.sp
.ne 2
.mk
.na
\fB\fIprobe_status\fR\fR
.ad
.RS 28n
.rt  
A number you specify between 0 and \fBSCDS_PROBE_COMPLETE_FAILURE\fR
that indicates the status of the data service. A value of \fB0\fR
implies that the recent data service check was successful. A value of \fBSCDS_PROBE_COMPLETE_FAILURE\fR means complete failure and implies that
the service has completely failed. You can also supply a value in between
0 and \fBSCDS_PROBE_COMPLETE_FAILURE\fR that implies a partial
failure of the service.
.RE

.sp
.ne 2
.mk
.na
\fB\fIelapsed_milliseconds\fR\fR
.ad
.RS 28n
.rt  
The time, in milliseconds, to complete the data service check. This
value is reserved for future use.
.RE

.SH RETURN VALUES
.sp
.LP
The \fBscds_fm_action()\fR function returns the following
values:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function
succeeded.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 20n
.rt  
The function failed.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 20n
.rt  
No action was taken, or a restart was successfully attempted.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_FAIL\fR\fR
.ad
.RS 20n
.rt  
A failover attempt was made but it did not succeed.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOMEM\fR\fR
.ad
.RS 20n
.rt  
System is out of memory.
.RE

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5)
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscds_fm_sleep\fR(3HA), \fBscds_initialize\fR(3HA), \fBscha_calls\fR(3HA), \fBscha_control\fR(3HA), \fBscha_fm_print_probes\fR(3HA), \fBscha_resource_setstatus\fR(3HA), \fBattributes\fR(5)
