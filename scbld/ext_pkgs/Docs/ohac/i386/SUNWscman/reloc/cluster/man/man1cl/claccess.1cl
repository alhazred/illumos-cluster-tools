'\" te
.\" Copyright 2008 Sun Microsystems, Inc.
.\" All rights reserved. Use is subject to license terms.
.TH claccess 1CL "11 Sep 2008" "Sun Cluster 3.2" "Sun Cluster Maintenance Commands"
.SH NAME
claccess \- manage Sun Cluster access policies
for nodes
.SH SYNOPSIS
.LP
.nf
\fB/usr/cluster/bin/claccess\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess [\fIsubcommand\fR]\fR 
\fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess \fIsubcommand\fR\fR 
[\fIoptions\fR] \fB-v\fR [\fIhostname\fR[,\&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess allow\fR  \fB-h\fR \fIhostname\fR[,\&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess allow-all\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess deny\fR  \fB-h\fR \fIhostname\fR[,\&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess deny-all\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess list\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess set\fR  \fB-p\fR protocol=\fIauthprotocol\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess show\fR 
.fi

.SH DESCRIPTION
.sp
.LP
The \fBclaccess\fR command controls the network
access policies for machines that attempt to access the cluster configuration.
The \fBclaccess\fR command has no short form.
.sp
.LP
The cluster maintains a list of machines that can access the
cluster configuration. The cluster also stores the name of the authentication
protocol that is used for these nodes to access the cluster configuration. 
.sp
.LP
When a machine attempts to access the cluster configuration,
for example when it asks to be added to the cluster configuration
(see \fBclnode\fR(1CL)), the cluster checks this list to determine
whether the node has access permission. If the node has permission,
the node is authenticated and allowed  access to the cluster configuration.
.sp
.LP
You can use the \fBclaccess\fR command for the
following tasks:
.RS +4
.TP
.ie t \(bu
.el o
To allow any new machines to add themselves to the
cluster configuration and remove themselves from the cluster configuration
.RE
.RS +4
.TP
.ie t \(bu
.el o
To prevent any nodes from adding themselves to the
cluster configuration and removing themselves from the cluster configuration
.RE
.RS +4
.TP
.ie t \(bu
.el o
To control the authentication type to check
.RE
.sp
.LP
You can use this command only in the global zone.
.sp
.LP
The general form of  the \fBclaccess\fR command
is as follows:
.sp
.LP
 \fBclaccess\fR [\fIsubcommand\fR]
[\fIoptions\fR]
.sp
.LP
You can omit \fIsubcommand\fR only if \fIoptions\fR specifies the \fB-?\fR option or 
the \fB-V\fR option.
.sp
.LP
Each option of this command has a long form and a short form.
Both forms of each option are provided with the description of the
option in the "OPTIONS" section of this man page.
.SH SUBCOMMANDS
.sp
.LP
The following subcommands are supported:
.sp
.ne 2
.mk
.na
\fB\fBallow\fR\fR
.ad
.sp .6
.RS 4n
Allows the specified machine or machines to access
the cluster configuration. 
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR role-based
access control (RBAC) authorization to use this subcommand.  See  \fBrbac\fR(5).
.sp
See also the description of the \fBdeny\fR and
the \fBallow-all\fR subcommands.
.RE

.sp
.ne 2
.mk
.na
\fB\fBallow-all\fR\fR
.ad
.sp .6
.RS 4n
Allows all machines to add themselves to access the
cluster configuration.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization to use this subcommand.  See  \fBrbac\fR(5).
.sp
See also the description of the \fBdeny-all\fR and
the \fBallow\fR subcommands.
.RE

.sp
.ne 2
.mk
.na
\fB\fBdeny\fR\fR
.ad
.sp .6
.RS 4n
Prevents the specified machine or machines from accessing
the cluster configuration.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization to use this subcommand.  See \fBrbac\fR(5).
.sp
See also the description of the \fBallow\fR and
the \fBdeny-all\fR subcommands.
.RE

.sp
.ne 2
.mk
.na
\fB\fBdeny-all\fR\fR
.ad
.sp .6
.RS 4n
Prevents all machines from accessing the cluster configuration.
.sp
No access for any node is the default setting after the cluster
is configured the first time.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization to use this subcommand.  See \fBrbac\fR(5).
.sp
See also the description of the \fBallow-all\fR and
the \fBdeny\fR subcommands.
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
Displays the names of the machines that have authorization
to access the cluster configuration. To see the authentication protocol
as well, use the \fBshow\fR subcommand. 
.sp
Users other than superuser require \fBsolaris.cluster.read\fR \fBRBAC\fR authorization to use this subcommand.  See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fBset\fR\fR
.ad
.sp .6
.RS 4n
Sets the authentication protocol to the value that
you specify with the \fB-p\fR option. By default, the system
uses \fBsys\fR as the authentication protocol. See the \fB-p\fR option in "OPTIONS".
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization to use this subcommand.  See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
Displays the names of the machines that have permission
to access the cluster configuration. Also displays the authentication
protocol. 
.sp
Users other than superuser require \fBsolaris.cluster.read\fR \fBRBAC\fR authorization to use this subcommand.  See \fBrbac\fR(5).
.RE

.SH OPTIONS
.sp
.LP
The following options are supported:
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB-\fB-help\fR\fR
.ad
.sp .6
.RS 4n
Displays help information. When you use this option,
no other processing is performed.
.sp
You can specify this option without a subcommand or with a subcommand.
If you specify this option without a subcommand, the list of subcommands
of this command is displayed. If you specify this option with a subcommand,
the usage options for the subcommand are displayed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-h\fR \fIhostname\fR\fR
.ad
.br
.na
\fB-\fB-host\fR=\fIhostname\fR\fR
.ad
.br
.na
\fB-\fB-host\fR \fIhostname\fR\fR
.ad
.sp .6
.RS 4n
Specifies the name of the node being granted or denied
access. 
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR protocol=\fIauthentication-protocol\fR\fR
.ad
.br
.na
\fB-\fB-authprotocol=\fR\fIauthentication-protocol\fR\fR
.ad
.br
.na
\fB-\fB-authprotocol \fR\fIauthentication-protocol\fR\fR
.ad
.sp .6
.RS 4n
Specifies the authentication protocol that is used
to check whether a machine has access to the cluster configuration.
.sp
Supported protocols are \fBdes\fR and \fBsys\fR (or \fBunix\fR). The default authentication type is \fBsys\fR,
which provides the least amount of secure authentication. For more
information on adding and removing nodes, see \fIAdding a Cluster Node\fR in \fISun Cluster
System Administration Guide for Solaris OS\fR. For
more information on these authentication types, see Chapter 16, \fIUsing Authentication Services (Tasks),\fR in \fISystem Administration Guide: Security Services\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB-\fB-version\fR\fR
.ad
.sp .6
.RS 4n
Displays the version of the command.
.sp
Do not specify this option with subcommands, operands, or other
options. The subcommands, operands, or other options are ignored.
The \fB-V\fR option displays only the version of the command.
No other processing is performed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB-verbose\fR\fR
.ad
.sp .6
.RS 4n
Displays verbose information to standard output (\fBstdout\fR).
.RE

.SH EXIT STATUS
.sp
.LP
If the command is successful for all specified operands, it 
returns zero (\fBCL_NOERR\fR). If an error occurs for an
operand, the command processes the next operand in the operand list. The
returned exit code always reflects the error that occurred first.
.sp
.LP
The following exit codes can be returned:
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
No error
.sp
The command that you issued completed successfully.
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
Not enough swap space
.sp
A cluster node ran out of swap memory or ran out of other operating system resources.
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
Invalid argument
.sp
You typed the command incorrectly, or the syntax of the cluster
configuration information that you supplied with the \fB-i\fR option
was incorrect.
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
Permission denied
.sp
The object that you specified is inaccessible. You might need superuser 
or RBAC access to issue the command. See the 
\fBsu\fR(1M) 
and 
\fBrbac\fR(5) 
man pages for more information.
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR \fBCL_EINTERNAL\fR\fR
.ad
.sp .6
.RS 4n
Internal error was encountered
.sp
An internal error indicates a software defect or other defect.
.RE

.sp
.ne 2
.mk
.na
\fB\fB39\fR \fBCL_EEXIST\fR\fR
.ad
.sp .6
.RS 4n
Object exists
.sp
The device, device group, cluster interconnect component, node, cluster, resource, resource type, or resource group that you specified already exists.
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRAllow a New Host Access
.sp
.LP
The following \fBclaccess\fR command allows a new
host to access the cluster configuration.

.sp
.in +2
.nf
# \fBclaccess allow -h \fIphys-schost-1\fR\fR
.fi
.in -2
.sp

.LP
\fBExample 2 \fRSet the Authentication Type
.sp
.LP
The following \fBclaccess\fR command sets the current
authentication type to \fBdes\fR.

.sp
.in +2
.nf
# \fBclaccess set -p protocol=des\fR
.fi
.in -2
.sp

.LP
\fBExample 3 \fRDeny Access to All Hosts
.sp
.LP
The following \fBclaccess\fR command denies all
hosts access to the cluster configuration.

.sp
.in +2
.nf
# \fBclaccess deny-all\fR
.fi
.in -2
.sp

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWsczu
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL),  \fBclnode\fR(1CL), \fBcluster\fR(1CL)
.SH NOTES
.sp
.LP
The superuser user can run all forms of this command.
.sp
.LP
Any user can run this command with the following subcommands
and options:
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR option
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR option
.RE
.sp
.LP
To run this command with other subcommands, users other than
superuser require \fBRBAC\fR authorizations. See the
following table.
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
Subcommand\fBRBAC\fR Authorization
_
\fBallow\fR\fBsolaris.cluster.modify\fR
_
\fBallow-all\fR\fBsolaris.cluster.modify\fR
_
\fBdeny\fR\fBsolaris.cluster.modify\fR
_
\fBdeny-all\fR\fBsolaris.cluster.modify\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBset\fR\fBsolaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
.TE

