'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH rt_properties 5 "17 Sep 2008" "Sun Cluster 3.2" "Sun Cluster Miscellaneous"
.SH NAME
rt_properties \- resource-type properties
.SH DESCRIPTION
.sp
.LP
The following information describes the resource-type properties that are defined by Sun Cluster software. These descriptions have been developed for data service developers. For information about a particular data service, see the man page for that data service.
.SS "Resource-Type Property Values"
.sp
.ne 2
.mk
.na
\fBRequired\fR
.ad
.RS 28n
.rt  
The property requires an explicit value in the Resource Type Registration (RTR) file. Otherwise, the object to which the property belongs cannot be created. A blank or the empty string is not allowed as a value.
.RE

.sp
.ne 2
.mk
.na
\fBConditional\fR
.ad
.RS 28n
.rt  
To exist, the property must be declared in the RTR file. Otherwise, the RGM does not create the property, and the property is not available to administrative utilities. A blank or the empty string is allowed. If the property is declared in the RTR file but no value is specified,
the RGM supplies a default value.
.RE

.sp
.ne 2
.mk
.na
\fBConditional/Explicit\fR
.ad
.RS 28n
.rt  
To exist, the property must be declared in the RTR file with an explicit value. Otherwise, the RGM does not create the property and the property is not available to administrative utilities. A blank or the empty string is not allowed.
.RE

.sp
.ne 2
.mk
.na
\fBOptional\fR
.ad
.RS 28n
.rt  
The property can be declared in the RTR file. If the property is not declared in the RTR file, the RGM creates it and supplies a default value. If the property is declared in the RTR file but no value is specified, the RGM supplies the same default value as if the property were
not declared in the RTR file.
.RE

.sp
.ne 2
.mk
.na
\fBQuery-only\fR
.ad
.RS 28n
.rt  
The property cannot be set directly by an administrative utility. The property is not set in the RTR file. The value of the property is provided for information only.
.RE

.LP
Note - 
.sp
.RS 2
Resource-type properties cannot be updated by administrative utilities with the exception of \fBInstalled_nodes\fR and \fBRT_system\fR. \fBInstalled_nodes\fR cannot be declared in the RTR file and can only be set by the cluster administrator. \fBRT_system\fR can be assigned an initial value in the RTR file, and can also be set by the cluster administrator.
.RE
.SS "Resource-Type Properties and Descriptions"
.sp
.LP
A resource type is defined by a resource-type registration file that specifies standard and extension property values for the resource type.
.LP
Note - 
.sp
.RS 2
resource-type property names, such as \fBAPI_version\fR and \fBBoot\fR, are \fBnot\fR case sensitive. You can use any combination of uppercase and lowercase letters when you specify property names.
.RE
.sp
.ne 2
.mk
.na
\fB\fBAPI_version\fR (\fBinteger\fR)\fR
.ad
.sp .6
.RS 4n
The version of the resource management API that is used by this resource-type implementation.
.sp
The following information summarizes the maximum \fBAPI_version\fR that is supported by each release of Sun Cluster software.
.sp
.ne 2
.mk
.na
\fBBefore and up to 3.1\fR
.ad
.RS 28n
.rt  
2
.RE

.sp
.ne 2
.mk
.na
\fB3.1 10/03\fR
.ad
.RS 28n
.rt  
3
.RE

.sp
.ne 2
.mk
.na
\fB3.1 4/04\fR
.ad
.RS 28n
.rt  
4
.RE

.sp
.ne 2
.mk
.na
\fB3.1 9/04\fR
.ad
.RS 28n
.rt  
5
.RE

.sp
.ne 2
.mk
.na
\fB3.1 8/05\fR
.ad
.RS 28n
.rt  
6
.RE

.sp
.ne 2
.mk
.na
\fB3.2\fR
.ad
.RS 28n
.rt  
7
.RE

.sp
.ne 2
.mk
.na
\fB3.2 2/08\fR
.ad
.RS 28n
.rt  
8
.RE

.sp
.ne 2
.mk
.na
\fB3.2 1/09\fR
.ad
.RS 28n
.rt  
9
.RE

Declaring a value for \fBAPI_version\fR that is greater than 2 in the RTR file prevents that resource type from being installed on a version of Sun Cluster software that supports a lower maximum version. For example, if you declare \fBAPI_version=7\fR for a resource
type, that resource type cannot be installed on any version of Sun Cluster software that was released before the Sun Cluster 3.2 release.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Optional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
2
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBBoot\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes on a node or a zone when the following conditions occur:
.RS +4
.TP
.ie t \(bu
.el o
The node or zone joins or rejoins the cluster.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The resource group that contains the resource of this type is managed.
.RE
This method is expected to initialize resources of this type as the \fBInit\fR method does.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
None
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBFailover\fR (\fBboolean\fR)\fR
.ad
.sp .6
.RS 4n
If you set this property to \fBTRUE\fR, resources of this type cannot be configured in any group that can be online on multiple nodes or zones at the same time.
.sp
You use this resource-type property in combination with the \fBScalable\fR resource property, as follows:
.sp

.sp
.TS
tab();
cw(1.42i) cw(1.42i) cw(2.66i) 
lw(1.42i) lw(1.42i) lw(2.66i) 
.
\fBFailover\fR\fBScalable\fRDescription
_
\fBTRUE\fR\fBTRUE\fRT{
Do not specify this illogical combination.
T}
\fBTRUE\fR\fBFALSE\fRT{
Specify this combination for a failover service.
T}
\fBFALSE\fR\fBTRUE\fRT{
Specify this combination for a scalable service that uses a \fBSharedAddress\fR resource for network load balancing.The \fISun Cluster Concepts Guide for Solaris OS\fR describes \fBSharedAddress\fR in more detail.You can configure a scalable resource group to run in a non-global zone. But, do not configure a scalable resource to run in multiple non-global zones on the same node.
T}
\fBFALSE\fR\fBFALSE\fRT{
Use this combination to select a multi-master service that does not use network load balancing.You can use a scalable service of this type in zones.
T}
.TE

The description for \fBScalable\fR in \fBr_properties\fR(5) and \fIKey Concepts - Administration and Application Development\fR in \fISun Cluster Concepts Guide for Solaris OS\fR contain additional information.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Optional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
\fBFALSE\fR
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBFini\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes when a resource of this type is removed from RGM management.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBGlobal_zone\fR (\fBboolean\fR)\fR
.ad
.sp .6
.RS 4n
If you set this property to \fBTRUE\fR for a resource type, its methods execute in the global zone under all circumstances. If you set this property to \fBTRUE\fR, even if the resource group is running in a non-global zone, the method executes in the global
zone. Set this property to \fBTRUE\fR only for services that can be managed only from the global zone, such as network addresses and file systems.
.LP
Caution - 
.sp
.RS 2
Do not register a resource type for which the \fBGlobal_zone\fR property is set to \fBTRUE\fR unless the resource type comes from a known and trusted source. Resource types for which this property is set to \fBTRUE\fR circumvent zone isolation
and present a risk.
.LP
Do not set the \fBGlobal_zone\fR property to \fBTRUE\fR in an RTR file that is located in a zone cluster. All resource types for which this property is set to \fBTRUE\fR must be located in the global-cluster global zone.
.RE
The methods of a resource that is configured to start in a non-global zone and whose \fBGlobal_zone\fR property is set to \fBTRUE\fR are always run in the global zone. Such a resource, when configured in a non-global zone, does not benefit from the CPU shares and
dedicated processor set configuration. This resource does not benefit even if you set the \fBRG_slm_type\fR property to \fBAUTOMATED\fR. Sun Cluster software treats such a resource as though it is located in a resource group whose \fBRG_slm_type\fR property
is set to \fBMANUAL\fR.
.sp
Because methods for resource types for which the \fBGlobal_zone\fR property is set to \fBTRUE\fR run in the global zone, the RGM does not immediately consider these resource types offline when a non-global zone dies. In fact, the RGM runs methods such as \fBMonitor_stop\fR, \fBStop\fR, and \fBPostnet_stop\fR on these resource types, which include \fBLogicalHostname\fR, \fBSharedAddress\fR, and \fBHAStoragePlus\fR. However, the RGM considers the resources for which the \fBGlobal_zone\fR property
is set to \fBFALSE\fR to be offline when a non-global zone dies. The RGM cannot run stopping methods on such resources because the methods would have to run in the non-global zone.
.sp
A resource type that declares \fBGlobal_zone=TRUE\fR might also declare the \fBGlobal_zone_override\fR resource property. In that case, the value of the \fBGlobal_zone_override\fR property supersedes the value of the \fBGlobal_zone\fR property
for that resource. For more information about the \fBGlobal_zone_override\fR property, see the \fBr_properties\fR(5) man page.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Optional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
\fBFALSE\fR
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBInit\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes when a resource of this type becomes managed by the RGM.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBInit_nodes\fR (\fBenum\fR)\fR
.ad
.sp .6
.RS 4n
Indicates the nodes or zones on which the RGM is to call the \fBInit\fR, \fBFini\fR, \fBBoot\fR, and \fBValidate\fR methods. You can set this property to \fBRG_primaries\fR (just the nodes or zones that can master
the resource) or \fBRT_installed_nodes\fR (all nodes or zones on which the resource type is installed).
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Optional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
\fBRG_primaries\fR
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBInstalled_nodes\fR (\fBstring_array\fR)\fR
.ad
.sp .6
.RS 4n
A list of the names of nodes or zones on which the resource type is allowed to run. The RGM automatically creates this property. The cluster administrator can set the value. You cannot declare this property in the RTR file.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Can be configured by the cluster administrator
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
All cluster nodes or zones
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Any time
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBIs_logical_hostname\fR (\fBboolean\fR)\fR
.ad
.sp .6
.RS 4n
\fBTRUE\fR indicates that this resource type is some version of the \fBLogicalHostname\fR resource type that manages failover IP addresses.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Query-only
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBIs_shared_address\fR (\fBboolean\fR)\fR
.ad
.sp .6
.RS 4n
\fBTRUE\fR indicates that this resource type is some version of the \fBSharedAddress\fR resource type that manages shared IP (Internet Protocol) addresses.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Query-only
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_check\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes before doing a monitor-requested failover of a resource of this type. If the monitor-check program exits with nonzero on a node or zone, any attempt to fail over to that node or zone is prevented.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_start\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes to start a fault monitor for a resource of this type.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_stop\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
A callback method that is required if \fBMonitor_start\fR is set: the path to the program that the RGM invokes to stop a fault monitor for a resource of this type.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBPkglist\fR (\fBstring_array\fR)\fR
.ad
.sp .6
.RS 4n
An optional list of packages that are included in the resource-type installation.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBPostnet_stop\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes after calling the \fBStop\fR method of any network-address resources on which a resource of this type depends. This method is expected to perform \fBStop\fR actions that must be
performed after network interfaces are configured down.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBPrenet_start\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes before calling the \fBStart\fR method of any network-address resources on which a resource of this type depends. This method is expected to perform \fBStart\fR actions that must
be performed before network interfaces are configured up.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBProxy\fR (\fBboolean\fR)\fR
.ad
.sp .6
.RS 4n
Indicates whether a resource of this type is a proxy resource.
.sp
A \fBproxy resource\fR is a Sun Cluster resource that imports the state of a resource from another cluster framework such as Oracle Cluster Ready Services (CRS). Oracle CRS, which is now known as Oracle clusterware CRS, is a platform-independent set of system services for
cluster environments.
.sp
A proxy resource type uses the \fBPrenet_start\fR method to start a daemon that monitors the state of the external (proxied) resource. The \fBPostnet_stop\fR method stops the monitoring daemon. The     monitoring daemon issues the \fBscha_control\fR command
with the \fBCHANGE_STATE_ONLINE\fR or the \fBCHANGE_STATE_OFFLINE\fR tag to set the proxy resource's state to \fBOnline\fR or to \fBOffline\fR, respectively. The \fBscha_control()\fR function similarly uses the \fBSCHA_CHANGE_STATE_ONLINE\fR and \fBSCHA_CHANGE_STATE_OFFLINE\fR tags.
.sp
If you set this property to \fBTRUE\fR, the resource is a proxy resource.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Optional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
\fBFALSE\fR
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_list\fR (\fBstring_array\fR)\fR
.ad
.sp .6
.RS 4n
The list of all resources of the resource type. The administrator does not set this property directly. Rather, the RGM updates this property when the administrator adds or removes a resource of this type to or from any resource group.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Query-only
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
Empty list
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_type\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
The name of the resource type. To view the names of the currently registered resource types, type:
.sp
\fBclresourcetype list\fR
.sp
Starting with the Sun Cluster 3.1 release, a resource-type name includes the version, which is mandatory:
.sp
\fBvendor_id.resource_type:version\fR
.sp
The three components of the resource-type name are properties that are specified in the RTR file as \fIvendor-id\fR, \fIresource-type\fR, and \fIRT-version\fR. The \fBclresourcetype\fR command inserts the period
(.) and colon (:) delimiters. The \fBRT_version\fR suffix of the resource-type name is the same value as the \fBRT_version\fR property. To ensure that the \fIvendor-id\fR is unique, the recommended approach is to use the stock symbol for the company
creating the resource type.
.sp
Resource-type names that were created before the Sun Cluster 3.1 release continue to use this syntax: \fIvendor-id.resource-type\fR.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Required
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
Empty string
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_basedir\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
The directory path that is used to complete relative paths for callback methods. This path is expected to be set to the installation location for the resource-type packages. The path must be a complete path, that is, the path must start with a forward slash (\fB/\fR).
This property is not required if all the method path names are absolute.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Required, unless all method path names are absolute
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_description\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
A brief description of the resource type.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
Empty string
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_system\fR (\fBboolean\fR)\fR
.ad
.sp .6
.RS 4n
If you set this property to \fBTRUE\fR for a resource type, you cannot delete the resource type (\fBclresourcetype unregister \fIresource-type-name\fR\fR). This property is intended to help prevent accidental deletion of resource types, such
as \fBLogicalHostname\fR, that are used to support the cluster infrastructure. However, you can apply the \fBRT_system\fR property to any resource type.
.sp
To delete a resource type whose \fBRT_system\fR property is set to \fBTRUE\fR, you must first set the property to \fBFALSE\fR. Use care when you delete a resource type whose resources support cluster services.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Optional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
\fBFALSE\fR
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Any time
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_version\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
Starting with the Sun Cluster 3.1 release, a mandatory version string that identifies this resource-type implementation. This property was optional in Sun Cluster 3.0 software. The \fBRT_version\fR is the suffix component of the full resource-type name.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit or Required
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBSingle_instance\fR (\fBboolean\fR)\fR
.ad
.sp .6
.RS 4n
If you set this property to \fBTRUE\fR, the RGM allows only one resource of this type to exist in the cluster.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Optional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
\fBFALSE\fR
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBStart\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
A callback method: the path to the program that the RGM invokes to start a resource of this type.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Required, unless the RTR file declares a \fBPrenet_start\fR method
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBStop\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
A callback method: the path to the program that the RGM invokes to stop a resource of this type.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Required, unless the RTR file declares a \fBPostnet_stop\fR method
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBUpdate\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes when properties of a running resource of this type are changed.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBValidate\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
An optional callback method: the path to the program that the RGM invokes to check values for properties of resources of this type.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional/Explicit
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBVendor_ID\fR (\fBstring\fR)\fR
.ad
.sp .6
.RS 4n
See the \fBResource_type\fR property.
.sp
.ne 2
.mk
.na
\fBCategory\fR
.ad
.RS 20n
.rt  
Conditional
.RE

.sp
.ne 2
.mk
.na
\fBDefault\fR
.ad
.RS 20n
.rt  
No default
.RE

.sp
.ne 2
.mk
.na
\fBTunable\fR
.ad
.RS 20n
.rt  
Never
.RE

.RE

.SH SEE ALSO
.sp
.LP
\fBclresource\fR(1CL), \fBclresourcegroup\fR(1CL), \fBclresourcetype\fR(1CL), \fBrt_reg\fR(4), \fBSUNW.HAStoragePlus\fR(5), \fBproperty_attributes\fR(5), \fBr_properties\fR(5), \fBrg_properties\fR(5), \fBscha_control\fR(1HA), \fBscha_control\fR(3HA).
.sp
.LP
\fISun Cluster Concepts Guide for Solaris OS\fR, \fISun Cluster Data Services Developer's Guide for Solaris OS\fR, \fISun Cluster Data
Services Planning and Administration Guide for Solaris OS\fR
