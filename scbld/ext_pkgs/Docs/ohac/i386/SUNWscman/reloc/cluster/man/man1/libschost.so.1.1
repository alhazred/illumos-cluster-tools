'\" te
.\" Copyright 2006 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms
.TH libschost.so.1 1 "6 Jan 2006" "Sun Cluster 3.2" "User Commands"
.SH NAME
libschost.so.1 \- shared object to provide logical host name instead of a physical host name
.SH SYNOPSIS
.LP
.nf
\fBlibschost.so.1\fR 
.fi

.SH DESCRIPTION
.sp
.LP
The \fBlibschost.so.1\fR shared object provides a mechanism
by which the physical host name can be selectively configured for launched
processes and their descendants.
.sp
.LP
In the Sun Cluster environment, an application might attempt to access
the same host name after a failover or switchover. As a result, the failover
or switchover fails because the name of the physical host changes after a
failover or switchover. In such a scenario, the application data service can
use the \fBlibschost.so.1\fR shared object to provide a logical
host name to the application rather than a physical host name.
.sp
.LP
To enable \fBlibschost.so.1\fR, you need to set the \fBSC_LHOSTNAME\fR environment variable as well as the following two environment
variables:
.sp
.in +2
.nf
LD_PRELOAD_32=$LD_PRELOAD_32:/usr/cluster/lib/libschost.so.1
LD_PRELOAD_64=$LD_PRELOAD_64:/usr/cluster/lib/64/libschost.so.1
.fi
.in -2

.sp
.LP
By
setting both the \fBLD_PRELOAD_32\fR and \fBLD_PRELOAD_64\fR environment variables, you ensure that the \fBlibschost.so.1\fR shared object works with both 32-bit and 64-bit applications.
.SH SECURITY
.sp
.LP
The runtime linker accesses the default trusted directory \fB/usr/lib/secure\fR for 32-bit objects and \fB/usr/lib/secure/64\fR for 64-bit objects. If your secure applications use the \fBlibschost.so.1\fR shared object, you need to ensure that the \fBlibschost.so.1\fR shared object is accessed from a trusted directory.
.sp
.LP
To do so, create a symbolic link from \fB/usr/cluster/lib/libschost.so.1\fR to \fB/usr/lib/secure/libschost.so.1\fR for 32-bit
applications or from \fB/usr/cluster/lib/64/libschost.so.1\fR
to \fB/usr/lib/secure/64/libschost.so.1\fR for 64-bit applications.
.sp
.LP
After you create these symbolic links, the \fBLD_PRELOAD_32\fR
and \fBLD_PRELOAD_64\fR environment variables use the \fBlibschost.so.1\fR shared object from a trusted directory.
.sp
.LP
You can also use the \fBcrle\fR command to specify additional
trusted directories or to change the default trusted directory for secure
applications. See the \fBcrle\fR(1)
man page.
.SH ENVIRONMENT VARIABLES
.sp
.LP
Once preloaded, the \fBlibschost.so.1\fR shared object
reads the following environment variable and returns it as the host name.
.sp
.ne 2
.mk
.na
\fB\fBSC_LHOSTNAME=\fIhostname\fR\fR\fR
.ad
.sp .6
.RS 4n
\fBSC_LHOSTNAME\fR
specifies the logical host name. The specified host name is available to all
launched and descendant processes.
.sp
The \fIhostname\fR value can be a maximum of \fBMAXHOSTNAMELEN\fR characters long. The \fBMAXHOSTNAMELEN\fR
constant is defined as 256 characters in the \fBnetdb.h\fR
header file.
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRConfiguring a Logical Host Name With a Logical Host Name at Runtime
in C
.sp
.LP
The C code in the following example configures a host name with a logical
host name. This example includes a call to the \fBscds_get_rs_hostnames()\fR Sun Cluster function and includes references to the \fBscds_handle_t\fR and \fBscds_net_resource_list_t\fR Sun
Cluster data structures.

.sp
.LP
The \fBscds_get_rs_hostnames()\fR function provides a list
of host names that are used by a resource. The code assigns the first host
name value in this list to the \fBSC_LHOSTNAME\fR environment
variable.

.sp
.LP
Any application that starts after you execute the following code gets
a logical host name rather than a physical host name.

.sp
.in +2
.nf
    /* 13 bytes to hold "SC_LHOSTNAME=" string */
   #define HOSTLENGTH (MAXHOSTNAMELEN + 13)

   /* 14 bytes to hold "LD_PRELOAD_XX=" string */
   #define PATHLENGTH (MAXPATHLEN + 14)

   char lhostname[HOSTLENGTH], ld_32[PATHLENGTH], \e
        ld_64[PATHLENGTH];

   scds_get_rs_hostnames(scds_handle, &snrlp);
   if (snrlp != NULL && snrlp->num_netresources != 0) {
       snprintf(lhostname, HOSTLENGTH, "SC_LHOSTNAME=%s", \e
           snrlp->netresources[0].hostnames[0]);
       putenv(lhostname);
   }

   /* Setting LD_PRELOAD_32 environment variable */
   if (getenv("LD_PRELOAD_32") == NULL)
       snprintf(ld_32, PATHLENGTH, "LD_PRELOAD_32="
           "/usr/cluster/lib/libschost.so.1");
   else
       snprintf(ld_32, PATHLENGTH, "LD_PRELOAD_32=%s:"
           "/usr/cluster/lib/libschost.so.1", \e
            getenv("LD_PRELOAD_32"));

   putenv(ld_32);

   /* Setting LD_PRELOAD_64 environment variable */
   if (getenv("LD_PRELOAD_64") == NULL)
       snprintf(ld_64, PATHLENGTH, "LD_PRELOAD_64="
           "/usr/cluster/lib/64/libschost.so.1");
   else
       snprintf(ld_64, PATHLENGTH, 
           "LD_PRELOAD_64=%s:/usr/cluster/lib/"
           "64/libschost.so.1", getenv("LD_PRELOAD_64"));

   putenv(ld_64);
.fi
.in -2

.LP
\fBExample 2 \fRConfiguring a Logical Host Name With a Logical Host Name at Runtime\ With
Shell Commands
.sp
.LP
The shell commands in the following example show how an application
data service configures a host name with a logical host name by using the \fBgethostnames\fR command. The \fBgethostnames\fR command
takes the following arguments:

.RS +4
.TP
.ie t \(bu
.el o
\fB-R\fR \fIresource-name\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-G\fR \fIresourcegroup-name\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-T\fR \fIresourcetype-name\fR
.RE
.sp
.LP
The \fBgethostnames\fR command returns all the logical
host names that are associated with that resource, separated by a semicolon
(;). The commands assign the first host name value in this list to the \fBSC_LHOSTNAME\fR environment variable.

.sp
.in +2
.nf
phys-schost-1$ LD_PRELOAD_32=$LD_PRELOAD_32:/usr/cluster/lib/libschost.so.1
phys-schost-1$ LD_PRELOAD_64=$LD_PRELOAD_64:/usr/cluster/lib/64/libschost.so.1
phys-schost-1$ SC_LHOSTNAME=`/usr/cluster/lib/scdsbuilder/src/scripts/gethostnames \e
              -R nfs-r -G nfs-rg -T SUNW.nfs:3.1 |cut -f1 -d","`
phys-schost-1$ export LD_PRELOAD_32 LD_PRELOAD_64 SC_LHOSTNAME
.fi
.in -2
.sp

.LP
\fBExample 3 \fRConfiguring a Logical Host Name for Secure Applications With Shell
Commands
.sp
.LP
The shell commands in the following example configure the logical host
name. Any secure application that starts after you execute the following shell
commands gets the value of the \fBSC_LHOSTNAME\fR environment
variable (that is, a logical host name) rather than a physical host name.

.sp
.in +2
.nf
phys-schost-1$ cd /usr/lib/secure
phys-schost-1$ ln -s /usr/cluster/lib/libschost.so.1 .
phys-schost-1$ cd /usr/lib/secure/64
phys-schost-1$ ln -s /usr/cluster/lib/64/libschost.so.1 .
phys-schost-1$ LD_PRELOAD_32=$LD_PRELOAD_32:/usr/lib/secure/libschost.so.1
phys-schost-1$ LD_PRELOAD_64=$LD_PRELOAD_64:/usr/lib/secure/64/libschost.so.1
phys-schost-1$ SC_LHOSTNAME=test
phys-schost-1$ export LD_PRELOAD_32 LD_PRELOAD_64 SC_LHOSTNAME
.fi
.in -2
.sp

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libschost.so.1\fR\fR
.ad
.sp .6
.RS 4n
Default location of the shared object for 32-bit applications
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/64/libschost.so.1\fR\fR
.ad
.sp .6
.RS 4n
Default location of the shared object for 64-bit applications
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5)
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBcrle\fR(1), \fBcut\fR(1), \fBhostname\fR(1), \fBld\fR(1), \fBld.so.1\fR(1), \fBproc\fR(1), \fBuname\fR(1), \fBexec\fR(2), \fBsysinfo\fR(2), \fBuname\fR(2), \fBgethostname\fR(3C), \fBputenv\fR(3C), \fBsnprintf\fR(3C), \fBsystem\fR(3C), \fBproc\fR(4)
.SH NOTES
.sp
.LP
The logical host name is inherited.
.sp
.LP
User programs that fetch a host name by calling the following commands
or functions can obtain a logical host name rather than a physical host name:
.RS +4
.TP
.ie t \(bu
.el o
\fBhostname\fR command
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBuname\fR command
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBuname()\fR function
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBsysinfo()\fR function
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBgethostname()\fR function
.RE
.sp
.LP
User programs that fetch a host name by other commands or functions
cannot obtain a logical host name.
