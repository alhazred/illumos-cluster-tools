'\" te
.\" Copyright 2006 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH scconf_dg_vxvm 1M "2 Aug 2006" "Sun Cluster 3.2" "System Administration Commands"
.SH NAME
scconf_dg_vxvm \- add, change, or update VxVM device group configuration.
.SH SYNOPSIS
.LP
.nf
\fBscconf\fR  \fB-a\fR \fB-D\fR type=vxvm,\fIdevicegroup-options\fR[,localonly=true|false]
.fi

.LP
.nf
\fBscconf\fR  \fB-c\fR \fB-D\fR \fIdevicegroup-options\fR[,sync]
.fi

.LP
.nf
\fBscconf\fR  \fB-r\fR \fB-D\fR name=\fIdevicegroupname\fR
.fi

.SH DESCRIPTION
.LP
Note - 
.sp
.RS 2
Beginning with the Sun Cluster 3.2 release, Sun Cluster software includes an object-oriented command set. Although Sun Cluster software still supports the original command set, Sun Cluster procedural documentation uses only the object-oriented command set. For more information about the object-oriented command set, see the \fBIntro\fR(1CL) man page.
.LP
The following information is specific to the \fBscconf\fR command. To use the equivalent object-oriented commands, see the \fBcldevicegroup\fR(1CL) man page.
.RE
.sp
.LP
The \fBscconf_dg_vxvm\fR command is used to add, change, and remove the Veritas Volume Manager (\fBVxVM\fR) device groups to the Sun Cluster device-groups configuration. 
.SH OPTIONS
.sp
.LP
See the \fBscconf\fR(1M) man page for the list of supported generic device-group options.
.sp
.LP
The following action options describe the actions that the command performs. Only one action option is allowed in the command.
.sp
.ne 2
.mk
.na
\fB\fB-a\fR\fR
.ad
.RS 13n
.rt  
Add a VxVM device group to the cluster configuration.
.sp
The \fB-a\fR (add) option adds a new VxVM device group to the Sun Cluster device-groups configuration. With this option you define a name for the new device group, specify the nodes on which this group can be accessed, and specify a set of properties used to control actions. 
.sp
For VxVM device groups, you can only assign one VxVM disk group to a device group, and the device-group name must always match the name of the VxVM disk group. You cannot create a VxVM device group unless you first import the corresponding VxVM disk group on one of the nodes in that device's
node list. 
.sp
Before you can add a node to a VxVM device group, every physical disk in the disk group must be physically ported to that node. After you register the disk group as a VxVM device group, you must first deport the disk group from the current node owner and turn off the auto-import flag for
the disk group. 
.sp
To create a VxVM device group for a disk group, you must run the \fBscconf\fR command from the same node where the disk group was created.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-c\fR\fR
.ad
.RS 13n
.rt  
Change the ordering of the node preference list, change preference and failback policy, and change the desired number of secondaries.
.sp
The \fBscconf\fR \fB-c\fR (change) option changes the order of the potential primary node preference, to enable or disable failback, to add more global devices to the device group, and to change the desired number of secondaries.
.sp
The \fBsync\fR suboption is used to synchronize the clustering software with VxVM disk-group volume information. The \fBsync\fR suboption is only valid with the change form of the command. Use the \fBsync\fR suboption whenever you add or remove a VxVM
volume from a VxVM device group or change any volume attribute, such as owner, group, or access permissions.
.sp
Also use the \fBsync\fR suboption to change a device-group configuration to a replicated or non-replicated configuration.
.sp
For device groups that contain disks that use Hitachi TrueCopy data replication, this \fBsync\fR suboption synchronizes the device-group configuration and the replication configuration. This synchronization makes Sun Cluster software aware of disks that are configured
for data replication and enables the software to handle failover or switchover as necessary.
.sp
After you create a Solaris Volume Manager disk set that contain disks that are configured for replication, you must run the \fBsync\fR suboption for the corresponding \fBsvm\fR or \fBsds\fR device group. A Solaris Volume Manager disk set is automatically
registered with Sun Cluster software as an \fBsvm\fR or \fBsds\fR device group, but replication information is not synchronized at that time.
.sp
For newly created \fBvxvm\fR and \fBrawdisk\fR device-group types, you do not need to manually synchronize replication information for the disks. When you register a VxVM disk group or a raw-disk device group with Sun Cluster software, the software automatically
discovers any replication information on the disks.
.sp
To change the order-of-node preference list from \fBfalse\fR to \fBtrue\fR, you must specify in the \fBnodelist\fR all the nodes that currently exist in the device group. You must also set the \fBpreferenced\fR suboption to \fBtrue\fR.
.sp
If you do not specify the \fBpreferenced\fR suboption with the \fBchange\fR form of the command, the already established \fBtrue\fR or \fBfalse\fR setting is used.
.sp
If a disk group should be accessed by only one node, it should be configured with the \fBlocalonly\fR property set to \fBtrue\fR. This property setting puts the disk group outside the control of Sun Cluster software. Only one node can be specified in the node list
to create a \fBlocalonly\fR disk group.
.sp
To change a local-only disk group to a regular VxVM disk group, set the \fBlocalonly\fR property to \fBfalse\fR. 
.RE

.sp
.ne 2
.mk
.na
\fB\fB-r\fR\fR
.ad
.RS 13n
.rt  
Remove the specified VxVM device group from the cluster. 
.sp
The \fB-r\fR (remove) option removes a VxVM device group from the Sun Cluster device-groups configuration. You can also use this form of command to remove the nodes from the VxVM device group configuration.
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRUsing \fBscconf\fR Commands
.sp
.LP
The following \fBscconf\fR commands create a VxVM device group, change the order of the potential primary nodes, change the preference and failback policy for the device group, change the desired number of secondaries, and remove the VxVM device group from the cluster configuration.

.sp
.in +2
.nf
host1# \fBscconf -a -D type=vxvm,name=diskgrp1,nodelist=host1:host2:host3,\fR\
\fBpreferenced=false,failback=enabled\fR 
host1# \fBscconf -c -D name=diskgrp1,nodelist=host2:host1:host3,\fR\
\fBpreferenced=true,failback=disabled,numsecondaries=2 sync\fR
host1# \fBscconf -r -D name=diskgrp1,nodelist=node1\fR
.fi
.in -2
.sp

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscu
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL), \fBcldevicegroup\fR(1CL), \fBscconf\fR(1M), \fBattributes\fR(5)
