'\" te
.\" Copyright 2008 Sun Microsystems, Inc.  All
.\" rights reserved. Use is subject to license terms.
.TH scds_get_rs_hostnames 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_get_rs_hostnames \- get the network resources
used by a resource
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR
  \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_get_rs_hostnames\fR(\fBscds_handle_t\fR \fIhandle\fR,
\fBscds_net_resource_list_t **\fR\fInetresource_list\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_get_rs_hostnames()\fR function retrieves a
list of hostnames used by the resource. If the resource property \fBNetwork_resources_used\fR is set, then the hostnames correspond to the network resources
listed in \fBNetwork_resources_used\fR. Otherwise, they correspond
to all the network resources in the resource group containing the resource.
.sp
.LP
This function returns a pointer to the list in \fInetresource_list\fR.
It is possible for a resource group to contain no network resources or to
contain resources that do not use network resources, so this function can
return \fInetresource_list\fR set to \fBNULL\fR.
.sp
.LP
Free the memory allocated and returned by this function with \fBscds_free_net_list\fR(3HA).
.SH PARAMETERS
.sp
.LP
The following parameters are supported
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The handle returned from \fBscds_initialize\fR(3HA)
.RE

.sp
.ne 2
.mk
.na
\fB\fInetresource_list\fR\fR
.ad
.RS 20n
.rt  
List of network resources used by the resource group
.RE

.SH RETURN VALUES
.sp
.LP
The \fBscds_get_rs_hostnames()\fR function returns the
following:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function succeeded
.RE

.sp
.ne 2
.mk
.na
\fB\fBnon-zero\fR\fR
.ad
.RS 20n
.rt  
The function failed
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 36n
.rt  
Function succeeded.
.RE

.sp
.LP
See  \fBscha_calls\fR(3HA) for a description
of other error codes.
.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) 
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscds_free_net_list\fR(3HA), \fBscds_get_rg_hostnames\fR(3HA), \fBscds_initialize\fR(3HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5), \fBr_properties\fR(5)
