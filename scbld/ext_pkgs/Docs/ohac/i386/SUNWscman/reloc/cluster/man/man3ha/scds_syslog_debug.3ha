'\" te
.\" Copyright 2007 Sun Microsystems, Inc.  All
.\" rights reserved. Use is subject to license terms.
.TH scds_syslog_debug 3HA "7 Sep 2007" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_syslog_debug \- write a debugging message
to the system log
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBvoid\fR \fB scds_syslog_debug\fR(\fBint\fR \fIdebug_level\fR, \fBconstchar *\fR\fIformat\fR...
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_syslog_debug()\fR function writes a debugging
message to the system log. It uses the facility returned by the \fBscha_cluster_getlogfacility\fR(3HA) function. 
.sp
.LP
All syslog messages are prefixed with: \fBSC[<\fIresourceTypeName\fR>,<\fIresourceGroupName\fR>,<\fIresourceName\fR>,<\fImethodName\fR>\fR
.sp
.LP
If you specify a \fIdebug_level\fR greater than the
current debugging level being used, no information is written. 
.sp
.LP
The DSDL defines the maximum debugging level, \fBSCDS_MAX_DEBUG_LEVEL\fR, as \fB9\fR. The \fBscds_initialize\fR(3HA) function, which the calling program must call before \fBscds_syslog_debug()\fR, retrieves the current debugging level from the
file: \fB/var/cluster/rgm/rt/<\fIresourceTypeName\fR>/loglevel\fR.
.LP
Caution - 
.sp
.RS 2
Messages written to the system log are not internationalized.
Do not use \fBgettext()\fR or other message translation functions
in conjunction with this function.
.RE
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIdebug_level\fR\fR
.ad
.RS 20n
.rt  
Debugging level at which this message is to be written. Valid
debugging levels are between 1 and \fBSCDS_MAX_DEBUG_LEVEL\fR,
which is defined as 9 by the DSDL. If the specified debugging level is greater
than the debugging level set by the calling program, the message is not written
to the system log.
.RE

.sp
.ne 2
.mk
.na
\fB\fIformat\fR\fR
.ad
.RS 20n
.rt  
Message format string, as specified by \fBprintf\fR(3C)
.RE

.sp
.ne 2
.mk
.na
\fB\fI\&...\fR\fR
.ad
.RS 20n
.rt  
Variables, indicated by the \fIformat\fR parameter,
as specified by \fBprintf\fR(3C)
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRDisplay All Debugging Messages
.sp
.LP
To see all debugging messages for resource type \fBSUNW.iws\fR,
issue the following command on all nodes of your cluster

.sp
.in +2
.nf
echo 9 > /var/cluster/rgm/rt/SUNW.iws/loglevel
.fi
.in -2
.sp

.LP
\fBExample 2 \fRSuppress Debugging Messages
.sp
.LP
To suppress debugging messages for resource type \fBSUNW.iws\fR,
issue the following command on all nodes of your cluster

.sp
.in +2
.nf
echo 0 > /var/cluster/rgm/rt/SUNW.iws/loglevel
.fi
.in -2
.sp

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) 
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBprintf\fR(3C), \fBscds_syslog\fR(3HA), \fBscha_cluster_getlogfacility\fR(3HA), \fBsyslog\fR(3C), \fBsyslog.conf\fR(4), \fBattributes\fR(5)
