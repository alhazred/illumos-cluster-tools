'\" te
.\" Copyright 2008 Sun Microsystems, Inc.  All
.\" rights reserved. Use is subject to license terms.
.TH scds_pmf_restart_fm 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_pmf_restart_fm \- restart fault monitor using
PMF
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_pmf_restart_fm\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBint\fR \fIinstance\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_pmf_restart_fm()\fR function sends a \fBSIGKILL\fR signal to the fault monitor process tree to kill the fault monitor
and then uses PMF to restart it. This function uses the \fBMONITOR_STOP_TIMEOUT\fR property as its timeout value. That is, \fBscds_pmf_restart_fm()\fR waits
at most the value of the \fBMONITOR_STOP_TIMEOUT\fR property
for the process tree to die.
.sp
.LP
If the \fBMONITOR_STOP_TIMEOUT\fR property is not explicitly
set in the RTR file, the default timeout value is used.
.sp
.LP
One way to use this function is to call it in an \fBUPDATE\fR method
to restart the monitor, possibly with new parameters.
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The handle returned from \fBscds_initialize()\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fIinstance\fR\fR
.ad
.RS 20n
.rt  
For resources with multiple instances of the fault monitor,
this integer, starting at 0, uniquely identifies the fault monitor instance.
For single instance fault monitors, use 0. 
.RE

.SH RETURN VALUES
.sp
.LP
The \fBscds_pmf_restart_fm()\fR function returns the following:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function succeeded.
.RE

.sp
.ne 2
.mk
.na
\fB\fBnon-zero\fR\fR
.ad
.RS 20n
.rt  
The function failed.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 20n
.rt  
Function succeeded
.RE

.sp
.LP
See  \fBscha_calls\fR(3HA) for a description
of other error codes.
.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) 
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBpmfadm\fR(1M), \fBscha_calls\fR(3HA), \fBsignal\fR(3HEAD), \fBattributes\fR(5) , \fBr_properties\fR(5)
