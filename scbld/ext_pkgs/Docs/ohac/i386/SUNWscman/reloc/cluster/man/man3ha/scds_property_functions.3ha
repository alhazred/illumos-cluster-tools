'\" te
.\" Copyright 2008 Sun Microsystems, Inc.
.\" All rights reserved. Use is subject to license terms.
.TH scds_property_functions 3HA "11 Sep 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_property_functions \- A set of convenience
functions to retrieve values of commonly used resource properties,
resource group properties, resource type properties, and extension
properties
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fB\fIreturn-value-type scds-get-property-name\fR\fR(\fBscds_handle_t\fR \fB\fIhandle\fR\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The Data Service Development Library (DSDL) provides a set of
convenience functions to retrieve values of commonly used resource
properties, resource group properties, resource type properties, and
extension properties. Retrieve user-defined extension properties with \fBscds_get_ext_property\fR(3HA).
.sp
.LP
All convenience functions use the following conventions:
.RS +4
.TP
.ie t \(bu
.el o
The functions take only the \fIhandle\fR argument.
The \fIhandle\fR argument to be passed to the property
retrieval function is returned by a prior call to \fBscds_initialize\fR(3HA).
.RE
.RS +4
.TP
.ie t \(bu
.el o
Each function corresponds to a particular property.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The return value type of the function matches the
type of the property value that the function retrieves.
.RE
.RS +4
.TP
.ie t \(bu
.el o
These functions do not return errors because the return
values have been pre-computed in \fBscds_initialize\fR(3HA). For functions that return pointers, a \fBNULL\fR value is returned when an error condition is encountered,
for example, when \fBscds_initialize()\fR was not previously
called.
.RE
.RS +4
.TP
.ie t \(bu
.el o
If a new value for a property has been specified in
the command-line arguments that are passed to the calling program
(\fIargv[]\fR), this new value is returned (in the
case of the implementation of a Validate method). By this means, you
can validate prospective new property values before they are actually
set. Otherwise, these functions return the value that is retrieved
from the RGM.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Some of these convenience functions return a pointer
to memory belonging to the DSDL. Do not modify this memory. A call
to \fBscds_close\fR(3HA) invalidates
this pointer.
.RE
.sp
.LP
See the \fBr_properties\fR(5), \fBrg_properties\fR(5), and \fBrt_properties\fR(5) man pages for descriptions of standard properties.
See the individual data service man pages for descriptions of extension
properties.
.sp
.LP
See the \fBscha_calls\fR(3HA) man page and the \fBscha_types.h\fRheader
file for information about the data types used by these functions,
such as \fBscha_prop_type_t\fR, \fBscha_extprop_value_t\fR, \fBscha_initnodes_flag_t\fR, \fBscha_str_array_t\fR, \fBscha_failover_mode_t\fR, \fBscha_switch_t\fR, and \fBscha_rsstatus_t\fR.
.sp
.LP
These functions use the following naming conventions:
.sp
.ne 2
.mk
.na
\fBResource property\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_\fR\fIproperty-name\fR
.RE

.sp
.ne 2
.mk
.na
\fBResource group property\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rg_\fR\fIproperty-name\fR
.RE

.sp
.ne 2
.mk
.na
\fBResource type property\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rt_\fR\fIproperty-name\fR
.RE

.sp
.ne 2
.mk
.na
\fBCommonly used extension property\fR
.ad
.sp .6
.RS 4n
\fBscds_get_ext_\fR\fIproperty-name\fR
.RE

.LP
Note - 
.sp
.RS 2
Property names are \fBnot\fR case sensitive.
You can use any combination of uppercase and lowercase letters when
you specify property names.
.RE
.SS "Resource-Specific Functions"
.sp
.LP
The function returns the value of a specific resource property.
Some of the properties' values are explicitly set either in the RTR
file or by a \fBclresource\fR(1CL) command. Others are determined dynamically
by the RGM. The functions return data types that correspond to the
requested property.
.sp
.LP
Each of the following resource dependencies query functions
has a corresponding "Q" or "qualified"
version:
.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies\fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies_offline_restart\fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q_offline_restart\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies_restart\fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q_restart\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBscds_get_rs_resource_dependencies_weak\fR\fR
.ad
.sp .6
.RS 4n
\fBscds_get_rs_resource_dependencies_Q_weak\fR
.RE

.sp
.LP
The qualified version returns the scope, or qualifier, if any,
that was declared for each resource dependency. The \fB{LOCAL_NODE}\fR, \fB{ANY_NODE}\fR, and \fB{FROM_RG_AFFINITIES}\fR qualifiers are described in the \fBr_properties\fR(5) man page.
.sp
.ne 2
.mk
.na
\fB\fBCheap_probe_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_cheap_probe_interval(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBFailover_mode\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_failover_mode_t scds_get_rs_failover_mode(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_stop_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_monitor_stop_timeout(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitored_switch\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_switch_t scds_get_rs_monitored_switch(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBNetwork_resources_used\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_str_array_t * scds_get_rs_network_resources_used(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBOn_off_switch\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_switch_t scds_get_rs_on_off_switch(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q\fR (qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_offline_restart\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_offline_restart(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q_offline_restart\fR (qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q_offline_restart(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_restart\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_restart(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q_restart\fR (qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q_restart(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_weak\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_weak(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_dependencies_Q_weak\fR (qualified)\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rs_resource_dependencies_Q_weak(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_project_name\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rs_resource_project_name(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRetry_count\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_retry_count(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRetry_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_retry_interval(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBScalable\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean scds_get_rs_scalable(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStart_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_start_timeout(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStop_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_stop_timeout(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBThorough_probe_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rs_thorough_probe_interval(scds_handle_t
handle)\fR
.RE

.SS "Resource Group-Specific Functions"
.sp
.LP
The function returns the value of a specific resource group
property. Some of the properties' values are explicitly set by a \fBclresourcegroup\fR(1CL) command.
Others are determined dynamically by the RGM. The functions return
data types appropriate for the requested property.
.sp
.ne 2
.mk
.na
\fB\fBDesired_primaries\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_desired_primaries(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBGlobal_resources_used\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rg_global_resources_used(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBImplicit_network_dependencies\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean_t scds_get_rg_implicit_network_dependencies(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMaximum_primaries\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_maximum_primaries(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBNodelist\fR\fR
.ad
.sp .6
.RS 4n
\fB\fR\fBconst scha_str_array_t
* scds_get_rg_nodelist (scds_handle_t handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBPathprefix\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_pathprefix(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBPingpong_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_pingpong_interval(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBResource_list\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rg_resource_list(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_affinities\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rg_rg_affinities(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_mode\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_rgmode_t scds_get_rg_rg_mode(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_project_name\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_rg_project_name(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_cpu_shares\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_rg_slm_cpu_shares(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_pset_min\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rg_rg_slm_pset_min(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_pset_type\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_rg_slm_pset_type(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRG_slm_type\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rg_rg_slm_type(scds_handle_t
handle)\fR
.RE

.SS "Resource Type-Specific Functions"
.sp
.LP
The function returns the value of a specific resource type property.
Some of the properties' values are explicitly set either in the RTR
file or by a \fBclresourcetype\fR(1CL) command. Others are determined dynamically
by the RGM. The functions return data types appropriate for the requested
property.
.sp
.ne 2
.mk
.na
\fB\fBAPI_version\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_rt_api_version(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBFailover\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean_t scds_get_rt_failover(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBInit_nodes\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_initnodes_flag_t scds_get_rt_init_nodes(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBInstalled_nodes\fR\fR
.ad
.sp .6
.RS 4n
\fBconst scha_str_array_t * scds_get_rt_installed_nodes(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_basedir\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_rt_basedir(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBRT_version\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_rt_version(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBSingle_instance\fR\fR
.ad
.sp .6
.RS 4n
\fBboolean_t scds_get_rt_single_instance(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStart_method\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_start_method(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBStop_method\fR\fR
.ad
.sp .6
.RS 4n
\fBconst char * scds_get_rt_stop_method(scds_handle_t
handle)\fR
.RE

.SS "Extension Property-Specific Functions"
.sp
.LP
The function returns the value of a specific resource extension
property. The properties' values are explicitly set either in the
RTR file or by a \fBclresource\fR(1CL) command. The functions return data types appropriate
for the requested property. 
.sp
.LP
A resource type can define extension properties beyond the four
listed here, but these four properties have convenience functions
defined for them. You retrieve these properties with these convenience
functions or with the \fBscds_get_ext_property\fR(3HA) function. You must use \fBscds_get_ext_property()\fR to retrieve extension properties other than these four.
.sp
.ne 2
.mk
.na
\fB\fBConfdir_list\fR\fR
.ad
.sp .6
.RS 4n
\fBscha_str_array_t * scds_get_ext_confdir_list(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_retry_count\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_ext_monitor_retry_count(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBMonitor_retry_interval\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_ext_monitor_retry_interval(scds_handle_t
handle)\fR
.RE

.sp
.ne 2
.mk
.na
\fB\fBProbe_timeout\fR\fR
.ad
.sp .6
.RS 4n
\fBint scds_get_ext_probe_timeout(scds_handle_t
handle)\fR
.RE

.SH PARAMETERS
.sp
.LP
The following parameter is supported for all the convenience
functions:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The handle that is returned from \fBscds_initialize\fR(3HA).
.RE

.SH RETURN VALUES
.sp
.LP
The return value type of the function matches the type of the
property value that the function retrieves.
.sp
.LP
These functions do not return errors because the return values
have been pre-computed in \fBscds_initialize\fR(3HA). For functions that return pointers, a \fBNULL\fR value is returned when an error condition is encountered,
for example, when \fBscds_initialize()\fR was not previously
called.
.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBclresource\fR(1CL), \fBclresourcegroup\fR(1CL), \fBclresourcetype\fR(1CL), \fBscds_close\fR(3HA), \fBscds_get_ext_property\fR(3HA), \fBscds_get_port_list\fR(3HA), \fBscds_get_resource_group_name\fR(3HA), \fBscds_get_resource_name\fR(3HA), \fBscds_get_resource_type_name\fR(3HA), \fBscds_initialize\fR(3HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5), \fBr_properties\fR(5), \fBrg_properties\fR(5), and \fBrt_properties\fR(5) 
