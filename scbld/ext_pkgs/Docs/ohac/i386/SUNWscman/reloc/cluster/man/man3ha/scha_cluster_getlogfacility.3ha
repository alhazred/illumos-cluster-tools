'\" te
.\" Copyright 2007 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms.
.TH scha_cluster_getlogfacility 3HA "7 Sep 2007" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scha_cluster_getlogfacility \- cluster log facility
access
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR scha
#include <scha.h>

\fB scha_err_t\fR \fBscha_cluster_getlogfacility\fR(\fBint *\fR\fIlogfacility\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscha_cluster_getlogfacility()\fR function returns
the system log facility number that is being used as the cluster log. The
value is intended to be used with the Solaris \fBsyslog\fR(3C) function by resource type
implementations to record events and status messages to the cluster log.
.sp
.LP
The function returns an error status, and if successful, the facility
number in the location pointed to by the \fIlogfacility\fR argument.
.SH RETURN VALUES
.sp
.LP
The \fBscha_cluster_getlogfacility()\fR function returns
the following:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function succeeded.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 20n
.rt  
The function failed.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 36n
.rt  
The function succeeded.
.RE

.sp
.LP
See \fBscha_calls\fR(3HA) for a description
of other error codes.
.SH EXAMPLES
.LP
\fBExample 1 \fRUsing the \fBscha_cluster_getlogfacility()\fR Function
.sp
.in +2
.nf
main()
{
	scha_err_t	err_code;
	int logfacility;

	err_code = scha_cluster_getlogfacility(&logfacility);

	if (err_code == SCHA_ERR_NOERR) {
		openlog("test resource", LOG_CONS, logfacility);
		syslog(LOG_INFO, "Access function call succeeded.");
	}
}
.fi
.in -2

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.RS 36n
.rt  
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.RS 36n
.rt  
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for
descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBsyslog\fR(3C), \fBscha_calls\fR(3HA), \fBscha_cluster_get\fR(3HA), \fBscha_strerror\fR(3HA), \fBattributes\fR(5)
