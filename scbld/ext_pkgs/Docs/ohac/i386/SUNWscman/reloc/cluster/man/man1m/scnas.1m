'\" te
.\" Copyright 2007 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH scnas 1M "6 Sep 2007" "Sun Cluster 3.2" "System Administration Commands"
.SH NAME
scnas \- manage network-attached storage (NAS) device configuration data for Sun Cluster.
.SH SYNOPSIS
.LP
.nf
\fBscnas\fR [\fB-H\fR]
.fi

.LP
.nf
\fBscnas\fR \fB-a\fR [\fB-H\fR] [\fB-n\fR] \fB-h\fR \fIdevice-name\fR \fB-t\fR \fIdevice-type\fR \fB-o\fR \fIspecific-options\fR
[\fB-f\fR \fIinput-file\fR]
.fi

.LP
.nf
\fBscnas\fR \fB-c\fR [\fB-H\fR] [\fB-n\fR ] \fB-h\fR \fIdevice-name\fR \fB-o\fR \fIspecific-options\fR
[\fB-f\fR \fIinput-file\fR]
.fi

.LP
.nf
\fBscnas\fR \fB-p\fR [\fB-H\fR] [\fB-h\fR \fIdevice-name\fR] [\fB-t\fR \fIdevice-type\fR]
.fi

.LP
.nf
\fBscnas\fR \fB-r\fR [\fB-H\fR ] \fB-h\fR \fIdevice-name\fR
.fi

.SH DESCRIPTION
.LP
Note - 
.sp
.RS 2
Beginning with the Sun Cluster 3.2 release, Sun Cluster software includes an object-oriented command set. Although Sun Cluster software still supports the original command set, Sun Cluster procedural documentation uses only the object-oriented command set. For more information about the object-oriented command set, see the \fBIntro\fR(1CL) man page.
.RE
.sp
.LP
The \fBscnas\fR command manages NAS devices in a Sun Cluster configuration.  To manage NAS directories in the cluster, use the \fBscnasdir\fR command.
.sp
.LP
You can use the \fBscnas\fR command to create the NAS device configuration, to update the NAS type-specific properties, and to remove the device configuration from Sun Cluster. The options to this command are processed in the order in which they are typed on the command line.
.sp
.LP
The \fBscnas\fR command can only be run from an active cluster node. The results of running the command are always the same, regardless of the node that is used.
.sp
.LP
All forms of the \fBscnas\fR command accept the \fB-H\fR option. Specifying \fB-H\fR displays help information. All other options are ignored. Help information is also printed when \fBscnas\fR is run without options.
.sp
.LP
The NAS device must be set up before using the \fBscnas\fR command to manage a NAS device. Refer to the documentation for the particular NAS device for procedures for setting up a device.
.sp
.LP
You can use this command only in the global zone.
.SH OPTIONS
.SS "Basic Options"
.sp
.LP
The following options are common to all forms of the \fBscnas\fR command:
.sp
.ne 2
.mk
.na
\fB\fB-H\fR \fR
.ad
.sp .6
.RS 4n
If this option is specified on the command line at any position, the command prints help information. All other options are ignored and are not executed. Help information is also printed if \fBscnas\fR is run with no options.
.sp
You can use this option only in the global zone.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fR
.ad
.sp .6
.RS 4n
If this option is specified on the command line at any position, the \fBscnas\fR command only checks the usage and does not write the configuration data. If the \fB-n\fR option is specified with the \fB-f\fR option, the \fBscnas\fR command
checks the input file for the password.
.RE

.sp
.LP
The following options modify the basic form and function of the \fBscnas\fR command. None of these options can be combined on the same command line.
.sp
.ne 2
.mk
.na
\fB\fB-a\fR\fR
.ad
.sp .6
.RS 4n
Specifies the \fBadd\fR form of the \fBscnas\fR command.
.sp
You can use this option only in the global zone.
.sp
The \fB-a\fR option can be used to add a NAS device into the Sun Cluster configuration.
.sp
Depending on the type of your NAS device, you might have to set additional properties. These required properties are also explained in the \fB-t\fR option description in the "Additional Options" section.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-c\fR\fR
.ad
.sp .6
.RS 4n
Specifies the \fBchange\fR form of the \fBscnas\fR command. The \fB-c\fR option is used to change specific NAS device properties.
.sp
You can use this option only in the global zone.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-r\fR\fR
.ad
.sp .6
.RS 4n
Specifies the \fBremove\fR form of the \fBscnas\fR command. The \fB-r\fR option is used to remove the NAS device from the Sun Cluster configuration. 
.sp
You can use this option only in the global zone.
.sp
Before removing a device, all its exported directories must be removed by using \fBscnasdir\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR\fR
.ad
.sp .6
.RS 4n
Specifies the \fBprint\fR form of the \fBscnas\fR command. 
.sp
You can use this option only in the global zone.
.sp
When no other options are given, the \fB-p\fR option prints a listing of all the current NAS devices configured in Sun Cluster and all their associated properties. This option can be used with additional options to query a particular device or a particular type of device.
.RE

.SS "Additional Options"
.sp
.LP
The following additional options can be combined with one or more of the previously described basic options to configure all properties for a device. The device does not need to be online to use these options. Refer to the SYNOPSIS section to see the options that can be used with each form
of \fBscnas\fR.
.sp
.LP
The additional options are as follows:
.sp
.ne 2
.mk
.na
\fB\fB-h\fR \fIdevice-name\fR\fR
.ad
.sp .6
.RS 4n
Use this option to specify the name of the NAS device in the Sun Cluster configuration. The device name identifies the device and can be used to remotely access the device by using \fBrhs\fR or \fBtelnet\fR.
.sp
This device name must be specified for the \fBadd\fR, \fBchange\fR, and \fBremove\fR forms of the \fBscnas\fR command. 
.RE

.sp
.ne 2
.mk
.na
\fB\fB-t\fR \fIdevice-type\fR\fR
.ad
.sp .6
.RS 4n
The NAS device type. You must specify this option when you add a NAS device to the Sun Cluster configuration. The NAS device type is identified by the vendor name.
.sp
You can specify either \fBsun\fR for a NAS device from Sun Microsystems, Inc. or \fBnetapp\fR for a NAS device from Network Appliance, Inc.
.sp
Different types of NAS devices have different or in some cases, no properties.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o\fR \fIspecific-options\fR\fR
.ad
.sp .6
.RS 4n
Use this option to provide the properties that are specific to a NAS device type. For example, the NAS device from Network Appliance, Inc. has the following property:
.sp
.in +2
.nf
\fB-o userid=\fIuserid\fR\fR
.fi
.in -2
.sp

.LP
Note - 
.sp
.RS 2
You do \fBnot\fR specify properties for the NAS device from Sun Microsystems, Inc. As this device does not have any properties, the \fB-f\fR and \fB-o\fR options do \fBnot\fR apply.
.RE
The \fBuserid\fR property is used by the cluster to perform administrative duties on the device. When you add a \fBuserid\fR to the device configuration, you are prompted for its password. You can also place the password in a text file and use it by specifying the \fB-f\fR option.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-f\fR \fIinput-file\fR\fR
.ad
.sp .6
.RS 4n
For security reasons, the password cannot be specified in command-line options. To keep the password secure, place it in a text file and specify the file by using the \fB-f\fR option. If you do not specify an input file for the password, the command prompts for the password.
.LP
Note - 
.sp
.RS 2
You do \fBnot\fR specify properties for the NAS device from Sun Microsystems, Inc. As this device does not have any properties, the \fB-f\fR and \fB-o\fR options do \fBnot\fR apply.
.RE
 Set permissions of the input file to readable by root and prohibit access by either group or world.
.sp
In the input file, the password cannot be entered across multiple lines. Leading white spaces and tabs are ignored. Comments begin with an unquoted pound (#) sign, and continue to the next new line.
.sp
The parser ignores all comments. When you use an input file for the device user password, the # sign cannot be part of the password.
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fR Adding a NAS Device From Sun Microsystems, Inc. to a Cluster
.sp
.LP
The following \fBscnas\fR command adds a Sun Microsystems, Inc. storage system to the Sun Cluster configuration.

.sp
.in +2
.nf
# \fBscnas -a -h sunnas1 -t sun\fR
.fi
.in -2
.sp

.LP
\fBExample 2 \fR Adding a NAS Device From Network Appliance, Inc. to a Cluster
.sp
.LP
The following \fBscnas\fR command adds a Network Appliance, Inc. storage system to the Sun Cluster configuration.

.sp
.in +2
.nf
# \fBscnas -a -h netapp1 -t netapp -o userid=root\fR
Please enter password:
.fi
.in -2
.sp

.LP
\fBExample 3 \fRRemoving a NAS Device From a Cluster
.sp
.LP
The following \fBscnas\fR command removes a NAS device from the Sun Cluster configuration.

.sp
.in +2
.nf
# \fBscnas -r -h sunnas1\fR
.fi
.in -2
.sp

.SH EXIT STATUS
.sp
.LP
The following exit values are returned:
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fR
.ad
.RS 20n
.rt  
The command executed successfully.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 20n
.rt  
An error has occurred.
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWsczu
_
StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL), \fBclnasdevice\fR(1CL), \fBclquorum\fR(1CL), \fBcluster\fR(1CL), \fBscconf\fR(1M), \fBscnasdir\fR(1M)
