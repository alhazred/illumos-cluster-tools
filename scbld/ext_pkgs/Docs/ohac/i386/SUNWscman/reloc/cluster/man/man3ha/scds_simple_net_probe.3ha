'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms.
.TH scds_simple_net_probe 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_simple_net_probe \- probe by establishing and terminating a TCP connection
to an application
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_simple_net_probe\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBscds_netaddr_t\fR \fIaddr\fR,
    \fBtime_t\fR \fItimeout\fR, \fBscds_fmsock_status_t *\fR\fIstatus\fR, \fBint\fR \fIcount\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_simple_net_probe()\fR function is a wrapper
function around \fBscds_fm_net_connect\fR(3HA)
and \fBscds_fm_net_disconnect\fR(3HA).
For hosts that have multiple mappings, \fBscds_simple_net_probe()\fR
handles both IPv4 and IPv6 addresses for the supplied \fBhostname\fR.
.sp
.LP
You can retrieve a list of network addresses for the resource by using \fBscds_get_netaddr_list\fR(3HA).
.sp
.LP
The status for a connect to, or disconnect from, an IPv4 target is stored
in the first member of the \fBscds_fmsock_status_t\fR array.
The second member contains the status for an IPv6 target. If the \fBhostname\fR that is supplied to this function does not contain an IPv4
or IPv6 mapping, the corresponding status is set to \fBSCDS_FMSOCK_NA\fR.
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The
handle returned by \fBscds_initialize\fR(3HA).
.RE

.sp
.ne 2
.mk
.na
\fB\fIaddr\fR\fR
.ad
.RS 20n
.rt  
The \fBhostname\fR, TCP port number, and protocol identifier that specify
where the process is listening.
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
The timeout value in seconds to wait for a successful connection. Each socket
(IPv4 or IPv6) gets the same timeout period, and timeouts proceed in parallel.
.RE

.sp
.ne 2
.mk
.na
\fB\fIstatus\fR\fR
.ad
.RS 20n
.rt  
Array
of \fBSCDS_MAX_IPADDR_TYPES\fR members of type \fBscds_fmsock_status_t\fR. Each member in the array holds a status. This parameter is an
output argument that is set by this function.
.RE

.sp
.ne 2
.mk
.na
\fB\fIcount\fR\fR
.ad
.RS 20n
.rt  
The
number of members in the \fIsocklist\fR array. Set this parameter
to \fBSCDS_MAX_IPADDR_TYPES\fR.
.RE

.SH RETURN VALUES
.sp
.LP
The \fBscds_simple_net_probe()\fR function returns the
following values:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function
succeeded.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_INVAL\fR\fR
.ad
.RS 20n
.rt  
The function was called with invalid paramaters.
.RE

.sp
.ne 2
.mk
.na
\fBOther nonzero values\fR
.ad
.RS 20n
.rt  
At least one
connect operation failed due to a timeout, a refused connection, or some other
error. Inspect the \fBerr\fR field of all members of the \fBsocklist\fR array that are set to \fBSCDS_FMSOCK_ERR\fR
to determine the exact error.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 20n
.rt  
At least one connect or
disconnect operation failed. You can inspect the \fBscds_fmsock_status_t\fR array to determine if the failure was in an IPv4 target, an IPv6
target, or both.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
Indicates that the function succeeded. 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_INTERNAL\fR\fR
.ad
.RS 28n
.rt  
Indicates that an internal error occurred while the function was executing.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_STATE\fR\fR
.ad
.RS 28n
.rt  
Indicates that the connection request was refused by the server.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
Indicates that the connection request timed out.
.RE

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5)
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
Availability\fBSUNWscdev\fR
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscds_fm_net_connect\fR(3HA), \fBscds_fm_net_disconnect\fR(3HA), \fBscds_get_netaddr_list\fR(3HA), \fBscds_initialize\fR(3HA), \fBscds_simple_probe\fR(3HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5)
