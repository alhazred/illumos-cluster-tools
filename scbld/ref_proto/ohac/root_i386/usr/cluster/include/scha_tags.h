/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_TAGS_H_
#define	_SCHA_TAGS_H_

#pragma ident	"@(#)scha_tags.h	1.43	08/07/28 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Declaration of macros with string values to be used as operation tags
 * in the Sun Cluster High-Availability Resource Management API.
 * The tag values for accessing properties of resources, resource types,
 * and resource groups are also the standard names for those properties.
 *
 */

/*
 * Locality type tags
 *
 */
#define	SCHA_LOCAL_NODE			"LOCAL_NODE"
#define	SCHA_ANY_NODE			"ANY_NODE"
#define	SCHA_FROM_RG_AFFINITIES		"FROM_RG_AFFINITIES"

/*
 * Resource access operation tags for resource properties
 * To be used with scha_resource_get()
 */
#define	SCHA_R_DESCRIPTION		"R_description"
#define	SCHA_TYPE			"Type"
#define	SCHA_TYPE_VERSION		"Type_version"
#define	SCHA_ON_OFF_SWITCH		"On_off_switch"
#define	SCHA_ON_OFF_SWITCH_NODE		"On_off_switch_node"
#define	SCHA_MONITORED_SWITCH		"Monitored_switch"
#define	SCHA_MONITORED_SWITCH_NODE	"Monitored_switch_node"
#define	SCHA_RESOURCE_STATE		"Resource_state"
#define	SCHA_TYPE_VERSION		"Type_version"

/*
 * SCHA_UPDATE_FAILED, SCHA_INIT_FAILED, SCHA_FINI_FAILED, SCHA_BOOT_FAILED
 * are reserved for future use.
 */
#define	SCHA_UPDATE_FAILED		"Update_failed"
#define	SCHA_INIT_FAILED		"Init_failed"
#define	SCHA_FINI_FAILED		"Fini_failed"
#define	SCHA_BOOT_FAILED		"Boot_failed"

#define	SCHA_CHEAP_PROBE_INTERVAL	"Cheap_probe_interval"
#define	SCHA_THOROUGH_PROBE_INTERVAL	"Thorough_probe_interval"
#define	SCHA_RETRY_COUNT		"Retry_count"
#define	SCHA_RETRY_INTERVAL		"Retry_interval"
#define	SCHA_FAILOVER_MODE		"Failover_mode"
#define	SCHA_RESOURCE_PROJECT_NAME	"Resource_project_name"
#define	SCHA_RESOURCE_DEPENDENCIES	"Resource_dependencies"
#define	SCHA_RESOURCE_DEPENDENCIES_WEAK	"Resource_dependencies_weak"
#define	SCHA_RESOURCE_DEPENDENCIES_RESTART	"Resource_dependencies_restart"
#define	SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART \
	"Resource_dependencies_offline_restart"
#define	SCHA_IC_RESOURCE_DEPENDENTS	"IC_Resource_dependents"
#define	SCHA_IC_RESOURCE_DEPENDENTS_WEAK	"IC_Resource_dependents_weak"
#define	SCHA_IC_RESOURCE_DEPENDENTS_RESTART	"IC_Resource_dependents_restart"
#define	SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART \
	"IC_Resource_dependents_offline_restart"
#define	SCHA_RESOURCE_DEPENDENCIES_Q	"Resource_dependencies_Q"
#define	SCHA_RESOURCE_DEPENDENCIES_Q_WEAK	"Resource_dependencies_Q_weak"
#define	SCHA_RESOURCE_DEPENDENCIES_Q_RESTART \
	"Resource_dependencies_Q_restart"
#define	SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART \
	"Resource_dependencies_Q_offline_restart"
#define	SCHA_NETWORK_RESOURCES_USED	"Network_resources_used"
#define	SCHA_SCALABLE			"Scalable"
#define	SCHA_PORT_LIST			"Port_list"
#define	SCHA_AFFINITY_TIMEOUT		"Affinity_timeout"
#define	SCHA_UDP_AFFINITY		"UDP_Affinity"
#define	SCHA_WEAK_AFFINITY		"Weak_Affinity"
#define	SCHA_GENERIC_AFFINITY		"Generic_Affinity"
#define	SCHA_LOAD_BALANCING_POLICY	"Load_balancing_policy"
#define	SCHA_LOAD_BALANCING_WEIGHTS	"Load_balancing_weights"
#define	SCHA_ROUND_ROBIN		"Round_robin"
#define	SCHA_CONN_THRESHOLD		"Conn_threshold"
#define	SCHA_STATUS			"Status"
#define	SCHA_NUM_RESOURCE_RESTARTS	"Num_resource_restarts"
#define	SCHA_NUM_RG_RESTARTS		"Num_rg_restarts"
#define	SCHA_NUM_RESOURCE_RESTARTS_ZONE	"Num_resource_restarts_zone"
#define	SCHA_NUM_RG_RESTARTS_ZONE	"Num_rg_restarts_zone"
#define	SCHA_GLOBAL_ZONE_OVERRIDE	"Global_zone_override"

/*
 * Resource access operation tags for resource method timeout properties
 * To be used with scha_resource_get()
 */
#define	SCHA_START_TIMEOUT		"START_TIMEOUT"
#define	SCHA_STOP_TIMEOUT		"STOP_TIMEOUT"
#define	SCHA_VALIDATE_TIMEOUT		"VALIDATE_TIMEOUT"
#define	SCHA_UPDATE_TIMEOUT		"UPDATE_TIMEOUT"
#define	SCHA_INIT_TIMEOUT		"INIT_TIMEOUT"
#define	SCHA_FINI_TIMEOUT		"FINI_TIMEOUT"
#define	SCHA_BOOT_TIMEOUT		"BOOT_TIMEOUT"
#define	SCHA_MONITOR_START_TIMEOUT	"MONITOR_START_TIMEOUT"
#define	SCHA_MONITOR_STOP_TIMEOUT	"MONITOR_STOP_TIMEOUT"
#define	SCHA_MONITOR_CHECK_TIMEOUT	"MONITOR_CHECK_TIMEOUT"
#define	SCHA_PRENET_START_TIMEOUT	"PRENET_START_TIMEOUT"
#define	SCHA_POSTNET_STOP_TIMEOUT	"POSTNET_STOP_TIMEOUT"

/*
 * Resource access operation tags other than resource properties
 * To be used with scha_resource_get()
 */

#define	SCHA_STATUS_NODE		"Status_Node"
#define	SCHA_RESOURCE_STATE_NODE	"Resource_state_Node"
#define	SCHA_EXTENSION			"Extension"
#define	SCHA_EXTENSION_NODE		"Extension_node"
#define	SCHA_PER_NODE			"Per_node"
#define	SCHA_ALL_EXTENSIONS		"All_Extensions"
#define	SCHA_GROUP			"Group"

/*
 * Resource type access operation tags for resource type properties
 * To be used with scha_resourcetype_get()
 * and also scha_resource_get()
 */
#define	SCHA_RT_DESCRIPTION		"RT_description"
#define	SCHA_RT_BASEDIR			"RT_basedir"
#define	SCHA_SINGLE_INSTANCE		"Single_instance"
#define	SCHA_PROXY			"Proxy"
#define	SCHA_INIT_NODES			"Init_nodes"
#define	SCHA_INSTALLED_NODES		"Installed_nodes"
#define	SCHA_FAILOVER			"Failover"
#define	SCHA_GLOBALZONE			"Global_zone"
#define	SCHA_API_VERSION		"API_version"
#define	SCHA_RT_VERSION			"RT_version"
#define	SCHA_PKGLIST			"Pkglist"
#define	SCHA_RT_SYSTEM			"RT_system"

/*
 * Resource type access operation tags for resource type Method properties
 * To be used with scha_resourcetype_get()
 * and also scha_resource_get()
 */
#define	SCHA_START			"START"
#define	SCHA_STOP			"STOP"
#define	SCHA_VALIDATE			"VALIDATE"
#define	SCHA_UPDATE			"UPDATE"
#define	SCHA_INIT			"INIT"
#define	SCHA_FINI			"FINI"
#define	SCHA_BOOT			"BOOT"
#define	SCHA_MONITOR_START		"MONITOR_START"
#define	SCHA_MONITOR_STOP		"MONITOR_STOP"
#define	SCHA_MONITOR_CHECK		"MONITOR_CHECK"
#define	SCHA_PRENET_START		"PRENET_START"
#define	SCHA_POSTNET_STOP		"POSTNET_STOP"

/*
 * Resource type access operation tags for implicit resource type properties
 * To be used with scha_resourcetype_get()
 * and also scha_resource_get()
 */
#define	SCHA_IS_LOGICAL_HOSTNAME	"Is_Logical_hostname"
#define	SCHA_IS_SHARED_ADDRESS		"Is_Shared_address"

/*
 * Resource group access operation tags for resource group properties
 * To be used with scha_resourcegroup_get()
 */
#define	SCHA_RG_DESCRIPTION		"RG_description"
#define	SCHA_NODELIST			"Nodelist"
#define	SCHA_MAXIMUM_PRIMARIES		"Maximum_primaries"
#define	SCHA_DESIRED_PRIMARIES		"Desired_primaries"
#define	SCHA_FAILBACK			"Failback"
#define	SCHA_RESOURCE_LIST		"Resource_list"
#define	SCHA_RG_STATE			"RG_state"
#define	SCHA_RG_DEPENDENCIES		"RG_dependencies"
#define	SCHA_RG_AFFINITIES		"RG_affinities"
#define	SCHA_GLOBAL_RESOURCES_USED	"Global_resources_used"
#define	SCHA_RG_MODE			"RG_mode"
#define	SCHA_IMPL_NET_DEPEND		"Implicit_network_dependencies"
#define	SCHA_PATHPREFIX			"Pathprefix"
#define	SCHA_PINGPONG_INTERVAL		"Pingpong_interval"
#define	SCHA_RG_PROJECT_NAME		"RG_project_name"
#define	SCHA_RG_AUTO_START		"Auto_start_on_new_cluster"
#define	SCHA_RG_SYSTEM			"RG_System"
#define	SCHA_RG_IS_FROZEN		"RG_is_frozen"
#define	SCHA_RG_SUSP_AUTO_RECOVERY	"Suspend_automatic_recovery"
/* SC SLM addon start */
#define	SCHA_RG_SLM_TYPE		"RG_SLM_type"
#define	SCHA_RG_SLM_PSET_TYPE		"RG_SLM_pset_type"
#define	SCHA_RG_SLM_CPU_SHARES		"RG_SLM_CPU_SHARES"
#define	SCHA_RG_SLM_PSET_MIN		"RG_SLM_PSET_MIN"
/* SC SLM addon end */

/*
 * Resource group access operation tags other than resource group properties
 * To be used with scha_resourcegroup_get()
 */
#define	SCHA_RG_STATE_NODE		"RG_state_Node"

/*
 * Cluster information access operation tags
 * To be used with scha_cluster_get()
 */
#define	SCHA_NODENAME_LOCAL		"Nodename_Local"
#define	SCHA_NODENAME_NODEID		"Nodename_NodeId"
#define	SCHA_ALL_NODENAMES		"All_Nodenames"
#define	SCHA_NODEID_LOCAL		"NodeId_Local"
#define	SCHA_NODEID_NODENAME		"NodeId_Nodename"
#define	SCHA_ALL_NODEIDS		"All_NodeIds"
#define	SCHA_PRIVATELINK_HOSTNAME_LOCAL	"Privatelink_Hostname_Local"
#define	SCHA_PRIVATELINK_HOSTNAME_NODE	"Privatelink_Hostname_Node"
#define	SCHA_ALL_PRIVATELINK_HOSTNAMES	"All_Privatelink_Hostnames"
#define	SCHA_NODESTATE_LOCAL		"NodeState_Local"
#define	SCHA_NODESTATE_NODE		"NodeState_Node"
#define	SCHA_SYSLOG_FACILITY		"Syslog_Facility"
#define	SCHA_ALL_RESOURCEGROUPS		"All_ResourceGroups"
#define	SCHA_ALL_RESOURCETYPES		"All_ResourceTypes"
#define	SCHA_CLUSTERNAME		"Clustername"
#define	SCHA_ZONE_LOCAL			"Zone_Local"
#define	SCHA_ALL_ZONES			"All_Zones"
#define	SCHA_ALL_NONGLOBAL_ZONES	"All_Nonglobal_zones"
#define	SCHA_ALL_ZONES_NODEID		"All_Zones_nodeid"
#define	SCHA_ALL_NONGLOBAL_ZONES_NODEID	"All_Nonglobal_zones_nodeid"



/*
 * scha_control() operation tags
 * To be used with scha_control()
 */
#define	SCHA_GIVEOVER			"GIVEOVER"
#define	SCHA_RESTART			"RESTART"
#define	SCHA_RESOURCE_RESTART		"RESOURCE_RESTART"
#define	SCHA_RESOURCE_IS_RESTARTED	"RESOURCE_IS_RESTARTED"
#define	SCHA_CHECK_GIVEOVER		"CHECK_GIVEOVER"
#define	SCHA_CHECK_RESTART		"CHECK_RESTART"
#define	SCHA_RESOURCE_DISABLE		"RESOURCE_DISABLE"
#define	SCHA_CHANGE_STATE_ONLINE	"CHANGE_STATE_ONLINE"
#define	SCHA_CHANGE_STATE_OFFLINE	"CHANGE_STATE_OFFLINE"
#define	SCHA_IGNORE_FAILED_START	"IGNORE_FAILED_START"

#ifdef __cplusplus
}
#endif

#endif	/* _SCHA_TAGS_H_ */
