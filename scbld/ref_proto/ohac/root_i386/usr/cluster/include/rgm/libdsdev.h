/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_LIBDSDEV_H
#define	_LIBDSDEV_H

#pragma ident	"@(#)libdsdev.h	1.53	08/06/10 SMI"

#include <scha.h>
#include <syslog.h>
#include <signal.h>
#include <sys/socket.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Handle returned by scds_initialize().
 * Most scds routines take this handle as their first argument.
 */
typedef void *scds_handle_t;

/*
 * Value used by fault monitors to represent probe results.
 * The probe result of SCDS_PROBE_COMPLETE_FAILURE denotes total
 * application failure.
 */
#define	SCDS_PROBE_COMPLETE_FAILURE	100

/*
 * Value used by fault monitors to represent probe results.
 * The probe result of SCDS_PROBE_IMMEDIATE_FAILOVER requests an immediate
 * failover.
 */
#define	SCDS_PROBE_IMMEDIATE_FAILOVER	201

/* For resources that have only one instance */
#define	SCDS_PMF_SINGLE_INSTANCE	0

/* Argument type for pmf utilities */
typedef enum scds_pmf_type {
	SCDS_PMF_TYPE_SVC = 0,
	SCDS_PMF_TYPE_MON,
	SCDS_PMF_TYPE_OTHER
} scds_pmf_type_t;

#define	SCDS_MAX_DEBUG_LEVEL	9

/*
 * scds_initialize()
 *
 * SUMMARY:
 *	Allocate and initialize DSDL environment.
 *	Initialize logging and the property table for the resource.
 *	This function must be called at the start of each data
 *	service method.
 *
 * PARAMETERS:
 *	handle (OUT) - initialized here and passed to other DSDL functions
 *	argc   (IN)  - the number of arguments passed to the method
 *	argv   (IN)  - the arguments to the method
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_initialize(scds_handle_t *handle, int argc, char *argv[]);

/*
 * scds_close()
 *
 * SUMMARY:
 *	Free DSDL environment resources that were allocated
 *	by scds_initialize().  Call this function once, prior
 *	to termination of the program.
 *
 * PARAMETERS:
 *	handle 	(IN)  - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	void
 */
void
scds_close(scds_handle_t *handle);

/* get functions that return miscellaneous information */

/*
 * scds_get_resource_name()
 *
 * SUMMARY:
 *	Retrieve the name of the resource
 *	for which this data service method was called.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	Resource Name
 *	NULL on failure
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_resource_name(scds_handle_t handle);

/*
 * scds_get_resource_type_name()
 *
 * SUMMARY:
 *	Retrieve the name of the resource type of the resource
 *	for which this data service method was called.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	Resource Type Name
 *	NULL on failure
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_resource_type_name(scds_handle_t handle);

/*
 * scds_get_resource_group_name()
 *
 * SUMMARY:
 *	Retrieve the name of the resource group containing the resource
 *	for which this data service method was called.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	Resource Group Name
 *	NULL on failure
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_resource_group_name(scds_handle_t handle);

/*
 * scds_get_current_method_name()
 *
 * SUMMARY:
 *	Retrieve the last element of the path name by which this data
 *	service method was called.  See basename(3C) for further information.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	Name of Method Program.
 *	NULL on failure
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char*
scds_get_current_method_name(scds_handle_t handle);

/*
 * scds_get_zone_name()
 *
 * SUMMARY:
 *	This function is intended to be used only by resource types which
 *	set GLOBAL_ZONE=true in the RTR file.  Setting GLOBAL_ZONE=true
 *	means that the method code is executed in the global zone, even if
 *	the resource group is running in a non-global zone.  In the latter
 *	case only, this function returns the zone name of the zone in which
 *	the resource group is running.  It does not return the zone name
 *	in which this method is executing, which would be the global zone.
 *
 *	In all other cases, this function returns NULL.  This includes
 *	the following cases:
 *	  - Sun Cluster is running on an operating environment (for example,
 *	    Solaris 9) that does not support zones
 *	  - the resource group, and this method, are running in the global
 *	    zone.
 *	  - the GLOBAL_ZONE property of the resource type is false,
 *	    regardless of which zone the resource group and this method
 *	    are running in.
 *
 *	To obtain the zone name of the zone in which this method is actually
 *	executing, use the zonename(1) command instead.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	If GLOBAL_ZONE is true and the resource group is executing in a
 *	non-global zone: returns the name of that non-global zone.
 *
 *	NULL in all other cases.
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char*
scds_get_zone_name(scds_handle_t handle);
const char*
scds_get_zone_type(scds_handle_t handle);
boolean_t
scds_is_zone_cluster(scds_handle_t handle);
scha_err_t
scds_verify_ip(scds_handle_t handle, char *ip);
scha_err_t
scds_verify_adapter(scds_handle_t handle, char *ip);
scha_err_t
scds_verify_ip_adapter(char *zname, const char *scha_tag,
    char *arg);

/*
 * scds_get_ext_property()
 *
 * SUMMARY:
 *	Retrieve a resource extension property.
 *	The function first looks in the property table that was set up
 *	by scds_initialize().  If it doesn't find the property there,
 *	it uses scha_calls(3HA) to retrieve the property.
 *
 * PARAMETERS:
 *	handle          (IN) - the handle returned from scds_initialize()
 *	property_name   (IN) - the name of the requested extension property
 *	property_type   (IN) - the type of the requested extension property
 *	property_value (OUT) - the value of the requested extension property
 *
 * RETURN TYPE:	scha_err_t
 *
 * MEMORY MANAGEMENT:
 *	The caller of this function is responsible for freeing the memory
 *	allocated for property_value.  Use scds_free_ext_property().
 */
scha_err_t
scds_get_ext_property(scds_handle_t handle, const char *property_name,
    scha_prop_type_t property_type, scha_extprop_value_t **property_value);

/*
 * scds_free_ext_property()
 *
 * SUMMARY:
 *	Free the resource extension property memory
 *	allocated by a call to scds_get_ext_property()
 *
 * PARAMETERS:
 *	property_value (IN) - pointer to extension property value
 *
 * RETURN TYPE:	void
 */
void
scds_free_ext_property(scha_extprop_value_t *property_value);

/*
 * scds_failover_rg()
 *
 * SUMMARY:
 *	Issue a scha_control(3HA) SCHA_GIVEOVER request to fail over the
 *	resource group containing the resource for which this data service
 *	method was called.  On success, this routine never returns.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_err_t
 */
scha_err_t
scds_failover_rg(scds_handle_t handle);

/*
 * scds_restart_rg()
 *
 * SUMMARY:
 *	Issue a scha_control(3HA) SCHA_RESTART request to restart the
 *	resource group containing the resource for which this data service
 *	method was called.  This function is intended to be called from
 *	the fault monitor.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_err_t
 */
scha_err_t
scds_restart_rg(scds_handle_t handle);

/*
 * scds_restart_resource()
 *
 * SUMMARY:
 *	Restarts a resource by calling the STOP method followed by START
 *	of the resource type that this resource belongs to. PRENET_START
 *	and POSTNET_STOP, if defined, are ignored.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_err_t
 */
scha_err_t
scds_restart_resource(scds_handle_t handle);

/*
 * scds_timerun()
 *
 * SUMMARY:
 *	Execute the specified command under the hatimerun(1M) utility.  If
 *	command does not run within the allotted time period, it is killed by
 *	sending the specified signal.
 *
 * PARAMETERS:
 *	handle		 (IN) - the handle returned from scds_initialize()
 *	command		 (IN) - string containing the command to run
 *	timeout		 (IN) - time allotted to run the command
 *	signal		 (IN) - signal to use to kill the command in case of
 *				timeout
 *	cmd_exit_code	(OUT) - exit code from execution of command
 *
 * RETURN TYPE:	scha_err_t
 *
 * RETURN VALUES:
 *	SCHA_ERR_TIMEOUT
 *	SCHA_ERR_INTERNAL
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOERR
 */
scha_err_t
scds_timerun(scds_handle_t handle, const char *command, time_t timeout,
    int signal, int *cmd_exit_code);

/*
 * scds_svc_wait()
 *
 * SUMMARY:
 *	Wait for any processes launched by scds_pmf_start() to die.
 *	Intended to be called from the START method of a data service
 *	after having launched all needed processes via scds_pmf_start().
 *	The most natural place to do this is the svc_wait() routine.
 *	Upto Retry_count failures are tolerated by restarting the
 *	failed process(es) in Retry_interval. After restarting a process,
 *	the full value of specified timeout is used again to wait for
 *	process deaths.
 *
 * PARAMETERS:
 *	handle		 (IN) - the handle returned from scds_initialize()
 *	timeout		 (IN) - time to wait for
 *
 * RETURN TYPE:	scha_err_t
 *
 * RETURN VALUES:
 *	SCHA_ERR_INTERNAL	: A system error occurred or unexpected error
 *	SCHA_ERR_FAIL	:	Retry_count failures happened
 *	SCHA_ERR_NOERR	: No process death was detected
 */
scha_err_t
scds_svc_wait(scds_handle_t handle, time_t timeout);

/*
 * scds_error_string()
 *
 * SUMMARY:
 *	Convert an error code to an error string describing the error.
 *
 * PARAMETERS:
 *	error_code 	(IN) - error code returned by a DSDL function for
 *			which an explanation string is to be returned
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	Error String
 *	NULL on failure
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.
 */
const char *
scds_error_string(scha_err_t error_code);


/*
 * scds_error_string_i18n()
 *
 * SUMMARY:
 *	Convert an error code to an error string describing the error. The
 *	error string returned by this routine is internationalized.
 *
 * PARAMETERS:
 *	error_code 	(IN) - error code returned by a DSDL function for
 *			which an explanation string is to be returned
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	Error String
 *	NULL on failure
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.
 */

const char *
scds_error_string_i18n(scha_err_t error_code);

/* get functions which return resource type properties */

/*
 * scds_get_rt_api_version()
 *
 * SUMMARY:
 *	Retrieve the API_version property for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	API version
 */
int
scds_get_rt_api_version(scds_handle_t handle);

/*
 * scds_get_rt_rt_basedir()
 *
 * SUMMARY:
 *	Retrieve the RT_basedir property for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	RT basedir
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_rt_rt_basedir(scds_handle_t handle);

/*
 * scds_get_rt_failover()
 *
 * SUMMARY:
 *	Retrieve the Failover property for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	boolean_t
 *
 * RETURN VALUE:
 *	B_TRUE
 *	B_FALSE
 */
boolean_t
scds_get_rt_failover(scds_handle_t handle);

/*
 * scds_get_rt_init_nodes()
 *
 * SUMMARY:
 *	Retrieve the Init_nodes property for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_initnodes_flag_t
 *
 * RETURN VALUE:
 *	SCHA_INFLAG_RG_PRIMARIES
 *	SCHA_INFLAG_RT_INSTALLED_NODES
 */
scha_initnodes_flag_t
scds_get_rt_init_nodes(scds_handle_t handle);

/*
 * scds_get_rt_installed_nodes()
 *
 * SUMMARY:
 *	Retrieve the Installed_nodes property for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUE:
 *	Installed nodes
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const scha_str_array_t *
scds_get_rt_installed_nodes(scds_handle_t handle);

/*
 * scds_get_rt_single_instance()
 *
 * SUMMARY:
 *	Retrieve the Single_instance property for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	boolean_t
 *
 * RETURN VALUE:
 *	B_TRUE
 *	B_FALSE
 */
boolean_t
scds_get_rt_single_instance(scds_handle_t handle);

/*
 * scds_get_rt_start_method()
 *
 * SUMMARY:
 *	Retrieve the START method name for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	start method name
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_rt_start_method(scds_handle_t handle);

/*
 * scds_get_rt_stop_method()
 *
 * SUMMARY:
 *	Retrieve the STOP method name for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	stop method name
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_rt_stop_method(scds_handle_t handle);

/*
 * scds_get_rt_rt_version()
 *
 * SUMMARY:
 *	Retrieve the RT_version property for the resource type
 *	of this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	RT version
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_rt_rt_version(scds_handle_t handle);

/* get functions which return Resource Group Properties */

/*
 * scds_get_rg_desired_primaries()
 *
 * SUMMARY:
 *	Retrieve the Desired_primaries property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Desired primaries
 */
int
scds_get_rg_desired_primaries(scds_handle_t handle);

/*
 * scds_get_rg_global_resources_used()
 *
 * SUMMARY:
 *	Retrieve the Global_resources_used property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUE:
 *	Global Resources Used
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const scha_str_array_t *
scds_get_rg_global_resources_used(scds_handle_t handle);

/*
 * scds_get_rg_implicit_network_dependencies()
 *
 * SUMMARY:
 *	Retrieve the rg_impl_net_depend property for the resource group
 *    	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	boolean_t
 *
 * RETURN VALUE:
 *	B_TRUE
 *	B_FALSE
 */
boolean_t
scds_get_rg_implicit_network_dependencies(scds_handle_t handle);

/*
 * scds_get_rg_maximum_primaries()
 *
 * SUMMARY:
 *	Retrieve the Maximum_primaries property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Maximum primaries
 *
 */
int
scds_get_rg_maximum_primaries(scds_handle_t handle);

/*
 * scds_get_rg_nodelist()
 *
 * SUMMARY:
 *	Retrieve the Nodelist property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUE:
 *	Nodelist property
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const scha_str_array_t *
scds_get_rg_nodelist(scds_handle_t handle);

/*
 * scds_get_rg_pathprefix()
 *
 * SUMMARY:
 *	Retrieve the Pathprefix property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const char *
 *
 * RETURN VALUE:
 *	pathprefix
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const char *
scds_get_rg_pathprefix(scds_handle_t handle);

/*
 * scds_get_rg_pingpong_interval()
 *
 * SUMMARY:
 *	Retrieve the Pingpong_interval property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Pingpong interval
 */
int
scds_get_rg_pingpong_interval(scds_handle_t handle);

/*
 * scds_get_rg_resource_list()
 *
 * SUMMARY:
 *	Retrieve the Resource_list property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUE:
 *	Resources List
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 */
const scha_str_array_t *
scds_get_rg_resource_list(scds_handle_t handle);

/*
 * scds_get_rg_rg_mode()
 *
 * SUMMARY:
 *	Retrieve the rg_mode property for the resource group
 *	containing this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_rgmode_t
 *
 * RETURN VALUE:
 *	RGMODE_FAILOVER
 *	RGMODE_SCALABLE
 *
 */
scha_rgmode_t
scds_get_rg_rg_mode(scds_handle_t handle);

/*
 * scds_get_rg_rg_project_name()
 *
 * SUMMARY:
 *	Obtains the RG_project_name property of a resource group.
 *
 * PARAMETERS:
 *	scds_handle_t		 (IN) - handle of a resource
 *					obtained from scds_initialize
 *
 * RETURN TYPE:
 *	 char* (Project name).
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const char*
scds_get_rg_rg_project_name(scds_handle_t handle);

/*
 * scds_get_rg_rg_slm_type()
 *
 * SUMMARY:
 *	Obtains the RG_SLM_TYPE property of a resource group.
 *
 * PARAMETERS:
 *	scds_handle_t		 (IN) - handle of a resource
 *					obtained from scds_initialize
 *
 * RETURN TYPE:
 *	 char* (rg_slm_type).
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const char*
scds_get_rg_rg_slm_type(scds_handle_t handle);

/*
 * scds_get_rg_rg_slm_pset_type()
 *
 * SUMMARY:
 *	Obtains the RG_SLM_PSET_TYPE property of a resource group.
 *
 * PARAMETERS:
 *	scds_handle_t		 (IN) - handle of a resource
 *					obtained from scds_initialize
 *
 * RETURN TYPE:
 *	 char* (rg_slm_pset_type).
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const char*
scds_get_rg_rg_slm_pset_type(scds_handle_t handle);

/*
 * scds_get_rg_rg_slm_cpu_shares()
 *
 * SUMMARY:
 *	Obtains the RG_SLM_CPU_SHARES property of a resource group.
 *
 * PARAMETERS:
 *	scds_handle_t		 (IN) - handle of a resource
 *					obtained from scds_initialize
 *
 * RETURN TYPE:
 *	 int (rg_slm_cpu).
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
int
scds_get_rg_rg_slm_cpu_shares(scds_handle_t handle);

/*
 * scds_get_rg_rg_slm_pset_min()
 *
 * SUMMARY:
 *	Obtains the RG_SLM_PSET_MIN property of a resource group.
 *
 * PARAMETERS:
 *	scds_handle_t		 (IN) - handle of a resource
 *					obtained from scds_initialize
 *
 * RETURN TYPE:
 *	 int (rg_slm_cpu_min).
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
int
scds_get_rg_rg_slm_pset_min(scds_handle_t handle);

/*
 * scds_get_rg_rg_affinities()
 *
 * SUMMARY:
 *	Retrieves the resource group affinites
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Affinities for this resource group.
 *
 */
const scha_str_array_t*
scds_get_rg_rg_affinities(scds_handle_t handle);


/* get functions which return Resource properties */

/*
 * scds_get_rs_cheap_probe_interval()
 *
 * SUMMARY:
 *	Obtains the Cheap_probe_interval property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	cheap probe interval
 *
 */
int
scds_get_rs_cheap_probe_interval(scds_handle_t handle);

/*
 * scds_get_rs_failover_mode()
 *
 * SUMMARY:
 *	Obtains the Failover_mode property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_failover_mode_t
 *
 * RETURN VALUE:
 *	SCHA_FOMODE_NONE
 *	SCHA_FOMODE_HARD
 *	SCHA_FOMODE_SOFT
 *	SCHA_FOMODE_RESTART_ONLY
 *	SCHA_FOMODE_LOG_ONLY
 *
 */
scha_failover_mode_t
scds_get_rs_failover_mode(scds_handle_t handle);

/*
 * scds_get_rs_monitor_stop_timeout()
 *
 * SUMMARY:
 *	Obtains the MONITOR_STOP_TIMEOUT property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Monitor Stop Timeout
 *
 */
int
scds_get_rs_monitor_stop_timeout(scds_handle_t handle);

/*
 * scds_get_rs_monitored_switch()
 *
 * SUMMARY:
 *	Obtains the Monitored Switch property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_switch_t
 *
 * RETURN VALUES:
 *	SCHA_SWITCH_DISABLED
 *	SCHA_SWITCH_ENABLED
 *
 */
scha_switch_t
scds_get_rs_monitored_switch(scds_handle_t handle);

/*
 * scds_get_rs_on_off_switch()
 *
 * SUMMARY:
 *	Obtains the ON_OFF_SWITCH property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_switch_t
 *
 * RETURN VALUES:
 *	SCHA_SWITCH_DISABLED
 *	SCHA_SWITCH_ENABLED
 *
 */
scha_switch_t
scds_get_rs_on_off_switch(scds_handle_t handle);

/*
 * scds_get_rs_resource_dependencies()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_str_array_t*
 *
 * RETURN VALUES:
 *	Resource names upon which this resource is dependent.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies(scds_handle_t handle);

/*
 * scds_get_rs_resource_dependencies_weak()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies_weak property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Resource names upon which this resource has weak dependencies.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies_weak(scds_handle_t handle);


/*
 * scds_get_rs_resource_dependencies_restart()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies_restart property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Resource names upon which this resource has restart dependencies.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies_restart(scds_handle_t handle);

/*
 * scds_get_rs_resource_dependencies_offline_restart()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies_offline_restart property for this
 * resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Resource names upon which this resource has restart dependencies.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies_offline_restart(scds_handle_t handle);

/*
 * scds_get_rs_resource_dependencies_Q()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies property for this resource
 * with node locality qualifier.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Resource names upon which this resource has strong dependencies
 * with node locality qualifier.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies_Q(scds_handle_t handle);

/*
 * scds_get_rs_resource_dependencies_Q_restart()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies_restart property for this resource
 * with node locality qualifier.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Resource names upon which this resource has restart dependencies
 * with node locality qualifier.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies_Q_restart(scds_handle_t handle);

/*
 * scds_get_rs_resource_dependencies_Q_offline_restart()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies_offline_restart property for this
 * resource with node locality qualifier.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Resource names upon which this resource has offline restart dependencies
 * with node locality qualifier.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies_Q_offline_restart(scds_handle_t handle);

/*
 * scds_get_rs_resource_dependencies_Q_weak()
 *
 * SUMMARY:
 *	Obtains the Resource_dependencies_weak property for this resource
 * with node locality qualifier.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	const scha_str_array_t *
 *
 * RETURN VALUES:
 *	Resource names upon which this resource has weak dependencies
 * with node locality qualifier.
 *
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const scha_str_array_t *
scds_get_rs_resource_dependencies_Q_weak(scds_handle_t handle);

/*
 * scds_get_rs_resource_project_name()
 *
 * SUMMARY:
 *	Obtains the Resource_project_name property for this
 *	resource.
 *
 * PARAMETERS:
 *	scds_handle_t		 (IN) - handle of a resource
 *					obtained from scds_initialize
 *
 * RETURN TYPE:
 *	 char* (Project name).
 *
 * MEMORY MANAGEMENT:
 *	The pointer returned by this function points to memory belonging
 *	to the DSDL.  Do not modify or free the memory.  The pointer is
 *	not valid after a call to scds_close().
 *
 */
const char*
scds_get_rs_resource_project_name(scds_handle_t handle);


/*
 * scds_get_rs_retry_count()
 *
 * SUMMARY:
 *	Obtains the Retry_count property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Retry Count
 *
 */
int
scds_get_rs_retry_count(scds_handle_t handle);

/*
 * scds_get_rs_retry_interval()
 *
 * SUMMARY:
 *	Obtains the Retry_interval property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Retry Interval
 *
 */
int
scds_get_rs_retry_interval(scds_handle_t handle);

/*
 * scds_get_rs_start_timeout()
 *
 * SUMMARY:
 *	Obtains the START_TIMEOUT property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Start Timeout
 *
 */
int
scds_get_rs_start_timeout(scds_handle_t handle);

/*
 * scds_get_rs_stop_timeout()
 *
 * SUMMARY:
 *	Obtains the STOP_TIMEOUT property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Stop Timeout
 *
 */
int
scds_get_rs_stop_timeout(scds_handle_t handle);

/*
 * scds_get_rs_thorough_probe_interval()
 *
 * SUMMARY:
 *	Obtains the Thorough_probe_interval property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	Thorough probe interval
 *
 */
int
scds_get_rs_thorough_probe_interval(scds_handle_t handle);

/*
 * scds_get_rs_scalable()
 *
 * SUMMARY:
 *	Obtains the Scalable property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	boolean_t
 *
 * RETURN VALUE:
 *	B_TRUE
 *	B_FALSE
 *
 */
boolean_t
scds_get_rs_scalable(scds_handle_t handle);

/*
 * scds_get_rs_global_zone_override()
 *
 * SUMMARY:
 *	Obtains the Global_zone_override property for this resource.
 *
 * PARAMETERS:
 *	handle	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	boolean_t
 *
 * RETURN VALUE:
 *	B_TRUE
 *	B_FALSE
 *
 * NOTE:
 *	Global_zone_override is an optional/conditional property.  It is
 *	undeclared for most resource types.  It can only be declared for
 *	a resource type that sets Global_zone=TRUE.  If Global_zone_override
 *	is declared, its value (returned by this function) supersedes the
 *	value of Global_zone=TRUE set for the resource type.
 *
 *	This function should not be called by resource types for which
 *	Global_zone_override is undeclared.  In that case, this function
 *	would return B_FALSE, but that value would not be meaningful.
 *	The behavior of the resource within non-global zones would be governed
 *	only by the value of the Global_zone property of the resource type.
 *	See rt_properties(5) and r_properties(5) for more information.
 */
boolean_t
scds_get_rs_global_zone_override(scds_handle_t handle);

/*
 * get functions which return DSDL defined extension properties
 */

/*
 * scds_get_ext_confdir_list()
 *
 * SUMMARY:
 *	Obtains the Confdir_list extension property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	scha_str_array_t *
 *
 * RETURN VALUE:
 *	Confdir_list
 *
 * MEMORY MANAGEMENT:
 *	The caller of this function is responsible for freeing the memory
 *	allocated for property_value.  Use scds_free_ext_property().
 *
 */
scha_str_array_t *
scds_get_ext_confdir_list(scds_handle_t handle);

/*
 * scds_get_ext_monitor_retry_count()
 *
 * SUMMARY:
 *	Obtains the Monitor_retry_count extension property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	monitor retry count
 *	-1 for unset
 *
 */
int
scds_get_ext_monitor_retry_count(scds_handle_t handle);

/*
 * scds_get_ext_monitor_retry_interval()
 *
 * SUMMARY:
 *	Obtains the Monitor_retry_interval extension property for this
 *	resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	monitor retry interval
 *	-1 for unset
 *
 */
int
scds_get_ext_monitor_retry_interval(scds_handle_t handle);

/*
 * scds_get_ext_probe_timeout()
 *
 * SUMMARY:
 *	Retrieve the Probe_timeout extension property for this resource.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	int
 *
 * RETURN VALUE:
 *	probe timeout
 *	-1 for unset
 *
 */
int
scds_get_ext_probe_timeout(scds_handle_t handle);

/*
 * functions and data-structures to retrieve network resources related
 * information
 */

/* Structures to hold network resources */
typedef struct scds_net_resource {
	char	*name;		/* Name of the resource */
	int	num_hostnames;	/* Number of hostnames */
	char	**hostnames;	/* Array of hostnames */
} scds_net_resource_t;

/* A list of Network resources */
typedef struct scds_net_resource_list {
	int		    num_netresources;	/* num of resources */
	scds_net_resource_t *netresources;	/* array of */
						/* scds_net_resource_t */
} scds_net_resource_list_t;

/*
 * scds_get_rg_hostnames()
 *
 * SUMMARY:
 *	Get the IP addresses that are used by a resource group.
 *
 * PARAMETERS:
 * 	resourcegroup_name  	(IN)  - name of the resource group
 * 	netresource_list  	(OUT) - the struct scds_net_resource_list
 *				will be	created and filled in with the network
 *				resource information for all network resources
 *				in the given RG.  snrl will be
 *				set to point to the scds_net_resource_list
 *				that the library will allocate.  If no network
 *				resource is found, this pointer will be set
 *				to NULL.
 *
 * RETURN TYPE:	scha_err_t
 *
 * MEMORY MANAGEMENT:
 *	The caller of this function is responsible for freeing the memory
 *	allocated for property_value.  Use scds_free_net_list().
 *
 */
scha_err_t
scds_get_rg_hostnames(char *resourcegroup_name,
    scds_net_resource_list_t **netresource_list);
scha_err_t
scds_get_rg_hostnames_zone(char *zone_name,
    char *resourcegroup_name,
    scds_net_resource_list_t **netresource_list);

/*
 * scds_get_rs_hostnames()
 *
 * SUMMARY:
 *	Get the IP addresses that are used by a resource.
 *
 * PARAMETERS:
 *	handle		 (IN)  - the handle returned from scds_initialize()
 *	netresource_list (OUT) - the struct scds_net_resource_list will be
 *				created and filled in with the network
 *				resource information for all network resources
 *				used explicitly (via network_resource_used)
 *				or implicitly (all network resources in its
 *				containing Resource Group) by the resource
 *				snrl will be set to point to the
 *				scds_net_resource_list that the library will
 *				allocate.  If no network resource is found,
 *				this pointer will be set to NULL.
 *
 * RETURN TYPE:	scha_err_t
 *
 * MEMORY MANAGEMENT:
 *	The caller of this function is responsible for freeing the memory
 *	allocated for property_value.  Use scds_free_net_list().
 */
scha_err_t
scds_get_rs_hostnames(scds_handle_t handle,
	scds_net_resource_list_t **netresource_list);

/*
 * scds_free_net_list()
 *
 * SUMMARY:
 *	Free the memory allocated by scds_get_rs_hostnames() or
 *	scds_get_rg_hostnames().
 *
 * PARAMETERS:
 *	netresource_list (IN) - pointer to scds_net_resource_list that was
 *				filled in by a call to scds_get_rs_hostnames()
 *				or scds_get_rg_hostnames().  This function
 *				will iterate through all of the substructures
 *				and free all library allocated memory
 *				associated with the structure.
 *
 * RETURN TYPE:	void
 *
 */
void
scds_free_net_list(scds_net_resource_list_t *netresource_list);

/*
 * scds_print_net_list()
 *
 * SUMMARY:
 *	Debugging function to print the network resource
 *	structure at debug level
 *
 * PARAMETERS:
 *	handle		(IN) - the handle returned from scds_initialize()
 *	debug_level	(IN) - int between 1-9 (9 being the most verbose level)
 *	netresource_list (IN) - pointer to a scds_net_resource_list
 *				struct for printing out
 *
 * RETURN TYPE:	void
 *
 */
void
scds_print_net_list(scds_handle_t handle, int debug_level,
    const scds_net_resource_list_t *netresource_list);

/*
 * functions and data-structures to retrieve port and protocol
 * combination related information.
 */

#define	SCDS_IPPROTO_TCP IPPROTO_TCP
#define	SCDS_IPPROTO_UDP IPPROTO_UDP
#define	SCDS_IPPROTO_SCTP IPPROTO_SCTP
#define	SCDS_IPPROTO_TCP6 23593
#define	SCDS_IPPROTO_UDP6 23594

/*
 * Struct to hold port/proto combinations as
 * defined via the Port_list property.
 * Valid values are defined above
 */
typedef struct scds_port {
	int	port;	/* Port number */
	int	proto;	/* Protocol */
} scds_port_t;

/* List of ports */
typedef struct scds_port_list {
	int		num_ports;	/* Number of ports */
	scds_port_t	*ports;		/* Array of ports */
} scds_port_list_t;

/*
 * scds_get_port_list()
 *
 * SUMMARY:
 *	returns a list of port-protocol pairs used by the resource.
 *
 * PARAMETERS:
 *	handle 		(IN) - the handle returned from scds_initialize()
 *	port_list 	(OUT) - a list of scds_port_t structures
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_get_port_list(scds_handle_t handle, scds_port_list_t **port_list);

/*
 * scds_free_port_list()
 *
 * SUMMARY:
 *	Free the memory allocated by scds_get_port_list()
 *
 * PARAMETERS:
 *	port_list (IN) - pointer to scds_port_list_t that was
 *			filled in by a call to scds_get_port_list().
 *			This function will iterate through all of the
 *			substructures and free all library allocated
 *			memory associated with the structure.
 *
 * RETURN TYPE:	void
 *
 */
void
scds_free_port_list(scds_port_list_t *port_list);

/*
 * scds_print_port_list()
 *
 * SUMMARY:
 *	Debugging function to print the scds_port_list_t
 *	structure at debug level
 *
 * PARAMETERS:
 *	handle		(IN) -  the handle returned from scds_initialize()
 *	debug_level	(IN) -  int between 1-9 (9 being the
 *				most verbose level)
 *	port_list	(IN) -  pointer to a scds_port_list_t
 *				struct for printing out
 *
 * RETURN TYPE:	void
 *
 */
void
scds_print_port_list(scds_handle_t handle, int debug_level,
    const scds_port_list_t *port_list);

/*
 * functions and data-structures to retrieve Network Addresses
 * (hostname-port-protocol 3-tuples) related information
 */

typedef struct scds_netaddr {
	char		*hostname;	/* Hostname */
	scds_port_t	port_proto;	/* Port: includes proto */
} scds_netaddr_t;

typedef struct scds_netaddr_list {
	int		num_netaddrs;	/* Number of 3-tuples */
	scds_netaddr_t	*netaddrs;	/* Array of scds_netaddr_t */
} scds_netaddr_list_t;

/*
 * scds_get_netaddr_list()
 *
 * SUMMARY:
 *	Computes the list of network addresses (hostname-port-protocol
 *	3-tuples)
 *
 * PARAMETERS:
 *	handle 		(IN)  - the handle returned from scds_initialize()
 *	netaddr_list 	(OUT) - the list of all hostname-port-protocol
 *				combination 3-tuples in use by the resource
 *
 * RETURN TYPE:	scha_err_t
 *
 * MEMORY MANAGEMENT:
 *	The caller of this function is responsible for freeing the memory
 *	allocated for property_value.  Use scds_free_netaddr_list().
 *
 */
scha_err_t
scds_get_netaddr_list(scds_handle_t handle,
    scds_netaddr_list_t **netaddr_list);

/*
 * scds_free_netaddr_list()
 *
 * SUMMARY:
 *	Free the memory allocated by scds_get_netaddr_list()
 *
 * PARAMETERS:
 *	netaddr_list  	(IN) - 	pointer to scds_netaddr_list_t that was
 *				filled in by a call to scds_get_netaddr_list().
 *				This function will iterate through all of the
 *				substructures and free all library allocated
 *				memory associated with the structure.
 *
 * RETURN TYPE:	void
 *
 */
void
scds_free_netaddr_list(scds_netaddr_list_t *netaddr_list);

/*
 * scds_print_netaddr_list()
 *
 * SUMMARY:
 *	Debugging function to print the scds_netaddr_list_t
 *	structure at debug level
 *
 * PARAMETERS:
 *	handle		(IN) - 	the handle returned from scds_initialize()
 *	debug_level	(IN) - 	int between 1-9 (9 being the most verbose
 *				level)
 *	netaddr_list	(IN) - 	a list of scds_netaddr_list_t
 *				struct for printing out
 *
 * RETURN TYPE:	void
 *
 */
void
scds_print_netaddr_list(scds_handle_t handle, int debug_level,
    const scds_netaddr_list_t *netaddr_list);

/*
 * Utilities for connecting to and probing a server process over TCP
 */

/* Maximum IP address types - IPv4 and IPv6 for a total of 2 */
#define	SCDS_MAX_IPADDR_TYPES 2

/* Status of the socket fd */
typedef enum scds_fmsock_status {
	SCDS_FMSOCK_NA, /* Not applicable */
	SCDS_FMSOCK_OK, /* Valid socket fd */
	SCDS_FMSOCK_ERR /* Error occured, socket fd is invalid */
} scds_fmsock_status_t;

/* Socket and associated values used by fault monitoring APIs */
typedef struct scds_socket {
	int fd; 			/* Socket fd */
	scds_fmsock_status_t status;	/* Status of the fd */
	scha_err_t err;			/* Error code if status == error */
} scds_socket_t;

/*
 * scds_fm_net_connect()
 *
 * SUMMARY:
 * 	Establish TCP connection(s) to an application
 *
 * PARAMETERS:
 * 	handle    (IN) - handle returned from scds_initialize()
 * 	socklist (OUT) - socket(s) created by scds_fm_net_connect()
 * 	count     (IN) - number of elements in socklist
 * 	addr      (IN) - hostname/port/proto to connect to
 * 	timeout   (IN) - length of time to wait for successful connection(s)
 *
 * RETURN TYPE: scha_err_t
 *
 */
scha_err_t
scds_fm_net_connect(scds_handle_t handle, scds_socket_t *socklist, int count,
    scds_netaddr_t addr, time_t timeout);

/*
 * scds_fm_net_disconnect()
 *
 * SUMMARY:
 * 	Terminate TCP connection(s) to an application
 *
 * PARAMETERS:
 * 	handle    (IN) - handle returned from scds_initialize()
 * 	socklist  (IN) - socket(s) returned by scds_fm_net_connect()
 * 	count     (IN) - number of elements in socklist
 * 	timeout   (IN) - length of time to wait for successful disconnection(s)
 *
 * RETURN TYPE: scha_err_t
 *
 */
scha_err_t
scds_fm_net_disconnect(scds_handle_t handle, scds_socket_t *socklist,
    int count, time_t timeout);

/*
 * scds_fm_tcp_connect()
 *
 * SUMMARY:
 *	Make a TCP connection to the selected host and port number
 *
 * PARAMETERS:
 *	handle	 (IN) - the handle returned from scds_initialize()
 *	sock  	(OUT) - the socket handle created by scds_fm_tcp_connect()
 *	hostname (IN) - Internet hostname of machine which to connect.
 *	port     (IN) - Port number to make the connection with.
 *	timeout  (IN) - the length of time to wait for a successful connection.
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_fm_tcp_connect(scds_handle_t handle, int *sock,
    const char *hostname, int port, time_t timeout);

/*
 * scds_fm_tcp_disconnect()
 *
 * SUMMARY:
 *	Close a TCP socket connection under a timeout.
 *
 * PARAMETERS:
 *	handle	(IN) - the handle returned from scds_initialize()
 *	sock  	(IN) - the socket handle created by scds_fm_tcp_connect()
 *	timeout (IN) - the length of time to wait for a successful disconnect.
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_fm_tcp_disconnect(scds_handle_t handle, int sock, time_t timeout);

/*
 * scds_fm_tcp_read()
 *
 * SUMMARY:
 *	Read from a TCP socket connection under a timeout. It is up
 *	to the caller to have some mechanism to decide how much to
 *	read and when to stop reading.
 *
 * PARAMETERS:
 *	handle		(IN) - 	the handle returned from scds_initialize()
 *	sock  		(IN) - 	the socket handle created by
 *				scds_fm_tcp_connect()
 *	buffer  	(IN) -	the buffer used for reading.
 *	size	    (IN/OUT) -	the length of how much to read, this parameter
 *				is set to the actual number of bytes read.
 *	timeout 	(IN) -	the length of time to wait for a successful
 *				read.
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_fm_tcp_read(scds_handle_t handle, int sock,
    char *buffer, size_t *size, time_t timeout);

/*
 * scds_fm_tcp_write()
 *
 * SUMMARY:
 *	Write into a TCP socket under a timeout.
 *
 * PARAMETERS:
 *	handle		(IN) -	the handle returned from scds_initialize()
 *	sock  		(IN) -	the socket handle created by
 *				scds_fm_tcp_connect()
 *	buffer  	(IN) -	the buffer from which to write.
 *	size	    (IN/OUT) -	the length of how much to read, this parameter
 *				is set to the actual number of bytes written.
 *	timeout 	(IN) -	the length of time to wait for a successful
 *				write.
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_fm_tcp_write(scds_handle_t handle, int sock,
    char *buffer, size_t *size, time_t timeout);

/*
 * scds_simple_net_probe()
 *
 * SUMMARY:
 * 	A wrapper function around scds_fm_net_connect() and
 * 	scds_fm_net_disconnect() run under a timeout.
 *
 * PARAMETERS:
 *	handle   (IN) - the handle returned from scds_initialize()
 *	addr     (IN) - Internet hostname of machine which to connect.
 *	timeout  (IN) - the length of time to wait for a successful probe
 *	status  (OUT) - status of connection attempt(s)
 *	count    (IN) - number of elements in status
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_simple_net_probe(scds_handle_t handle, scds_netaddr_t addr, time_t timeout,
    scds_fmsock_status_t *status, int count);

/*
 * scds_simple_probe()
 *
 * SUMMARY:
 * 	A wrapper function around scds_fm_tcp_connect() and
 * 	scds_fm_tcp_disconnect() run under a timeout.
 *
 * PARAMETERS:
 *	handle	 (IN) - the handle returned from scds_initialize()
 *	hostname (IN) - Internet hostname of machine which to connect.
 *	port     (IN) - Port number to make the connection with.
 *	timeout  (IN) - the length of time to wait for a successful probe
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_simple_probe(scds_handle_t handle, const char *hostname, int port,
    time_t timeout);

/*
 * Utilities for launching/monitoring processes via the PMF facility
 */

typedef enum scds_pmf_status {
	SCDS_PMF_MONITORED = 0,
	SCDS_PMF_NOT_MONITORED
} scds_pmf_status_t;

/*
 * scds_pmf_get_status()
 *
 * SUMMARY:
 *	Determine if a PMF tag is currently active.
 *
 * PARAMETERS:
 *	handle 		(IN) - 	the handle returned from scds_initialize()
 * 	program_type  	(IN) - 	the type of program that this call is
 * 				being made for (ie, is it for a service
 * 				or for a monitor). This value will determine
 * 				the tag suffix that is used.
 * 	instance 	(IN) - 	for resources where there are multiple
 * 				instances in one resource, the index will
 * 				identify the instance uniquely.
 * 	pmf_status 	(OUT) - status of the given instance
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_pmf_get_status(scds_handle_t handle, scds_pmf_type_t program_type,
    int instance, scds_pmf_status_t *pmf_status);

/*
 * scds_pmf_restart_fm()
 *
 * SUMMARY:
 *	Restart the fault monitor if it is running.
 *
 * PARAMETERS:
 *	handle   	(IN) - 	the handle returned from scds_initialize()
 * 	instance 	(IN) - 	for resources where there are multiple
 * 				instances of the fault monitor, the index will
 * 				identify the instance uniquely.
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_pmf_restart_fm(scds_handle_t handle, int instance);

/*
 * scds_pmf_signal()
 *
 * SUMMARY:
 *	Send a signal to a command that is running under pmfadm.
 *	(pmfadm -k ...)
 *
 * PARAMETERS:
 *	handle  	(IN) - 	the handle returned from scds_initialize()
 *	program_type	(IN) -  the type of program that this call is
 *				being made for (ie, is it for a service
 *				or for a monitor). This value will determine
 *				the tag suffix that is used.
 *	instance  	(IN) -  for resources where there are multiple
 *				instances in one resource, the index will
 *				identify the instance uniquely.
 *	signal		(IN) - 	the signal to send to the command.
 *	timeout		(IN) - 	the timeout to specify to PMF
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_pmf_signal(scds_handle_t handle, scds_pmf_type_t program_type,
    int instance, int signal, time_t timeout);

/*
 * scds_pmf_start()
 *
 * SUMMARY:
 *	Start a command under pmfadm. (pmfadm -c ...)
 *
 * 	If this data service does not support multiple instances within
 * 	one resource, the index flag should be set to SCDS_PMF_SINGLE_INSTANCE
 *
 * PARAMETERS:
 *	handle 		(IN) - 	the handle returned from scds_initialize()
 *	program_type 	(IN)  - the type of program that this call is
 *				being made for (ie, is it for a service
 *				or for a monitor). This value will determine
 *				the tag suffix that is used.
 *	instance 	(IN)  - for resources where there are multiple
 *				instances in one resource, the index will
 *				identify the instance uniquely.
 *	command		(IN)  - command to start running under pmfadm.
 *				This command can include command line arguments
 *				to pmfadm at the beginning of the command
 *				string.
 *	child_monitor_level (IN) - The level of children which should
 *				be monitored. Corresponds to the -C option to
 *				pmfadm(1m)
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_pmf_start(scds_handle_t handle, scds_pmf_type_t program_type,
    int instance, const char *command, int child_monitor_level);


/*
 * scds_pmf_start_env()
 *
 * SUMMARY:
 *	Start a command under pmfadm. (pmfadm -c ...). This command is
 *	identical to scds_pmf_start() except for the environment
 *	variables.
 *
 * 	If this data service does not support multiple instances within
 * 	one resource, the index flag should be set to SCDS_PMF_SINGLE_INSTANCE
 *
 * PARAMETERS:
 *	handle 		(IN) - 	the handle returned from scds_initialize()
 *	program_type 	(IN)  - the type of program that this call is
 *				being made for (ie, is it for a service
 *				or for a monitor). This value will determine
 *				the tag suffix that is used.
 *	instance 	(IN)  - for resources where there are multiple
 *				instances in one resource, the index will
 *				identify the instance uniquely.
 *	command		(IN)  - command to start running under pmfadm.
 *				This command can include command line arguments
 *				to pmfadm at the beginning of the command
 *				string.
 *	child_monitor_level (IN) - The level of children which should
 *				be monitored. Corresponds to the -C option to
 *				pmfadm(1m)
 *	env		(IN)	- The environment string for the new process
 *				  image to be launched. An "environment
 *				  string" is of the form name=value-- see
 *				  exec(2) and getenv(3C).
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_pmf_start_env(scds_handle_t handle, scds_pmf_type_t program_type,
    int instance, const char *command, int child_monitor_level, char **env);



/*
 * scds_pmf_stop()
 *
 * SUMMARY:
 *	Stop a command that is running under pmfadm. (pmfadm -s ...)
 *
 * PARAMETERS:
 *	handle 		(IN) - 	the handle returned from scds_initialize()
 *	program_type	(IN) - 	the type of program that this call is
 *				being made for (ie, is it for a service
 *				or for a monitor). This value will determine
 *				the tag suffix that is used.
 *	instance 	(IN) -	for resources where there are multiple
 *				instances in one resource, the index will
 *				identify the instance uniquely.
 *	signal		(IN) - 	the signal to use to stop the command.
 *				if set to SCDS_PMF_NOSIGNAL, the process is
 *				still running after pmfadm, but not monitored
 *				by pmf.
 *	timeout		(IN) - 	the timeout to specify to PMF
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_pmf_stop(scds_handle_t handle, scds_pmf_type_t program_type,
    int instance, int signal, time_t timeout);

/*
 * scds_pmf_stop_monitoring()
 *
 * SUMMARY:
 *	Stop monitoring the given process tree, or take it out of PMF
 *	monitoring
 *
 * PARAMETERS:
 *	handle 		(IN) - 	the handle returned from scds_initialize()
 *	program_type 	(IN) - 	the type of program that this call is
 *				being made for (ie, is it for a service
 *				or for a monitor). This value will determine
 *				the tag suffix that is used.
 *	instance 	(IN) - 	for resources where there are multiple
 *				instances in one resource, the index will
 *				identify the instance uniquely.
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_pmf_stop_monitoring(scds_handle_t handle, scds_pmf_type_t program_type,
    int instance);

/*
 * functions for accessing and evaluating fault monitoring data
 */

/*
 * scds_fm_sleep()
 *
 * SUMMARY:
 *	Wait for the specified period of time. A fault monitor
 *	should call this routine to wait between performing
 *	health checks of the data service. It should not use
 *	the system defined sleep() utility.
 *
 * PARAMETERS:
 *	handle 	(IN) - the handle returned from scds_initialize()
 * 	timeout (IN) - time to wait
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_fm_sleep(scds_handle_t handle, time_t timeout);

/*
 * scds_fm_action()
 *
 * SUMMARY:
 *	Utility called by fault monitors that will take the necessary
 *	action on behalf of the data service.  This function should be
 *	called after a normal probe completes, regardless if the probe
 *	is successful of not.  It is expected that if the probe has
 *	detected a failure in the instance, the probe will have made
 *	a scha_resource_setstatus() call to mark the instance as
 *	degraded with a descriptive message of the failure that has
 *	been detected.
 *
 * PARAMETERS:
 *	handle 		(IN)  - the handle returned from scds_initialize()
 * 	probe_status 	(IN)  - status of the most recent probe
 * 	elapsed_milliseconds (IN)  - time it took to probe the instance
 *
 * RETURN TYPE:	scha_err_t
 *
 */
scha_err_t
scds_fm_action(scds_handle_t handle, int probe_status,
    long elapsed_milliseconds);

/*
 * scds_fm_print_probes()
 *
 * SUMMARY:
 *	Print out the probe history at the specified debug level
 *
 * PARAMETERS:
 *	handle		(IN) - the handle returned from scds_initialize()
 *	debug_level 	(IN) - int between 1-9 (9 being the most verbose level)
 *
 * RETURN TYPE:	void
 *
 */
void
scds_fm_print_probes(scds_handle_t handle, int debug_level);

/*
 * Utility routines for using and accessing the system log
 */

/*
 * scds_syslog()
 *
 * SUMMARY:
 *	Logs message to syslog using the priority field.
 *	Priority field may be LOG_INFO, LOG_ERR, etc.
 *
 * PARAMETERS:
 *	priority (IN) - the syslog priority to assign to this message
 *	format   (IN) - the format string for the message. It is
 *			assumed that additional parameters will be
 *			passed to fill in the placeholders in the
 *			format string (as is done for the printf() family of
 *			functions which also take variable arguments.)
 *
 * RETURN TYPE:	void
 *
 */
void
scds_syslog(int priority, const char *format, ...);

/*
 * scds_syslog_debug()
 *
 * SUMMARY:
 *	Logs debug message to syslog using the debug level field.
 *
 * PARAMETERS:
 *	debug_level 	(IN) - 	the debug level to assing to this message.
 *				between 1-9 (9 being the most verbose level)
 *	format    	(IN) - 	the format string for the message. It is
 *				assumed that additional parameters will be
 *				passed to fill in the placeholders in the
 *				format string (as is done for the printf()
 *				family of functions which also take variable
 *				arguments.)
 *
 * RETURN TYPE:	void
 *
 */
void
scds_syslog_debug(int debug_level, const char *format, ...);


/* status codes for HAStoragePlus utilities */
typedef enum scds_hasp_status {
	SCDS_HASP_NO_RESOURCE = 0,		/* no hasp resource */
	SCDS_HASP_ERR_CONFIG,			/* improperly configured */
	SCDS_HASP_NOT_ONLINE,			/* not online anywhere */
	SCDS_HASP_ONLINE_NOT_LOCAL,		/* online elsewhere */
	SCDS_HASP_ONLINE_LOCAL			/* online here */
} scds_hasp_status_t;

/*
 * scds_hasp_check()
 *
 * SUMMARY:
 *	Retrieves status information about HAStoragePlus resources
 * 	used by a resource.
 *
 * PARAMETERS:
 *	scds_handle_t		 (IN) - handle of a resource
 *					obtained from scds_initialize
 *	scds_hasp_status_t	(OUT) - status of HAStoragePlus resources
 *
 * RETURN TYPE: scha_err_t
 *
 */
scha_err_t
scds_hasp_check(scds_handle_t handle, scds_hasp_status_t *hasp_status);


#ifdef	__cplusplus
}
#endif

#endif	/* _LIBDSDEV_H */
