/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_ERR_H_
#define	_SCHA_ERR_H_

#pragma ident	"@(#)scha_err.h	1.54	08/08/01 SMI"

#ifdef __cplusplus
extern "C" {
#endif

typedef int scha_err_t;

/*
 * Definition of error codes returned by the scha_ api
 */

#define	SCHA_ERR_NOERR 		0	/* no error was found */
#define	SCHA_ERR_NOMEM  	1	/* not enough swap */
#define	SCHA_ERR_HANDLE 	2	/* invalid resource management handle */
#define	SCHA_ERR_INVAL  	3	/* invalid input argument */
#define	SCHA_ERR_TAG  		4	/* invalid API tag */
#define	SCHA_ERR_RECONF  	5	/* cluster is reconfiguring */
#define	SCHA_ERR_ACCESS		6	/* permission denied */
#define	SCHA_ERR_SEQID 		7	/* resource, resource group or */
					/* resource type has been updated */
					/* since last scha_*_open call */
#define	SCHA_ERR_DEPEND		8	/* object dependency error */
#define	SCHA_ERR_STATE 		9	/* object is in wrong state */
#define	SCHA_ERR_METHOD		10	/* invalid method */
#define	SCHA_ERR_NODE  		11	/* invalid node */
#define	SCHA_ERR_RG  		12	/* invalid resource group */
#define	SCHA_ERR_RT  		13	/* invalid resource type */
#define	SCHA_ERR_RSRC  		14	/* invalid resource */
#define	SCHA_ERR_PROP  		15	/* invalid property */
#define	SCHA_ERR_CHECKS		16	/* sanity checks failed */
#define	SCHA_ERR_RSTATUS  	17	/* bad resource status */
#define	SCHA_ERR_INTERNAL  	18	/* internal error was encountered */
#define	SCHA_ERR_CLUSTER  	19	/* internal error was encountered */
#define	SCHA_ERR_ZONE_CLUSTER	20	/* invalid zone cluster */
#define	SCHA_ERR_ZC_DOWN	21	/* zone cluster not in "Running" */
					/* state */
#define	SCHA_ERR_TIMEOUT	31	/* Operation timed out */
#define	SCHA_ERR_FAIL		32	/* Failover attempt failed */

#ifdef __cplusplus
}
#endif

#endif	/* _SCHA_ERR_H_ */
