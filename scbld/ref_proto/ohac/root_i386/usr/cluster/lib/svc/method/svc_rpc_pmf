#!/sbin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#	ident	"@(#)svc_rpc_pmf	1.22	08/07/31 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# Start/stop processes required for rpc pmf
#

. /lib/svc/share/smf_include.sh

LIBSCDIR=/usr/cluster/lib/sc
BINDIR=/usr/cluster/bin
USRBIN=/usr/bin
SCLIB=/usr/cluster/lib/sc
SERVER=rpc.pmfd

PROGNAME=pmfd
FILE=/var/run/${PROGNAME}_door

#
# is_farm_member()
#
#	Return 0 if this node is NOT a member of the farm
#	Return 1 if this node is a member of the farm
#
is_farm_member()
{
	# Detect whether we are running in a global or non-global zone
	ZONECMD=${USRBIN}/zonename
	IS_GLOBAL=1
	if [ -x ${ZONECMD} ]; then
		if [ "`${ZONECMD}`" != "global" ]; then
			IS_GLOBAL=0
		fi
	fi

	if [ ${IS_GLOBAL} -eq 0 ] ; then
		if [ -x /usr/cluster/lib/sc/frgmd ]; then
			return 1
		fi
		return 0
	fi
	if [ -x /usr/cluster/lib/sc/scxcfg ]; then
		NODEID=`/usr/cluster/lib/sc/scxcfg -n`
		ENABLED=`/usr/cluster/lib/sc/scxcfg \
			-g "farm.nodes.${NODEID}.state" 2>/dev/null`
		if [ $? = 0 ] && [ "$ENABLED" = "enabled" ]
		then
			return 1
		fi
		return 0
	fi
	return 0
}

#
# is_cluster_member()
#
#	Return 0 if this node is NOT a member of the cluster
#	Return 1 if this node is a member of the cluster
#
is_cluster_member()
{
	if [ -x /usr/sbin/clinfo ]; then
		/usr/sbin/clinfo > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			return 1
		fi
	fi

	return 0
}

#
# daemon_exists daemonname
#
# Tests whether a daemon exists with the daemonname, running
# as root and with parent process id of 1.  We don't need
# the actual pid, just the boolean status of whether it
# exists.  Returns 0 for exists, non-zero otherwise.
#
daemon_exists()
{
	${USRBIN}/pgrep -u 0 -x $1 >/dev/null 2>&1
	return $?
}

#
# daemon_kills daemonname signal
#
# Kill the daemon named daemon name
# (if several process are named daemonname then all
# of them are killed, however this should not happen)
daemon_kills()
{
	PIDS=`${USRBIN}/pgrep -u 0 -x $1`
	if [ -n "${PIDS}" ]
	then
		# kill all processes in list
		kill -$2 ${PIDS}
		${SCLIB}/scds_syslog -p error -t INITPMF -m "Error: ${SERVER} is being stopped"
	fi
}

start_pmf()
{
	# Find if an instance of the server is already running and if yes exit
	# We only allow one rpc.pmfd at a time.
	if daemon_exists ${SERVER}
	then
		${USRBIN}/logger -p local0.err -t INITPMF \
		    "Error: ${SERVER} is already running."
                exit $SMF_EXIT_OK;
	fi

	# Support start/restart without rebooting.
	# Cannot rely on /var/run being cleaned by the boot.
	# ${FILE} is used below for synchronization with daemon.
	/bin/rm -f ${FILE}

	# Clear the SMF enviornment variables before starting pmf so
	# that the processes started under PMF control will not have
	# these variables
	smf_clear_env

	printf "Starting ${SERVER}"
	${LIBSCDIR}/${SERVER}

	if [ $? -ne 0 ]; then
		${SCLIB}/scds_syslog -p error -t INITPMF -m "Error: Startup of ${SERVER} failed."
		exit $SMF_EXIT_ERR_CONFIG
	fi

        exit $SMF_EXIT_OK;
	# SKV
	# Loop for 2 min checking that the server is up.
	#
	# We need to wait until the server is up and registered before
	# moving on, so that init scripts that follow us will be able
	# to succeed if they depend on us being up.
	# We rely on the file created by libsecurity to detect if the
	# server is ready
	COUNTER=120
	while [ ${COUNTER} -gt 0 ]
	do
		COUNTER=`expr ${COUNTER} - 1`

		${USRBIN}/pgrep -u 0 -x ${SERVER} > /dev/null 2>&1
		if [ $? -ne 0 ]
		then
		    # if the process disappeared there is no need to
		    # wait for the file to be created. startup failed.
		    ${USRBIN}/logger -p local0.err -t INITPMF \
			"Error: Startup of ${SERVER} failed."
		    exit $SMF_EXIT_ERR_CONFIG
		fi

		# As soon as the file is created we assume that the server is up
		# and running.
		if [ -r ${FILE} ]
		then
	    		# check that rpc.pmfd is accessible with pmfadm command
	    		${BINDIR}/pmfadm -L 2>/dev/null 1>/dev/null
	    		if [ $? -ne 0 ]; then
			${SCLIB}/scds_syslog -p error -t INITPMF -m "Error: ${SERVER} is running but not accessible."
			# kill rpc.pmfd to force the other init scripts to fail
			daemon_kills ${SERVER} ABRT
			exit $SMF_EXIT_ERR_CONFIG
		    fi

		    # rpc.pmfd is ready
		    printf ": [ OK ]\n"
		    exit $SMF_EXIT_OK
		fi

		# we log an error every 30 seconds in case of failure
		if [ `expr ${COUNTER} % 30` -eq 0 ]
		then
			${SCLIB}/scds_syslog -p notice -t INITPMF -m "Waiting for ${SERVER} to be ready."
		fi

		${USRBIN}/sleep 1

	done

	# startup failed
	# We log an error
	${SCLIB}/scds_syslog -p error -t INITPMF -m "Error: Startup of ${SERVER} failed."
	daemon_kills ${SERVER} ABRT
	exit $SMF_EXIT_ERR_CONFIG
}

stop_pmf()
{
	# Find if rpc.pmfd is running and if yes kill it.
	# This works on a list, but there shouldn't be more than one
	# instance running (see start case).
	daemon_kills ${SERVER} TERM

	# Loop for 30 secs checking that the server is down.
	#
	# We need to wait until the server is down before moving on,
	# so that init scripts that follow us will be able
        # to succeed if they depend on us being down.
        COUNTER=30
        while [ ${COUNTER} -gt 0 ]
        do
                COUNTER=`expr ${COUNTER} - 1`

		if daemon_exists ${SERVER} ; then
			# we log an error every 10 seconds
			# in case of failure
			if [ `expr ${COUNTER} % 10` -eq 0 ]
			then
				# SCMSGS
				# @explanation
				# The PMF service is being stopped.
				# @user_action
				# Please wait. No action is required.
				# This is an informational message.
	                        ${LIBSCDIR}/scds_syslog -p notice -t INITPMF\
					 -m "Waiting for the PMF to stop"
			fi
		else
			exit $SMF_EXIT_OK
		fi
		${USRBIN}/sleep 1
	done

	# stop failed
	# We log an error

	# SCMSGS
	# @explanation
	# The PMF service fails to stop.
	# @user_action
	# Please reboot the node,
	${LIBSCDIR}/scds_syslog -p error -t INITPMF\
		 -m "Error stopping the PMF"
	exit $SMF_EXIT_ERR_CONFIG
}

Start()
{
	is_cluster_member || start_pmf
	is_farm_member || start_pmf
	exit $SMF_EXIT_ERR_CONFIG
}

Stop()
{
	is_cluster_member || exit $SMF_EXIT_OK
	is_farm_member || stop_pmf
	exit $SMF_EXIT_ERR_CONFIG
}

case "$1" in
    start)
	Start
	;;
    stop)
	Stop
	;;
esac

