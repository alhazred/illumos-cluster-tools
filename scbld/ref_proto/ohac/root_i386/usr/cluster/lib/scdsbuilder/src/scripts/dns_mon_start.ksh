#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_mon_start.ksh	1.15	08/05/01 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# Monitor start Method for DNS.
#
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'R:G:T:' opt
	do
		case "$opt" in
		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}
####################################################################
# extract_property_value()
#
#  For extension properties, API returns data type followed by value
#  This function discards first line (data type) and prints
#       remaining lines.
#
####################################################################
extract_property_value()
{
        typeset type
        typeset value
        typeset rc=0

        read type
        rc=$?
        [ $rc -ne 0 ] && return $rc

        while read value
        do
                print $value
        done

        rc=$?
        return $rc
}

#############################################################
# read_extension_property_value()
#
# This function reads extension property values
#
# Parameter 1: <property name>
#
#############################################################
read_extension_property_value()
{

        typeset rc
        typeset property="${1:-''}"

        scha_resource_get -O Extension \
                 -R "$RESOURCE_NAME" -G "$RESOURCEGROUP_NAME" "$property" \
                | extract_property_value

        rc=$?
        if [ $rc -ne 0 ]; then
		# SCMSGS
		# @explanation
		# This is an internal error. Program failed to read an
		# extension property of the resource.
		# @user_action
		# If the problem persists, save the contents of
		# /var/adm/messages and contact your Sun service
		# representative.
                scds_syslog -p error -t $(syslog_tag) -m \
                	"Error (%s) when reading extension property <%s>." \
			"$rc" "${property}"
        fi
        return $rc
}

##############################################################################
# MAIN
##############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

# Obtain the syslog facility to use. This will be used to log the messages.
SYSLOG_FACILITY=`scha_cluster_get -O SYSLOG_FACILITY`

# Parse the arguments that have been passed to this method
parse_args "$@"

# We need to know where the probe method resides. This is specified in the 
# RT_BASEDIR property of the resource type.
RT_BASEDIR=`scha_resource_get -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon
SYSLOG_TAG=$RESOURCETYPE_NAME,$RESOURCEGROUP_NAME,$RESOURCE_NAME

# Get the value for monitor retry count from the RTR file.
MON_RETRY_CNT=$(read_extension_property_value Monitor_retry_count)

# Get the value for monitor retry interval from the RTR file.
MON_RETRY_INTRVAL=$(read_extension_property_value Monitor_retry_interval)

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

# Start the probe for the data service under PMF. Use the Monitor_retry_count retries 
# option to start the probe. Pass the the Resource name, type and group to the 
# probe method. 
pmfadm -c $PMF_TAG -n $MON_RETRY_CNT -t $MON_RETRY_INTRVAL \
	$RT_BASEDIR/dns_probe.ksh -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME

# Log a message indicating that the monitor for DNS has been started.
if [ $? -eq 0 ]; then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"Monitor for DNS successfully started"
fi

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

exit 0
