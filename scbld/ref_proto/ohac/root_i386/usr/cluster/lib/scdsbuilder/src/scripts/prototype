#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
#
#ident	"@(#)prototype	1.12	08/02/26 SMI"
#

# packaging files
i pkginfo
i preremove
i depend

#
# source locations relative to the prototype file
#
# SUNWdns
#

# top-level directories and sub-directories
d none SUNWdns 0775 root bin
d none SUNWdns/bin 0755 root bin
d none SUNWdns/etc 0755 root bin
d none SUNWdns/util 0755 root bin
d none SUNWdns/man 0755 root bin
d none SUNWdns/man/man1m 0755 root bin

# the readme file
f none SUNWdns/README.dns 0644 root bin

# the scripts for various methods
f none SUNWdns/bin/dns_svc_start.ksh 0555 root bin
f none SUNWdns/bin/dns_svc_stop.ksh 0555 root bin
f none SUNWdns/bin/dns_mon_start.ksh 0555 root bin
f none SUNWdns/bin/dns_mon_stop.ksh 0555 root bin
f none SUNWdns/bin/dns_mon_check.ksh 0555 root bin
f none SUNWdns/bin/dns_update.ksh 0555 root bin
f none SUNWdns/bin/dns_validate.ksh 0555 root bin
f none SUNWdns/bin/dns_probe.ksh 0555 root bin
f none SUNWdns/bin/dns_pmf_action.ksh 0555 root bin
f none SUNWdns/bin/gethostnames 0555 root bin
f none SUNWdns/bin/simple_probe 0555 root bin
f none SUNWdns/bin/hasp_check 0555 root bin
f none SUNWdns/bin/dns_scha_ctrl_wrapper.ksh 0555 root bin

# The RTR file
f none SUNWdns/etc/SUNW.dns 0555 root bin
d none cluster 0755 root bin
d none cluster/lib 0755 root bin
d none cluster/lib/rgm 0755 root bin
d none cluster/lib/rgm/rtreg 0755 root bin
s none cluster/lib/rgm/rtreg/SUNW.dns=../../../../SUNWdns/etc/SUNW.dns

f none SUNWdns/util/startdns 0555 root bin
f none SUNWdns/util/removedns 0555 root bin
f none SUNWdns/util/stopdns 0555 root bin
f none SUNWdns/util/dns_config 0644 root bin

f none SUNWdns/man/man1m/startdns.1m 0644 root bin
f none SUNWdns/man/man1m/removedns.1m 0644 root bin
f none SUNWdns/man/man1m/stopdns.1m 0644 root bin
f none SUNWdns/man/man1m/dns_config.1m 0644 root bin
