#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN 
# ident	"@(#)SUNW.xfs	1.21 08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
# Registration information and Paramtable for XFS
#
# NOTE: Keywords are case insensitive, i.e. users may use any
# capitalization style they wish
#

RESOURCE_TYPE = "xfs";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "xfs server for Sun Cluster";

RT_VERSION ="4"; 
API_VERSION = 2;	 
FAILOVER = FALSE;

INIT_NODES = RG_PRIMARIES;

RT_BASEDIR=/opt/SUNWxfs/bin;

START				=	xfs_svc_start;
STOP				=	xfs_svc_stop;

VALIDATE			=	xfs_validate;
UPDATE	 			=	xfs_update;

MONITOR_START			=	xfs_monitor_start;
MONITOR_STOP			=	xfs_monitor_stop;
MONITOR_CHECK			=	xfs_monitor_check;


#$upgrade
#$upgrade_from "1.0" anytime

# The paramtable is a list of bracketed resource property declarations 
# that come after the resource-type declarations
# The property-name declaration must be the first attribute
# after the open curly of a paramtable entry
#
# The following are the system defined properties. Each of the system defined
# properties have a default value set for each of the attributes. Look at 
# man rt_reg(4) for a detailed explanation.
#
{  
	PROPERTY = Start_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{
	PROPERTY = Stop_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{ 
	PROPERTY = Validate_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{
        PROPERTY = Update_timeout;
	MIN = 60;
        DEFAULT = 300;
}
{ 
	PROPERTY = Monitor_Start_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{ 
	PROPERTY = Monitor_Stop_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{ 
	PROPERTY = Monitor_Check_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{
        PROPERTY = FailOver_Mode;
        DEFAULT = SOFT;
        TUNABLE = ANYTIME;
}
{
        PROPERTY = Network_resources_used;
        TUNABLE = WHEN_DISABLED;
	DEFAULT = "";
}
{ 
	PROPERTY = Thorough_Probe_Interval; 
	MAX = 3600; 
	DEFAULT = 60; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Count; 
	MAX = 10; 
	DEFAULT = 2; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Interval; 
	MAX = 3600; 
	DEFAULT = 370; 
	TUNABLE = ANYTIME;
}

{
	PROPERTY = Port_list;
	DEFAULT = "7100/tcp";
	TUNABLE = AT_CREATION;
}

{
        PROPERTY = Scalable;
        DEFAULT = FALSE;
        TUNABLE = AT_CREATION;
}

{
        PROPERTY = Load_balancing_policy;
        DEFAULT = LB_WEIGHTED;
        TUNABLE = AT_CREATION;
}

{
        PROPERTY = Load_balancing_weights;
        DEFAULT = "";
        TUNABLE = ANYTIME;
}

#
# Extension Properties
#

{
	PROPERTY = Confdir_list;
	EXTENSION;
	STRINGARRAY;
	DEFAULT = "";
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The Configuration Directory Path(s)";
}

# These two control the restarting of the fault monitor itself
# (not the server daemon) by PMF.
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}

{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time window (minutes) for fault monitor restarts";
}

# Time out value for the probe
{
	PROPERTY = Probe_timeout;
	EXTENSION;
	INT;
	MIN = 2;
	DEFAULT = 30;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time out value for the probe (seconds)";
}

# Child process monitoring level for PMF (-C option of pmfadm)
# Default of -1 means: Do NOT use the -C option to PMFADM
# A value of 0-> indicates the level of child process monitoring
# by PMF that is desired.
{
	PROPERTY = Child_mon_level;
	EXTENSION;
	INT;
	DEFAULT = -1;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Child monitoring level for PMF";
}

# This is an optional property, which determines the signal sent to the
# application for being stopped.
#
{
	PROPERTY = Stop_signal;
	EXTENSION;
	INT;
	MIN = 1;
	MAX = 37;
	DEFAULT = 15;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The signal sent to the application for being stopped";
}

# This is an optional property, which determines whether to failover when
# retry_count is exceeded during retry_interval.
#
{
	PROPERTY = Failover_enabled;
	EXTENSION;
	BOOLEAN;
	DEFAULT = TRUE;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Determines whether to failover when retry_count is exceeded during retry_interval";
}
# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^
