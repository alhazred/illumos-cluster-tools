#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_validate.ksh	1.18	08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# Validate method for DNS.
# ex: When the resource is being created command args will be
#   
# dns_validate -c -R <..> -G <...> -T <..> -r <sysdef-prop=value>... 
#       -x <extension-prop=value>.... -g <resourcegroup-prop=value>....
#
# when the resource property is being updated
#
# dns_validate -u -R <..> -G <...> -T <..> -r <sys-prop_being_updated=value>
#   OR
# dns_validate -u -R <..> -G <...> -T <..> -x <extn-prop_being_updated=value>
#
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'cur:x:g:R:T:G:' opt
	do
		case "$opt" in

		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		r)      
		# We are not accessing any system defined property.
		# So, this is a no-op
		;;

		g)
		# This is a no-op as we are not bothered about 
		# Resource group properties
		;;

		c)
		# This is a no-op as this just a flag which indicates
		# that the validate method is being called while
		# creating the resource.
		;;

		u)
		# This is a flag to indicate the updating of property 
		# of an existing resource.
		UPDATE_PROPERTY=1
		;;

		x)
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}

###############################################################################
# MAIN
##############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

# Obtain the syslog facility to use. This will be used to log the messages.
SYSLOG_FACILITY=`scha_cluster_get -O SYSLOG_FACILITY`

UPDATE_PROPERTY=0

# Parse the arguments that have been passed to this method.
parse_args "$@"

SYSLOG_TAG=$RESOURCETYPE_NAME,$RESOURCEGROUP_NAME,$RESOURCE_NAME

if [[ -f /opt/SUNWdns/bin/hasp_check && \
	-x /opt/SUNWdns/bin/hasp_check ]]; then
	/opt/SUNWdns/bin/hasp_check "$@"
	hasp_status=$?
else
	# the binary doesn't exist so we cannot call it
	hasp_status=2
fi

case "$hasp_status" in

	1) 
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Internal Error. Failed to check status of \
SUNW.HAStoragePlus resource."
	exit 1
	;;

	2)
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"This resource doesn't depend on any \
SUNW.HAStoragePlus resources. Proceeding with the normal checks."

	;;

	3)
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"One or more of the SUNW.HAStoragePlus resources \
that this resource depends on is in a different RG. \
Failing validate method configuration checks."
	exit 1
	;;

	4)
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"One or more of the SUNW.HAStoragePlus resources \
that this resource depends on is not online anywhere. \
Failing validate method."
	exit 1
	;;

	5)
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"All the SUNW.HAStoragePlus resources that this \
resource depends on are not online on the local node. \
Skipping the checks for the existence and permissions of the start/stop/probe \
commands."
	exit 0
	;;

	6)
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"All the SUNW.HAStoragePlus resources that this \
resource depends on are online on the local node. \
Proceeding with the checks for the existence and permissions of the \
start/stop/probe commands."
	;;

	*)
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Unknown status code %s" \
		$hasp_status
	;;

esac

start_cmd_args=
start_cmd_prog=`echo $start_cmd_args | nawk '{print $1}'`
if [[ ! -f $start_cmd_prog || ! -x $start_cmd_prog ]]; then
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"File %s is missing or not executable" \
		$start_cmd_prog
	exit 1
fi

stop_cmd_args=
stop_cmd_prog=`echo $stop_cmd_args | nawk '{print $1}'`
if [[ ! -z $stop_cmd_prog ]]; then
	if [[ ! -f $stop_cmd_prog || ! -x $stop_cmd_prog ]]; then
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"File %s is missing or not executable" \
			$stop_cmd_prog
		exit 1
	fi
fi

probe_cmd_args=
probe_cmd_prog=`echo $probe_cmd_args | nawk '{print $1}'`
if [[ ! -z $probe_cmd_prog ]]; then
	if [[ ! -f $probe_cmd_prog || ! -x $probe_cmd_prog ]]; then
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"File %s is missing or not executable" \
			$probe_cmd_prog	
		exit 1
	fi
fi

validate_cmd_args=
validate_cmd_prog=`echo $validate_cmd_args | nawk '{print $1}'`
if [[ ! -z $validate_cmd_prog ]]; then
        if [[ ! -f $validate_cmd_prog || ! -x $validate_cmd_prog ]]; then
                scds_syslog -p error -t "${SYSLOG_TAG}" -m \
                        "File %s is missing or not executable" \
			$validate_cmd_prog
                exit 1
        fi
fi

if [[ -f $validate_cmd_prog && -x $validate_cmd_prog ]]; then
	${validate_cmd_args}
	rc_validate=$?

	if [ "$rc_validate" -ne 0 ]
		then
        	# SCMSGS
        	# @explanation
        	# Need explanation of this message!
        	# @user_action
        	# Need a user action for this message.
        	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
                        	"File %s did not execute properly" \
				$validate_cmd_prog
        	exit 1
	fi
fi

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

# Log a message indicating that validate method was successful.

# SCMSGS
# @explanation
# Need explanation of this message!
# @user_action
# Need a user action for this message.
scds_syslog -p info -t "${SYSLOG_TAG}" -m \
	"Validate method for resource %s completed successfully" \
	$RESOURCE_NAME

exit 0
