#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_mon_stop.ksh	1.12	08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# Monitor stop method for DNS
#
# Stops the monitor that is running. This is done via PMF.
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'R:G:T:' opt
	do
		case "$opt" in
		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}

###############################################################################
# MAIN
##############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

# Obtain the syslog facility to use. This will be used to log the messages.
SYSLOG_FACILITY=`scha_cluster_get -O SYSLOG_FACILITY`

# Parse the arguments that have been passed to this method
parse_args "$@"

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon
SYSLOG_TAG=$RESOURCETYPE_NAME,$RESOURCEGROUP_NAME,$RESOURCE_NAME

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

# See if the monitor is running, and if so, kill it. 
if pmfadm -q $PMF_TAG; then 
	# User added code -- BEGIN vvvvvvvvvvvvvvv
	# User added code -- END   ^^^^^^^^^^^^^^^

	pmfadm -s $PMF_TAG KILL
	if [ $? -ne 0 ]; then 
		# SCMSGS
		# @explanation
		# Need explanation of this message!
		# @user_action
		# Need a user action for this message.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Could not stop monitor for resource %s" \
			$RESOURCE_NAME
		exit 1
	else
		# could successfully stop the monitor. Log a message.

		# SCMSGS
		# @explanation
		# Need explanation of this message!
		# @user_action
		# Need a user action for this message.
		scds_syslog -p info -t "${SYSLOG_TAG}" -m \
			"Monitor for resource %s successfully stopped" \
			$RESOURCE_NAME
	fi

	# User added code -- BEGIN vvvvvvvvvvvvvvv
	# User added code -- END   ^^^^^^^^^^^^^^^

fi

exit 0
