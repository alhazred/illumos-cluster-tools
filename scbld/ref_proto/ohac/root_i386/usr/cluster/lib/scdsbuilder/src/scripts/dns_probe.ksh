#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_probe.ksh	1.20	08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# Probe method for DNS.
#
# This method checks the health of the data service.
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
alias scha_resourcegroup_get="/usr/cluster/bin/scha_resourcegroup_get"
alias scha_resource_get="/usr/cluster/bin/scha_resource_get"
alias scha_control="/usr/cluster/bin/scha_control"
alias scha_resource_setstatus="/usr/cluster/bin/scha_resource_setstatus"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'R:G:T:' opt
	do
		case "$opt" in

		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}

###############################################################################
# decide_restart_or_failover ()
#
# This function decides the action to be taken upon the failure of a probe 
# The action could be to restart the data service locally or it could be to
# failover the data service to another node in the cluster. 
#
function decide_restart_or_failover
{
	typeset rc=0
	# Get the resource number of restarts
	num_resource_restart=`scha_resource_get -O NUM_RESOURCE_RESTARTS \
		-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
	if [[ $? -ne 0 ]]; then
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"scha_resource_get operation %s failed for Resource %s" \
			"NUM_RESOURCE_RESTARTS" "${RESOURCE_NAME}"

		# set the status to faulted state.
		scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
			-R $RESOURCE_NAME \
			-s FAULTED \
			-m "scha_resource_get operation failed."
		exit 1
	fi


	# Get the Retry count value from the system defined property Retry_count
	retry_count=`scha_resource_get -O RETRY_COUNT \
			-G "${RESOURCEGROUP_NAME}" \
			-R "${RESOURCE_NAME}"`
	if [[ $? -ne 0 ]]; then
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"scha_resource_get operation %s failed for Resource %s" \
			"RETRY_COUNT" "${RESOURCE_NAME}"

		# set the status to faulted state.
		scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
			-R $RESOURCE_NAME \
			-s FAULTED \
			-m "scha_resource_get operation failed."
		exit 1
	fi

	
	# Initiate a local restart if the number of resource restarts
	# is less than the retry count in the retry time interval window.
        if [[ $num_resource_restart -lt $retry_count ]]; then

		scha_control -O RESOURCE_RESTART -G "${RESOURCEGROUP_NAME}" \
				-R "${RESOURCE_NAME}"
		rc=$?

		if [[ $rc -ne 0 ]]; then
                         # don't log a trace if the error is SCHA_ERR_CHECKS
                         # Probe may fail repeatedly with Failover_mode setting
                         # preventing giveover [or restart], and we don't want
                         # to fill up syslog
                	if [[ $rc -ne 16 ]]; then
                         	# SCMSGS
                         	# @explanation
                         	# This message indicated that the rgm didn't process
                         	# a restart request, most likely due to the
                         	# configuration settings.
                         	# @user_action
                         	# This is an informational message.
                        	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
                                	"Restart operation failed for Resource %s" \
					"${RESOURCE_NAME}"
			elif [[ $rc -eq 16 ]]; then
				# set rc = 0 to keep the probe running
				rc=0
                	fi
			# set the status to faulted state.
			scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
				-R $RESOURCE_NAME \
				-s FAULTED \
				-m "Restart failed."
		else
                        scds_syslog -p info -t "${SYSLOG_TAG}" -m \
                		"Successfully restarted service."
                fi
        else
                # the fact that the number of resource restarts have exceeded
                # implies that the dataservice has already passed the max no
		# of retries allowed. Hence there is no point in trying to restart
		# the dataservice again. We might as well try to failover
                # to another node in the cluster.

		scha_control -O GIVEOVER -G $RESOURCEGROUP_NAME \
			-R $RESOURCE_NAME
		rc=$?
		# if the resource is not able to failover set it to 
		# faulted state

		if [ $rc -ne 0 ]; then
			scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
				-R $RESOURCE_NAME \
				-s FAULTED \
				-m "Unable to Failover to other node."
			# SCMSGS
			# @explanation
			# The number of restarts of services under the resource has
			# reached the max. Hence Failover was attempted, but failed
			# and the resource group remains in faulted status on its
			# current master.
			# @user_action
			# Examine the syslog to determine the cause of the failures
			# and execute "clresourcegroup switch" to attempt to switch
			# the resource group to a different node, or 
			# "clresourcegroup restart" to restart it.
                        scds_syslog -p error -t "${SYSLOG_TAG}" -m \
                                "Error in failing over the resource group:%s" \
				"${RESOURCE_NAME}"
                fi
        fi
return $rc
}


###############################################################################
# MAIN
###############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

# Parse the arguments that have been passed to this method
parse_args "$@"

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon

# The interval at which probing is to to be done is set in the system defined
# property THOROUGH_PROBE_INTERVAL. Obtain this information using 
# scha_resource_get 
PROBE_INTERVAL=`scha_resource_get -O THOROUGH_PROBE_INTERVAL \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`

# Obtain the time-out value allowed for the probe. 
# This value is set in the extension property probe_timeout of the data
# service.
probe_timeout_info=`scha_resource_get -O Extension -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME Probe_timeout`
PROBE_TIMEOUT=`echo $probe_timeout_info | awk '{print $2}'`

# We need to know the full path for the gettime utility which resides in the
# directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`scha_resource_get -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

hostnames=`$RT_BASEDIR/gethostnames -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME`

probe_cmd_args=

rstatus=""



# Obtain the program name of the probe command and make sure it exists.
probe_cmd_prog=`echo $probe_cmd_args | nawk '{print $1}'`

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

while :
do
	# The interval at which the probe needs to run is specified 
	# in the property THOROUGH_PROBE_INTERVAL. So we need to 
	# sleep for a duration of <THOROUGH_PROBE_INTERVAL>
	sleep $PROBE_INTERVAL

	# User added code -- BEGIN vvvvvvvvvvvvvvv
	# User added code -- END   ^^^^^^^^^^^^^^^

	if [[ -f $probe_cmd_prog && -x $probe_cmd_prog ]]; then
		hatimerun -t $PROBE_TIMEOUT $probe_cmd_args

		# User added code -- BEGIN vvvvvvvvvvvvvvv
		# User added code -- END   ^^^^^^^^^^^^^^^
	else
		# We were not supplied with a probe command. We will use the 
		# simple_probe that comes bundled as an utility.
		hatimerun -t $PROBE_TIMEOUT $RT_BASEDIR/simple_probe \
			-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
			-T $RESOURCETYPE_NAME

		# User added code -- BEGIN vvvvvvvvvvvvvvv
		# User added code -- END   ^^^^^^^^^^^^^^^
	fi

	if [[ $? != 0 ]]; then
		# User added code -- BEGIN vvvvvvvvvvvvvvv
		# User added code -- END   ^^^^^^^^^^^^^^^
		
		decide_restart_or_failover
		if [[ $? -ne 0 ]] then
			# SCMSGS
			# @explanation
			# The attempted recovery actions after a probe failure on
			# the resource or resource group has failed and the resource
			# is left in faulted status on its current master.
			# @user_action
			# Examine the /var/adm/messages to determine the cause
			# of the failures, and after taking corrective action,
			# attempt to restart the resource or switch over the
			# resource group. The resource can be restarted by the
			# sequence "clresource disable <resource>; clresource
			# enable <resource>". Or, the whole resource group can be
			# restarted using "clresourcegroup restart <group>".
			# The resource group can be switched over to the next
			# primary node by executing "clresourcegroup switch". 
			# If problem persists, contact your Sun service representative.
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
				"Could not Restart/Failover the dataservice."
			exit 1
		fi
	else
		# SCMSGS
		# @explanation
		# This message indicated that the probe is successful.
		# @user_action
		# This is a debug message.
		scds_syslog -p debug -t "${SYSLOG_TAG}" -m \
			"Probe for resource DNS successful."
	fi

	# User added code -- BEGIN vvvvvvvvvvvvvvv
	# User added code -- END   ^^^^^^^^^^^^^^^
done
