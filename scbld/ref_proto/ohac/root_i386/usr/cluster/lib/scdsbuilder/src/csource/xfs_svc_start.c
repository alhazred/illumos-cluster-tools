/*
 * Copyright (c) 2008, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Gate Specific Code -- BEGIN */
#pragma ident "@(#)xfs_svc_start.c 1.10 08/02/23 SMI"
/* Gate Specific Code -- END */
/* Sun Cluster Data Services Builder template version 1.0 */
/*
 * xfs_svc_start.c - Start method for XFS
 */

#include <rgm/libdsdev.h>
#include "xfs.h"

/*
 * The start method for XFS. Does some sanity checks on
 * the resource settings then starts the XFS under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int rc;
	/* GDS Specific Code -- BEGIN */
	boolean_t 	network_aware = B_TRUE;
	scha_extprop_value_t *network_aware_prop = NULL;
	/* GDS Specific Code -- END */

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "Failed to validate configuration.");
		goto finished;
	}

	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle);
	if (rc != 0) {
		goto finished;
	}

	/* GDS Specific Code -- BEGIN */
	rc = scds_get_ext_property(scds_handle, "network_aware",
	    SCHA_PTYPE_BOOLEAN, &network_aware_prop);

	if (rc == SCHA_ERR_NOERR) {
		network_aware = network_aware_prop->val.val_boolean;
		scds_syslog(LOG_INFO,
		    "Extension property <network_aware> has a value "
		    "of <%d>", network_aware);
	} else {
		scds_syslog(LOG_INFO,
		    "Either extension property <network_aware> is not "
		    "defined, or an error occured while retrieving this "
		    "property; using the default value of TRUE.");
	}

	if (network_aware) {
	/* GDS Specific Code -- END */
	/* Network aware applications code -- BEGIN */
	rc = svc_wait_network_aware(scds_handle);
	/* Network aware applications code -- END */
	/* GDS Specific Code -- BEGIN */
	} else {
	/* GDS Specific Code -- END */
	/* Stand alone applications code -- BEGIN */
	rc = svc_wait_non_network_aware(scds_handle);
	/* Stand alone applications code -- END */
	/* GDS Specific Code -- BEGIN */
	}
	/* GDS Specific Code -- END */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Returned from svc_wait");

finished:
	/* GDS Specific Code -- BEGIN */
	if (network_aware_prop)
		scds_free_ext_property(network_aware_prop);
	/* GDS Specific Code -- END */

	/* Free up the Environment resources that were allocated */
	(void) scds_close(&scds_handle);

	return (rc);
}
