#! /usr/perl5/bin/perl -w
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident   "@(#)scsnapshot.pl 1.3     08/05/20 SMI"
# 
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# Perl front-end for scsnapshot.
# Retrieves arguments passed on the command-line and runs the generation 
# process accordingly.
#

require 5.005;
use locale;
use strict;
use ScSnapshot::ConfigDataParser;
use ScSnapshot::XMLReader;
use ScSnapshot::XMLWriter;
use ScSnapshot::MetadataRepository;
use ScSnapshot::Analyzer;
use ScSnapshot::InternalViewDumper;
use ScSnapshot::ShellScriptDumper;
use ScSnapshot::CodeGenerator;
use ScSnapshot::Error;
use Sun::Solaris::Utils qw(gettext);

# Error codes
my $errorCodes = ScSnapshot::Error->new();

# Get the version of the local SunCluster software
my $scSoftVersion = `/usr/cluster/bin/scinstall -p 2>/dev/null | /usr/bin/head -1`;
chomp($scSoftVersion);

# Get the version of the Operating system and architecture

my ($osVersion, $arch) = split(/\s+/,`/usr/bin/uname -r -p 2>/dev/null`);
chomp($osVersion);
chomp($arch);

my $olddata;
my $newdata;

if (!$ENV{Pflag} && !$ENV{Nflag}) {
  # Running the generate-from-scratch-flavored scsnapshot.

  # Create the configuration data parser and parse the CCR data
  my $parser = ScSnapshot::ConfigDataParser->new($scSoftVersion,
						 $osVersion,
						 $arch,
						 $errorCodes);

  my $ccrdata = `LC_ALL=C; export LC_ALL; /usr/cluster/bin/scrgadm -pvv`;

  if(!$ccrdata) {
    print gettext("WARNING: No input data to treat") . "\n";
    exit 0;
  }
    
  print gettext("Parsing the CCR data... This may take a while") . "\n";
  $parser->parse($ccrdata);
  $newdata = $parser->data();
  
  # Serialize the Internal Data Representation in XML if requested
  if ($ENV{oflag}) {
    my $writer = ScSnapshot::XMLWriter->new($errorCodes);
    my $filename = $ENV{oval};
    $writer->write($newdata, $filename);
    print gettext("Wrote image file") . " " . $filename . "\n";
  }
} else {
  # Running the generate-a-diff-flavored scsnapshot. Both image file are 
  # specified
    
  my $oldimagefilename = $ENV{Pval};
  # Deserialize the Internal Data Representation from XML 
  my $oldreader = ScSnapshot::XMLReader->new($errorCodes);
  print gettext("Reading old configuration from file :") . " "  
    . $oldimagefilename."\n";
  $olddata = $oldreader->read($oldimagefilename);

  # Look for the new configuration
  my $newimagefilename = $ENV{Nval};
  # Deserialize the Internal Data Representation from XML 
  my $newreader = ScSnapshot::XMLReader->new($errorCodes);
  print gettext("Reading new configuration from file :") . " " 
    . $newimagefilename . "\n";
  $newdata = $newreader->read($newimagefilename);
}

# Parse the metadata repository
my $metadata = ScSnapshot::MetadataRepository->new(
			     "$ENV{SCSNAPSHOT_LIB_DIR}/metadata.txt");

# Generate an Internal View of the actions required to perform the changes
my $analyzer = ScSnapshot::Analyzer->new($metadata);
my $view = $analyzer->diff($olddata, $newdata);

# Generate the final code
my $generator = 
  ScSnapshot::CodeGenerator->new(
				 ScSnapshot::ShellScriptDumper->new(),
				 $errorCodes );

$generator->generate($view, ($ENV{sflag}?$ENV{sval}:undef));

if ($ENV{sflag}) {
  print gettext("Wrote script file") . " " . $ENV{sval} . "\n";
}
