#! /usr/perl5/bin/perl -w
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident   "@(#)ConfigDataParser.pm 1.3     08/05/20 SMI"
# 
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# Module: ScSnapshot::ConfigDataParser
# 

package ScSnapshot::ConfigDataParser;
use strict; 
use ScSnapshot::RecDescentParser;
use Sun::Solaris::Utils qw(gettext);

#
# Constructor
# Arguments: 
#   1) ScSnapshot::Logging object used to print debug messages.
#      Cannot be undefined.
#   2) A string giving the version of SunCluster software.
#
sub new 
{
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self  = {};
    $self->{DATA} = {};
    $self->{DATA}->{scsoftvers} = shift;
    $self->{DATA}->{osversion} = shift;
    $self->{DATA}->{arch} = shift;
    $self->{EXIT_CODE} = shift;
    bless ($self, $class);
    return $self;
}

#
# Gets/Sets the Internal Data Representation resulting from the parsing of 
# the CCR configuration data.
#
sub data 
{
    my $self = shift;
    if (@_) 
    { 
        $self->{DATA} = shift;
    }
    return $self->{DATA};
}

#
# Performs the parsing of the CCR configuration data.
# Fails on any parsing error.
# Arguments:
#   1) String containing the data to be parsed.
#      Cannot be undefined.
#
sub parse
{
    my $self = shift;
    my $data = shift;
    # Create a new ScSnapshot::RecDescentParser.
    # Provide it with a reference on our Internal Data Representation
    # so that it can be updated dynamically during the descent.
    my $parser = ScSnapshot::RecDescentParser->new($self->{DATA});
    # Initiate the parsing on the grammar's starting rule.
    if(!defined($parser->startrule($data))) {
      print STDERR gettext ("FATAL INTERNAL ERROR: An unexpected error occured while parsing the CCR data\n");
      exit $self->{EXIT_CODE}->getCode("ENOEXEC");
    }
}

# Return success for module load
1; 
