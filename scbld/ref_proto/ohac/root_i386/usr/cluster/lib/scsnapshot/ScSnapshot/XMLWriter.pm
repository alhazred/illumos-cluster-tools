#! /usr/perl5/bin/perl -w
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident   "@(#)XMLWriter.pm 1.3     08/05/20 SMI"
# 
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# 
# Module: ScSnapshot::XMLWriter
#

package ScSnapshot::XMLWriter;
use strict; 
use XML::Writer;
use Sun::Solaris::Utils qw(gettext);

#
# Constructor.
# Arguments:
#   1) Error list wrapper
#
sub new 
{
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self  = {};
    $self->{EXIT_CODE} = shift;
    bless ($self, $class);
    return $self;
}

#
# XMLizes the given Internal Data Representation to the given output.
# Arguments:
#   1) The Internal Data Representation to be serialized in XML
#      Cannot be undefined.
#   2) The name of the output file.
#      Cannot be undefined.
#
sub write
{
    my $self = shift;
    my $data = shift;
    my $filename = shift;

    # Move previous generated file 
    if(open(FILE2MOVE, "<" . $filename)) {
      if( system('/usr/bin/mv', $filename, $filename . ".old") != 0) {
	print STDERR gettext ("FATAL ERROR: Unable to rename file:")
	  . "\n\t[" . $filename . "] --> [" . $filename . ".old]\n";
	exit $self->{EXIT_CODE}->getCode("EIO");
      }
      close(FILE2MOVE);
    }
    
    # Create output file
    my $output = IO::File->new(">" . $filename);
    
    if(!defined($output))
      {
	print STDERR gettext("FATAL ERROR: Unable to open file for writing:") 
              . "\n\t[" . $filename . "]\n";
        exit $self->{EXIT_CODE}->getCode("EIO");
      }

    # Create a new XML::Writer
    my $writer = XML::Writer->new(DATA_MODE => 1, 
                                  DATA_INDENT=> 1, 
                                  OUTPUT => $output);

    # Open XML document
    $writer->xmlDecl();

    # Create root tag
    $writer->startTag('config', 'software-version' => $data->{scsoftvers},
		      'os-version' => $data->{osversion},
		      'machine-arch' => $data->{arch} );

    # Write code for resource types
    $writer->startTag('resource-types');

    foreach my $rtdef (@{$data->{rtdefs}}) {
        $writer->startTag('resource-type');
        foreach my $rtattribute (keys(%{$rtdef})) {
            if (($rtattribute ne 'params') and
                ($rtattribute ne 'methods')) {
                $writer->startTag('attribute', 'name' => $rtattribute);
                $writer->characters($rtdef->{$rtattribute});
                $writer->endTag('attribute');
            }
        }
        $writer->startTag('methods');
        foreach my $rtmethod (keys(%{$rtdef->{methods}})) {
            $writer->startTag('method', 'type' => $rtmethod);
            $writer->characters($rtdef->{methods}->{$rtmethod});
            $writer->endTag('method');
        }
        $writer->endTag('methods');
        $writer->startTag('parameters');
        foreach my $rtparam (@{$rtdef->{params}}) {
            $writer->startTag('parameter');
            foreach my $rtparamattribute (keys(%{$rtparam})) {
                $writer->startTag('attribute', 'name' => $rtparamattribute);
                $writer->characters($rtparam->{$rtparamattribute});
                $writer->endTag('attribute');
            }
            $writer->endTag('parameter');
        }
        $writer->endTag('parameters');
        $writer->endTag('resource-type');
    }

    $writer->endTag('resource-types');

    $writer->startTag('resource-groups');

    
    # Write code for resource groups
    foreach my $rgdef (@{$data->{rgdefs}}) {
        $writer->startTag('resource-group');
        foreach my $rgattribute (keys(%{$rgdef})) {
            if ($rgattribute ne 'resources') {
                $writer->startTag('attribute', 'name' => $rgattribute);
                $writer->characters($rgdef->{$rgattribute});
                $writer->endTag('attribute');
            }
        }
        # Write code for resources
        $writer->startTag('resources');
        foreach my $rgresource (@{$rgdef->{resources}}) {
            $writer->startTag('resource');
            foreach my $rgresourceattribute (keys(%{$rgresource})) {
                if ($rgresourceattribute ne 'properties') {
                    $writer->startTag('attribute', 
				      'name' => $rgresourceattribute);
                    $writer->characters($rgresource->{$rgresourceattribute});
                    $writer->endTag('attribute');
                }
            }
            $writer->startTag('properties');
            foreach my $rgresourceproperty (@{$rgresource->{properties}}) {
                $writer->startTag('property');
                foreach my $rgresourcepropertyattribute 
		  (keys %{$rgresourceproperty}) {
                    $writer->startTag('attribute', 
				      'name' => $rgresourcepropertyattribute);
                    $writer->characters(
			  $rgresourceproperty->{$rgresourcepropertyattribute});
                    $writer->endTag('attribute');
                }
                $writer->endTag('property');
            }
            $writer->endTag('properties');
            $writer->endTag('resource');
        }
        $writer->endTag('resources');
        $writer->endTag('resource-group');
    }

    $writer->endTag('resource-groups');

    # XML closing tags
    $writer->endTag('config');

    $writer->end();

    # Close output
    $output->close();
}

# Return success for module load
1; 
