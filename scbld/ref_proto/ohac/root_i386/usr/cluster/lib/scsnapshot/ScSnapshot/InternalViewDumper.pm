#! /usr/perl5/bin/perl -w
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident   "@(#)InternalViewDumper.pm 1.3     08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# 
# Module: ScSnapshot::InternalViewDumper
#
# Abstract superclass representing the code generation backend.
# Should be derived in order to register an instance as the tool's 
# real backend.
#

package ScSnapshot::InternalViewDumper;
use strict;

#
# Performs the code generation for the given view.
# Arguments:
#   1) The view for which the code should be generated.
#      Cannot be undefined.
#   2) The file handle onto which the output should be written.
#      Cannot be undefined.
#
sub dump {} 

# Return success for module load
1; 
