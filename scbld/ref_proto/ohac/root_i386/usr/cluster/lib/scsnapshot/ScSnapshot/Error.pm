#! /usr/perl5/bin/perl -w
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident   "@(#)Error.pm 1.3     08/05/20 SMI"
# 
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# This file listed the error code defined for scsnapshot tool.
#

package ScSnapshot::Error;
use strict;

#
# Constructor.
#
sub new 
{
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self  = {};

    # Error codes
    $self->{CODE} = { OK =>      0, # Success indicator
		      ENOENT =>  2, # No Such file or directory
		      EIO =>     5, # Generic I/O errors
		      ENOEXEC => 8, # Invalid image file format : Parsing error
		      EACCES => 13, # Permission Denied
		      EINVAL => 22  # Invalid Argument
		    };

    bless ($self, $class);
    return $self;
}

#
# Returns the code matching the passed code name.
# Arguments:
#   1) The name of the exit code to retrieve. If the passed name does not
#      match a defined error the program will die with a internal error
#      message.
# 
sub getCode {

  my $self = shift;
  my $name = shift;

  if (!defined($name) || !defined($self->{CODE}->{$name})) {
    die("FATAL INTERNAL ERROR: $name is not a valid exit code name\n");
  }

  return $self->{CODE}->{$name};
}

# Return success for module load
1;
