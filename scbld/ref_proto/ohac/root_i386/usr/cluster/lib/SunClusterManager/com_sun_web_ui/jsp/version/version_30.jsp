<jsp:root version="1.2" 
	xmlns:f="http://java.sun.com/jsf/core" 
        xmlns:h="http://java.sun.com/jsf/html" 
        xmlns:jsp="http://java.sun.com/JSP/Page" 
        xmlns:ui="http://www.sun.com/web/ui">
  <jsp:directive.page contentType="text/html;charset=UTF-8" 
	pageEncoding="UTF-8" />

<!-- Version Page -->
<f:view>
  <ui:page>
    <ui:head title="#{versionBean.windowTitle}" />
      <ui:body>
        <ui:form id="form1">

          <ui:versionPage id="com_sun_web_appVersion" 
            binding="#{versionBean.version}" />
    </ui:form>
   </ui:body>
  </ui:page>
</f:view>
</jsp:root>
