<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DeviceGroupTopology.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="Quorum" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean 
      className="com.sun.cluster.spm.devicegroup.DeviceGroupTopologyViewBean">

<!-- Page header component -->
<spm:header pageTitle="devicegroup.title" 
           copyrightYear="2002" 
           baseName="com.sun.cluster.spm.resources.Resources" 
           bundleID="scBundle" 
           event="ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_config_change,ESC_cluster_dcs_state_change"/>

<jato:form name="DGTopologyForm" method="post"
           defaultCommandChild="/TopologyImage">

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="devicegroup.topology.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="devicegroup.topology.pageTitle.help"/>

<BR>
<!-- Set the layout style -->
<div class="content-layout">

   <cc:topology name="TopologyImage" bundleID="scBundle"/>

</div>

</jato:form>
</jato:useViewBean>
