<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceGroupProperties.jsp	1.9	08/07/14 SMI"
 */
--%>
<%@ page language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.rgm.ResourceGroupPropertiesViewBean">


<!-- Page header component -->
<spm:header pageTitle="rgm.resourcegroup.title"
            copyrightYear="2003"
            baseName="com.sun.cluster.spm.resources.Resources"
            bundleID="scBundle"
            event=""
	    tableTitle="rgm.resourcegroup.props.table.summary" >

<jato:containerView name="EditableScript">
  <jsp:include page="../edit/EditablePagelet.jsp"/>
</jato:containerView>

<jato:form name="scForm" method="post">

<!-- Bread Crumb component -->
<cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />
            
<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<!-- Alert Inline for command result -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- Page Title -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="rgm.resourcegroup.props.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="rgm.resourcegroup.props.pageTitle.help">

<cc:hidden name="Hidden"/>


 <!-- Action Buttons -->
<spm:rbac auths="solaris.cluster.modify">
  <BR>
    <div class="ConMgn"> 
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <cc:wizardwindow name="ResourceGroupWizard" bundleID="scBundle"/>
          </td>
        </tr>
      </table>
    </div>
</spm:rbac>

<div class="ConMgn"> 
  <table border="0" cellpadding="0" cellspacing="0">
    <!-- RG Name  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="NameLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.name"
                    styleLevel="2"
                    elementName="NameValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="NameValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>

    <!-- RG Type  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="TypeLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.type"
                    styleLevel="2"
                    elementName="TypeValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="TypeValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>

    <!-- RG Status  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="StatusLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.status"
                    styleLevel="2"
                    elementName="StatusValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:alarm name="StatusAlarm" bundleID="scBundle"/>
            <cc:text name="StatusValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>

    <!-- RG Current Primaries  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="CurrentPrimLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.currentPrimary"
                    styleLevel="2"
                    elementName="CurrentPrimValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="CurrentPrimValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>
  </table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<!-- Action Table -->
<cc:actiontable name="PropertiesTable"
                bundleID="scBundle"
                title="rgm.resourcegroup.props.table.title"
                summary="rgm.resourcegroup.props.table.summary"
                empty="rgm.resourcegroup.props.table.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>

</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>
