<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeHeaderPagelet.jsp	1.9	08/05/20 SMI"
 */
--%>

<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<jato:pagelet>

<script type="text/javascript" src="/com_sun_web_ui/js/browserVersion.js">

</script>


<SCRIPT LANGUAGE="JavaScript">

  function displayConfirmation() {
     var result = confirm('<cc:text name="ConfirmMsgStaticText" defaultValue="nodes.evacuate.confirmation" bundleID="scBundle"/>');
     if (result) {
      unRegisterEvents();
     }   
     return result;
  }
</SCRIPT>

<!-- Buttons -->
<div class="ConMgn"> 
 <!-- Buttons -->
 <table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
  <spm:rbac auths="solaris.cluster.admin">
   <jato:content name="Evacuate">
	<br>
     <cc:button name="EvacuateButton" bundleID="scBundle" defaultValue="nodes.evacuate"
          onClick="return displayConfirmation()" title="nodes.evacuate.title"/>
   </jato:content>
  </spm:rbac>
    </td>
  </tr>
 </table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="5" alt=""></div>

<div class="ConMgn"> 
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
   <td valign="top">
      <div class="ConTblCl1Div">
       <span class="LblLev2Txt">
			<label><cc:text name="StaticText" bundleID="scBundle"
         defaultValue="nodeDetailConfiguration.node"/></label>
       </span>
      </div>
   </td>
   <td valign="top">
       <div class="ConTblCl2Div">
			<span id="psLbl2" class="ConDefTxt"><cc:text name="NodeText"/></span>
       </div
   </td>
 </tr>
 <tr>
   <td valign="top">
      <div class="ConTblCl1Div">
       	<span class="LblLev2Txt">
				<label><cc:text name="StaticText" bundleID="scBundle" defaultValue="nodeDetailConfiguration.status"/></label>
       </span>
      </div>
   </td>
   <td>
      <div class="ConTblCl2Div">
       	<span id="psLbl2" class="ConDefTxt"><cc:alarm name="SummaryAlarmStatus"/><cc:text name="StatusText"/></span>
      </div>
   </td>  
 </tr>
</table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

</jato:pagelet>
