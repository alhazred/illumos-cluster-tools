<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)PublicInterfaceUsage.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="PublicInterfaceUsage" language="java" %>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.netif.PublicInterfaceUsageViewBean">

<script language="javascript">

  function PublicAdapterGraphWindow() {
    var windowFeatures = "<cc:text name='GraphWindowFeatureText'/>";
    var GraphWin = window.open(
      '../netif/PublicInterfaceUsageGraph?SelectedItems=' + getSelectedAdapters(),
      'PublicAdapterUsageGraphWindow',
      "'" + windowFeatures + "'");
    GraphWin.focus();
  }


  function getSelectedAdapters() {
    // Element name (prefix) of selection checkbox.
    var elementName = "PublicInterfaceUsage.PublicAdapterUsageTable.SelectionCheckbox";

    // Document form.
    var form = document.scForm;

    // Set flag if a selection has been made.
    var retval = "";
    for (i = 0; i < form.elements.length; i++) {
      var e = form.elements[i];
      if (e.name.indexOf(elementName) != -1 && e.name.indexOf("jato_boolean") == -1) {
        if (e.checked) {
          var nameItem = e.name;
          var ItemNumIndex = nameItem.lastIndexOf("Checkbox")+8;
          var selectedRow = nameItem.substring(ItemNumIndex, nameItem.length);

          var item = "PublicInterfaceUsage.UsageTableTiledView[" +
            selectedRow + "].PublicAdapterHiddenName";
          retval += document.scForm.elements[item].value + ",";
        }
      }
    }

    return retval.substring(0, retval.length-1); // remove the last "," and return
  }


  function togglePublicInterfaceButtons() {

    var graphButton = "PublicInterfaceUsage.PublicAdapterGraphButton";
    var elementName = "PublicInterfaceUsage.PublicAdapterUsageTable.SelectionCheckbox";

    // Document form.
    var form = document.scForm;

    // Flag indicating to disable action button and menu options.
    var disabled = true;

    // Set flag if a selection has been made.
    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];

        if (e.name.indexOf(elementName) != -1 && e.name.indexOf("jato_boolean") == -1) {
            if (e.checked) {
                disabled = false;
                break;
            }
        }
    }
    ccSetButtonDisabled(graphButton, "scForm", disabled);
  }

</script>



<!-- Page header component -->
<spm:header
  pageTitle="netif.publicInterface.usage.header"
  copyrightYear="2006"
  baseName="com.sun.cluster.spm.resources.Resources"
  bundleID="scBundle"
  event="ESC_cluster_ipmp_group_state,ESC_cluster_ipmp_if_change,ESC_cluster_ipmp_group_member_change,ESC_cluster_slm_thr_state"
  onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField" />

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<spm:SLMcheck name="SLMCheck" summary="slmcheck.message.summary"
  detail="slmcheck.message.detail">

<!-- Alert Inline for command result -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- Page Title -->
<cc:pagetitle name="GenericPageTitle"
  bundleID="scBundle"
  pageTitleText="netif.publicInterface.usage.title"
  showPageTitleSeparator="true"
  pageTitleHelpMessage="netif.publicInterface.usage.help">

<br>

<!-- Action Table -->
<cc:actiontable name="PublicAdapterUsageTable"
  bundleID="scBundle"
  title="netif.publicInterface.TableTitle"
  summary="netif.publicInterface.TableSummary"
  empty="netif.publicInterface.emptyTable"
  rowSelectionType="multiple"
  selectionJavascript="togglePublicInterfaceButtons()"
  showAdvancedSortingIcon="false"
  showLowerActions="false"
  showPaginationControls="false"
  showPaginationIcon="false"
  showSelectionIcons="true"
  showSortingRow="false"
  page="1" />

</cc:pagetitle>
</spm:SLMcheck>
</jato:form>
</spm:header>
</jato:useViewBean>
