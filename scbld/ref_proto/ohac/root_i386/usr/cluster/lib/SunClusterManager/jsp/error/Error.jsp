<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Error.jsp	1.3	08/05/20 SMI"
 */
--%>

<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>


<!-- You must only set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.error.ErrorViewBean">

<!-- Page header component -->
<spm:header pageTitle="error.title" copyrightYear="2002" baseName="com.sun.cluster.spm.resources.Resources" bundleID="scBundle" >

<!-- PageTitle -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
 pageTitleText="error.title" 
 showPageTitleSeparator="true"
 pageTitleHelpMessage="error.help">

<br>

<spm:alertinline name="Alert" bundleID="testBundle" />

</cc:pagetitle>
</spm:header>
</jato:useViewBean>
