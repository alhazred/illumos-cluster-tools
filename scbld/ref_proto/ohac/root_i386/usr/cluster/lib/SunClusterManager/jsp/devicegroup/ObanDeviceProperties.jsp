<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ObanDeviceProperties.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="ObanDeviceProperties" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean 
    className="com.sun.cluster.spm.devicegroup.ObanDevicePropertiesViewBean">

<!-- Page header component -->
<spm:header pageTitle="devicegroup.title"
           copyrightYear="2003" 
           baseName="com.sun.cluster.spm.resources.Resources" 
           bundleID="scBundle" onLoad="reloadTree()"
           event="ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_state_change">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="devicegroup.properties.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="devicegroup.properties.pageTitle.help"/>

<spm:rbac auths="solaris.cluster.modify solaris.cluster.admin"
	      any="true">
<BR>
</spm:rbac>

<!-- Summary -->
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
 <tr>
   <td valign="top">
     <div class="ConTblCl1Div">&nbsp;
       <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.name"/>
         </label>
       </span>
     </div>
   </td>
   <td valign="top">
     <div class="ConTblCl2Div">
       <span id="psLbl2" class="ConDefTxt">
         <cc:text name="DGName"/>
       </span>
     </div>
   </td>
 </tr>
 <tr>
   <td valign="top">
     <div class="ConTblCl1Div">&nbsp;
       <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.devicetype"/>
         </label>
       </span>
     </div>
   </td>
   <td valign="top">
     <div class="ConTblCl2Div">
       <span id="psLbl2" class="ConDefTxt">
         <cc:text name="DGType"/>
       </span>
     </div>
   </td>
 </tr>
 <tr>
   <td valign="top">
     <div class="ConTblCl1Div">&nbsp;
       <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.status"/>
         </label>
       </span>
     </div>
   </td>
   <td>
     <div class="ConTblCl2Div">
       <span id="psLbl2" class="ConDefTxt">
         <cc:alarm name="DGStatusAlarm"/>
         <cc:text name="DGStatusText"/>
       </span>
     </div>
   </td>
 </tr>
<tbody>
</table>

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

<!-- Action Table -->
<cc:actiontable name="DGPropertiesTable"
                bundleID="scBundle"
                title="devicegroup.properties.table.title"
                summary="devicegroup.properties.table.summary"
                empty="devicegroup.properties.table.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                maxRows="10"
                page="1"/>

</jato:form>
</spm:header>
</jato:useViewBean>
