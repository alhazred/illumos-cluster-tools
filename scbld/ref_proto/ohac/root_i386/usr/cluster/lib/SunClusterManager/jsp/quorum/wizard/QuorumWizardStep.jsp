<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)QuorumWizardStep.jsp	1.7	08/05/20 SMI"
 */
--%>
<%@ page language="java" %> 
<%@ page import="com.iplanet.jato.view.ViewBean" %>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<jato:pagelet>

<cc:i18nbundle id="scBundle" 
               baseName="com.sun.cluster.spm.resources.Resources" />

<!--
  Work-around for a Lockhart's bug in generated HTML (BugID 4930639):
  The wizard tag generates an additional <table> tag before the
  property sheet but without any <tr><td> tags.
  As a consequence, in some browsers (namely Mozilla or Netscape 7),
  only the first section of the property sheet is displayed.
  The work-around consists in adding those <tr><td> tags; this has no
  impact on the global layout.
  -->
</table>
<!--
  Work-around for Lockhart's bug on wizard closing (BugID 4933149):
  When a wizard is opened, if the underlying page is changed, an
  application error is displayed.
  With this work-around, if the underlying page is unknown, the
  wizard will not attempt to refresh it.
  -->
<script type="text/javascript">
function ccWizardForwardAndClose(refreshForm, refreshCmdChild) {
    if (refreshForm != '' && refreshCmdChild != '') {
            var f = window.opener.document.forms[refreshForm];
            if (f != null) {
		var wizardButtonName = null;

            if(refreshCmdChild == 'QuorumStatus.QuorumDeviceVoteView.QuorumWizardExit') {
		wizardButtonName = 'QuorumStatus.QuorumDeviceVoteView.AddQuorumWizard';
            }

            var wizardButton = f.elements[wizardButtonName];
		if (wizardButton) {
                    f.action = f.action + "?" + refreshCmdChild + "=";
		f.submit();
            }
	}
    }
    close();
}

function wizardPageInit() {
	
    var hiddenField1 = window.document.wizWinForm.elements['WizardWindow.Wizard.NasReviewPageView.CloseWizardHiddenField'];
    var hiddenField2 = window.document.wizWinForm.elements['WizardWindow.Wizard.SdiskReviewPageView.CloseWizardHiddenField'];
    var hiddenField3 = window.document.wizWinForm.elements['WizardWindow.Wizard.QserverReviewPageView.CloseWizardHiddenField'];

    if (!hiddenField1) {
	hiddenField1 = window.document.wizWinForm.elements['WizardWindow.Wizard.NasReviewPageView.CloseWizardHiddenField'];
    }
    if (!hiddenField2) {
	hiddenField2 = window.document.wizWinForm.elements['WizardWindow.Wizard.SdiskReviewPageView.CloseWizardHiddenField'];
    }
    if (!hiddenField3) {
	hiddenField3 = window.document.wizWinForm.elements['WizardWindow.Wizard.QserverReviewPageView.CloseWizardHiddenField'];
    }

    if (hiddenField1 || hiddenField2 || hiddenField3) {
	var closeWizard1 = "false";
	var closeWizard2 = "false";
	var closeWizard3 = "false";
	if (hiddenField1) closeWizard1 = hiddenField1.value;
	if (hiddenField2) closeWizard2 = hiddenField2.value;
	if (hiddenField3) closeWizard3 = hiddenField3.value;

	if ((closeWizard1 == "true") || (closeWizard2 == "true") || (closeWizard3 == "true")) {
		var f = window.opener.document.forms['scForm'];
		var refreshCmdChild;

		if (f.elements['QuorumStatus.QuorumDeviceVoteView.AddQuorumWizard']) {
			refreshCmdChild = 'QuorumStatus.QuorumDeviceVoteView.QuorumWizardExit';
                }
		ccWizardForwardAndClose('scForm', refreshCmdChild);
	}
    }
}

WizardWindow_Wizard.pageInit = wizardPageInit;
</script>

<table border="0" cellpadding="0" cellspacing="0">

<!-- Alert Inline -->
<tr><td>

<div class="ConMgn">
  <cc:hidden name="CloseWizardHiddenField"/>
  <cc:alertinline name="Alert" bundleID="scBundle" />
</div>

</td></tr>

<tr><td>

<!-- PropertySheet -->
<cc:propertysheet name="PropertySheet" 
                  bundleID="scBundle" 
                  showJumpLinks="false"
                  addJavaScript="false" />

<!--
  End Lockhart's bug work-around (BugID 4930639).
  -->
</td></tr>

</jato:pagelet>
