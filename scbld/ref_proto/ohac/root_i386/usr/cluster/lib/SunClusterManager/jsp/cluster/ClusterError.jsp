<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterError.jsp	1.9	08/05/20 SMI"
 */
--%>

<%@page info="ClusterError" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>


<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.cluster.ClusterErrorViewBean">

<!-- Page header component -->
<spm:header pageTitle="clusterError.title" copyrightYear="2003" baseName="com.sun.cluster.spm.resources.Resources" bundleID="scBundle" event="ESC_cluster_ipmp_group_state,ESC_cluster_ipmp_group_member_change,ESC_cluster_tp_path_config_change,ESC_cluster_tp_path_state_change,ESC_cluster_quorum, ESC_cluster_node_config_change,ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_config_change,ESC_cluster_dcs_state_change">


<jato:form name="scForm" method="post">

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<!-- Alert Inline for command result -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle" pageTitleText="clusterError.pageTitle" showPageTitleSeparator="true" pageTitleHelpMessage="clusterError.help"/>

<br>

<div class="content-layout">
<jato:content name="NoError">
 <cc:text name="StaticText" bundleID="scBundle"
          defaultValue="clusterError.ok"/>
</jato:content>
</div>

<spm:rbac auths="solaris.cluster.read">
  <jato:content name="GlobalDevice">
    <jato:containerView name="StatusTableView">
      <jsp:include page="../devicegroup/DeviceGroupTablePagelet.jsp"/>
    </jato:containerView>
  </jato:content>

<br>
</spm:rbac>

<spm:rbac auths="solaris.cluster.read">
<jato:content name="Node">
<jato:containerView name="NodeInfoView">
<br>
<jsp:include page="../node/NodeInfoPagelet.jsp"/>
</jato:containerView>
</jato:content>
</spm:rbac>

<spm:rbac auths="solaris.cluster.read">
<jato:content name="ResourceGroups">
<br>
<jato:containerView name="ResourceGroupsStatusView">
<jsp:include page="../rgm/ResourceGroupsStatusPagelet.jsp"/>
</jato:containerView>
</jato:content>
</spm:rbac>

<spm:rbac auths="solaris.cluster.read">
<jato:content name="Interconnects">
<br>
<jato:containerView name="PrivateInteconnectsView">
<jsp:include page="../transport/TransportTablePagelet.jsp"/>
</jato:containerView>
</jato:content>
</spm:rbac>

<spm:rbac auths="solaris.cluster.read">
<jato:content name="PublicInterfaces">
<br>
<jato:containerView name="PublicInterfacesView">
<jsp:include page="../netif/PublicInterfacesTablePagelet.jsp"/>
</jato:containerView>
</jato:content>
</spm:rbac>


<spm:rbac auths="solaris.cluster.read">
<jato:content name="Quorum">
<br>
<jato:containerView name="QuorumVoteView">
<jsp:include page="../quorum/QuorumDeviceVotePagelet.jsp"/>
</jato:containerView>
</jato:content>
</spm:rbac>

<jato:content name= "Odyssey">
 <jato:containerView name="OdysseyContainer">
  <jsp:include page="../cluster/OdysseyClusterPagelet.jsp"/>
 </jato:containerView>
</jato:content>

</jato:form>

</spm:header>

</jato:useViewBean>
