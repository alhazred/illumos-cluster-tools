<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)JunctionProperties.jsp	1.6	08/05/20 SMI"
 */
--%>

<%@page info="JunctionsStatus" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.transport.JunctionPropertiesViewBean">

<!-- Page header component -->
<spm:header 	pageTitle="transport.junctions.properties.title" 
		copyrightYear="2004" 
		baseName="com.sun.cluster.spm.resources.Resources" 
		bundleID="scBundle"
		onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Bread Crumb component -->
<cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />


<!-- Command Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- PageTitle -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
 pageTitleText="transport.junctions.properties.title" 
 showPageTitleSeparator="true"
 pageTitleHelpMessage="transport.junctions.properties.help">

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="5" alt=""></div>

<!-- Summary  -->

<div class="ConMgn">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	<cc:label name="JunctionLabel" bundleID="scBundle"
         defaultValue="transport.junctions.properties.interconnect.label"/>
	</div>	
   </td>
   <td valign="top">
	<div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
	<cc:text name="JunctionValue" bundleID="scBundle"/>
   	</span>
        </div>
   </td>
 </tr>
 <tr>
   <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	 <cc:label name="EnabledLabel" bundleID="scBundle"
          defaultValue="transport.junctions.properties.enabled.label"/>
	</div>
   </td>
   <td valign="top">
	<div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
	<cc:alarm name="StatusAlarm"/>
	<jato:content name="Enabled">
	 <cc:text name="EnabledValue" bundleID="scBundle"
          defaultValue="yesValue"/>
	</jato:content>
	<jato:content name="Disabled">
	 <cc:text name="DisabledValue" bundleID="scBundle"
          defaultValue="noValue"/>
	</jato:content>
	</span>
        </div>
   </td>
 </tr>
</table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

<jato:containerView name="TableView">

<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="transport.junctions.properties.TableTitle"
summary="transport.junctions.properties.TableSummary"
empty="transport.junctions.properties.emptyTable"
rowSelectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="false"
showSortingRow="false"
page="1"/>

</jato:containerView>
</cc:pagetitle>

</jato:form>
</spm:header>
</jato:useViewBean>
