<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)TaskWizardStep.jsp	1.7	08/05/20 SMI"
 */
--%>
<%@ page language="java" %> 
<%@ page import="com.iplanet.jato.view.ViewBean" %>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<jato:pagelet>

<cc:i18nbundle id="dsBundle" 
               baseName="com.sun.cluster.dswizards.common.WizardResource" />

<!--
  Work-around for a Lockhart's bug in generated HTML (BugID 4930639):
  The wizard tag generates an additional <table> tag before the
  property sheet but without any <tr><td> tags.
  As a consequence, in some browsers (namely Mozilla or Netscape 7),
  only the first section of the property sheet is displayed.
  The work-around consists in adding those <tr><td> tags; this has no
  impact on the global layout.
  -->
</table>
<script type="text/javascript">
function wizardPageInit() {
}
WizardWindow_Wizard.pageInit = wizardPageInit;

function disableTblChkBox(){
        var form = document.wizWinForm;
        for (i = 0; i < form.elements.length; i++) {
                var e = form.elements[i];
                if (e.name.indexOf("SelectionCheckbox") != -1) {
			e.disabled = true;
			e.checked = false;
                }
		if (e.name.indexOf("SelectionRadiobutton") != -1) {
			e.disabled = true;
			e.checked = false;
                }
        }
}

function enableTblChkBox(){
        var form = document.wizWinForm;
        for (i = 0; i < form.elements.length; i++) {
                var e = form.elements[i];
                if (e.name.indexOf("SelectionCheckbox") != -1) {
			e.disabled = false;
                }
		if (e.name.indexOf("SelectionRadiobutton") != -1) {
			e.disabled = false;
                }
        }
}

</script>

<table border="0" cellpadding="0" cellspacing="0">

<!-- Alert Inline -->
<tr><td>

<div class="ConMgn">
  <cc:hidden name="CloseWizardHiddenField"/>
  <cc:alertinline name="Alert" bundleID="dsBundle" />
</div>

</td></tr>

<tr><td>

<!-- PropertySheet -->
<cc:propertysheet name="PropertySheet" 
                  bundleID="dsBundle" 
                  showJumpLinks="false"
                  addJavaScript="true" />

<!--
  End Lockhart's bug work-around (BugID 4930639).
  -->
</td></tr>

</jato:pagelet>
