<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)QuorumDetail.jsp	1.6	08/05/20 SMI"
 */
--%>

<%@page info="Quorum" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.quorum.QuorumDetailViewBean">

<!-- Page header component -->
<spm:header pageTitle="quorum.title" copyrightYear="2003"
            baseName="com.sun.cluster.spm.resources.Resources"
            bundleID="scBundle"
            event="ESC_cluster_quorum"
            onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="quorum.properties.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="quorum.properties.pageTitle.help"/>

<div class="ConMgn"> 
<!-- Summary -->
<table border="0" cellpadding="0" cellspacing="0">
  <!-- Quorum Name  -->
  <tr>
    <td valign="top">
      <div class="ConTblCl1Div">&nbsp;
        <cc:label name="NameLabel" bundleID="scBundle"
                  defaultValue="quorum.properties.label.name"
                  styleLevel="2"
                  elementName="NameValue" />
      </div>
    </td>
    <td valign="top">
      <div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
          <cc:text name="NameValue" bundleID="scBundle"/>
        </span>
      </div>
    </td>
  </tr>
  <tr>
    <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	<cc:label name="TypeLabel" bundleID="scBundle"
		  defaultValue="quorum.properties.label.type"
		  styleLevel="2"
		  elementName="TypeValue" />
	</div>
    </td>
    <td valign="top">
	<div class="ConTblCl2Div">
	<span id="psLbl2" class="ConDefTxt">
	  <cc:text name="TypeValue" bundleID="scBundle"/>
	</span>
	</div>
    </td>
  </tr>
  <tr>
    <td valign="top">
      <div class="ConTblCl1Div">&nbsp;
        <cc:label name="StatusLabel" bundleID="scBundle"
                  defaultValue="quorum.properties.label.status"
                  styleLevel="2"
                  elementName="StatusValue" />
      </div>
    </td>
    <td valign="top">
      <div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
		  <cc:alarm name="StatusAlarm"/>
          <cc:text name="StatusValue" bundleID="scBundle"/>
        </span>
      </div>
    </td>
  </tr>
</table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<!-- Action Table -->
<cc:actiontable name="PropertiesTable"
                bundleID="scBundle"
                title="quorum.properties.table.title"
                summary="quorum.properties.table.summary"
                empty="quorum.properties.table.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>

</jato:form>
</spm:header>
</jato:useViewBean>
