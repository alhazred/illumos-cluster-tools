<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Tree.jsp	1.12	08/07/10 SMI"
 */
--%>

<%@page info="Tree" language="java"%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<jato:useViewBean className="com.sun.cluster.spm.tree.TreeViewBean">

<!-- Define the resource bundle, html, head, meta, stylesheet and body tags -->
<spm:header pageTitle="tree.title" copyrightYear="2003" bgColor="#ffffff"
            baseName="com.sun.cluster.spm.resources.Resources" bundleID="tree"
            frame="menu"
            event="ESC_zonecluster_state_change,ESC_cluster_node_config_change,ESC_cluster_node_state_change,ESC_cluster_quorum_config_change,ESC_cluster_quorum_state_change,ESC_cluster_rg_state,ESC_cluster_rg_config_change,ESC_cluster_r_state,ESC_cluster_r_config_change,ESC_cluster_tp_path_state_change,ESC_cluster_tp_if_state_change,ESC_cluster_dcs_config_change,ESC_cluster_dcs_state_change,ESC_cluster_dpm_disk_path_ok,ESC_cluster_dpm_disk_path_failed,ESC_cluster_dpm_disk_path_monitored,ESC_cluster_dpm_disk_path_unmonitored,ESC_cluster_ipmp,ESC_cluster_zones_state,ESC_cluster_slm_thr_state,ESC_cluster_cmm_reconfig">

<jato:form name="scForm" method="post">
<div class="tree-body">

<!-- The Lockhart menu tree -->
<cc:ctree name="Tree" targetFrame="content" isPersistent="true" />

</div>
</jato:form>
</spm:header>
</jato:useViewBean>
