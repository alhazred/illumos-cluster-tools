<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeDetailConfiguration.jsp	1.4	08/05/20 SMI"
 */
--%>

<%@page info="ClusterStatus" language="java"%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.node.NodeDetailConfigurationViewBean">

<!-- Define the resource bundle, html, head, meta, stylesheet and body tags -->
<spm:header	pageTitle="clusterStatus.title" 
		copyrightYear="2004"
 		baseName="com.sun.cluster.spm.resources.Resources" 
		bundleID="scBundle"
		event="ESC_cluster_node_config_change">

<jato:form name="scForm" method="post">

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle" pageTitleText="nodeConfig.pageTitle" showPageTitleSeparator="true" pageTitleHelpMessage="nodeConfig.help"/>

<jato:containerView name="NodeHeaderView">
<jsp:include page="NodeHeaderPagelet.jsp"/>
</jato:containerView>

<!-- Action Table -->
<cc:actiontable
name="ConfigurationTable"
bundleID="scBundle"
title="nodeDetailConfiguration.title"
summary="nodeDetailConfiguration.information"
empty="nodeDetailConfiguration.emptyTable"
rowSelectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="false"
showSortingRow="false"
page="1"/>



</jato:form>

</spm:header>
</jato:useViewBean>
