<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterConfiguration.jsp	1.4	08/05/20 SMI"
 */
--%>

<%@page info="ClusterConfiguration" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>


<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.cluster.ClusterConfigurationViewBean">

<!-- Page header component -->
<spm:header pageTitle="clusterConfiguration.title" copyrightYear="2003" baseName="com.sun.cluster.spm.resources.Resources" bundleID="scBundle" >


<jato:form name="scForm" method="post">

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle" pageTitleText="clusterConfig.pageTitle" showPageTitleSeparator="true" pageTitleHelpMessage="clusterConfig.help"/>

<br>

<div class="content-layout">

 
<!-- Action Table -->
<cc:actiontable
name="TableConfiguration"
bundleID="scBundle"
title="clusterConfiguration.table"
summary="clusterConfiguration.summary"
empty="clusterConfiguration.empty"
rowSelectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="false"
showSortingRow="false"
maxRows="10"
page="1"/>

</div>

</jato:form>

</spm:header>

</jato:useViewBean>
