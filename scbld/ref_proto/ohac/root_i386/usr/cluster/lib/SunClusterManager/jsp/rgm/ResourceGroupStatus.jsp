<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceGroupStatus.jsp	1.11	08/07/14 SMI"
 */
--%>
<%@ page language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.rgm.ResourceGroupStatusViewBean">

    <!-- Page header component -->
    <spm:header pageTitle="rgm.resourcegroup.title" copyrightYear="2003"
        baseName="com.sun.cluster.spm.resources.Resources"
        bundleID="scBundle"
        onLoad="reloadTree()"
        event="ESC_cluster_rg,ESC_cluster_r_">

        <jato:form name="scForm" method="post">

            <!-- Hidden Field for the tree -->
            <cc:hidden name="TreeNodeHiddenField"/>

            <!-- Bread Crumb component -->
            <cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />

            <!-- Navigation Tabs -->
            <cc:tabs name="GenericTabs" bundleID="scBundle" />

            <!-- Alert Inline -->
            <spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

            <!-- Page Title -->
            <cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
                pageTitleText="rgm.resourcegroup.status.pageTitle.title" 
                showPageTitleSeparator="true"
                pageTitleHelpMessage="rgm.resourcegroup.status.pageTitle.help">


                <SCRIPT LANGUAGE="JavaScript">

                    function getHiddenFieldValue(hiddenTextFieldName) {
     
                    // Element name (prefix) of selection checkbox.
                    var elementName = "<cc:text name='SelectionNameText'/>";

                    // Document form.
                    var form = document.scForm;

                    var nameItem, ItemNumIndex, selectedRow, prefix, resGrpComp, ej = null, retValue = null;
                    var sValue, s11, s12, s21, s22, s3, tmpS, sEnabled = false, sDisabled = false; 
                    var sMonitored = false, sNMonitored = false, sCSFailedFlag = false;
                    var countChecked = 0;

                    var enabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_ENABLED%>";
                    var monitored =  "<%=com.sun.cluster.common.ClusterState.CLSTATE_MONITORED%>";
                    var stopFailed = "<%=com.sun.cluster.common.ClusterState.CLSTATE_STOPFAILED%>";
    
    
                    for (i = 0; i < form.elements.length; i++) {
                    var e = form.elements[i];
                    if ((e.name.indexOf(elementName) != -1)&&(e.type=="checkbox")) {
                    if (e.checked) {
                    countChecked = countChecked + 1;
                    nameItem = e.name;
                    ItemNumIndex = nameItem.lastIndexOf("Checkbox")+8;
                    selectedRow = nameItem.substring(ItemNumIndex, nameItem.length);
                    prefix =  elementName.substring(0,elementName.indexOf("ResourcesTable.SelectionCheckbox"));
                    resGrpComp = prefix + "TableTiledView[" + selectedRow + "]." + hiddenTextFieldName;
                    ej = form.elements[resGrpComp];
                    if (hiddenTextFieldName == 'HiddenResName'){
                    if (retValue == null)
                    retValue = ej.value;
                    else 
                    retValue = retValue + ", " + ej.value;
                    }
                    else if (hiddenTextFieldName == 'HiddenResBtnState'){
                    sValue =  ej.value; 
                    s11 = sValue.substring(0, 1);
                    s12 = sValue.substring(2, 3);
                    s21 = sValue.substring(4, 5);
                    s22 = sValue.substring(6, 7);
                    s3  = sValue.substring(8, 11);
    
                    if (s11==enabled) sEnabled = true;
                    else sDisabled = true;
                    if (s21==monitored) sMonitored = true;
                    else sNMonitored = true;
                    if (s3==stopFailed) sCSFailedFlag = true;
                    else sCSFailedFlag = false;
                    }
                    }
                    }
                    }

                    if (countChecked==0) return '';

                    if (hiddenTextFieldName == 'HiddenResBtnState'){
                    if ((sEnabled) && (!sDisabled))
                    retValue = "1";
                    else if ((!sEnabled) && (sDisabled))
                    retValue = "0";
                    else if ((sEnabled) && (sDisabled))
                    retValue = "2";
                    else retValue = "3";

                    if ((sMonitored) && (!sNMonitored))
                    retValue = retValue + "1";
                    else if ((!sMonitored) && (sNMonitored))
                    retValue = retValue + "0";
                    else if ((sMonitored) && (sNMonitored))
                    retValue = retValue + "2";                
                    else retValue = retValue + "3";
                    if (sCSFailedFlag) retValue = retValue + "1";
                    else retValue = retValue + "0";
                    if (countChecked > 1) retValue = retValue + "0";
                    else retValue = retValue + "1";
	
                    }
    
                    return retValue;  
                    }

                    function displayConfirmation(operation) {
   
                    var msg = "null";

                    if (operation == '0') {
                    unRegisterEvents();
                    return false;

                    } else if (operation == '1') {
                    msg = '<cc:text name="StaticText" 
                    defaultValue="rgm.resource.operation.disable.confirm"
                    bundleID="scBundle"/>';

                    } else if (operation == '2') {
                    msg = '<cc:text name="StaticText" 
                    defaultValue="rgm.resource.operation.enable.confirm"
                    bundleID="scBundle"/>';
  
                    } else if (operation =='3') {
                    msg = '<cc:text name="StaticText" 
                    defaultValue="rgm.resource.operation.disableMonitor.confirm"
                    bundleID="scBundle"/>';

                    } else if (operation == '4') {
                    msg = '<cc:text name="StaticText" 
                    defaultValue="rgm.resource.operation.enableMonitor.confirm"
                    bundleID="scBundle"/>';
  
  
                    } else if (operation == 'remove') {
                    msg = '<cc:text name="StaticText" 
                    defaultValue="rgm.resource.operation.remove.confirm"
                    bundleID="scBundle"/>';

                    } else if (operation == 'clearStopFailed') {
                    msg = '<cc:text name="StaticText" 
                    defaultValue="rgm.resource.operation.clearStopFailed.confirm"
                    bundleID="scBundle"/>';
                    }

                    msg = msg.replace('{0}', getHiddenFieldValue("HiddenResName"));

                    var result = confirm(msg + '\n <cc:text name="StaticText"
                    defaultValue="selectionConfirmation"
                    bundleID="scBundle"/>');
                    if (result) {
                    unRegisterEvents();
                    }   
  
                    return result;
                    }	

                    // This function will toggle the disabled state of action buttons
                    // depending on single or multiple selections.
                    function toggleDisabledState() {
                    // Action button.
                    var clearStopFailedActionButton = "<cc:text name='ClearStopFailedActionButtonNameText'/>";
                    var deleteActionButton = "<cc:text name='DeleteActionButtonNameText'/>";
                    var rsJumpMenu = "<cc:text name='RsActionMenuNameText'/>";

                    var btnValue = getHiddenFieldValue("HiddenResBtnState");
        
                    if (btnValue!=''){
                    if (btnValue.charAt(0)=='1'){

                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,1);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,2);                    
                    }
                    else if (btnValue.charAt(0)=='0'){
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,1);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,2); 
                    }
                    else if (btnValue.charAt(0)=='2'){
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,1);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,2);
                    }        
                    else {
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,1);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,2);
                    }
                    if (btnValue.charAt(1)=='1'){
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,3);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,4);
                    }
                    else if (btnValue.charAt(1)=='0'){
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,3);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,4);
                    }
                    else if (btnValue.charAt(1)=='2'){
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,3);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,4);
                    }
                    else{
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,3);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",false,4);
                    }       

                    if ((btnValue.charAt(2)=='1') && (btnValue.charAt(3)=='1'))
                    ccSetButtonDisabled(clearStopFailedActionButton, "scForm", false);    
                    else
                    ccSetButtonDisabled(clearStopFailedActionButton, "scForm", true);

                    if (btnValue.charAt(3)=='1' && btnValue.charAt(0)=='0')
                    ccSetButtonDisabled(deleteActionButton, "scForm", false);
                    else 
                    ccSetButtonDisabled(deleteActionButton, "scForm", true);

                    return;
            
                    }

                    // Toggle action button disabled state.
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,1);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,2);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,3);
                    ccSetDropDownMenuOptionDisabled(rsJumpMenu,"scForm",true,4);
                    ccSetButtonDisabled(clearStopFailedActionButton, "scForm", true);
                    ccSetButtonDisabled(deleteActionButton, "scForm", true);
                    }	
                </SCRIPT>

                <div class="ConMgn"> 
                    <table border="0" cellpadding="0" cellspacing="0">
                        <!-- RG Name  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="NameLabel" bundleID="scBundle"
                                    defaultValue="rgm.resourcegroup.label.name"
                                    styleLevel="2"
                                    elementName="NameValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="NameValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- RG Type  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="TypeLabel" bundleID="scBundle"
                                    defaultValue="rgm.resourcegroup.label.type"
                                    styleLevel="2"
                                    elementName="TypeValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="TypeValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- RG Status  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="StatusLabel" bundleID="scBundle"
                                    defaultValue="rgm.resourcegroup.label.status"
                                    styleLevel="2"
                                    elementName="StatusValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:alarm name="StatusAlarm" bundleID="scBundle"/>
                                        <cc:text name="StatusValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- Suspended -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="SuspendedLabel" bundleID="scBundle"
                                    defaultValue="rgm.resourcegroup.label.suspended"
                                    styleLevel="2"
                                    elementName="SuspendedValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:alarm name="SuspendedAlarm" bundleID="scBundle"/>
                                        <cc:text name="SuspendedValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>
    
                        <!-- RG Current Primaries  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="CurrentPrimLabel" bundleID="scBundle"
                                    defaultValue="rgm.resourcegroup.label.currentPrimary"
                                    styleLevel="2"
                                    elementName="CurrentPrimValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="CurrentPrimValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- RG Potential Primaries  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="PotentialPrimariesLabel" bundleID="scBundle"
                                    defaultValue="rgm.resourcegroup.status.label.potentialPrimaries"
                                    styleLevel="2"
                                    elementName="PotentialPrimariesValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="PotentialPrimariesValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div><img src="/com_sun_web_ui/images/other/dot.gif"
                width="1" height="10" alt=""></div>

                <!-- Action Table for Resources -->
                <cc:actiontable name="ResourcesTable"
                bundleID="scBundle"
                title="rgm.resourcegroup.status.table.title"
                summary="rgm.resourcegroup.status.table.summary"
                empty="rgm.resourcegroup.status.table.empty"
                selectionJavascript="setTimeout('toggleDisabledState()', 0)"
                selectionType="multiple"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="true"
                showSelectionSortIcon="false"
                showSortingRow="true"
                page="1"/>
                <BR>

                <!-- Action Table for RG dependencies -->
                <cc:actiontable name="DependenciesTable"
                bundleID="scBundle"
                title="rgm.resourcegroup.status.table.dependencies.title"
                summary="rgm.resourcegroup.status.table.dependencies.summary"
                empty="rgm.resourcegroup.status.table.dependencies.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>
                <BR>

                <!-- Action Table for RG affinities -->
                <cc:actiontable name="AffinitiesTable"
                bundleID="scBundle"
                title="rgm.resourcegroup.status.table.affinities.title"
                summary="rgm.resourcegroup.status.table.affinities.summary"
                empty="rgm.resourcegroup.status.table.affinities.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>

            </cc:pagetitle>
        </jato:form>
    </spm:header>
</jato:useViewBean>
