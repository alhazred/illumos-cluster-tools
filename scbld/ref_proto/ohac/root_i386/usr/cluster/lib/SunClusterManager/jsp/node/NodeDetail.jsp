<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeDetail.jsp	1.16	08/07/14 SMI"
 */
--%>

<%@page info="Node" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.node.NodeDetailViewBean">

<!-- Page header component -->
<spm:header pageTitle="nodes.title" copyrightYear="2006" baseName="com.sun.cluster.spm.resources.Resources" bundleID="scBundle" 
event="ESC_cluster_tp_path_config_change,ESC_cluster_tp_if_state_change,ESC_cluster_node_state_change,ESC_cluster_quorum,ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_config_change,ESC_cluster_dcs_state_change,ESC_cluster_zones_state" onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<spm:alertinline name="StatusAlert" bundleID="scBundle" />

<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle" pageTitleText="nodeStatus.pageTitle" showPageTitleSeparator="true" pageTitleHelpMessage="nodeStatus.help"/>

<!-- Set the layout style -->

<jato:containerView name="NodeHeaderView">
<jsp:include page="NodeHeaderPagelet.jsp"/>
</jato:containerView>

<spm:rbac auths="solaris.cluster.read">
<jato:containerView name="ResourceGroupsStatusView">
<jsp:include page="../rgm/ResourceGroupsStatusPagelet.jsp"/>
</jato:containerView>
</spm:rbac>

<br>
<jato:containerView name="ZonesTableView">

<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="zones.tableTitle"
summary="zones.tableSummary"
empty="zones.tableEmpty"
rowSelectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="false"
showSortingRow="false"
page="1"/>

</jato:containerView>

<br>   
<jato:containerView name="ClusterZonesTableView">

<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="cZones.tableTitle"
summary="cZones.tableSummary"
empty="cZones.tableEmpty"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSortingRow="false"
maxRows="10"
page="1"/>

</jato:containerView>
<br>    

<spm:rbac auths="solaris.cluster.read">
<jato:containerView name="StatusTableView">
<jsp:include page="../devicegroup/DeviceGroupTablePagelet.jsp"/>
</jato:containerView>
</spm:rbac>

<br>


<jato:containerView name="StatusTable">
<jsp:include page="../disk/DiskTablePagelet.jsp"/>
</jato:containerView>



<br>

<spm:rbac auths="solaris.cluster.read">
<jato:containerView name="QuorumVoteView">
<jsp:include page="../quorum/QuorumDeviceVotePagelet.jsp"/>
</jato:containerView>
</spm:rbac>

<br>

  <cc:actiontable name="SNMPModulesTable"
                  bundleID="scBundle"
                  title="node.detail.snmp.table.title"
                  summary="node.detail.snmp.table.summary"
                  empty="node.detail.snmp.table.empty"
                  selectionType="none"
                  showAdvancedSortingIcon="false"
                  showLowerActions="false"
                  showPaginationControls="false"
                  showPaginationIcon="false"
                  showSelectionIcons="false"
                  showSortingRow="false"
                  page="1"/>

<br> 

<jato:containerView name="AdaptersStatusView">
<jsp:include page="../transport/AdaptersStatusPagelet.jsp"/>
</jato:containerView>

<br> 

<jato:content name= "Odyssey">
 <jato:containerView name="OdysseyContainer">
  <jsp:include page="../cluster/OdysseyClusterPagelet.jsp"/>
 </jato:containerView>
</jato:content>

<br>

</jato:form>
</spm:header>
</jato:useViewBean>
