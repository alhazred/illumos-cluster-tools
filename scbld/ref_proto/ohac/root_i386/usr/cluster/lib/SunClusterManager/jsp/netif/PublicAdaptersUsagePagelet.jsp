<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)PublicAdaptersUsagePagelet.jsp	1.4	08/05/20 SMI"
 */
--%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<jato:pagelet>

<script language="javascript">

  function PublicAdapterGraphWindow() {
    var windowFeatures = "<cc:text name='GraphWindowFeatureText'/>";
    var GraphWin = window.open(
      '../netif/PublicInterfaceUsageGraph?SelectedItems=' + getSelectedAdapters(),
      'PublicAdapterUsageGraphWindow',
      "'" + windowFeatures + "'");
    GraphWin.focus();
  }


  function getSelectedAdapters() {
    // Element name (prefix) of selection checkbox.
    var viewName = "<cc:text name='ViewNameText'/>";
    var checkboxName = viewName + ".Table.SelectionCheckbox";
    var graphAlertMessage = "<cc:text name='GraphAlertText'/>";

    // Document form.
    var form = document.scForm;

    // Set flag if a selection has been made.
    var retval = "";
    var n = 0;
    for (i = 0; i < form.elements.length; i++) {
      var e = form.elements[i];
      if (e.name.indexOf(checkboxName) != -1 && e.name.indexOf("jato_boolean") == -1) {
        if (e.checked) {
          var nameItem = e.name;
          var ItemNumIndex = nameItem.lastIndexOf("Checkbox")+8;
          var selectedRow = nameItem.substring(ItemNumIndex, nameItem.length);

          var item = viewName + ".TableTiledView[" + selectedRow +
            "].PublicAdapterHiddenName";
          retval += document.scForm.elements[item].value + ",";
          n = n + 1;

          if (n >= 6) {
            alert(graphAlertMessage);
            break;
          }
        }
      }
    }

    return retval.substring(0, retval.length-1); // remove the last "," and return
  }


  function toggleAdapterActionButtons() {

    var graphButton = "<cc:text name='GraphButtonNameText'/>";
    var checkboxName = "<cc:text name='ViewNameText'/>" + ".Table.SelectionCheckbox";

    // Document form.
    var form = document.scForm;

    // Flag indicating to disable action button and menu options.
    var disabled = true;

    // Set flag if a selection has been made.
    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];

        if (e.name.indexOf(checkboxName) != -1 && e.name.indexOf("jato_boolean") == -1) {
            if (e.checked) {
                disabled = false;
                break;
            }
        }
    }
    ccSetButtonDisabled(graphButton, "scForm", disabled);
  }

</script>

<!-- Action Table -->
<cc:actiontable name="Table"
  bundleID="scBundle"
  title="transport.TableTitle"
  summary="transport.usage.TableSummary"
  empty="transport.emptyTable"
  rowSelectionType="multiple"
  selectionJavascript="toggleAdapterActionButtons()"
  showAdvancedSortingIcon="false"
  showLowerActions="false"
  showPaginationControls="false"
  showPaginationIcon="false"
  showSelectionIcons="true"
  showSortingRow="true"
  page="1" />

</jato:pagelet>

