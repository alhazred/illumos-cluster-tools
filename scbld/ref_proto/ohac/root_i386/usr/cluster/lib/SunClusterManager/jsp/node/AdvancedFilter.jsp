<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)AdvancedFilter.jsp	1.5	08/05/20 SMI"
 */
--%>

 <%@ page language="java" %> 
 <%@ page import="com.iplanet.jato.view.ViewBean" %>
 <%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
 <%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

 <% String pageTitle = (request.getParameter("pageTitle") != null)
        ? request.getParameter("pageTitle") : ""; 

    String formName = (request.getParameter("formName") != null)
        ? request.getParameter("formName") : "";
%>

 <jato:pagelet>
 <cc:i18nbundle id="testBundle"
  baseName="com.sun.cluster.spm.resources.Resources" />

<SCRIPT LANGUAGE="JavaScript">

 var dateType;
 
 function checkKeyPressAndSubmit(e){
    
    if(window.event) // IE
    {
          keynum = e.keyCode
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
          keynum = e.which
    }
    if (keynum==13){
         submitValues();
         window.close();
    }
  }

 function displayWindow(type) {

   var name;

   dateType = type;
   if (type == 'start') {
	name = "<cc:text name='StartDateName'/>";
   } else {
        name = "<cc:text name='EndDateName'/>";
   }

   

   var value = document.testForm.elements[name].value; 

   var hostname = location.href.replace(/https:\/\//, '');
   hostname = hostname.slice(0, hostname.indexOf('/'));
   var win = window.open('https://' + hostname + '/SunClusterManager/node/SystemLogDate?type=' + type + '&date=' + value, 'DateWindow', 'height=400,width=600,top='+((screen.height-(screen.height/1.618))-(400/2))+',left='+((screen.width-600)/2)+',scrollbars=yes,resizable');
  win.focus();
}

function submitValues() {
  var tf = document.testForm;
  var pf = window.opener.document.<%=formName %>;

  var entryMenu = tf.elements["<cc:text name='EntryMenuName'/>"];
  var nbEntries = entryMenu.options[entryMenu.selectedIndex].value;

  var filterMenu = tf.elements["<cc:text name='FilterMenuName'/>"];
  var filterChoice = filterMenu.options[filterMenu.selectedIndex].value;

  var startDate = tf.elements["<cc:text name='StartDateName'/>"].value;
  var endDate = tf.elements["<cc:text name='EndDateName'/>"].value;
  var regExp = tf.elements["<cc:text name='RegExpName'/>"].value;

  var higherRadioButton = tf.elements["<cc:text name='RadioButtonName'/>"];

  var i = 0;
  while (!higherRadioButton[i].checked) {
    i++;
  }
  var higherValue = higherRadioButton[i].value;

  var url = window.opener.location.href;
  url=url.replace(/&start=.*&filter=.*&/g, "");
  url=url.replace(/&start=.*&filter=.*/g, "");	
  window.opener.location.href =  url +
	"&start=" + startDate +
        "&end=" + endDate +
        "&regexp=" + regExp +
        "&entries=" + nbEntries +
        "&higher=" + higherValue +
        "&filter=" + filterChoice ;
}

</SCRIPT>

 <jato:form name="testForm" method="post">
 <cc:pagetitle name="PageTitle" pageTitleText="<%=pageTitle %>"
  showPageButtonsTop="false" bundleID="testBundle">

 <!-- PropertySheet -->
 <cc:propertysheet 
  name="PropertySheet" 
  bundleID="testBundle" 
  showJumpLinks="false"/>


 </cc:pagetitle>
 </jato:form>
 </jato:pagelet>

 
