<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)AddHost.jsp	1.4	08/05/20 SMI"
 */
--%>

<%@page info="SNMP" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.snmp.AddHostViewBean">

<!-- Page header component -->
<spm:header pageTitle="snmp.title" copyrightYear="2005"
            baseName="com.sun.cluster.spm.resources.Resources"
            bundleID="scBundle">

<jato:form name="scForm" method="post" defaultCommandChild="AddButton">

<!-- Bread Crumb -->
<cc:breadcrumbs name="AddHostBreadCrumb" bundleID="scBundle" useGrayBg="true" />

<!-- Inline Error Alert -->
<spm:alertinline name="SelectionErrorAlert"  bundleID="scBundle" />


<cc:pagetitle name="GenericAddPageTitle" bundleID="scBundle"
              pageTitleText="snmp.host.pageTitle.title"
              showPageTitleSeparator="true"
	      showPageButtonsTop="false"
              pageTitleHelpMessage="snmp.host.pageTitle.help">

<cc:propertysheet name="PropertySheet" bundleID="scBundle"/>

</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>
