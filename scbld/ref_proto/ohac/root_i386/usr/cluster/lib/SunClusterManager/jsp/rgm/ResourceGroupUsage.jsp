<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceGroupUsage.jsp	1.6	08/07/14 SMI"
 */
--%>
<%@ page language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.rgm.ResourceGroupUsageViewBean">

<script language="javascript">

  function ResourceGroupNodeGraphWindow() {
    var windowFeatures = "<cc:text name='GraphWindowFeatureText'/>";
    var GraphWin = window.open(
      '../rgm/ResourceGroupUsageGraph?SelectedNodes=' + getSelectedNodes(),
      'ResourceGroupUsageGraphWindow',
      "'" + windowFeatures + "'");
    GraphWin.focus();
  }


  function getSelectedNodes() {
    // Element name (prefix) of selection checkbox.
    var elementName = "ResourceGroupUsage.ResourceGroupUsageTable.SelectionCheckbox";

    // Document form.
    var form = document.scForm;

    // Set flag if a selection has been made.
    var retval = "";
    for (i = 0; i < form.elements.length; i++) {
      var e = form.elements[i];
      if (e.name.indexOf(elementName) != -1 && e.name.indexOf("jato_boolean") == -1) {
        if (e.checked) {
          var nameItem = e.name;
          var ItemNumIndex = nameItem.lastIndexOf("Checkbox")+8;
          var selectedRow = nameItem.substring(ItemNumIndex, nameItem.length);

          var nodeComp = "ResourceGroupUsage.RGTableTiledView[" +
            selectedRow + "].ResourceGroupNodeHiddenName";
          retval += document.scForm.elements[nodeComp].value + ",";
        }
      }
    }
    
    return retval.substring(0, retval.length-1); // remove the last "," and return
  }


  function toggleActionButtons(table) {

    var graphButton = "ResourceGroupUsage.ResourceGroupNodeGraphButton";
    var elementName = "ResourceGroupUsage.ResourceGroupUsageTable.SelectionCheckbox";

    // Document form.
    var form = document.scForm;

    // Flag indicating to disable action button and menu options.
    var disabled = true;

    // Set flag if a selection has been made.
    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];

        if (e.name.indexOf(elementName) != -1 && e.name.indexOf("jato_boolean") == -1) {
            if (e.checked) {
                disabled = false;
                break;
            }
        }
    }
    ccSetButtonDisabled(graphButton, "scForm", disabled);
    //ccSetButtonDisabled(exportButton, "scForm", disabled);
  }

</script>

<!-- Page header component -->
<spm:header pageTitle="rgm.resourcegroup.title"
  copyrightYear="2006"
  baseName="com.sun.cluster.spm.resources.Resources"
  onLoad="reloadTree()"
  bundleID="scBundle"
  event="ESC_cluster_rg,ESC_cluster_slm_thr_state">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField" />

<!-- Bread Crumb component -->
<cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<spm:SLMcheck name="SLMCheck" summary="slmcheck.message.summary" 
  detail="slmcheck.message.detail">

<!-- Alert Inline for command result -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- Page Title -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
  pageTitleText="rgm.resourcegroup.usage.pageTitle"
  showPageTitleSeparator="true"
  pageTitleHelpMessage="rgm.resourcegroup.usage.help" />

<br>

<div class="ConMgn"> 
  <table border="0" cellpadding="0" cellspacing="0">
    <tr><td>
      <cc:button name="GraphButton" 
        bundleID="scBundle" 
        defaultValue="usage.graph.GraphButton.label"
        onClick="javascript: GraphWindow(); return false" />
    </td></tr>
  </table>
</div>

<div class="ConMgn"> 
  <table border="0" cellpadding="0" cellspacing="0">
    <!-- RG Name  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="NameLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.name"
                    styleLevel="2"
                    elementName="NameValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="NameValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>

    <!-- RG Type  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="TypeLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.type"
                    styleLevel="2"
                    elementName="TypeValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="TypeValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>

    <!-- RG Status  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="StatusLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.status"
                    styleLevel="2"
                    elementName="StatusValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:alarm name="StatusAlarm" bundleID="scBundle"/>
            <cc:text name="StatusValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>

    <!-- RG Current Primaries  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="CurrentPrimLabel" bundleID="scBundle"
                    defaultValue="rgm.resourcegroup.label.currentPrimary"
                    styleLevel="2"
                    elementName="CurrentPrimValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="CurrentPrimValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>
  </table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<cc:actiontable
  name="DetailMonitorTable"
  bundleID="scBundle"
  title="detail.monitor.table.title"
  summary="detail.monitor.table.summary"
  empty="detail.monitor.table.empty"
  rowSelectionType="none"
  showAdvancedSortingIcon="false"
  showLowerActions="false"
  showPaginationControls="false"
  showPaginationIcon="false"
  showSelectionIcons="false"
  showSortingRow="false"
  page="1"/>

<br>

<!-- Action Table -->
<cc:actiontable 
  name="ResourceGroupUsageTable"
  bundleID="scBundle"
  title="rgm.resourcegroup.usage.table.title"
  summary="rgm.resourcegroup.usage.table.summary"
  empty="rgm.resourcegroup.status.table.empty"
  rowSelectionType="multiple"
  selectionJavascript="toggleActionButtons('ResourceGroupNode')"
  showAdvancedSortingIcon="false"
  showLowerActions="false"
  showPaginationControls="false"
  showPaginationIcon="false"
  showSelectionIcons="true"
  showSortingRow="false"
  page="1" />

<br>

</spm:SLMcheck>
</jato:form>
</spm:header>
</jato:useViewBean>

