<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DiskDetailUsage.jsp	1.3	08/05/20 SMI"
 */
--%>

<%@page info="AdaptersStatus" language="java" %>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.disk.DiskDetailUsageViewBean">

<!-- Page header component -->
<spm:header
  pageTitle="disk.usage.pageTitle"
  copyrightYear="2006"
  baseName="com.sun.cluster.spm.resources.Resources"
  bundleID="scBundle"
  event="ESC_cluster_tp_if_state_change,ESC_cluster_slm_thr_state"
  onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Bread Crumb component -->
<cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />

<spm:SLMcheck name="SLMCheck" summary="slmcheck.message.summary"
  detail="slmcheck.message.detail">

<!-- Command Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- PageTitle -->
<cc:pagetitle
  name="GenericPageTitle" bundleID="scBundle"
  pageTitleText="disk.usage.pageTitle"
  showPageTitleSeparator="true"
  pageTitleHelpMessage="disk.usage.help" />

<br>

<div class="ConMgn">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr><td>
      <cc:button name="GraphButton"
        bundleID="scBundle"
        defaultValue="usage.graph.GraphButton.label"
        onClick="javascript: GraphWindow(); return false" />
    </td></tr>
  </table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

<cc:actiontable
  name="DetailMonitorTable"
  bundleID="scBundle"
  title="detail.monitor.table.title"
  summary="detail.monitor.table.summary"
  empty="detail.monitor.table.empty"
  rowSelectionType="none"
  showAdvancedSortingIcon="false"
  showLowerActions="false"
  showPaginationControls="false"
  showPaginationIcon="false"
  showSelectionIcons="false"
  showSortingRow="false"
  page="1"/>

</spm:SLMcheck>
</jato:form>
</spm:header>
</jato:useViewBean>

