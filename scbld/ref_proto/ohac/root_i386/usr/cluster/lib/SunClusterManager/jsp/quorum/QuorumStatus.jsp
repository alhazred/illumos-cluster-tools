<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)QuorumStatus.jsp	1.10	08/05/20 SMI"
 */
--%>

<%@page info="Quorum" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.quorum.QuorumStatusViewBean">

<!-- Page header component -->
<spm:header pageTitle="quorum.title" copyrightYear="2004"
            baseName="com.sun.cluster.spm.resources.Resources"
            bundleID="scBundle"
            event="ESC_cluster_quorum,ESC_cluster_node_config_change,ESC_cluster_cmm_reconfig"
            onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<SCRIPT LANGUAGE="JavaScript">
 function displayConfirmation() {
  return confirm('<cc:text name="StaticText"
                           defaultValue="selectionConfirmation"
                           bundleID="scBundle"/>');
 }
</SCRIPT>

<!-- Status Alert -->
<spm:alertinline name="StatusAlert" bundleID="scBundle" />

<!-- Command Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="quorum.status.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="quorum.status.pageTitle.help"/>

<!-- Buttons -->
<spm:rbac auths="solaris.cluster.modify">
<BR>
<div class="ConMgn"> 
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    
    <jato:content name="InstallMode">
      <td>
        <cc:button name="ResetButton" bundleID="scBundle"
                   defaultValue="quorum.status.button.reset"
                   onClick="return displayConfirmation()"/>
      </td>
    </jato:content>
  </tr>
</table>
</div>

</spm:rbac>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>


<jato:containerView name="QuorumDeviceVoteView">
  <jsp:include page="QuorumDeviceVotePagelet.jsp"/>
</jato:containerView>

<BR>

<spm:rbac auths="solaris.cluster.read">
  <cc:actiontable name="NodeTable"
                  bundleID="scBundle"
                  title="quorum.status.node.table.title"
                  summary="quorum.status.node.table.summary"
                  empty="quorum.status.node.table.empty"
                  rowSelectionType="none"
                  showAdvancedSortingIcon="false"
                  showLowerActions="false"
                  showPaginationControls="false"
                  showPaginationIcon="false"
                  showSelectionIcons="false"
                  showSortingRow="false"
                  page="1"/>
</spm:rbac>

<BR>
<spm:rbac auths="solaris.cluster.read">
<jato:containerView name="QuorumSummaryVoteView">
  <jsp:include page="QuorumSummaryVotePagelet.jsp"/>
</jato:containerView>
</spm:rbac>

</jato:form>
</spm:header>
</jato:useViewBean>
