<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DeviceGroupSwitchPrimaries.jsp	1.4	08/05/20 SMI"
 */
--%>

<%@page info="DeviceGroupSwitchPrimaries" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.devicegroup.DeviceGroupSwitchPrimariesViewBean">

<!-- Page header component -->
<spm:header pageTitle="devicegroup.title" 
            copyrightYear="2003" 
            baseName="com.sun.cluster.spm.resources.Resources" 
            bundleID="scBundle">

<jato:form name="scForm" method="post">

<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericAddPageTitle" bundleID="scBundle"
              pageTitleText="devicegroup.switch.pageTitle.title"
              showPageTitleSeparator="true"
              showPageButtonsTop="false"
              pageTitleHelpMessage="devicegroup.switch.pageTitle.help">

<!-- Set the layout style -->
<div class="content-layout">

<!-- Alert Warning on the action-->
<spm:alertinline name="Warning" bundleID="scBundle" />

<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top">
      <div class="ConTblCl1Div">&nbsp;
        <cc:label name="CurrentPrimaryLabel" bundleID="scBundle"
                  defaultValue="devicegroup.switch.label.currentPrimary"
                  styleLevel="2"
                  elementName="CurrentPrimaryValue" />
      </div>
    </td>
    <td valign="top">
      <div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
          <cc:text name="CurrentPrimaryValue" bundleID="scBundle"/>
        </span>
      </div>
    </td>
  </tr>

  <tr>
    <td valign="top">
      <div class="ConTblCl1Div">&nbsp;
        <cc:label name="NewPrimaryLabel" bundleID="scBundle"
                  defaultValue="devicegroup.switch.label.newPrimary"
                  styleLevel="2"
                  elementName="NewPrimaryValue" />
      </div>
    </td>
    <td valign="top">
      <div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
          <cc:dropdownmenu name="NewPrimaryValue" bundleID="scBundle"
                           type="jump"/>
        </span>
      </div>
    </td>
  </tr>

</table>

</div>
</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>

