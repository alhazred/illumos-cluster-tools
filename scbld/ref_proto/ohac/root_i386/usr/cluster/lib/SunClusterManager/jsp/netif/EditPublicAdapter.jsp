<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)EditPublicAdapter.jsp	1.6	08/05/20 SMI"
 */
--%>

<%@page info="EditPublicAdapter" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.netif.EditPublicAdapterViewBean">

<!-- Page header component -->
<spm:header 	pageTitle="netif.publicAdapter.edit.title" 
		copyrightYear="2004" 
		baseName="com.sun.cluster.spm.resources.Resources" 
		bundleID="scBundle">

<jato:form name="scForm" method="post" defaultCommandChild="AddButton">

<script language="JavaScript">
function changeTestAddressState() {
  var radio = document.scForm.elements['EditPublicAdapter.IPVersionName'];
  var i = 0;
  while (!radio[i].checked) {
    i++;
  }
  var radiovalue = radio[i].value;
  var text = document.scForm.elements['EditPublicAdapter.TestAddressName'];
  if ((radiovalue == 'IPv4 only') ||
      (radiovalue == 'IPv4 and IPv6')) {
     text.disabled = false;
  } else if (radiovalue == 'IPv6 only') {
     text.disabled = true;
  }
}
</script>

<!-- Inline Error Alert -->
<spm:alertinline name="SelectionErrorAlert"  bundleID="scBundle" />

<!-- Command Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- PageTitle -->
<cc:pagetitle name="GenericAddPageTitle" bundleID="scBundle"
 pageTitleText="netif.publicAdapter.edit.title" 
 showPageTitleSeparator="true"
 showPageButtonsTop="false"
 pageTitleHelpMessage="netif.publicAdapter.edit.help">

<!-- PropertySheet -->
<cc:propertysheet name="PropertySheet" bundleID="scBundle"/>

</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>
