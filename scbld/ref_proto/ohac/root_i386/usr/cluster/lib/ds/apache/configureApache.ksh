#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)configureApache.ksh	1.7	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# Configure apache on the node 
#
#####################################################
typeset -r FUNC=${1}

############################################################
# Tar the document root and copy under temp directory
############################################################
copyDocumentRoot() {
	typeset -r dirToCD=${1}
	typeset -r dirToTar=${2}	
	cd ${dirToCD} 
	/usr/bin/tar cf /opt/cluster/lib/ds/history/tmp_docroot.tar ${dirToTar}
}

############################################################
# Copy edited httpd.conf and document root to the HA location
#############################################################
copyConfiguration() {

    typeset -r WIZ_NODE=${1}
    typeset APACHE_MOUNT=${2}
    typeset -r OLD_CONF_FILE_NAME=${3}
    typeset -r DOCROOT=${4}
    typeset -r LOC=${5}
    typeset -r APACHE_RES=${6}
    typeset -r MKDIR=/usr/bin/mkdir
    typeset -r CP=/usr/bin/cp	
    typeset -r SCRCONF=/usr/cluster/lib/sc/scrconf
    typeset -r DSTEMP=/opt/cluster/lib/ds/history
    typeset -r SCHA_CLUSTER_GET=/usr/cluster/bin/scha_cluster_get 

    # Copy configuration files from the wizard node to this node
    thisnode=`hostname`
    if [ ${thisnode} != ${WIZ_NODE} ]; then
	/usr/bin/rm -f ${DSTEMP}/tmp_apachectl
        /usr/bin/rm -f ${DSTEMP}/tmp_docroot.tar
        /usr/bin/rm -f ${DSTEMP}/tmp_httpd.conf 

        priv_hostname_wiz_node=`${SCHA_CLUSTER_GET} -O PRIVATELINK_HOSTNAME_NODE ${WIZ_NODE}` 
        ${SCRCONF} -g -N ${priv_hostname_wiz_node} ${DSTEMP}/tmp_apachectl ${DSTEMP}/tmp_apachectl
        ${SCRCONF} -g -N ${priv_hostname_wiz_node} ${DSTEMP}/tmp_docroot.tar ${DSTEMP}/tmp_docroot.tar
        ${SCRCONF} -g -N ${priv_hostname_wiz_node} ${DSTEMP}/tmp_httpd.conf ${DSTEMP}/tmp_httpd.conf
    fi

    # Configuration Files are copied under <APACHE_MOUNT>/<APACHE_RES> directory 

    APACHE_MOUNT="${APACHE_MOUNT}""/""${APACHE_RES}"

    if [ ! -d ${APACHE_MOUNT} ]; then
        ${MKDIR} -p ${APACHE_MOUNT}
    fi

    # copy edited control file under HAMountPoint/bin
    if [ ! -d ${APACHE_MOUNT}/bin ]; then
        ${MKDIR} -p ${APACHE_MOUNT}/bin
    fi
    ${CP} ${DSTEMP}/tmp_apachectl ${APACHE_MOUNT}/bin/apachectl
    /usr/bin/chmod 755 ${APACHE_MOUNT}/bin/apachectl

    # Copy edited conf file from node to the HA Mount Point
    if [ ! -d ${APACHE_MOUNT}/etc/apache ]; then
    	${MKDIR} -p ${APACHE_MOUNT}/etc/apache
    fi
    ${CP} ${DSTEMP}/tmp_httpd.conf ${APACHE_MOUNT}/etc/apache/${OLD_CONF_FILE_NAME} 

    # Copy Document root 
    if [ "${LOC}" != "/" ]; then
    	# cp docroot from the node to the HA mount point
    	if [ ! -d ${LOC}${DOCROOT} ]; then
        	${MKDIR} -p ${LOC}${DOCROOT}
	fi
        cd ${LOC}${DOCROOT}/..
        /usr/bin/tar xf ${DSTEMP}/tmp_docroot.tar 
    fi
}

if [ "${FUNC}" = "copyConfiguration" ]; then
	copyConfiguration $2 $3 $4 $5 $6 $7
else
	copyDocumentRoot $2 $3
fi
