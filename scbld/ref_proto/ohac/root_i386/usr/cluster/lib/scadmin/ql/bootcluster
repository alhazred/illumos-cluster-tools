#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)bootcluster.sh	1.4	08/05/20 SMI"
#
# bootcluster.sh
#
#
# Startup script for clusters during QL.
#
# This is a modified version of bootcluster. The original version of
# bootcluster is located at usr/src/cmd/initpkg/init.d/bootcluster
#
# This modified version will be used only during QL in order to
# prevent the partition B nodes from booting up as a part of cluster.
#


#
# Do not edit this comment below. It is used to uniquely identify this file.
#

# Temporary Disable Bootcluster for Quantum Leap

# Check if SMF is present
INCLUDE_FILE="/lib/svc/share/smf_include.sh"
if [ -f $INCLUDE_FILE ]
then
	#
	# Prevent this script from running from within a non-global zone.
	#

	#
	# Check if executing from non-global zone. Executing from a non-global
	# zone is not allowed for this command.
	#
	if [ -f /usr/bin/zonename ]
	then
		zone_name=`/usr/bin/zonename`

		if [ "${zone_name}" != "global" ]
		then
			echo "Command not supported from non-global zone."
			exit 1
		fi
	fi
	
	#
	# Save a copy of file descriptors to the
	# Greenline logs
	#
	exec 4>&1 5>&2
else
	exec 4>/dev/null 5>&4
fi

# Set up stdout and stderr to write to the console
exec 1>/dev/msglog 2>&1

# binaries core handling
SCCOREDIR=/var/tmp/SUNWscu/core
LIBSCDIR=/usr/cluster/lib/sc

if [ ! -d ${SCCOREDIR} ]
then
	mkdir -p ${SCCOREDIR}
fi
/usr/bin/coreadm -p ${SCCOREDIR}/core.%n.%f.%p.%t $$

CORES_FILES=`ls ${SCCOREDIR}`

if [ "${CORES_FILES}" != "" ]
then
	echo "Core files in "${SCCOREDIR}": "${CORES_FILES}

	if [ -x ${LIBSCDIR}/scds_syslog ]
	then
		${LIBSCDIR}/scds_syslog -p notice -t "Cluster.Upgrade" -m \
		    "Core files in ${SCCOREDIR}: ${CORES_FILES}"
	fi
fi

QL_MESSAGE="Dual Partition Software Swap is in progress."
HALT_MESSAGE="Please reboot in non cluster mode(boot -x)"

#
# Test if we are booting as part of a cluster.
#
/usr/sbin/clinfo > /dev/null 2>&1
if [ $? != 0 ] ; then
	echo "Not booting as part of a cluster"
else
	# SCMSGS
	# @explanation
	# Dual-partition cluster upgrade is in progress. Until the upgrade is
	# completed, you can boot the node only into noncluster mode.
	# @user_action
	# Boot the node into noncluster mode (boot -x). Then continue with the
	# upgrade.
	${LIBSCDIR}/scds_syslog -p error -t "Cluster.Upgrade" -m \
	    "Dual-partition upgrade state prevents booting into cluster mode."
	echo $QL_MESSAGE
	echo $HALT_MESSAGE
	/usr/sbin/halt

fi

#
# Init DID again now that we've got a current
# copy of the CCR.
#
if [ -c /dev/did/admin -a -x /usr/cluster/bin/scdidadm ]
then
	/usr/cluster/bin/scdidadm -u -i 2>&1 | tee /dev/msglog 1>&4
fi
