#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident   "@(#)update_quorum_disks.ksh	1.7     08/05/20 SMI"
#
# Script used to update the default fencing protocols for quorum disks after
# the scsi3 default fencing feature has been enabled.
#

SCSTAT=/usr/cluster/bin/scstat
SCCONF=/usr/cluster/bin/scconf
SCDIDADM=/usr/cluster/bin/scdidadm
ZONESCHECK=/usr/cluster/lib/sc/sc_zonescheck

# typesets
typeset answer=		# holds answer from call to get_y_or_n
typeset -i numOnlineNodes=
typeset thiscommand=

# functions

function get_y_or_n {
	typeset -l input=
	while :; do
		printf "$(gettext 'Warning: Please make sure that you are running this command from one cluster node ONLY. Execution of this command can result in loss of cluster nodes or the entire cluster, are you sure you want to continue? [y/n] ')" 
		read input
		case $input in
			[yY]|[yY][eE][sS])	answer="y"
						return
						;;
			[nN]|[nN][oO])		answer="n"
						return
						;;
			*)		print "\nPlease answer 'yes' or 'no'.\n"
					;;
		esac
	done
}

###############
#
# Main function
#
###############

thiscommand=$0

# Warning message. 
get_y_or_n
if [[ $answer = "n" ]]; then
	exit 0
fi

# Verify that the current number of online nodes is at least 2.
numOnlineNodes=$($SCSTAT -n | grep -c Online)
if [[ $? != 0 ]]; then
	printf "$(gettext 'Failed to get the number of online nodes. Exit...')\n"
	exit 1
fi

if (( $numOnlineNodes < 2 ))
then
	printf "$(gettext '%s: Only one node online. Requires at least TWO nodes be online. Exit...')\n" $thiscommand
	exit 1
fi

# Check if we are running in global zone. 
$ZONESCHECK
if [[ $? != 0 ]]; then
	exit 1
fi

trap "/bin/rm -f /tmp/quorumdata.$$" EXIT

# Update Quorum disks. 
$SCSTAT -q >/tmp/quorumdata.$$
if [[ $? != 0 ]]; then
	printf "$(gettext 'Failed to get Quorum device information. Exit...')\n"
	exit 1
fi

grep "Device votes" /tmp/quorumdata.$$ > /dev/null 2>&1
if [[ $? != 0 ]]; then
	printf "$(gettext 'No Quorum device configured. Done.')\n"
	exit 0
fi

# Go through all existing quorum devices and update them one by one.
# The precedure of each update is as follows:
# 1. Remove the quorum device.
# 2. set device's default fencing protocol to the default value. (When the
#    scsi3 default fencing feature is enabled, it sets all the instances that
#    are configured as quorum devices with two paths to SCSI2.)
# 3. Add the quorum device back.
grep "Device votes" /tmp/quorumdata.$$ | while read line
do
	set ${line}
	# Get the global did name of the quorum device
	QuorumDevice=`echo ${3} | sed -e 's/s[0-9]$//' -e 's,^/dev/did/rdsk/,,'`
	printf "$(gettext 'Removing Quorum Device %s...')\n" $QuroumDevice
	$SCCONF -rq globaldev=$QuorumDevice
	if [[ $? != 0 ]]; then
		printf "$(gettext 'Failed to remove $QuorumDevice ... skip.')\n"
	else
		# Set the default fencing to default
		printf "$(gettext 'Updating the default fencing protocol for %s ...')\n" $QuorumDevice
		$SCDIDADM -F useglobal $QuorumDevice
		if [[ $? != 0 ]]; then
			printf "$(gettext 'Failed to update %s default fencing protocol')\n" $QuorumDevice
		fi
		printf "$(gettext 'Adding Back Quorum Device %s ...')\n" $QuorumDevice
		$SCCONF -aq globaldev=$QuorumDevice
		if [[ $? != 0 ]]; then
			printf "$(gettext 'Failed to add back quorum device %s. Please fix the error and add $QuorumDevice manually.')\n" $QuorumDevice
		fi
	fi
done

printf "$(gettext 'Done!')\n" 
