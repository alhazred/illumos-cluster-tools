/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_ERR_PRIV_H_
#define	_SCHA_ERR_PRIV_H_

#pragma ident	"@(#)scha_priv.h	1.22	08/07/28 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <scha_err.h>

typedef struct scha_errmsg {
	scha_err_t	err_code;
	char		*err_msg;
} scha_errmsg_t;

/*
 * The public SCHA_ERR_* error codes start with 0 and (are expected to)
 * go upto 100 (there are only 18 of them right now).
 * The private SCHA_ERR_* error codes start with 101 and (are expected to)
 * go upto 200.
 * The SCHA_ERR_* warning codes start with 201 and go upto 255.
 *
 * NOTE: It appears that most shells use a short int to store the exit
 * codes of the commands that they launch. Consequently, any exit code
 * greater than 255 will be truncated with bizzare results. Therefore,
 * please make sure that none of the error codes in this file exceed 255.
 *
 */

/*
 * !!! IMPORTANT NOTE - begin !!!
 * !!! IMPORTANT NOTE - begin !!!
 *
 * Some of the following exit codes must always be synchronized
 * with the codes defined in scinstall.
 *
 * !!! IMPORTANT NOTE - end   !!!
 * !!! IMPORTANT NOTE - end   !!!
 */

#define	SCHA_ERR_ORB  		101	/* ORB problems */
#define	SCHA_ERR_CCR  		102	/* read/write to CCR failed */
#define	SCHA_ERR_AUTH  		103	/* RPC authentication failed */
#define	SCHA_ERR_RGRECONF	104	/* one or more RGs are reconfiguring */
#define	SCHA_ERR_CLRECONF  	105	/* cluster is reconfiguring */
/* SCHA_ERR_FILE is also used in scinstall please synchrnise */
#define	SCHA_ERR_FILE  		106	/* file or dir name not exist */
					/* can't create */
#define	SCHA_ERR_DUPNODE  	107	/* duplicate node name in args */
#define	SCHA_ERR_MEMBER  	108	/* node not a cluster member */
#define	SCHA_ERR_RGSTATE  	109	/* bad RG state */
#define	SCHA_ERR_DETACHED  	110	/* RG contains DETACHED resource(s) */
#define	SCHA_ERR_RSTATE  	111	/* bad R state */
#define	SCHA_ERR_UPDATE  	112	/* update non-updatable R/RG/RT */
					/* property */
#define	SCHA_ERR_VALIDATE  	113	/* VALIDATE method failed */
#define	SCHA_ERR_MEMBERCHG  	114	/* cluster membership changed */
#define	SCHA_ERR_INSTLMODE  	115	/* cluster is in install mode */
#define	SCHA_ERR_METHODNAME  	116	/* invalid method name */
					/* during call */
#define	SCHA_ERR_STOPFAILED  	117	/* RG in ERROR_STOP_FAILED state */
					/* requires operator attention */
#define	SCHA_ERR_NOMASTER  	118	/* can't find healthy potential */
					/* master of RG */
#define	SCHA_ERR_TIMESTAMP  	119	/* can't create/access timestamp */
					/* file for R */
#define	SCHA_ERR_USAGE  	120	/* command usage error */
#define	SCHA_ERR_DUPP		121	/* duplicate extension property in */
					/* RT registration file */
#define	SCHA_ERR_PINT		122	/* integer is out of range */
#define	SCHA_ERR_PBOOL  	123	/* invalid boolean value */
#define	SCHA_ERR_PENUM  	124	/* invalid enum value */
#define	SCHA_ERR_PSTR  		125	/* invalid string length of property */
#define	SCHA_ERR_PSARRAY  	126	/* invalid number of elements of */
					/* string array type */
#define	SCHA_ERR_STARTFAILED	127	/* RG failed to start on chosen node */
#define	SCHA_ERR_DELETE  	128	/* R exists can't delete RG or RT */
#define	SCHA_ERR_SWRECONF  	129	/* switchover was interrupted by a */
					/* node death */
#define	SCHA_ERR_FINIMETHOD  	130	/* R disabled but not deleted */
					/* because all FINI methods have */
					/* not yet run */
#define	SCHA_ERR_NODEFAULT  	132	/* property that does not have */
					/* default values must be set */
					/* when the resource is created */

#define	SCHA_ERR_PINGPONG	133	/* pingpong failure */
#define	SCHA_ERR_INVALID_REMOTE_AFF 134 /*  invalid affinity */


#define	SCHA_ERR_RS_VALIDATE  	201
					/*
					 * Warning: some node which should
					 * run validate method isn't inside of
					 * the current cluster membership.
					 */

#define	RGM_RECEP_FILE		"/var/cluster/rgm/recep_file"
#define	RGM_RECEP_FILE_TMP	"/var/cluster/rgm/recep_file.tmp"
#define	SCHA_IC_RG_AFFINITY_SOURCES	"IC_RG_affinity_sources"

#ifdef __cplusplus
}
#endif

#endif	/* _SCHA_ERR_PRIV_H_ */
