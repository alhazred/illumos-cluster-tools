/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_EVENTDEFS_H
#define	_CL_EVENTDEFS_H

#pragma ident	"@(#)cl_eventdefs.h	1.33	08/08/01 SMI"

/*
 * cl_eventdefs.h - declarations/definitions for generating cluster
 *		    events using the sysevent API.
 *
 * ATTENTION - THE DEFINITIONS IN THIS FILE MUST BE IN SYNC WITH
 *  usr/src/lib/cmass/agent_clusterevent/
 *    com/sun/cluster/agent/event/ClEventDefs.java
 *
 * IF YOU EXTEND THE DEFINITIONS IN THIS FILE PLEASE ALSO EXTEND THE JAVA
 * DEFINITIONS
 */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Event class reserved for use by Sun Cluster components.
 * This definition MUST BE in sync with the definition in
 * /usr/include/sys/sysevent/eventdefs.h (PSARC 2001/722).
 *
 */

#ifndef EC_CLUSTER
#define	EC_CLUSTER "EC_Cluster"
#endif

/*
 * Required cluster event attribute names.
 *	Each cluster event is required to provide the following
 *	set of attributes:
 *
 *	- name of the cluster where event took place
 *	- ID of the cluster where event took place
 *	- Nodename of the cluster where event took place
 *	- Event date
 *	- Event time
 *	- Event initiator
 *	- Event severity
 *
 */

#define	CL_CLUSTER_NAME "ev_cluster_name"
#define	CL_CLUSTER_ID "ev_cluster_id"
#define	CL_EVENT_NODE "ev_node_name"
#define	CL_EVENT_TS_SEC "ev_ts_sec"
#define	CL_EVENT_TS_USEC "ev_ts_usec"
#define	CL_EVENT_INITIATOR "ev_initiator"
#define	CL_EVENT_SEVERITY "ev_severity"
#define	CL_EVENT_BROADCAST "ev_broadcast"

/* Cluster event version */

#define	CL_EVENT_VERSION "ev_version"
#define	CL_EVENT_VERSION_NUMBER 2

/* Cluster event publishers */

#define	CL_EVENT_PUB_RGM "rgm"
#define	CL_EVENT_PUB_PMF "pmf"
#define	CL_EVENT_PUB_CMM "cmm"
#define	CL_EVENT_PUB_NET "net"
#define	CL_EVENT_PUB_DCS "dcs"
#define	CL_EVENT_PUB_DPM "dpm"
#define	CL_EVENT_PUB_TP  "tp"
#define	CL_EVENT_PUB_UCMM "ucmm"
#define	CL_EVENT_PUB_GDS "gds"
#define	CL_EVENT_PUB_CMD_START "cmd_start"
#define	CL_EVENT_PUB_CMD_END "cmd_end"
#define	CL_EVENT_PUB_ZONES "sc_zones"
#define	CL_EVENT_PUB_SLM "slm"
#define	CL_EVENT_PUB_FMM "fmm"
#define	CL_EVENT_PUB_HBR "hbr"
#define	CL_EVENT_PUB_CMD "cmd"

/*
 * Configuration actions for static configuration
 * change events.
 *
 */

#define	CL_CONFIG_ACTION "config_action"

typedef enum cl_event_config_action_enum {
	CL_EVENT_CONFIG_ADDED = 0,
	CL_EVENT_CONFIG_REMOVED,
	CL_EVENT_CONFIG_ENABLED,
	CL_EVENT_CONFIG_DISABLED,
	CL_EVENT_CONFIG_MON_ENABLED,
	CL_EVENT_CONFIG_MON_DISABLED,
	CL_EVENT_CONFIG_MANAGED,
	CL_EVENT_CONFIG_UNMANAGED,
	CL_EVENT_CONFIG_PROP_CHANGED,
	CL_EVENT_CONFIG_VOTE_CHANGE,
	CL_EVENT_CONFIG_SUSPENDED,
	CL_EVENT_CONFIG_RESUMED
} cl_event_config_action_t;

/*
 * The following is a set of general purpose event attribute
 * names that should be used for publication of cluster events.
 * Before adding new names to the list, make sure a suitable
 * name is not already on the list.
 *
 */

#define	CL_NODE_NAME "node_name"
#define	CL_RT_NAME "rt_name"
#define	CL_RG_NAME "rg_name"
#define	CL_R_NAME "r_name"
#define	CL_QUORUM_NAME "quorum_name"
#define	CL_METHOD_NAME "method_name"
#define	CL_TP_PATH_NAME "tp_path_name"
#define	CL_TP_IF_NAME "tp_if_name"
#define	CL_AFFINITY_RG_NAME "affinity_rg_name"
#define	CL_DS_NAME "ds_name"
#define	CL_DS_SERVICE_CLASS "ds_service_class"

#define	CL_NODE_LIST "node_list"
#define	CL_NODE_STATE_LIST "state_list"
#define	CL_FROM_NODE_LIST "from_node_list"
#define	CL_TO_NODE_LIST "to_node_list"

#define	CL_OLD_STATE "old_state"
#define	CL_NEW_STATE "new_state"
#define	CL_OLD_STATUS "old_status"
#define	CL_NEW_STATUS "new_status"
#define	CL_STATUS_MSG "status_msg"
#define	CL_EVENT_CONTROL_MSG "event_control_msg"

#define	CL_STEP_NAME "step_name"
#define	CL_SUBSTEP_NAME "substep_name"
#define	CL_START_TIME "start_time"
#define	CL_DURATION "duration"
#define	CL_TIMESTAMP "timestamp"

#define	CL_METHOD_DURATION "method_duration"
#define	CL_METHOD_PATH "method_path"
#define	CL_METHOD_TIMEOUT "method_timeout"

#define	CL_SCHA_API_OPTAG "scha_api_optag"
#define	CL_SCHA_API_FUNC "scha_api_func"

#define	CL_PMF_NAME_TAG "pmf_name_tag"
#define	CL_PMF_CMD_PATH "pmf_cmd_path"
#define	CL_TOTAL_ATTEMPTS "total_attempts"
#define	CL_ATTEMPT_NUMBER "attempt_number"
#define	CL_CMD_PATH "cmd_path"
#define	CL_RETRY_NUMBER "retry_number"
#define	CL_RETRY_COUNT "retry_count"
#define	CL_VOTE_COUNT "vote_count"
#define	CL_DESIRED_PRIMARIES "desired_primaries"

#define	CL_DISK_PATH "disk_path"

#define	CL_RESULT_CODE "result_code"

#define	CL_PROPERTY_NAME "property_name"
#define	CL_PROPERTY_VALUE "property_value"

#define	CL_EVENT_CMD_UID "cmd_uid"
#define	CL_EVENT_CMD_EUID "cmd_euid"
#define	CL_EVENT_CMD_LOGIN "cmd_login"
#define	CL_EVENT_CMD_TTY_STDIN "cmd_tty_stdin"
#define	CL_EVENT_CMD_TTY_STDOUT "cmd_tty_stdout"
#define	CL_EVENT_CMD_PID "cmd_pid"
#define	CL_EVENT_CMD_PPID "cmd_ppid"
#define	CL_EVENT_CMD_NAME "cmd_name"
#define	CL_EVENT_CMD_OPT "cmd_opt"
#define	CL_EVENT_CMD_RES "cmd_res"

#define	CL_EVENT_ZONE_STATE "zone_state"
#define	CL_EVENT_ZONE_NAME "zone_name"

#define	CL_DIVIDEND "value_dividend"
#define	CL_DIVISOR "value_divisor"
#define	CL_OBJECT_TYPE "object_type"
#define	CL_OBJECT_INSTANCE "object_instance"
#define	CL_TELEMETRY_ATTRIBUTE "telemetry_attribute"
#define	CL_DIRECTION "direction"
#define	CL_CCR_TABLE_NAME "ccr_table_name"

#define	CL_GLOBAL_CLUSTER_NAME "global"
#define	CL_ZC_NAME "zonecluster_name"

typedef  enum cl_event_result_enum {
	CL_RC_OK = 0,
	CL_RC_FAILED,
	CL_RC_TIMEOUT,
	CL_RC_RECONFIG
} cl_event_result_t;

/*
 * Reasons why a given event was generated.
 * Reason codes are used in addition to the event
 * subclass to determine the cause of complex
 * events.
 *
 */

#define	CL_REASON_CODE "reason_code"

typedef enum cl_event_reason_code_enum {
	CL_REASON_RG_SWITCHOVER = 0,
	CL_REASON_RG_RESTART,
	CL_REASON_RG_GIVEOVER,
	CL_REASON_RG_GETOFF,
	CL_REASON_RG_SC_RESTART,
	CL_REASON_RG_NODE_DIED,
	CL_REASON_RG_REBALANCE,
	CL_REASON_RG_FAILBACK,
	CL_REASON_RG_AFFINITY,
	CL_REASON_R_START_FAILED,
	CL_REASON_CMM_NODE_JOINED,
	CL_REASON_CMM_NODE_LEFT,
	CL_REASON_CMM_NODE_SHUTDOWN,
	CL_REASON_PMF_ACTION_SUCCESS,
	CL_REASON_PMF_RESTART,
	CL_REASON_DS_STARTUP,
	CL_REASON_DS_SWITCHOVER,
	CL_REASON_DS_FAILOVER,
	CL_REASON_UNKNOWN,
	CL_REASON_FARM_NODE_JOINED,
	CL_REASON_FARM_NODE_LEFT,
	CL_REASON_FARM_NODE_MONITORED,
	CL_REASON_FARM_NODE_UNMONITORED,
	CL_REASON_HBR_LINK_DOWN,
	CL_REASON_HBR_LINK_UP,
	CL_REASON_ZC_CONFIGURED,
	CL_REASON_ZC_CONFIGURE_FAILED,
	CL_REASON_ZC_INSTALLED,
	CL_REASON_ZC_INSTALL_FAILED,
	CL_REASON_ZC_UNINSTALLED,
	CL_REASON_ZC_UNINSTALL_FAILED,
	CL_REASON_ZC_DELETED,
	CL_REASON_ZC_DELETE_FAILED
} cl_event_reason_code_t;


/*
 * Failure reasons for specific types of failure
 * events.
 *
 */
#define	CL_FAILURE_REASON "failure_reason"

typedef enum cl_event_fail_code_enum {
	CL_FR_NO_MASTER = 0,
	CL_FR_PINGPONG,
	CL_FR_MON_CHECK_FAILED,
	CL_FR_BUSY,
	CL_FR_FROZEN,
	CL_FR_PMF_ACTION_FAILED,
	CL_FR_PMF_PROC_THROTTLED,
	CL_FR_PMF_RETRIES_EXCEEDED
} cl_event_fail_code_t;

typedef enum cl_event_scha_func_enum {
	CL_SCHA_API_QUERY = 0,
	CL_SCHA_API_SET_STATUS,
	CL_SCHA_API_SCHA_CONTROL
} cl_event_scha_func_t;

/*
 *  Different states that device services could be in
 *  when an event is generated.
 */
#define	CL_DS_STARTING "ds_starting"
#define	CL_DS_STARTED "ds_started"
#define	CL_DS_START_FAILED "ds_start_failed"
#define	CL_DS_STOPPING "ds_stopping"
#define	CL_DS_STOPPED "ds_stopped"
#define	CL_DS_STOP_FAILED "ds_stop_failed"
#define	CL_DS_MAINT_MODE_STARTING "ds_mainenance_mode_starting"
#define	CL_DS_MAINT_MODE "ds_maintenance_mode"
#define	CL_DS_MAINT_MODE_FAILED "ds_maintenance_mode_failed"
#define	CL_DS_SWITCHOVER_STARTED "ds_switchover_started"
#define	CL_DS_SWITCHOVER_ENDED "ds_switchover_ended"
#define	CL_DS_SWITCHOVER_FAILED "ds_switchover_failed"
#define	CL_DS_FAILOVER_STARTED "ds_failover_started"
#define	CL_DS_FAILOVER_ENDED "ds_failover_ended"
#define	CL_DS_FAILOVER_FAILED "ds_failover_failed"
#define	CL_DS_BECOME_PRIMARY_STARTED "ds_become_primary_started"
#define	CL_DS_BECOME_PRIMARY_ENDED "ds_become_primary_ended"
#define	CL_DS_BECOME_PRIMARY_FAILED "s_become_primary_failed"
#define	CL_DS_BECOME_SECONDARY_STARTED "ds_become_secondary_started"
#define	CL_DS_BECOME_SECONDARY_ENDED "ds_become_secondary_ended"
#define	CL_DS_BECOME_SECONDARY_FAILED "ds_become_secondary_failed"

/*
 * The following is a list of Sun Cluster 3.x event subclasses
 * that are generated by cluster components.
 *
 */

/* Generic Events */

#define	ESC_CLUSTER_GENERIC_EVENT "ESC_cluster_generic_event"
#define	ESC_CLUSTER_CONFIG_CHANGE "ESC_cluster_cofig_change"
#define	ESC_CLUSTER_STATE_CHANGE "ESC_cluster_state_change"
#define	ESC_CLUSTER_CMD "ESC_cluster_cmd"
#define	ESC_CLUSTER_ZC_STATE_CHANGE "ESC_zonecluster_state_change"

/* CMM/UCMM/Quorum Events */

#define	ESC_CLUSTER_NODE_CONFIG_CHANGE "ESC_cluster_node_config_change"
#define	ESC_CLUSTER_NODE_STATE_CHANGE "ESC_cluster_node_state_change"

#define	ESC_CLUSTER_CMM_RECONFIG "ESC_cluster_cmm_reconfig"
#define	ESC_CLUSTER_UCMM_RECONFIG "ESC_cluster_ucmm_reconfig"
#define	ESC_CLUSTER_UCMM_RECONFIG_SUBSTEP "ESC_cluster_ucmm_reconfig_substep"

#define	ESC_CLUSTER_QUORUM_CONFIG_CHANGE "ESC_cluster_quorum_config_change"
#define	ESC_CLUSTER_QUORUM_STATE_CHANGE "ESC_cluster_quorum_state_change"

/* FMM Events */
#define	ESC_CLUSTER_FARM_NODE_STATE_CHANGE "ESC_cluster_farm_node_state_change"
#define	ESC_CLUSTER_HBR_LINK_STATE_CHANGE "ESC_cluster_hbr_link_state_change"

/* RGM Events */

#define	ESC_CLUSTER_MEMBERSHIP "ESC_cluster_membership"

#define	ESC_CLUSTER_RG_STATE "ESC_cluster_rg_state"
#define	ESC_CLUSTER_RG_PRIMARIES_CHANGING "ESC_cluster_rg_primaries_changing"
#define	ESC_CLUSTER_RG_REMAINING_OFFLINE "ESC_cluster_rg_remaining_offline"
#define	ESC_CLUSTER_RG_GIVEOVER_DEFERRED "ESC_cluster_rg_giveover_deferred"
#define	ESC_CLUSTER_RG_NODE_REBOOTED "ESC_cluster_rg_node_rebooted"
#define	ESC_CLUSTER_RG_CONFIG_CHANGE "ESC_cluster_rg_config_change"

#define	ESC_CLUSTER_R_STATE "ESC_cluster_r_state"
#define	ESC_CLUSTER_R_METHOD_COMPLETED "ESC_cluster_r_method_completed"
#define	ESC_CLUSTER_R_CONFIG_CHANGE "ESC_cluster_r_config_change"

#define	ESC_CLUSTER_FM_R_STATUS_CHANGE "ESC_cluster_fm_r_status_change"
#define	ESC_CLUSTER_FM_R_RESTARTING "ESC_cluster_fm_r_restarting"

#define	ESC_CLUSTER_SCHA_API_INVALID "ESC_cluster_scha_api_invalid"

/* PMF Events */

#define	ESC_CLUSTER_PMF_PROC_RESTART "ESC_cluster_pmf_proc_restart"
#define	ESC_CLUSTER_PMF_PROC_NOT_RESTARTED "ESC_cluster_pmf_proc_not_restarted"

/* SC_ZONES Events */
#define	ESC_CLUSTER_ZONES_STATE "ESC_cluster_zones_state"

/* SC_SLM Events */
#define	ESC_CLUSTER_SLM_THR_STATE "ESC_cluster_slm_thr_state"
#define	ESC_CLUSTER_SLM_CONFIG_CHANGE "ESC_cluster_slm_config_change"

/* Transport Events */

#define	ESC_CLUSTER_TP_PATH_CONFIG_CHANGE "ESC_cluster_tp_path_config_change"
#define	ESC_CLUSTER_TP_IF_CONFIG_CHANGE "ESC_cluster_tp_path_config_change"
#define	ESC_CLUSTER_TP_PATH_STATE_CHANGE "ESC_cluster_tp_path_state_change"
#define	ESC_CLUSTER_TP_IF_STATE_CHANGE "ESC_cluster_tp_if_state_change"

#define	ESC_CLUSTER_IPMP_GROUP_STATE "ESC_cluster_ipmp_group_state"
#define	ESC_CLUSTER_IPMP_GROUP_CHANGE "ESC_cluster_ipmp_group_change"
#define	ESC_CLUSTER_IPMP_GROUP_MEMBER_CHANGE \
			"ESC_cluster_ipmp_group_member_change"
#define	ESC_CLUSTER_IPMP_IF_CHANGE "ESC_cluster_ipmp_if_change"

/* DCS Events */

#define	ESC_CLUSTER_DCS_CONFIG_CHANGE "ESC_cluster_dcs_config_change"
#define	ESC_CLUSTER_DCS_STATE_CHANGE "ESC_cluster_dcs_state_change"
#define	ESC_CLUSTER_DCS_PRIMARIES_CHANGING "ESC_cluster_dcs_primaries_changing"

/* Disk Path Monitoring events */

#define	ESC_CLUSTER_DPM_DISK_PATH_OK	 "ESC_cluster_dpm_disk_path_ok"
#define	ESC_CLUSTER_DPM_DISK_PATH_FAILED "ESC_cluster_dpm_disk_path_failed"
#define	ESC_CLUSTER_DPM_DISK_PATH_MONITORED \
	    "ESC_cluster_dpm_disk_path_monitored"
#define	ESC_CLUSTER_DPM_DISK_PATH_UNMONITORED \
	    "ESC_cluster_dpm_disk_path_unmonitored"
#define	ESC_CLUSTER_DPM_AUTO_REBOOT_NODE "ESC_cluster_dpm_auto_reboot_node"

/* GDS - problem isolation events */

#define	ESC_CLUSTER_GDS_START	"ESC_cluster_gds_start"
#define	ESC_CLUSTER_GDS_STOP	"ESC_cluster_gds_stop"
#define	ESC_CLUSTER_GDS_PROBE	"ESC_cluster_gds_probe"
#define	ESC_CLUSTER_GDS_OTHER	"ESC_cluster_gds_other"

#define	IPMP_GROUP_NAME "group_name"
#define	IPMP_GROUP_STATE "group_state"
#define	IPMP_GROUP_OPERATION "group_operation"
#define	IPMP_IF_NAME "if_name"
#define	IPMP_IF_STATE "if_state"

#define	IPMP_GROUP_ADD CL_EVENT_CONFIG_ADDED
#define	IPMP_GROUP_REMOVE CL_EVENT_CONFIG_REMOVED
#define	IPMP_IF_ADD CL_EVENT_CONFIG_ADDED
#define	IPMP_IF_REMOVE CL_EVENT_CONFIG_REMOVED

typedef enum cl_event_ipmp_group_state_enum {
	IPMP_GROUP_OK = 0,
	IPMP_GROUP_FAILED
} cl_event_ipmp_group_state_t;

typedef enum cl_event_ipmp_if_state_enum {
	IPMP_IF_OK = 0,
	IPMP_IF_FAILED
} cl_event_ipmp_if_state_t;

/* Cluster event severities */

typedef enum cl_event_severity_enum {
	CL_EVENT_SEV_INFO = 0,
	CL_EVENT_SEV_WARNING,
	CL_EVENT_SEV_ERROR,
	CL_EVENT_SEV_CRITICAL,
	CL_EVENT_SEV_FATAL
} cl_event_severity_t;

/* Cluster event initiator codes */

typedef enum cl_event_initiator_enum {
	CL_EVENT_INIT_UNKNOWN = 0,
	CL_EVENT_INIT_SYSTEM,
	CL_EVENT_INIT_OPERATOR,
	CL_EVENT_INIT_AGENT
} cl_event_initiator_t;

typedef enum cl_event_error_enum {
	CL_EVENT_ENOMEM = 0,
	CL_EVENT_ECLADM,
	CL_EVENT_EATTR,
	CL_EVENT_EINVAL,
	CL_EVENT_EPOST
} cl_event_error_t;


#ifdef __cplusplus
}
#endif

#endif	/* _CL_EVENTDEFS_H */
