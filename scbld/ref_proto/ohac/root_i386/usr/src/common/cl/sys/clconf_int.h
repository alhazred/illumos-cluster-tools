/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLCONF_INT_H
#define	_CLCONF_INT_H

#pragma ident	"@(#)clconf_int.h	1.115	08/06/20 SMI"

#include <sys/types.h>
#include <sys/clconf.h>
#include <sys/quorum_int.h>
#include <netinet/in.h>
#include <sys/sc_syslog_msg.h>
#include <sys/cl_ipspace.h>
#include <sys/sol_version.h>

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	UNODE
#else
#undef	UNODE
#endif

/*
 * This header file specifies the data structures and functions to access
 * the configuration database.
 */

/*
 * The CCR name of the private net number
 */
#define	CCRENT_PRIVATE_NET_NUMBER	"cluster.properties.private_net_number"
#define	CCRENT_PRIVATE_NETMASK	"cluster.properties.private_netmask"
#define	CCRENT_PRIVATE_SUBNET_NETMASK \
	"cluster.properties.private_subnet_netmask"

/*
 * Define the maximum nodeid allowed in the cluster.
 */
#define	NODEID_MAX	64

/* The max length of a field or a key */
#define	CL_MAX_LEN	256

/* A guaranteed invalid clconf object id */
#define	CLCONF_ID_UNKNOWN	0

/* The error codes, will be expanded when we discover more error types */
typedef enum {
	CL_NOERROR,		/* call actually worked! */
	CL_BUF_TOO_SMALL,	/* The message buf is too small */
	CL_BADNAME,		/* name contains illegal char '.'  or */
				/* creates conflicts */
	CL_INVALID_ID,		/* The id conflicts with others */
	CL_PARENT_DISABLED,	/* cannot enable child of disabled parent */
	CL_NO_NODES,		/* cluster doesn't have any nodes */
	CL_INVALID_OBJ,		/* The clconf_object has invalid values */
	CL_INVALID_TREE,	/* The clconf_cluster isn't configured right */
	CL_NODE_ISOLATED,	/* A tree which will result in node isolation */
	CL_TREE_OUTDATED,	/* A tree believed to be current is not */
	CL_NO_CLUSTER,		/* Not booted as a cluster */

	/* The following are used by the quorum code */

	CL_QUORUM_CONFIG_MULTIPLE_CHANGES,
	CL_QUORUM_CONFIG_LARGE_CHANGE,		/* at least one >1 change */
	CL_QUORUM_CONFIG_WILL_LOSE_QUORUM,	/* change will result in */
						/* cluster losing quorum */
	CL_QUORUM_CONFIG_ILLEGAL,		/* illegal nid or qid */
	CL_QD_SCRUB_FAILED,			/* quorum device scrubbing */
						/* had errors. scrubbing */
						/* attempts to remove */
						/* registration and */
						/* reservation keys from */
						/* the device */
	CL_QD_REMOVE_FAILED,			/* disk removal had errors */
	/* Generic zone error code for supporting zones */
#if (SOL_VERSION >= __s10)
	CL_ZONE_ERROR,				/* Failed to retrieve zone */
						/* information. */
#endif
	/* The following are used by the transport code */
	CL_INVALID_CABLE_ENDPOINT,		/* invalid cable endpoint */

	/* The following are used by the scsymon code */
	CL_ALREADY_REGISTERED,			/* callback exists */
	CL_NULL_CALLBACK,			/* register callback is null */
	CL_NOT_REGISTERED,			/* no callback registered */
	CL_CCR_ERROR,				/* ccr-error encountered */
	CL_MEMALLOC_FAIL,			/* memory allocation failed */
	CL_QD_OPEN_VALIDATION_FAIL,		/* quorum_open on quorum */
						/* device failed */
	CL_QD_READ_RES_VALIDATION_FAIL,		/* quorum_read_reservations */
						/* on quorum device failed */
	CL_QD_READ_KEYS_VALIDATION_FAIL,	/* quorum_read_keys on */
						/* quorum device failed */
	CL_QD_NOT_REGISTERED,			/* quorum device not */
						/* registered */
	CL_NO_DEV_REGISTRY,			/* unable to get device type */
						/* registry reference */
	CL_EXEC_ERROR				/* Failed to execute a task */
						/* using the remote command */
						/* execution infrastructure. */

} clconf_errnum_t;

/*
 * The object types.
 */
typedef enum {
	CL_UNKNOWN_OBJ,
	CL_CLUSTER,
	CL_NODE,
	CL_ADAPTER,
	CL_PORT,
	CL_BLACKBOX,
	CL_CABLE,
	CL_QUORUM_DEVICE,
	CL_PATH
} clconf_objtype_t;

typedef enum {
	CL_LOCAL = 0,
	CL_REMOTE
} clconf_endtype_t;

/*
 * Types of updates that can happen to the CCR.
 */
typedef enum {
	CCR_RECOVERED,
	CCR_TABLE_ADDED,
	CCR_TABLE_REMOVED,
	CCR_TABLE_MODIFIED,
	CCR_INV_UPDATE
} clconf_ccr_update_t;

/*
 * Adapter properties
 */
#define	AP_NONE		0x0
#define	AP_IP_ADDRESS	0x1
#define	AP_NETMASK	0x2
#define	AP_ADAPTER_ID	0x4
#define	AP_BANDWIDTH	0x8
#define	AP_NW_BANDWIDTH	0x10
#define	AP_HBT_TIMEOUT	0x20
#define	AP_HBT_QUANTUM	0x40
#define	AP_PORTS	0x80

/*
 * The function type for callbacks registered for ccr changes.
 */
typedef void (*clconf_ccr_callback_handler) \
	(const char *, const char *, clconf_ccr_update_t);

/*
 * Iterator. A helper object. It can't be typecasted into any of the objects
 * below.
 */
typedef struct clconf_iter clconf_iter_t;

/*
 * A static iterator. The size of it should be >= the size of the real
 * iterator, IntrList::ListIterator. Note that the members in the struct
 * should NEVER be used directly. You should use it the following way:
 * clconf_siter_t siter;
 * clconf_iter_t *iter = clconf_siter_init(&siter, ...);
 * and use iter from now on.
 */
typedef struct clconf_siter {
	void *ptr;
} clconf_siter_t;

/*
 * The basic clconf object.
 * All the specific objects' pointer can be typecasted into "clconf_obj_t *".
 * A "clconf_obj_t *" can be typecasted into a specific object
 * pointer below. (e.g. "clconf_cluster_t *").
 */
typedef struct clconf_obj clconf_obj_t;

/*
 * The cluster object.
 */
typedef struct clconf_cluster clconf_cluster_t;

/*
 * The cluster node object.
 */
typedef struct clconf_node clconf_node_t;

/*
 * The cluster adapter object.
 */
typedef struct clconf_adapter clconf_adapter_t;

/*
 * The cluster port object.
 */
typedef struct clconf_port clconf_port_t;

/*
 * The black box object.
 */
typedef struct clconf_bb clconf_bb_t;

/*
 * The cluster cable object.
 */
typedef struct clconf_cable clconf_cable_t;

/* The quorum device object */
typedef struct clconf_quorum clconf_quorum_t;

/* The path object */
typedef struct clconf_path clconf_path_t;

#ifndef _KERNEL_ORB
/* initialize the ORB and reads clconf from CCR, returns 0 on success */
int clconf_lib_init(void);

/* shutdown the ORB */
void clconf_lib_shutdown(void);
#else
#ifndef _KERNEL
int clconf_lib_init(void);
#endif
#endif

/* Get the local nodeid */
nodeid_t clconf_get_local_nodeid(void);

clconf_path_t *clconf_get_path(clconf_port_t *, clconf_port_t *);

/* Get cluster name */
char *clconf_get_clustername();

/* Get local node reservation key */
int clconf_get_local_key(uint64_t*);

/* Get nodename for the given nodeid */
void clconf_get_nodename(nodeid_t nodeid, char *nodename);

#ifndef DEBUGGER_PRINT
/* Get the quorum info from clconf. */
int clconf_get_quorum_table(clconf_cluster_t *, quorum_table_t **);

/* Release the quorum table allocated through clconf_get_quorum_table(). */
void clconf_release_quorum_table(quorum_table_t *);

/* Get the quorum status from the quorum algorithm. */
int cluster_get_quorum_status(quorum_status_t **);

/* Release the struct allocated through cluster_get_quorum_status(). */
void cluster_release_quorum_status(quorum_status_t *);

/* Scrub the quorum device in question. */
clconf_errnum_t clconf_quorum_device_scrub(char *, nodeid_t, char *,
    clconf_obj_t *);

/* Remove a quorum device. */
clconf_errnum_t clconf_quorum_remove(char *, nodeid_t, char *,
    clconf_obj_t *);

/*
 * ***********************************************************************
 * Interfaces with the CCR callback interfaces.
 * ***********************************************************************
 */

/*
 * clconf_register_ccr_callback()
 *
 * Register a callback fn with the CCR.
 * A single registration is supported per address space,
 * subsequent registrations will return CL_ALREADY_REGISTERED.
 * This function will be called whenever there is any change
 * to the CCR.
 */
clconf_errnum_t clconf_register_ccr_callback(clconf_ccr_callback_handler);

/*
 * clconf_unregister_ccr_callback()
 *
 * Unregister a previously registered callback fn with the CCR.
 * Will return CL_NOT_REGISTERED if register has not been
 * called or has been called with a NULL fn.
 */
clconf_errnum_t clconf_unregister_ccr_callback(void);
#endif // DEBUGGER_PRINT

/*
 * ***********************************************************************
 * Interfaces with the iterator.
 * ***********************************************************************
 */

/*
 * Note that unless you use a static iterator, you should ALWAYS call
 * clconf_iter_release to free up the memory after you are done.
 */

/* Advance the iterator */
void		clconf_iter_advance(clconf_iter_t *);

/* Get the current element, returns NULL when there is no more elements */
clconf_obj_t	*clconf_iter_get_current(clconf_iter_t *);

/* Get the number of elements left in the iterator */
int		clconf_iter_get_count(clconf_iter_t *);

/* Release the iterator */
void		clconf_iter_release(clconf_iter_t *);


/*
 * ***********************************************************************
 * Interfaces with the static iterator.
 * ***********************************************************************
 */

/*
 * initialize the static interator to iterate through the specified children
 * of obj. Returns a clconf_iter_t* that is ready to use. Note that
 * you SHOULD NOT to call clconf_iter_release after you are done with the
 * iterator in this case.
 */
clconf_iter_t *clconf_siter_init(clconf_siter_t *, clconf_obj_t *obj,
    clconf_objtype_t childtype);

/*
 * ***********************************************************************
 * Interfaces with a basic clconf object.
 * ***********************************************************************
 */

/*
 * Get the object's id as string. Every object has an id that distinguish
 * it from its siblings of the same type. It returns NULL if the id is not set.
 */
const char	*clconf_obj_get_idstring(clconf_obj_t *);

/*
 * Get the object's id. Every object has an id that distinguish it from
 * its siblings of the same type. It returns -1 if the id is not set.
 */
int		clconf_obj_get_id(clconf_obj_t *);

/*
 * Set the object's id. Returns CL_INVALID_ID if the id is conflict with others
 */
clconf_errnum_t	clconf_obj_set_id(clconf_obj_t *, int id);

/*
 * Get the object's type.
 */
clconf_objtype_t clconf_obj_get_objtype(clconf_obj_t *);

/*
 * Get the object's state.
 */
const char	*clconf_obj_get_state(clconf_obj_t *);

/*
 * Enable the clconf object. It doesn't do anything to its subcomponents.
 */
clconf_errnum_t	clconf_obj_enable(clconf_obj_t *);

/*
 * Disable the clconf object and all its subcomponents.
 */
clconf_errnum_t	clconf_obj_disable(clconf_obj_t *);

/*
 * Whether this object is enabled. Returns 1 if true 0 if false. The default
 * is enabled.
 */
int		clconf_obj_enabled(clconf_obj_t *);

/*
 * Get the child with the specified type and id. Returns NULL if not found.
 */
clconf_obj_t	*clconf_obj_get_child(clconf_obj_t *, clconf_objtype_t,
    int id);

/*
 * Get the parent. Returns NULL if used on a clconf_cluster.
 */
clconf_obj_t	*clconf_obj_get_parent(clconf_obj_t *);

/*
 * Get the specified property. Returns NULL if no such property.
 */
const char	*clconf_obj_get_property(clconf_obj_t *,
    const char *property_name);

/*
 * Returns the properties this object has. Returns NULL if it doesn't have
 * any properties.
 */
clconf_iter_t *clconf_obj_get_properties(clconf_obj_t *);

/*
 * Set the specified property. Create the property if it doesn't exist.
 * If property_value is NULL we will delete the property.
 * Returns CL_BADNAME if the property name is invalid.
 */
clconf_errnum_t	clconf_obj_set_property(clconf_obj_t *,
    const char *property_name, const char *property_value);

/*
 * Returns name of object. Returns NULL if name was never set.
 */
const char	*clconf_obj_get_name(clconf_obj_t *);

/*
 * Set the name of the object.
 */
void		clconf_obj_set_name(clconf_obj_t *, const char *name);

/*
 * Returns the internal name of object. The internal name is the key string
 * used in CCR to identify this object. For example, "cluster.nodes.1"
 * is the internal name for node 1.
 */
char	*clconf_obj_get_int_name(clconf_obj_t *);

/*
 * Releases a string referenced by name.  The name is typically
 * the one obtained through clconf_obj_get_int_name() but can also be
 * any memory allocated with new.
 */
void    clconf_free_string(char *name);

/*
 * checks the object to make sure it's properly filled out,
 * but doesn't check relationships to other objects. Returns
 * CL_INVALID_OBJ if the check fails and returns the detailed
 * info in the errorbuff.
 */
clconf_errnum_t	clconf_obj_check(clconf_obj_t *, char *errorbuff,
    uint_t maxlen);

/*
 * Every clconf tree has a hold count. The tree will be deleted only when the
 * hold count reaches zero.
 */

/*
 * Obtain a hold on the tree. This garantees the tree doesn't go away.
 * When the receiver is done, it should do a release.
 */
void		clconf_obj_hold(clconf_obj_t *);

/*
 * Release the hold we have on the object.
 */
void		clconf_obj_release(clconf_obj_t *);

/*
 * deep copy an existing object and return with hold count = 1
 */
clconf_obj_t	*clconf_obj_deep_copy(clconf_obj_t *);

/*
 * delete the object along with all its children.
 */
void		clconf_obj_deep_delete(clconf_obj_t *);

/*
 * Returns the incarnation number for the tree this object is in.
 */
uint_t	clconf_obj_get_incarnation(clconf_obj_t *);


/*
 * ***********************************************************************
 * Interfaces with a cluster object.
 * ***********************************************************************
 */

/*
 * Get the current cluster object.
 */
clconf_cluster_t	*clconf_cluster_get_current(void);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
/*
 * Get the cluster object for a virtual cluster.
 * Can be used in global zone only.
 * Returns NULL if called in non-global zone.
 */
clconf_cluster_t	*clconf_cluster_get_vc_current(const char *);
#endif

/*
 * Get the old cluster object.
 */
clconf_cluster_t	*clconf_cluster_get_old(void);

/*
 * Set the old root to the root, to be used only before the shutdown.
 */
void			clconf_cluster_set_oldroot_to_root(void);

/*
 * Create an empty cluster object.
 */
clconf_cluster_t	*clconf_cluster_create(void);

uint_t			clconf_get_pcode(const char *);

/*
 * Determines if transition is feasible. Returns an error code indicating
 * why the transition is not feasible and the detailed info in errorbuf.
 * The error codes are:
 * CL_BUF_TOO_SMALL, the errorbuf is not big enough to contain the message.
 * CL_INVALID_OBJ, the proposed clconf_cluster contains invalid objects.
 * CL_INVALID_TREE, the proposed cluster is not valid.
 * XXX to be filled in.
 */
clconf_errnum_t		clconf_cluster_check_transition(
    clconf_cluster_t *existing,
    clconf_cluster_t *proposed,
    char *errorbuf,
    uint_t maxlen,
    boolean_t *reconf_needed);

/*
 * Commit and swap. Replace the current clconf tree with the proposed one
 * and update CCR. Returns:
 * CL_BUF_TOO_SMALL, the errorbuf is not big enough to contain the message.
 * CL_TREE_OUTDATED, the clconf_cluster pointed by existing is no longer
 *     current. This means the changes proposed are based on outdated
 *     information. The caller should sync up with the current clconf_cluster
 *     and redo the changes.
 * CL_INVALID_TREE, the proposed cluster is not valid.
 * XXX to be filled in.
 */
clconf_errnum_t		clconf_cluster_commit(clconf_cluster_t *proposed,
    char *errorbuf, uint_t maxlen);

#ifndef _KERNEL
int clconf_modify_ip(char *filename, char *netnum, char *c_netmask,
    char *netmask, char *subnet_netmask, char *maxnodes,
    char *maxprivnets);
clconf_errnum_t		clconf_cluster_write_nonclust_ccr(
		clconf_cluster_t *proposed);
#endif

/* Get the iterators */
clconf_iter_t		*clconf_cluster_get_nodes(clconf_cluster_t *);
clconf_iter_t		*clconf_cluster_get_cables(clconf_cluster_t *);
clconf_iter_t		*clconf_cluster_get_bbs(clconf_cluster_t *);
clconf_iter_t		*clconf_cluster_get_quorum_devices(clconf_cluster_t *);

/*
 * Add components to a cluster.
 * Returns CL_BADNAME if the component added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t		clconf_cluster_add_node(clconf_cluster_t *,
    clconf_node_t *);
clconf_errnum_t		clconf_cluster_add_cable(clconf_cluster_t *,
    clconf_cable_t *);
clconf_errnum_t		clconf_cluster_add_bb(clconf_cluster_t *,
    clconf_bb_t *);
clconf_errnum_t		clconf_cluster_add_quorum_device(clconf_cluster_t *,
    clconf_quorum_t *);

/*
 * Remove components from a cluster.
 * CL_INVALID_TREE if the component to be removed is not found in the cluster.
 */
clconf_errnum_t		clconf_cluster_remove_node(clconf_cluster_t *,
    clconf_node_t *);
clconf_errnum_t		clconf_cluster_remove_cable(clconf_cluster_t *,
    clconf_cable_t *);
clconf_errnum_t		clconf_cluster_remove_bb(clconf_cluster_t *,
    clconf_bb_t *);
clconf_errnum_t		clconf_cluster_remove_quorum_device(clconf_cluster_t *,
    clconf_quorum_t *);

/* The following maps to what we have today */
int clconf_cluster_get_msgretries(clconf_cluster_t *);
int clconf_cluster_get_orb_return_timeout(clconf_cluster_t *);
int clconf_cluster_get_orb_stop_timeout(clconf_cluster_t *);
int clconf_cluster_get_orb_abort_timeout(clconf_cluster_t *);
int clconf_cluster_get_orb_step_timeout(clconf_cluster_t *);
int clconf_cluster_get_failfast_grace_time(clconf_cluster_t *);
int clconf_cluster_get_failfast_panic_delay(clconf_cluster_t *);
int clconf_cluster_get_failfast_proxy_panic_delay(clconf_cluster_t *);
int clconf_cluster_get_node_fenceoff_timeout(clconf_cluster_t *);
int clconf_cluster_get_boot_delay(clconf_cluster_t *);
int clconf_cluster_get_node_halt_timeout(clconf_cluster_t *);
const char *clconf_cluster_get_failfast_mode(clconf_cluster_t *);
const char *clconf_cluster_get_nodename_by_nodeid(clconf_cluster_t *, nodeid_t);
nodeid_t clconf_cluster_get_nodeid_by_nodename(char *nodename);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
unsigned int clconf_cluster_get_num_clusters();
char **clconf_cluster_get_cluster_names();
nodeid_t clconf_zc_get_nodeid_by_nodename(char * zcname, char *nodename);
#endif

/* Get node id given the ip address. */
nodeid_t clconf_cluster_get_nodeid_for_ipaddr(nodeid_t *, int *, in_addr_t);

/* Get node id given the private hostname ip address */
nodeid_t clconf_cluster_get_nodeid_for_private_ipaddr(in_addr_t ipaddr);

/* Execute the specified program on the given node. */
clconf_errnum_t clconf_do_execution(const char *, nodeid_t, boolean_t *,
    boolean_t, boolean_t, boolean_t);

/* Request the Cluster Membership Monitor to run a reconfiguration. */
void clconf_cluster_reconfigure(void);

/* Add a path */
void clconf_cluster_add_path(clconf_path_t *);

/* Remove a path */
void clconf_cluster_remove_path(clconf_path_t *);

/* Get a path */
clconf_path_t *clconf_cluster_get_path(int local_adapterid,
    nodeid_t remote_nodeid, int remote_adapterid);


/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 */


/*
 * ***********************************************************************
 * Interfaces with a quorum_device object
 * ***********************************************************************
 */

/* Create a quorum device */
clconf_quorum_t *clconf_quorum_device_create(void);

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 * vote: the vote a quorum device have.
 * path_i where i is the node id: the path from this quorum device to node i.
 */

/*
 * ***********************************************************************
 * Interfaces with a node object.
 * ***********************************************************************
 */

/*
 * Create a cluster node object.
 */
clconf_node_t		*clconf_node_create(void);

/* Get the adapter iterator */
clconf_iter_t		*clconf_node_get_adapters(clconf_node_t *);

/*
 * Add an adapter to a node.
 * Returns CL_BADNAME if the adapter added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t		clconf_node_add_adapter(clconf_node_t *,
    clconf_adapter_t *);

/*
 * Remove an adapter from a node.
 * CL_INVALID_TREE if the adapter to be removed is not found in the node.
 */
clconf_errnum_t		clconf_node_remove_adapter(clconf_node_t *,
    clconf_adapter_t *);

/*
 * Get the private hostname of this node. The default would be
 * "<nodename>-priv"
 */
const char		*clconf_node_get_private_hostname(clconf_node_t *);

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 * quorum_vote: the vote the node has.
 * quorum_resv_key: the key this node uses when it reserves the quorum.
 */



/*
 * ***********************************************************************
 * Interfaces with an adapter object.
 * ***********************************************************************
 */

/*
 * Create a cluster adapter object.
 */
clconf_adapter_t	*clconf_adapter_create(void);

/* Get the adapter properties */
const char 	*clconf_adapter_get_device_name(clconf_adapter_t *);
const char 	*clconf_adapter_get_dlpi_device_name(clconf_adapter_t *);
int 		clconf_adapter_get_device_instance(clconf_adapter_t *);
int 		clconf_adapter_get_vlan_id(clconf_adapter_t *);
const char	*clconf_adapter_get_vlan_device_name(clconf_adapter_t *);

/* Set the properties */
void 		clconf_adapter_set_device_name(clconf_adapter_t *,
    const char *name);
void 		clconf_adapter_set_device_instance(clconf_adapter_t *,
    int instance_num);


/* Get the port iterator */
clconf_iter_t	*clconf_adapter_get_ports(clconf_adapter_t *);

/*
 * Add a port to an adapter.
 * Returns CL_BADNAME if the port added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t		clconf_adapter_add_port(clconf_adapter_t *,
    clconf_port_t *);

/*
 * Remove a port from an adapter. Returns
 * CL_INVALID_TREE if the port to be removed is not found in the adapter.
 */
clconf_errnum_t		clconf_adapter_remove_port(clconf_adapter_t *,
    clconf_port_t *);

/*
 * Get/set the transport type.
 */
const char		*clconf_adapter_get_transport_type(clconf_adapter_t *);
void			clconf_adapter_set_transport_type(clconf_adapter_t *,
    const char *);

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 */

/*
 * ***********************************************************************
 * Interfaces with an port object.
 * ***********************************************************************
 */

/*
 * Create a cluster port object.
 */
clconf_port_t	*clconf_port_create(void);

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 */


/*
 * ***********************************************************************
 * Interfaces with an black box object.
 * ***********************************************************************
 */

/*
 * Create a cluster bb object.
 */
clconf_bb_t	*clconf_bb_create(void);

/*
 * Set and get type.
 */
void		clconf_bb_set_type(clconf_bb_t *, const char *);
const char	*clconf_bb_get_type(clconf_bb_t *);

void		clconf_bb_set_transport_type(clconf_bb_t *, const char *);
const char	*clconf_bb_get_transport_type(clconf_bb_t *);

void		clconf_bb_set_vlan_id(clconf_bb_t *, const int);
const int	clconf_bb_get_vlan_id(clconf_bb_t *);

/* Get the port iterator */
clconf_iter_t	*clconf_bb_get_ports(clconf_bb_t *);

/*
 * Add a port to a black box.
 * Returns CL_BADNAME if the port added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t	clconf_bb_add_port(clconf_bb_t *, clconf_port_t *);

/*
 * Remove a port from the black box. Returns
 * CL_INVALID_TREE if the port to be removed is not found in the bb.
 */
clconf_errnum_t		clconf_bb_remove_port(clconf_bb_t *,
    clconf_port_t *);

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 * product_name: it tells you whether it a ethernet hub or sci switch. It
 *     also tell you what vendor it's from.
 */


/*
 * ***********************************************************************
 * Interfaces with an cable object.
 * ***********************************************************************
 */

/*
 * Create a cluster cable object.
 */
clconf_cable_t	*clconf_cable_create(void);

/*
 * Get and set port. End number can be 1 or 2.
 * If the endnum is neither 1 nor 2, get returns NULL and set returns
 * CL_BADNAME.
 */
clconf_port_t	*clconf_cable_get_endpoint(clconf_cable_t *, int endnum);
clconf_errnum_t	clconf_cable_set_endpoint(clconf_cable_t *, int endnum,
    clconf_port_t *);

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 */

/*
 * ***********************************************************************
 * Interfaces with a path object.
 * ***********************************************************************
 */
/* Creates a path with the two ports as specified. */
clconf_path_t	*clconf_path_create(clconf_port_t *local,
    clconf_port_t *remote, uint_t quantum, uint_t timeout);

/* Duplicate the path */
clconf_path_t	*clconf_path_dup(clconf_path_t *);

/* Release the path */
void		clconf_path_release(clconf_path_t *);

/* Set the quantum in milliseconds */
void		clconf_path_set_quantum(clconf_path_t *, uint_t);

/* Set the timeout in milliseconds */
void		clconf_path_set_timeout(clconf_path_t *, uint_t);

/* Get the quantum in milliseconds */
uint_t		clconf_path_get_quantum(clconf_path_t *);

/* Get the timeout in milliseconds */
uint_t		clconf_path_get_timeout(clconf_path_t *);

/* Get the adapter id for the specified end */
int		clconf_path_get_adapterid(clconf_path_t *, clconf_endtype_t);

/* Get the port id for the specified end */
int		clconf_path_get_portid(clconf_path_t *, clconf_endtype_t);

/* Deletes all the paths */
void		clconf_path_remove_all(void);

/* Get the remote nodeid for the path, a helper function for transport code */
nodeid_t	clconf_path_get_remote_nodeid(clconf_path_t *);

/*
 * Gets the clconf object for the remote adapter of the path from the
 * specified clconf tree.
 */
clconf_adapter_t *clconf_path_get_adapter(clconf_path_t *, clconf_endtype_t,
    clconf_cluster_t *cl);

/*
 * Gets the specified endpoint of the path from the specified clconf tree.
 */
clconf_port_t	*clconf_path_get_endpoint(clconf_path_t *, clconf_endtype_t,
    clconf_cluster_t *cl);

/*
 * constructs a more-readable adapter name. Caller should freeup the memory
 * returned by this routine
 */
char *clconf_get_adaptername(clconf_adapter_t *);

/*
 * constructs a more-readable path name. Caller should freeup the memory
 * returned by this routine
 */
char *clconf_get_pathname(clconf_path_t *, clconf_cluster_t *);

/*
 * constructs internal pathname
 */
char *clconf_get_internal_pathname(nodeid_t, uint_t, nodeid_t, uint_t);

/*
 * ***********************************************************************
 * Non-ORB User level get_ccr_entry interface
 * ***********************************************************************
 */

typedef struct {
	char		*table_name;
	size_t		table_name_len;
	char		*entry_name;
	size_t		entry_name_len;
	char 		*buf;
	size_t		buf_len;
	clconf_errnum_t	rslt;
} get_ccr_entry_t;

#if defined(_SYSCALL32)

typedef struct {
	caddr32_t	table_name;
	size32_t	table_name_len;
	caddr32_t	entry_name;
	size32_t	entry_name_len;
	caddr32_t	buf;
	size32_t	buf_len;
	clconf_errnum_t	rslt;
} get_ccr_entry32_t;

#endif /* defined(_SYSCALL32) */

#if (SOL_VERSION >= __s10)
/*
 * ***********************************************************************
 * Non-ORB User level get_ccr_privip interface
 * ***********************************************************************
 */

typedef struct {
	uint_t		count;
	char		*buf;
	size_t		buf_len;
	clconf_errnum_t	rslt;
} get_ccr_privip_t;

#if defined(_SYSCALL32)

typedef struct {
	uint_t		count;
	caddr32_t	buf;
	size32_t	buf_len;
	clconf_errnum_t	rslt;
} get_ccr_privip32_t;

#endif /* defined(_SYSCALL32) */
#endif /* (SOL_VERSION >= __s10) */

/* Maximum number of hops on the way to a non leaf clconf tree node */
#define	MAX_CLCONF_PATH_LEN	4

/*
 * Structure used for the CL_GET_CLCONF_CHILD_STATS query.
 *
 * The node is identified by the first three members.
 * path_len		The number of hops in the clconf tree from the
 *			root node to reach the specified node
 * path_node_types	Node types on the way to the node in question
 * path_node_indices	Node indices on the way to the node in question
 * The member argument is the child type of interest.
 * The number of children of the above type and the maximum index of
 * any child is retuned in child_count and max_child_index.
 *
 * As an example, to obtain information about the number of ports on
 * node 3's adapter 2, the first four members should be passed as:
 * path_len = 2, path_node_types = {CL_NODE, CL_ADAPTER},
 * path_node_indices = {3, 2}, child_type = CL_PORT
 */
typedef struct {
	int			path_len;
	clconf_objtype_t	path_node_types[MAX_CLCONF_PATH_LEN];
	int			path_node_indices[MAX_CLCONF_PATH_LEN];
	clconf_objtype_t	child_type;
	int			child_count;
	int			max_child_index;
	clconf_errnum_t		rslt;
} get_clconf_child_stats_t;

/*
 * Structure used for the CL_GET_VP_RUNNING_VERSION query.
 * proto = name of the protocol for which we need the version number
 * mode = VM_NODE_CLUSTER or VM_NODEPAIR version number
 * nodeid = nodeid to get the version number for (needed for NODEPAIR)
 * maj_num = the major number of the protocol - return value
 * min_num = the minor number of the protocol - return value
 */
typedef struct {
	nodeid_t	nodeid;
	uint16_t	mode;
	uint16_t	maj_num;
	uint16_t	min_num;
	char		vp_name[257];
} get_clconf_proto_vers_num_t;

/*
 * XXX - Code removed
 */
#ifdef _KERNEL_ORB
clconf_errnum_t	clconf_get_ccr_entry(char *table_name, char *entry_name,
	char *buf, size_t buf_len);
clconf_errnum_t clconf_get_clconf_child_stats(int, clconf_objtype_t *,
	int *, clconf_objtype_t, int *, int *);
#endif

/*
 * ***********************************************************************
 * Determine whether we are using flexible IP Addressing scheme.
 * ***********************************************************************
 */
int flex_ip_address();

/*
 * Return the network address the cluster is using for private IP addresses
 */
const struct in_addr *clconf_get_cluster_network_addr(void);

/*
 * ***********************************************************************
 * Get user-level IP Address and netmask from CCR.
 * The in_addr returned is the IP hosted by lndid.
 * ***********************************************************************
 */

int clconf_get_user_ipaddr(nodeid_t ndid, struct in_addr *addr);
int clconf_get_user_network(struct in_addr *addr);
int clconf_get_user_netmask(struct in_addr *addr);

#ifdef _KERNEL
/*
 * Map the specified logical ip to the physical ip of the adapter that's
 * hosting the logical ip.  If we can't find the logical_ip_address in our
 * mapping then we return the logical_ip_address.
 */
in_addr_t clconf_get_physical_ip(in_addr_t logical_ip_address);

#endif /* _KERNEL */

/*
 * ***********************************************************************
 * Query Interface
 * ***********************************************************************
*/
typedef struct {
	sc_state_code_t	state;
	char		*state_string;
	char		*type;
	char		*int_name;
} cladmin_component_state_t;

typedef struct {
	uint_t				listlen;
	cladmin_component_state_t	*list;
} cladmin_component_state_list_t;

#ifndef _KERNEL

/*
 * Get state of a named component
 */
cladmin_component_state_t *cladmin_get_component_state(const char *cmp_name);

/*
 * get state of all components in cluster
 */
cladmin_component_state_list_t *cladmin_get_component_state_list(void);

/*
 * Helper to delete the state structure
 */
void cladmin_free_component_state(cladmin_component_state_t *state_ptr);

/*
 * Helper to delete the state structure list
 */
void
cladmin_free_component_state_list(cladmin_component_state_list_t *list_ptr);

/*
 * Is Europa farm management enabled ?
 */
boolean_t
clconf_is_farm_mgt_enabled();

#endif // !_KERNEL

/*
 * ***********************************************************************
 * Shutdown Interface
 * ***********************************************************************
 */

#if (SOL_VERSION < __s10) || defined(UNODE)
/*
 * Starting Solaris 10, these clconf interfaces to enable and disable
 * failfast are removed. However, these interfaces still exist for unodes
 * on Solaris 10.
 */
int clconf_disable_failfast(nodeid_t);
int clconf_enable_failfast(nodeid_t);
#endif

int clconf_disable_path_monitoring(nodeid_t);
int clconf_enable_path_monitoring(nodeid_t);
int clconf_cluster_shutdown(nodeid_t);
int clconf_get_cluster_shutdown(nodeid_t, boolean_t*);
int clconf_enable_cluster_wide_panic(nodeid_t);
int clconf_disable_cluster_wide_panic(nodeid_t);

boolean_t conf_is_cluster_member(nodeid_t id);

/*
 * ***********************************************************************
 * Cluster-Wide Zone Interface
 * ***********************************************************************
 */
#if (SOL_VERSION >= __s10)

/*
 * Get the cluster id of the Cluster-Wide Zone given the name of the
 * cluster-wide zone.
 * Will return:
 * ENOENT if the specified cluster-wide zone does not exist.
 * EACCES if there is no privilege to get the cluster id of the given
 * cluster-wide zone.
 * 0 upon success, will also return the cluster id of the given
 * cluster-wide zone.
 */
int clconf_get_cluster_id(char *cluster, uint_t *clid);

/*
 * Get the name of the Cluster-Wide Zone given the cluster id of the
 * cluster-wide zone.
 * Will return:
 * ENOENT if the specified cluster-wide zone does not exist.
 * EACCES if there is no privilege to get the name of the given
 * cluster-wide zone.
 * 0 upon success, will also return the name of the given
 * cluster-wide zone.
 */
int clconf_get_cluster_name(uint_t clid, char **cluster);

#ifndef UNODE
/*
 * Returns B_TRUE if running inside a virtual cluster,
 * else returns B_FALSE.
 */
boolean_t clconf_inside_virtual_cluster();
#endif
#endif // (SOL_VERSION >= __s10)

#ifdef _KERNEL
/*
 * The clconf functionality in cl_haci needs to call
 * clconf_exec_program_with_opts. For user and unode, the function is
 * compiled directly into the libclcomm or unode executable respectively.
 * For kernel, the modules need to be separated for architectural reasons,
 * so a function pointer is being used instead of a direct function call.
 * The function pointer is declared here, and will be assigned to point
 * to the function as part of the CMM initialization.
 */
extern clconf_errnum_t (*clconf_exec_program_with_opts_ptr)
	(const char *program, nodeid_t id, boolean_t *clust_memb,
	boolean_t realtime);
#endif // _KERNEL

clconf_errnum_t clconf_exec_program_with_opts
	(const char *program, nodeid_t id, boolean_t *clust_memb,
	boolean_t realtime);

#ifdef __cplusplus
}
#endif

#endif	/* _CLCONF_INT_H */
