/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLCONF_PROPERTY_H
#define	_CLCONF_PROPERTY_H

#pragma ident	"@(#)clconf_property.h	1.10	08/05/20 SMI"

#include <sys/clconf_int.h>

#ifdef  __cplusplus
extern "C" {
#endif

typedef enum {
	CL_STRING,
	CL_INT,
	CL_ENUM,
	CL_BOOLEAN
} clpl_type;

typedef struct clconf_propdesc {
	const char *name;	/* Name of the property */
	clpl_type type;		/* type of the property */
	int min;		/* Min for int, min length for string */
	int max;		/* Max for int, max length for string */
	const char *defaultv;	/* default value in string */
	unsigned char required;	/* 1 if the property is required, 0 if not */
	const char *enumlist;	/* Valid values for enum, seperated by ':' */
} clconf_propdesc_t;

/*
 * The name of a generic object.
 */
#define	CLPL_GENERIC	"generic"

/* This is to be used to get the valid properties of clconf entities */

/*
 * Get rid of the old information. We only keep one property list
 * at a time. So after refresh is called, the old property list will be
 * destroyed, and the old clconf_obj_t pointers shouldn't be used anymore.
 */
void clpl_refresh(void);

/*
 * Reads the property list configurations from the specified file.
 */
void clpl_read(char *filename);

/*
 * Get valid objects for a certain type.
 * obj_type: the type of objects to get, eg CL_ADAPTER.
 */
clconf_iter_t *clpl_get_objects(clconf_objtype_t obj_type);

/*
 * Get a object of the specified type and name. For example,
 * clpl_get_object(CL_ADAPTER, "hme") to get the hme adapter.
 * If the specified object is not found then the generic object
 * is returned. So if clpl_get_object(CL_ADAPTER, "hme") is called
 * while there is no entry for hme in the configuration file, then
 * the result is the same as clpl_get_object(CL_ADAPTER, "generic").
 */
clconf_obj_t *clpl_get_object(clconf_objtype_t, const char *);

/*
 * Get the list of properties for an object. The properties will contain
 * all those of the 'generic' object. When there is a conflict between
 * a specific object and the generic object, the property of the specific
 * object will be kept.
 */
clconf_iter_t *clpl_obj_get_properties(clconf_obj_t *);

/*
 * Get the name of the specified property.
 */
const char *clpl_get_name(clconf_obj_t *property);

/*
 * Get the description for the property. Note that after a clpl_refresh
 * the const char* in clconf_propdesc_t will become invalid.
 */
clconf_propdesc_t clpl_prop_get_description(clconf_obj_t *);

#ifdef  __cplusplus
}
#endif

#endif	/* _CLCONF_PROPERTY_H */
