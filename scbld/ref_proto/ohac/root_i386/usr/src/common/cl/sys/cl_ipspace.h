/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_IPSPACE_H
#define	_CL_IPSPACE_H

#pragma ident	"@(#)cl_ipspace.h	1.11	08/05/20 SMI"

/*
 * Here we'll describe how the IP numbers used over the private interconnect
 * are divided up.
 *
 * The cluster uses an entire IP address network for two different
 * classes of numbers.  The first class are the numbers used by the
 * topology manager to assign to network adapters belonging to the
 * private networks. Each private network forms its own subnet and the
 * IP addresses from within that subnet are assigned to the network
 * adapters belonging to that private network.
 *
 * The second class of numbers are logical per-node addresses. These
 * are assigned to the nodes and are plumbed on the clprivnet device.
 * They are assigned from a separate subnet from within the given IP
 * address range.
 *
 * The given IP address range is divided up as follows to provide the
 * subnets for the physical private networks and for the per-node
 * network:
 *
 * Based on the number of nodes required, the size of the subnets
 * needed for the the physical network adapters is determined. Then,
 * the given IP address range is divided up into subnets of this
 * size. Say the total number of such subnets is N. Then out of the
 * N subnets, the first N/2 subnets (excluding subnet 0) are used for
 * physical network adapters. Then the next N/4 subnets are combined
 * together to give a subnet which is used for the per-node IP addresses
 * and for any other IP addresses being implemented in the future, for
 * example, per-zone IP addresses and private failover IP addresses. Then
 * the remaining N/4 subnets are again used for physical network adapters.
 * (excluding the last subnet).
 */

/*
 * Define default private network address and netmasks
 */
#define	CL_DEFAULT_NET_NUMBER		"172.16.0.0"
#define	CL_DEFAULT_NETMASK		"255.255.248.0"
#define	CL_DEFAULT_SUBNET_NETMASK	"255.255.255.128"
#define	CL_DEFAULT_USER_NET_NUMBER	"172.16.4.0"
#define	CL_DEFAULT_USER_NETMASK		"255.255.254.0"

/*
 * Define the pairwise logical network masks, shifts, and tags
 * These are no longer required as of 3.1u1 but they are still
 * around since the code that refers to these is still around
 * for rolling upgrade.
 */
#define	LOGICAL_PAIRWISE_MASK		0x0000C000UL
#define	LOGICAL_PAIRWISE_DST_LOW_TAG	0x00000001UL
#define	LOGICAL_PAIRWISE_DST_HIGH_TAG	0x00000002UL
#define	LOGICAL_PAIRWISE_NETMASK	0xFFFFFFFCUL
#define	LOGICAL_PAIRWISE_HIGH_SHIFT	8
#define	LOGICAL_PAIRWISE_LOW_SHIFT	2
#define	LOGICAL_PAIRWISE_NODE_MASK	0xFFFFFFC0UL

/*
 * Define the per-node logical network mask. These apply to the
 * old scheme of IP addressing where a 16-bit address was
 * a requirement. Also define the physical subnet netmask.
 */
#define	LOGICAL_PERNODE_B3		0xC1
#define	LOGICAL_PERNODE_NETMASK		0xFFFFFFFFUL
#define	LOGICAL_PERNODE_NETWORK_NETMASK	0xFFFFFF00UL
#define	CL_PHYSICAL_NETMASK		0xFFFFFF80UL


#endif	/* _CL_IPSPACE_H */
