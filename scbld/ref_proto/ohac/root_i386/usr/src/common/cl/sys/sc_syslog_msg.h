/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SC_SYSLOG_MSG_H
#define	_SC_SYSLOG_MSG_H

#pragma ident	"@(#)sc_syslog_msg.h	1.32	08/05/20 SMI"

/*
 * sc_syslog_msg.h - has declarations callable by C programs.
 *	      See os.h for object-oriented version of sc_syslog_msg API.
 *
 */

#include <sys/types.h>

#ifndef _KERNEL_ORB
#include <sys/syslog.h>
#endif /* _KERNEL_ORB */

/*
 * Define the resource type specific tags.
 */
#include <sys/rsrc_tag.h>

/*
 * sc_syslog_msg API provides a wrapper to log messages from both
 * user-land and kernel-land modules. kernel-land modules cannot
 * use the non-C++ version of the API declared in this file. kernel-land
 * modules should use the sc_syslog_msg class defined in os.h
 *
 * Both C++ class and C functions are available for user-land modules.
 * C functions are declared in this file. C++ class sc_syslog_msg is
 * defined in os.h
 *
 * The function calls in turn invoke openlog(3) and syslog(3) in
 * user-land and cmn_err(9F) in kernel-land.
 *
 * Parameters to openlog(3) are set to following:
 * - ident is set to "SC"
 * - logopt is set to LOG_CONS
 * - facility is set to LOG_DAEMON
 *
 * user-land modules that use sc_syslog_msg API should not call
 * openlog(3) because it is not MT-SAFE.
 */

/*
 * A message logged by sc_syslog_msg API will have additional information
 * prepended in its unformatted part:
 *
 *  "ID["<resource type specific tag>"."<resource name>"."<event type>"]: "
 *
 *  where,
 * <resource type specific tag> := sequence of {a-z,A-Z,.,_,0-9}
 * <resource name> := sequence of {a-z,A-Z,.,_,0-9}
 * <event type> :=
 *  "ADDED"|"REMOVED"|"OWNERSHIP_CHANGED"|"PROPERTY_CHANGED"|"STATE_CHANGED"
 *  |"MESSAGE"
 *
 * The admin tools determine resource type from <resource type specific tag>
 * If possible, use resource type specific tags defined in this file.
 *
 * The admin tools use <resource name> to query state, properties, information
 * about new primaries etc. Therefore, it helps admin tools if <resource name>
 * specified in sc_syslog_msg calls are same as <resource names> needed in
 * these queries.
 */

#ifdef __cplusplus
extern "C" {
#endif

#define	STRLOG_MAKE_MSGID(fmt, msgid)					\
{									\
	uchar_t *__cp = (uchar_t *)fmt;					\
	uchar_t __c;							\
	uint32_t __id = 0;						\
	while ((__c = *__cp++) != '\0')					\
		if (__c >= ' ')						\
			__id = (__id >> 5) + (__id << 27) + __c;	\
	msgid = (__id % 899981) + 100000;				\
}

typedef enum sc_syslog_msg_status_enum {
	SC_SYSLOG_MSG_STATUS_GOOD = 0,
	SC_SYSLOG_MSG_STATUS_ERROR
} sc_syslog_msg_status_t;

typedef enum sc_state_code_enum {
	/* resource is running */
	SC_STATE_ONLINE = 1,

	/* resource is stopped due to user action */
	SC_STATE_OFFLINE,

	/* resource is stopped due to a failure */
	SC_STATE_FAULTED,

	/* resource is running but has a minor problem */
	SC_STATE_DEGRADED,

	/* resource is in transition from a state to another */
	SC_STATE_WAIT,

	/*
	 * resource is monitored but state of the resource is
	 * not known because either the monitor went down or
	 * the monitor cannot report resource state temporarily.
	 */
	SC_STATE_UNKNOWN,

	/* There is no monitor to check state of the resource */
	SC_STATE_NOT_MONITORED
} sc_state_code_t;

typedef int	sc_event_type_t;

/*
 * common priority values
 * priority values available in kernel-land are proper subset of
 * those available in user-land.
 */

#ifdef _KERNEL_ORB

/* couldn't include sys/cmn_err.h */
#define	SC_SYSLOG_NOTICE	1	/* same as CE_NOTE. See cmn_err(9F) */
#define	SC_SYSLOG_WARNING	2	/* same as CE_WARN. See cmn_err(9F) */
#define	SC_SYSLOG_PANIC		3	/* same as CE_PANIC. See cmn_err(9F) */

#else /* !_KERNEL_ORB */

#define	SC_SYSLOG_NOTICE	LOG_NOTICE
#define	SC_SYSLOG_WARNING	LOG_WARNING
#define	SC_SYSLOG_ERROR		LOG_ERR
#define	SC_SYSLOG_PANIC		LOG_ALERT

#endif /* _KERNEL_ORB */

/*
 * Following constants should be used in the event_type argument to
 * sc_syslog_msg_log
 */
#define	MESSAGE			0	/* do not generate an event */
#define	STATE_CHANGED		1	/* state change event */
#define	ADDED			2	/* new resource added event */
#define	REMOVED			3	/* resource removed event */
#define	PROPERTY_CHANGED	4	/* change of a property event */
#define	OWNERSHIP_CHANGED	5	/* owners changed event */

#define	SC_SYSLOG_MESSAGE(x)	x	/* print the same value */
#define	NO_SC_SYSLOG_MESSAGE(x)	x	/* print the same value */

typedef void * sc_syslog_msg_handle_t;

#ifndef BUG_4517304
#define	BUG_4517304
#endif /* BUG_4517304 */

/*
 * sc_syslog_msg_set_syslog_tag()
 *
 * Not available for Kernel-land modules, however is used by unode.
 *
 * Paramters:
 *
 * tag -
 * 	This tag will be used to specify identifier to openlog(3).
 *	If openlog(3) has already been called once, closelog(3)
 *	would be called, followed by openlog(3) with the tag.
 *	The effect will be to set a new identifier for messages logged.
 *	If tag is NULL, the function returns with error.
 *
 */
extern sc_syslog_msg_status_t sc_syslog_msg_set_syslog_tag(const char *tag);

#ifdef BUG_4517304
/*
 *	A new version of this function has been created as a workaround
 *	for 4517304. The new version, sc_syslog_msg_set_syslog_tag_no_fork,
 *	calls openlog() with the LOG_CONS flag cleared. This version should
 *	be used by programs which link with libclcomm. This is done by
 *	interposing a function named sc_syslog_msg_set_syslog_tag which is a
 *	wrapper for sc_syslog_msg_set_syslog_tag_no_fork.
 *
 * The purpose of the LOG_CONS flag is to request that syslog messages be
 * printed to the console in case syslogd dies, isn't started, or has other
 * problems. A side effect of this workaround is that no messages will be
 * printed to the console in the event of an unhealthy syslogd.
 */
extern sc_syslog_msg_status_t sc_syslog_msg_set_syslog_tag_no_fork(
    const char *tag);
#endif /* BUG_4517304 */

/*
 * sc_syslog_msg_initialize()
 *
 * Paramters:
 *
 * handle -  is returned by the call. It is opaque.
 *
 * resource_type_specific_tag -
 *           This tag will become part of the message id. of the logged
 *           message.
 *	     - It should not be NULL.
 *	     - It should uniquely identify a resource type
 *	     - It is preferred that tags start with prefix "Cluster"
 *	       e.g. tag for Resource Group could be "Cluster.RGM.RG"
 *	     **********************************************************
 *	       NOTICE:
 *		  Please notify cluster management team about resource
 *		  type specific tags your team is using for components.
 *		  A recommended way to do this is to put all tags in a
 *		  .h file and let cluster management team know location
 *		  and name of the .h file.
 *	     **********************************************************
 *
 * resource_name -
 *           resource_type_specific_tag and resource_name are
 *           concatenated to create the <message tag> part of the
 *           message id.
 *	     - It should not be NULL.
 *	     - It should be unique clusterwide.
 *	       For example, if a message is logged by RGM for
 *	       resource group red, resource_name should be "red".
 *	       If name is not unique, prepend it with name of
 *	       the resource's container.
 *	       For example,
 *		Data service instance names should be:
 *		<data svc type>.<resource group>.<instance node>
 *
 * *********************************************************************
 *
 */
extern sc_syslog_msg_status_t sc_syslog_msg_initialize(
				sc_syslog_msg_handle_t *handle,
				const char *resource_type_specific_tag,
				const char *resource_name);

#ifdef BUG_4517304
/*
 *	A new version of this function has been created as a workaround
 *	for 4517304. The new version, sc_syslog_msg_initialize_no_fork,
 *	calls openlog() with the LOG_CONS flag cleared. This version should
 *	be used by programs which link with libclcomm. This is done by
 *	interposing a function named sc_syslog_msg_initialize which is a
 *	wrapper for sc_syslog_msg_initialize__no_fork.
 *
 * The purpose of the LOG_CONS flag is to request that syslog messages be
 * printed to the console in case syslogd dies, isn't started, or has other
 * problems. A side effect of this workaround is that no messages will be
 * printed to the console in the event of an unhealthy syslogd.
 */
extern sc_syslog_msg_status_t sc_syslog_msg_initialize_no_fork(
				sc_syslog_msg_handle_t *handle,
				const char *resource_type_specific_tag,
				const char *resource_name);
#endif /* BUG_4517304 */

/*
 * sc_syslog_msg_log() - Logs a message with the syslog facility
 *
 * Parameters:
 *	handle: should be same as the one passed to sc_syslog_msg_initialize()
 *
 *	priority: should be same as first argument passed to
 *		syslog(3).
 *
 * 	event: it could be one of the 5 event types enumerated above.
 *
 *	format: a string specifying format of the message. See syslog(3)
 *		  for more information on this parameter.
 *
 */
extern sc_syslog_msg_status_t sc_syslog_msg_log(
				sc_syslog_msg_handle_t handle,
				int priority,
				sc_event_type_t event,
				const char *format,
				...);

/*
 * sc_syslog_msg_done()
 */
extern void sc_syslog_msg_done(sc_syslog_msg_handle_t *handle);

#ifdef __cplusplus
}
#endif

#endif	/* _SC_SYSLOG_MSG_H */
