/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _QUORUM_INT_H
#define	_QUORUM_INT_H

#pragma ident	"@(#)quorum_int.h	1.28	08/05/21 SMI"

/* Interface to define quorum device configuration */

#include <sys/os.h>
#include <sys/cladm.h>
#include <sys/types.h>
#ifndef	linux
#include <sys/mhd.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* maximum number of quorum devices supported in a cluster */
#define	MAX_QUORUM_DEVICES		64

/* bitmask representing nodes in a cluster; LSB => node 1; MSB => node 64 */
typedef unsigned long long node_bitmask_t;

typedef uint32_t quorum_device_id_t;

typedef enum quorum_state_enum {
	QUORUM_STATE_ONLINE = 0,
	QUORUM_STATE_OFFLINE = 1
} quorum_state_t;

#define	QD_SCSI2	"scsi2"
#define	QD_SCSI3	"scsi3"
#define	QD_SQ_DISK	"sq_disk"

/* Enum for returning the level of amnesia algorithm supported by the QD. */
enum amnesia_level_supported {
	AMN_NO_SUPPORT,
	AMN_SC30_SUPPORT
};

typedef struct node_info_struct {
	nodeid_t		nodenum;
	uint32_t		votes;
	mhioc_resv_key_t		reservation_key;
} node_info_t;

typedef struct node_info_list {
	uint32_t		listlen;	/* # of elements in array */
	node_info_t		*list;		/* pointer to array */
} node_info_list_t;

typedef struct quorum_device_info_struct {
	quorum_device_id_t	qid;
	char			*gdevname;
	char			*type;
	uint32_t		votes;
	node_bitmask_t		nodes_with_enabled_paths;
	node_bitmask_t		nodes_with_configured_paths;
	char			*access_mode;
} quorum_device_info_t;

typedef struct quorum_device_info_list {
	uint32_t		listlen;	/* # of elements in array */
	quorum_device_info_t	*list;		/* pointer to array */
} quorum_device_info_list_t;

typedef struct quorum_table_struct {
	node_info_list_t		nodes;
	quorum_device_info_list_t	quorum_devices;
} quorum_table_t;

/*
 * Status of a specific node as seen by the quorum algorithm
 */
typedef struct quorum_node_status_struct {
	nodeid_t		nid;
	mhioc_resv_key_t	reservation_key;
	uint32_t		votes_configured;
	quorum_state_t		state;
} quorum_node_status_t;

/*
 * Status of a specific quorum device as seen by the quorum algorithm
 */
typedef struct quorum_device_status_struct {
	quorum_device_id_t	qid;
	uint32_t		votes_configured;
	char 			*gdevname;
	node_bitmask_t		nodes_with_enabled_paths;
	node_bitmask_t		nodes_with_configured_paths;
	quorum_state_t		state;
	nodeid_t		reservation_owner; /* 0 if no owner */
} quorum_device_status_t;

/*
 * Status of all nodes and quorum devices as seen by the quorum
 * algorithm. It lists all the nodes and quorum devices that
 * are configured, including ones that are not currently up or
 * available.
 */
typedef struct quorum_status_struct {
	uint32_t			current_votes;
	uint32_t			configured_votes;
	uint32_t			votes_needed_for_quorum;
	quorum_node_status_t		*nodelist; /* array */
	uint32_t			num_nodes;
	quorum_device_status_t		*quorum_device_list; /* array */
	uint32_t			num_quorum_devices;
} quorum_status_t;

#ifdef __cplusplus
}
#endif

#ifndef NOINLINES
/* #include <sys/quorum_int_in.h> */
#endif // _NOINLINES

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _QUORUM_INT_H */
