#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)mgr_config 1.3     07/08/07 SMI"
#
# This file will be sourced in by mgr_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#	+++ Required parameters +++
#
#          RS - name of the SC resource for the Queue Manager
#          RG - name of the SC resource group to contain RS 
#        QMGR - name of the Queue Manager
#          LH - name of the SC LogicalHostname resource
#      HAS_RS - name of the SC HAStoragePlus resource
#      LSR_RS - name of the SC Listener resource
#	        (Leave blank if the Queue Manager should not
#	        be dependent on the Listener)
#     CLEANUP - Cleanup IPC entries YES or NO 
#		(Default CLEANUP=YES)
#    SERVICES - WebSpehre MQ v6 only
#               SERVICES=NO (Equivalent to strmqm -ns ${QMGR}
#               SERVICES=YES (Equivalent to strmqm ${QMGR}
#		(Default SERVICES=NO)
#      USERID - name of the WebSphere MQ userid
#		(Default USERID=mqm)
#
#	+++ Optional parameters +++
#
# DB2INSTANCE - name of the DB2 Instance name
# ORACLE_HOME - name of the Oracle Home Directory
#  ORACLE_SID - name of the Oracle SID
#   START_CMD - pathname and name of the renamed strmqm program
#    STOP_CMD - pathname and name of the renamed endmqm program
#
#       +++ Non-global (failover) zone parameters +++
#       +++ Only required if WebSphere MQ should  +++
#       +++ run within a Solaris 10 failover zone +++
#
#     RS_ZONE - name of the non-global (failover) zone managed by the
#               Sun Cluster Data Service for Solaris Containers
#     PROJECT - name for the Solaris Project to be used for this resource
#               (Default PROJECT=default)
#     TIMEOUT - stop timeout value
#               (Default TIMEOUT=300)
#
# Note 1: Optional parameters
#
#	Null entries for optional parameters are allowed if not used.
#
# Note 2: XAResourceManager processing
#
#	If DB2 will participate in global units of work then set
#	DB2INSTANCE=
#
#	If Oracle will participate in global units of work then set
#	ORACLE_HOME=
#	ORACLE_SID=
#
# Note 3: Renamed strmqm/endmqm programs
#	
#	This is only recommended if WebSphere MQ is deployed onto 
#	Global File Systems for qmgr/log files. You should specify 
#	the full pathname/program, i.e. /opt/mqm/bin/<renamed_strmqm>
#
# Note 4: Cleanup IPC
#
#	Under normal shutdown and startup WebSphere MQ manages it's
#	cleanup of IPC resources with the following fix packs.
#
#	MQSeries v5.2 Fix Pack 07 (CSD07) or later
#	WebSphere MQ v5.3 Fix Pack 04 (CSD04) or later
#	WebSphere MQ v6.0 or later
#
#	Please refer to APAR number IY38428.
#
#	However, while running in a failover environment, the IPC keys
#	that get generated will be different between nodes. As a result
#	after a failover of a Queue Manager, some shared memory segments
#	can remain allocated on the node although not used. 
#
#	Although this does not cause WebSphere MQ a problem when starting
#	or stopping (with the above fix packs applied), it can deplete
#	the available swap space and in extreme situations a node may 
#	run out of swap space. 
#
#	To resolve this issue, setting CLEANUP=YES will ensure that 
#	IPC shared memory segments for WebSphere MQ are removed whenever
#	a Queue Manager is stopped. However IPC shared memory segments 
#	are only removed under strict conditions, namely
#
#	- The shared memory segment(s) are owned by
#		OWNER=mqm and GROUP=mqm
#	- The shared memory segment has no attached processes
#	- The CPID and LPID process ids are not running
#	- The shared memory removal is performed by userid mqm
#
#	Setting CLEANUP=NO will not remove any shared memory segments.
#
#	Setting CLEANUP=YES will cleanup shared memory segments under the
#	conditions described above.
#
# Note 5: Non-global (failover) zone parameters
#
#       These parameters are only required when WebSphere MQ should run
#       within a failover zone managed by the Sun Cluster Data Service
#       for Solaris Containers.
#
#       To use the Sun Cluster Data Service for WebSphere MQ together with
#       the Sun Cluster Data Service for Solaris Containers, you must do
#       the following,
#
#       1. Install the Sun Cluster Data Service for Solaris Containers.
#
#       2. Follow the Sun Cluster Data Service for Solaris Containers Guide
#          for instructions on how to create and install a non-global zone
#          in a failover configuration.
#
#	   The following text represents condensed documentation and shows the
#	   required steps to install WebSphere MQ into a failover zone on a
#	   two node Sun Cluster. 
#
#	   On all nodes within the cluster, ensure that "exclude lofs" is commented 
#	   out within /etc/system and that patch 120590-03 or later is installed. 
#	   
#          In the examples below, WebSphere MQ is installed in the failover zone's
#	   zonepath. /opt/mqm and /var/mqm are simply directories within the zone. 
#
#	   If a global or highly available local file system is required for WebSphere 
#	   MQ in the failover zone, you must configure the "Mounts" parameter within the
#	   sczcbt_config file in Step 3.
#
#          You must create a "whole root" failover zone for the installation of
#          WebSphere MQ on both nodes of the cluster. Performing this tasks on both 
#	   nodes of the cluster is required to ensure that the zone is "known" to 
#          both nodes of the cluster, i.e. configured and installed.
#
#          node1# scrgadm -a -g zone1-rg
#          node1# scrgadm -a -g zone1-rg -L -l zone1 -j zone-lh
#          node1# scrgadm -a -g zone1-rg -t SUNW.HAStoragePlus -j zone1-has \
#          node1# -x FileSystemMountPoints=<failover file system> 
#          node1# 
#          node1# scswitch -Z -g zone1-rg
#          node1# scswitch -z -g zone1-rg -h node1
#          node1# 
#          node1# zonecfg -z zone1
#          zone1: No such zone configured
#          Use 'create' to begin configuring a new zone.
#          zonecfg:zone1> create -b
#          zonecfg:zone1> set zonepath=<failover file system>/zone1
#          zonecfg:zone1> set autoboot=false
#          zonecfg:zone1> add inherit-pkg-dir
#          zonecfg:zone1:inherit-pkg-dir> set dir=/opt/SUNWscmqs
#          zonecfg:zone1:inherit-pkg-dir> end
#          zonecfg:zone1> exit
#          node1# 
#          node1# zoneadm -z zone1 install
#          node1# scswitch -z -g zone1-rg -h node2
#
#          node2# cd <failover file system>
#          node2# rm -r zone1
#          node2# 
#          node2# zonecfg -z zone1
#          zone1: No such zone configured
#          Use 'create' to begin configuring a new zone.
#          zonecfg:zone1> create -b
#          zonecfg:zone1> set zonepath=<failover file system>/zone1
#          zonecfg:zone1> set autoboot=false
#          zonecfg:zone1> add inherit-pkg-dir
#          zonecfg:zone1:inherit-pkg-dir> set dir=/opt/SUNWscmqs
#          zonecfg:zone1:inherit-pkg-dir> end
#          zonecfg:zone1> exit
#          node2# 
#          node2# zoneadm -z zone1 install
#          node2# zoneadm -z zone1 boot
#        
#          At this stage zone1 is configured and installed, but Solaris within 
#	   zone1 is not yet configured. This needs to be done as follows,
#
#          node2# zlogin -C zone1
#          zone1# ~.
#          node2# zoneadm -z zone1 halt
#
#       3. Bring online the "whole root" non-global zone, so that it is now
#          managed by the Sun Cluster Data Service for Solaris Containers.
#
#	   Edit sczbt_config and execute sczbt_register from /opt/SUNWsczone/sczbt/util
#
#          node2# cd /opt/SUNWsczone/sczbt/util
#          node2# vi sczbt_config
#          node2# ./sczbt_register 
#          node2# 
#          node2# scswitch -e -j <res>
#
#       4. Install WebSphere MQ in the "whole root" failover zone.
#
#          node2# zlogin zone1
#          zone1# mkdir -p /opt/mqm /var/mqm
#          zone1# groupadd -g <gid> mqm
#          zone1# useradd -g <gid> -u <uid> -d <home dir> mqm
#          zone1#   
#          zone1# cd <mqm package directory>
#          zone1# ./mqlicense.sh
#          zone1# pkgadd -d .
#	
#	   Install the latest WebSphere MQ Fix Pack.
#
#       5. Create the WebSphere MQ Queue manager
#
#          node2# zlogin zone1
#          zone1# su - mqm
#          zone1# crtmqm qmgr1
#   
#       5. Provide values for the parameters below and include values for
#          RS_ZONE and PROJECT.
#
#          The value for the RG= parameter should be the same SC resource group
#     	   name that was used by the Sun Cluster Data Service for Solaris Containers
#	   agent for the failover zone. 
#
#	   RS_zone should also be the same SC resource name that was used by the 
#	   Sun Cluster Data Service for Solaris Containers agent for the failover zone.
#
#	   Refer to Example 2  Configuration parameters for deployment of WebSphere MQ
#	                         within a Solaris 10 non-global (failover) zone.
#
#          node2# vi mgr_config
#
#       6. Register the SC resource using the mqr_register script.
#
#          node2# ./mgr_register
#
#       7. Bring online the SC resource that was registered.
#
# Example 1 - Configuration parameters for deployment of WebSphere MQ 
#		within Solaris 10 global zone nodes or pre-Solaris 10 nodes.
#	
# 	+++ Required parameters +++
#	RS=wmq1-qmgr1
#	RG=wmq1-rg
#	QMGR=qmgr1
#	LH=wmq1-lh
#	HAS_RS=wmq1-has
#	LSR_RS=
#	CLEANUP=YES
#	SERVICES=NO
#	USERID=mqm
#	
# 	+++ Optional parameters +++
#	DB2INSTANCE=
#	ORACLE_HOME=
#	ORACLE_SID=
#	START_CMD=
#	STOP_CMD=
#	
# 	+++ Failover zone parameters +++
#	RS_ZONE=
#	PROJECT=default
#
# Example 2 - Configuration parameters for deployment of WebSphere MQ
#		within a Solaris 10 non-global (failover) zone.
#	
# 	+++ Required parameters +++
#	RS=zone1-qmgr1
#	RG=zone1-rg
#	QMGR=qmgr1
#	LH=zone1-lh
#	HAS_RS=zone1-has
#	LSR_RS=
#	CLEANUP=YES
#	SERVICES=NO
#	USERID=mqm
#	
# 	+++ Optional parameters +++
#	DB2INSTANCE=
#	ORACLE_HOME=
#	ORACLE_SID=
#	START_CMD=
#	STOP_CMD=
#	
# 	+++ Failover zone parameters +++
#	RS_ZONE=zone1-res
#	PROJECT=default
#	
# +++ Required parameters +++
RS=
RG=
QMGR=
LH=
HAS_RS=
LSR_RS=
CLEANUP=YES
SERVICES=NO
USERID=mqm

# +++ Optional parameters +++
DB2INSTANCE=
ORACLE_HOME=
ORACLE_SID=
START_CMD=
STOP_CMD=

# +++ Failover zone parameters +++
# These parameters are only required when WebSphere MQ should run
#  within a failover zone managed by the Sun Cluster Data Service
# for Solaris Containers.
RS_ZONE=
PROJECT=default
TIMEOUT=300
